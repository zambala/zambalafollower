#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "URBNActivityViewController.h"
#import "URBNBodyProvider.h"
#import "URBNImageProvider.h"
#import "URBNSafariActivity.h"
#import "URBNShareKit.h"

FOUNDATION_EXPORT double URBNShareKitVersionNumber;
FOUNDATION_EXPORT const unsigned char URBNShareKitVersionString[];

