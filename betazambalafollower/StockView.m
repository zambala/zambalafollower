//
//  StockView.m
//  testing
//
//  Created by zenwise mac 2 on 11/24/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "StockView.h"
#import "TableViewCell.h"
#import "TableViewCell1.h"
#import "AppDelegate.h"

@interface StockView ()
{
    AppDelegate * delegate2;
}

@end

@implementation StockView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.detailsTable.delegate=self;
    self.detailsTable.dataSource=self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0 )
    {
        return 1;
    }
    else if(section==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section) {
            
        case 0:
        {
            NSString * reuse=@"que";
            TableViewCell * cell1=[tableView dequeueReusableCellWithIdentifier:reuse];
            NSArray *cellArray=[[NSBundle mainBundle]loadNibNamed:@"TableViewCell" owner:self options:nil];
            cell1=[cellArray objectAtIndex:0];
            NSString * str=@"%";
            
             cell1.changeLabel.text=delegate2.changeString;
             cell1.changePercentLabel.text = [NSString stringWithFormat:@"(%@%@)",delegate2.changePercentageStr,str];
            
            if([delegate2.changePercentageStr containsString:@"-"])
            {
    
                cell1.changePercentLabel.textColor=[UIColor redColor];
                
                cell1.changeLabel.textColor=[UIColor redColor];
                
            }
            else
            {
               
                cell1.changePercentLabel.textColor=[UIColor greenColor];
                
                cell1.changeLabel.textColor=[UIColor greenColor];
            }
            

            
            
            return cell1;
        }
            break;
        case 1:
        {
            NSString * reuse=@"que";
            TableViewCell1 * cell1=[tableView dequeueReusableCellWithIdentifier:reuse];
            NSArray *cellArray=[[NSBundle mainBundle]loadNibNamed:@"TableViewCell1" owner:self options:nil];
            cell1=[cellArray objectAtIndex:0];
//            cell1.attendence.text=[delegate1.revealArray objectAtIndex:indexPath.row];
             
            cell1.openLabel.text=delegate2.openString;
            cell1.lowLabel.text=delegate2.lowString;
            cell1.highLabel.text=delegate2.highString;
            cell1.volumeLabel.text=delegate2.volumeString;
            cell1.timeLabel.text=delegate2.lastTimeStr;
            
            
            
            
            
            return cell1;
        }
            break;
            
        default:
            break;
    }
    
    
    
    
    return nil;
    
}

//
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            return 110;
        }
        
    }
    else if(indexPath.section==1)
    {
        if(indexPath.row==0)
        {
            return 137;
        }
    }
    return 0;
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
