//
//  FundTransferView.h
//  
//
//  Created by zenwise technologies on 19/12/16.
//
//

#import <UIKit/UIKit.h>

@interface FundTransferView : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>


- (IBAction)showingMainView:(id)sender;

- (IBAction)segmentBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *segmentBtnOutlet;

@property (weak, nonatomic) IBOutlet UITextField *amoutTxtFld;

@property (weak, nonatomic) IBOutlet UIButton *netBankingBtn;



@property (weak, nonatomic) IBOutlet UIButton *upiBtn;
- (IBAction)upiBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paymentViewHgt;

- (IBAction)makePayementBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *destinationVpa;
@property (weak, nonatomic) IBOutlet UITextField *vpaFld;

@property (weak, nonatomic) IBOutlet UIPickerView *segmentPickerView;
@property (weak, nonatomic) IBOutlet UIView *segmentview;

@property (weak, nonatomic) IBOutlet UILabel *enterVpaLbl;

@property (weak, nonatomic) IBOutlet UILabel *targetVpaLbl;

@end
