//
//  FeedListDetail.h
//  testing
//
//  Created by zenwise mac 2 on 12/19/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedListDetail : UIViewController<UIWebViewDelegate>

{
}

@property (strong, nonatomic) IBOutlet UIScrollView *detailScrollView;

@property (strong, nonatomic) IBOutlet UIImageView *detailImg;
@property (strong, nonatomic) IBOutlet UILabel *DesLbl;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *detailDesLbl;

@property (weak, nonatomic) IBOutlet UIWebView *web;



@property (retain, nonatomic) NSString *descriptionString;
@property (retain, nonatomic) NSString *imgString;
@property (retain, nonatomic) NSString *timeString;
@property (retain, nonatomic) NSString *detailDesString;




@property(nonatomic,retain)NSMutableArray *imageDetailArray;

@end
