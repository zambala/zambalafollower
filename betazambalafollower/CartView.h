//
//  CartView.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartView : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *cartTbl;

@property (strong, nonatomic) IBOutlet UIButton *paymentBtn;

@end
