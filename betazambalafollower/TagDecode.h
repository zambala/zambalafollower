//
//  TagDecode.h
//  
//
//  Created by zenwise technologies on 20/07/17.
//
//

#import <UIKit/UIKit.h>

@interface TagDecode : UIViewController
-(void)TagDecode:(NSMutableArray *)btBuffer;
-(BOOL)CheckCapacity:(int)inLength;
-(short)NextTag;
-(short)TagShort;
-(int)GetIndex;
-(BOOL)TagBool;
-(int)TagInt;
-(NSString *)TagString;
-(Byte)TagByte;
-(double)TagDouble;
-(long)TagLong;
-(NSMutableArray*)TagBinary;
-(NSString *)TagStringBuffer:(short)shLength;
-(BOOL)ReadTagDefault:(short)shVal;
-(BOOL)TagBool1;
@end
