//
//  TadeView.m
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "TadeView.h"
#import "TradeCellTableViewCell.h"
#import "TabBar.h"
#import "AppDelegate.h"
#import "NoTradesCell.h"
#import "TagEncode.h"
#import "TagDecode.h"
#import "MT.h"
#import "NewLoginViewController.h"
#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Endian.h>
#import "MTORDER.h"
#import <Mixpanel/Mixpanel.h>

@interface TadeView ()
{
    AppDelegate *delegate2;
    NSArray * filterArray;
    NSMutableArray * symbolArray;
    NSMutableArray * newSymbolArray;
    NSMutableArray * detailsArray;
    NSString * filterString1;
    //mt//
    
    MT *sharedManager;
    MTORDER * sharedManagerOrder;
    NSMutableArray * newSymbolResponseArray;
    NSMutableArray * securityTokenArray;
    NSMutableArray * tradeOrderIDArray;
    NSMutableArray * mtExchangeArray;

}

@end

@implementation TadeView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.tradeTableView.delegate=self;
//    self.tradeTableView.dataSource=self;
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    filterArray=[[NSArray alloc]init];
    symbolArray=[[NSMutableArray alloc]init];
    newSymbolArray=[[NSMutableArray alloc]init];
    delegate2.tradeCompletedArray=[[NSMutableArray alloc]init];
    
    
    
    orderIDArray = [[NSMutableArray alloc]init];
    tradingSymArray = [[NSMutableArray alloc]init];
    orderTimeArray = [[NSMutableArray alloc]init];
    exchangeArray = [[NSMutableArray alloc]init];
    orderTypeArray = [[NSMutableArray alloc]init];
    transactionTypeArray = [[NSMutableArray alloc]init];
    priceArray = [[NSMutableArray alloc]init];
    totalQtyArray = [[NSMutableArray alloc]init];
    newSymbolResponseArray = [[NSMutableArray alloc]init];
    self.controller.delegate=self;
    self.controller.searchResultsUpdater=self;
    
    //mt//
    sharedManager = [MT Mt1];
    sharedManagerOrder = [MTORDER Mt2];
    
   
   
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMainMenu:)
                                                 name:@"trade" object:nil];
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self GetTraders];
    }
    
    else
    {
        delegate2.scripsMt=[[NSMutableArray alloc]init];
        delegate2.allTradeArray=[[NSMutableArray alloc]init];
        TagEncode * tag = [[TagEncode alloc]init];
        delegate2.mtCheck=true;
        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
        [tag GetBuffer];
        
        [self newMessage];
        
        delegate2.scripsMt=[[NSMutableArray alloc]init];
        delegate2.allTradeArray=[[NSMutableArray alloc]init];
        tag = [[TagEncode alloc]init];
        delegate2.mtCheck=true;
        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.TradeDownloadRequest];
        [tag GetBuffer];
        
        [self newMessage];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)GetTraders
{
    
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url;
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary * headers;
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
    url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/trades?api_key=%@&access_token=%@",delegate2.APIKey,delegate2.accessToken]];
        
    }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/trade-book"]];
        
        headers  = @{ @"cache-control": @"no-cache",
                      @"Content-Type" : @"application/json",
                      @"authorization":access,
                      @"x-api-key":@"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1"
                      };
    }
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request setHTTPMethod:@"GET"];
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [request setAllHTTPHeaderFields:headers];
    }
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data!=nil)
        {
        NSDictionary *traderResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"orderDict----%@",traderResponse);
        
        
        tradeArray = [traderResponse valueForKey:@"data"];
        
        for (int i = 0; i<[tradeArray count]; i++)
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
            orderIdStr = [[tradeArray objectAtIndex:i]valueForKey:@"order_id"];
            tradingSymStr = [[tradeArray objectAtIndex:i]valueForKey:@"tradingsymbol"];
            orderTimeStr = [[tradeArray objectAtIndex:i]valueForKey:@"order_timestamp"];
            exchangeStr = [[tradeArray objectAtIndex:i]valueForKey:@"exchange"];
//            fillQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
//            pendingQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
//            orderTypeStr = [[tradeArray objectAtIndex:i]valueForKey:@"order_type"];
            transactionTypeStr = [[tradeArray objectAtIndex:i]valueForKey:@"transaction_type"];
            priceStr = [[[tradeArray objectAtIndex:i]valueForKey:@"average_price"]stringValue];
            totalQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
            
            
            [orderIDArray addObject:orderIdStr];
            [tradingSymArray addObject:tradingSymStr];
            [orderTimeArray addObject:orderTimeStr];
            [exchangeArray addObject:exchangeStr];
//            [filledArray addObject:fillQtyStr];
//            [pendingArray addObject:pendingQtyStr];
//            [orderTypeArray addObject:orderTypeStr];
            [transactionTypeArray addObject:transactionTypeStr];
            [priceArray addObject:priceStr];
            [totalQtyArray addObject:totalQtyStr];
            
            }
            
            else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
                
                orderIdStr = [[tradeArray objectAtIndex:i]valueForKey:@"trade_id"];
                tradingSymStr = [[tradeArray objectAtIndex:i]valueForKey:@"symbol"];
                orderTimeStr = [[tradeArray objectAtIndex:i]valueForKey:@"exchange_time"];
                exchangeStr = [[tradeArray objectAtIndex:i]valueForKey:@"exchange"];
                //            fillQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
                //            pendingQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
                //            orderTypeStr = [[tradeArray objectAtIndex:i]valueForKey:@"order_type"];
                
                NSString * string=[NSString stringWithFormat:@"%@",[[tradeArray objectAtIndex:i]valueForKey:@"transaction_type"]];
                
                if([string isEqualToString:@"B"])
                {
                     transactionTypeStr = @"BUY";
                }
                
                else if([string isEqualToString:@"S"])
                {
                    transactionTypeStr = @"SELL";
                    
                }
               
                priceStr = [[[tradeArray objectAtIndex:i]valueForKey:@"traded_price"]stringValue];
                totalQtyStr = [[[tradeArray objectAtIndex:i]valueForKey:@"traded_quantity"]stringValue];
                
                
                [orderIDArray addObject:orderIdStr];
                [tradingSymArray addObject:tradingSymStr];
                [orderTimeArray addObject:orderTimeStr];
                [exchangeArray addObject:exchangeStr];
                //            [filledArray addObject:fillQtyStr];
                //            [pendingArray addObject:pendingQtyStr];
                //            [orderTypeArray addObject:orderTypeStr];
                [transactionTypeArray addObject:transactionTypeStr];
                [priceArray addObject:priceStr];
                [totalQtyArray addObject:totalQtyStr];
            }
            
            
            
            Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
            
            [mixpanel1 identify:delegate2.userID];
            
//            NSMutableString * string=[[NSMutableString alloc]init];
//
//            for(int i=0;i<tradingSymArray.count;i++)
//            {
//                NSMutableString * string1=[[NSMutableString alloc]init];
//                string1=[NSMutableString stringWithFormat:@"%@,",[tradingSymArray objectAtIndex:i]];
//
//                [string appendString:string1];                                       }
//
//
//
//
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
            
            [mixpanel1.people set:@{@"TradesList":tradeArray}];
            }
            
            else
            {
                   [mixpanel1.people set:@{@"TradesList":delegate2.tradeCompletedArray}];
            }
            
            
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.tradeTableView.delegate=self;
            self.tradeTableView.dataSource=self;

            [self.tradeTableView reloadData];
            
        });
            
        }
    }];
        
    
    
    [postDataTask resume];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    
//    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
//    
//    [self presentViewController:tabPage animated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//search//

- (IBAction)searchAction:(id)sender {
    
    self.controller = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.controller.searchResultsUpdater = self;
    self.controller.hidesNavigationBarDuringPresentation = YES;
    self.controller.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
    //    self.controller.searchBar.scopeButtonTitles = @[@"Posts", @"Users", @"Subreddits"];
    
    
    
    
    
    [self presentViewController:self.controller animated:YES
                     completion:nil];

}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    
        @try {
            if(newSymbolArray.count>0)
            {
                [newSymbolArray removeAllObjects];
                
            }
            if(symbolArray.count>0)
            {
                [symbolArray removeAllObjects];
            }
            for(int i=0;i<tradeArray.count;i++)
            {
                
                [symbolArray addObject:[[tradeArray objectAtIndex:i]objectForKey:@"tradingsymbol"]];
            }
            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchController.searchBar.text];
            NSLog(@"%@",predicate);
            if(self.controller.searchBar.text.length>0)
            {
                filterArray =[[NSArray alloc]init];
                
                
                filterArray=[symbolArray  filteredArrayUsingPredicate:predicate];
                
                
                    
                
                

                
                for(int i=0;i<filterArray.count;i++)
                {
                    
                    detailsArray=[[NSMutableArray alloc]init];
                    
                    if(newSymbolArray.count>0&&filterArray.count>0)
                    {
                        
                        filterString1=[filterArray objectAtIndex:i];
                        for(int k=0;k<newSymbolArray.count;k++)
                        {
                            [detailsArray addObject:[[newSymbolArray objectAtIndex:k]objectForKey:@"firstname"]];
                            
                        }
                        
                    }
                    for(int j=0;j<tradeArray.count;j++)
                    {
                        NSString * filterString=[filterArray objectAtIndex:i];
                        NSString * responseString=[[tradeArray  objectAtIndex:j]objectForKey:@"tradingsymbol"];
                        
                        
                        
                        
                        if(detailsArray.count>0)
                        {
                            
                            if([detailsArray containsObject:filterString1])
                            {
                                
                                
                            }
                            
                            
                            else
                            {
                                if([filterString isEqualToString:responseString])
                                {
                                    [newSymbolArray addObject:[tradeArray objectAtIndex:j]];
                                }
                            }
                            
                            
                        }
                        
                        else
                        {
                            if([filterString isEqualToString:responseString])
                            {
                                [newSymbolArray addObject:[tradeArray objectAtIndex:j]];
                            }
                        }

                    }
                }
            }
            [self.tradeTableView reloadData];
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
    
}

        
        
    



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.controller.active==YES)
    {
        return [newSymbolArray count];
    }
    
    else if(tradeArray.count>0||delegate2.tradeCompletedArray.count>0)
    {
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            return [tradeArray count];
        }
        
        else
        {
            return [delegate2.tradeCompletedArray count];
            
        }
        
    }
    else
    {
        return 1;
    }
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if(self.controller.active==YES)
    {
        
        
        if(newSymbolArray.count>0)
        {
            
        
        TradeCellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
        
        cell.orderIdLbl.text = [NSString stringWithFormat:@"%@",[[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"order_id"]];
        cell.timeLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"order_timestamp"]];
        cell.tradingSymLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"tradingsymbol"]];
        cell.exchangeLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"exchange"]];
        cell.transactionTypeLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"transaction_type"]];
        cell.priceLbl.text =[NSString stringWithFormat:@"%@",[[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"average_price"]];
        //    cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
        //    cell.fillQtyLbl.text = [NSString stringWithFormat:@"%@", [filledArray objectAtIndex:indexPath.row]];
        //    cell.pedingQtyLbl.text = [NSString stringWithFormat:@"%@", [pendingArray objectAtIndex:indexPath.row]];
        cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [[newSymbolArray objectAtIndex:indexPath.row]valueForKey:@"quantity"]];
        
        return cell;
            
        }
        
    }
    
    else
    {
    if(tradeArray.count>0||delegate2.tradeCompletedArray.count>0)
    {
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            
        {
        TradeCellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    cell.orderIdLbl.text = [NSString stringWithFormat:@"%@", [orderIDArray objectAtIndex:indexPath.row]];
    cell.timeLbl.text = [NSString stringWithFormat:@"%@", [orderTimeArray objectAtIndex:indexPath.row]];
    cell.tradingSymLbl.text = [NSString stringWithFormat:@"%@", [tradingSymArray objectAtIndex:indexPath.row]];
    cell.exchangeLbl.text = [NSString stringWithFormat:@"%@", [exchangeArray objectAtIndex:indexPath.row]];
    cell.transactionTypeLbl.text = [NSString stringWithFormat:@"%@", [transactionTypeArray objectAtIndex:indexPath.row]];
    cell.priceLbl.text =[NSString stringWithFormat:@"%@", [priceArray objectAtIndex:indexPath.row]];
//    cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
//    cell.fillQtyLbl.text = [NSString stringWithFormat:@"%@", [filledArray objectAtIndex:indexPath.row]];
//    cell.pedingQtyLbl.text = [NSString stringWithFormat:@"%@", [pendingArray objectAtIndex:indexPath.row]];
    cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [totalQtyArray objectAtIndex:indexPath.row]];
    
        return cell;
        }
        
        
        
        else
        {
            TradeCellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
           
            cell.timeLbl.text = [NSString stringWithFormat:@"%@",[[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"stOrderTime"]];
            
            cell.exchangeLbl.text = [NSString stringWithFormat:@"%@",[[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"stExchange"]];
            
            NSString * string1=[[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"btSide"];
             int orderType1=[string1 intValue];
            if(orderType1==1)
            {
                string1=@"BUY";
            }
            
            else if(orderType1==2)
            {
                string1=@"SELL";
            }
            
            int securityId=[[[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"stSecurityID"] intValue];
            
            for(int i=0;i< delegate2.scripsMt.count;i++)
            {
                int securityId2=[[[ delegate2.scripsMt objectAtIndex:i] objectForKey:@"exchange_token"] intValue];
                
                if(securityId==securityId2)
                {
                    cell.tradingSymLbl.text=[[ delegate2.scripsMt objectAtIndex:i] objectForKey:@"tradingsymbol"];
                }
            }
            
            

            
            cell.transactionTypeLbl.text = [NSString stringWithFormat:@"%@",string1];

            
            for(int i=0;i<delegate2.allTradeOrderArray.count;i++)
            {
                 int orderID=[[[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"ATINOrderId"] intValue];
                
                 int orderID1=[[[delegate2.allTradeOrderArray objectAtIndex:i] objectForKey:@"ATINOrderId"] intValue];
                
                if(orderID==orderID1)
                {
                    float finalPrice=[[[delegate2.allTradeOrderArray objectAtIndex:i] objectForKey:@"dbPrice"] intValue];
                    
                     cell.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
                    
                     cell.orderIdLbl.text = [NSString stringWithFormat:@"%@",[[delegate2.allTradeOrderArray objectAtIndex:indexPath.row] objectForKey:@"stTradeID"]];
                    
                }
                
                
                
            }
           
           
           

            //    cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
                cell.filledLabel.text = [NSString stringWithFormat:@"%@", [[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"inExeQty"]];
            
            NSString * pendingStr= [NSString stringWithFormat:@"%@", [[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"inPendingQty"]];
            if([pendingStr isEqualToString:@"(null)"])
            {
                 cell.pendingLabel.text =@"0";
            }
            
            else
            {
                cell.pendingLabel.text =pendingStr;
                
            }
            
            cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.tradeCompletedArray objectAtIndex:indexPath.row] objectForKey:@"inQty"]];
            
            
            
            return cell;

            
        }
        
        
    }
    
    else
    {
        NoTradesCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        
        return cell;
    }
        
    }
    
    return nil;
}

- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
    
    
    if(indexPath.row % 2 == 0)
    {
        
        cell.backgroundColor = [UIColor whiteColor];
    }
           else
           {
               
               cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];

           }
       
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tradeArray.count>0||delegate2.allTradeArray.count>0)
    { return 104;
    }
    else
    {
        return 393 ;
    }
   
}

//mt//

-(void)newMessage
{
    
    

    //    [delegate2.outputStream setDelegate:self];
    Byte UUID[delegate2.btOutBuffer.count];
    
    for (int i = 0; i < delegate2.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate2.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp1;
        [scanner scanHexInt:&temp1];
        NSLog(@"%d",temp1);
        
        UUID[i]=temp1;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate2.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate2.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate2.outputStream write:bytes maxLength:delegate2.btOutBuffer.count];
    
}



- (void)showMainMenu:(NSNotification *)note
{
    
    securityTokenArray=[[NSMutableArray alloc]init];
    
     mtExchangeArray=[[NSMutableArray alloc]init];
    
    
    for(int i=0;i<delegate2.allTradeArray.count;i++)
    {
        NSString * string=[[delegate2.allTradeArray objectAtIndex:i] objectForKey:@"btOrderStatus"];
        
        int status=[string intValue];
        
        
          if(status==2)
        {
            [delegate2.tradeCompletedArray addObject:[delegate2.allTradeArray objectAtIndex:i]];
            [securityTokenArray addObject:[NSString stringWithFormat:@"%@",[[delegate2.allTradeArray objectAtIndex:i] objectForKey:@"stSecurityID"]]];
            
           
             [mtExchangeArray addObject:[NSString stringWithFormat:@"%@",[[delegate2.allTradeArray objectAtIndex:i] objectForKey:@"stExchange"]]];
            
            
            
            [delegate2.allTradeArray removeObjectAtIndex:i];
        }
    }
    
  
    
    
    
    //    [delegate2.allOrderHistory removeAllObjects];
        [self symbolMethod];
    
    
    
    
    //         NSLog(@"Received Notification - Someone seems to have logged in");
}


-(void)symbolMethod
{
    if(securityTokenArray.count>0)
    {
        NSArray * userArray=[securityTokenArray mutableCopy];
        NSSet *set = [NSSet setWithArray:userArray];
        NSArray *uniqueArray = [set allObjects];
        NSArray * userArray1=[mtExchangeArray mutableCopy];
        NSSet *set1 = [NSSet setWithArray:userArray1];
        NSArray *uniqueArray1 = [set1 allObjects];
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   };
        NSDictionary *parameters = @{ @"exchangetokens":uniqueArray,
                                       @"exchangevalue":uniqueArray1
                                      };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@stock/tokens",delegate2.baseUrl]]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data!=nil)
                                                        {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                                
                                                                NSMutableArray * symbolResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                for(int i=0;i<symbolResponse.count;i++)
                                                                {
                                                                    [newSymbolResponseArray addObject:[symbolResponse objectAtIndex:i]];
                                                                    
                                                                }
                                                                
                                                                
                                                                NSArray * symbols=[newSymbolResponseArray mutableCopy];
                                                                NSSet *set = [NSSet setWithArray:symbols];
                                                                delegate2.scripsMt = [set allObjects];
                                                                
                                                            }
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.tradeTableView.delegate=self;
                                                            self.tradeTableView.dataSource=self;
                                                            
                                                            [self.tradeTableView reloadData];
                                                                
                                                            
                                                            
                                                        });
                                                        
                                                    }];
        [dataTask resume];
        
    }
    
    
    
}

@end
