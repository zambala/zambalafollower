//
//  NewProfileTableViewCell.m
//  betazambalafollower
//
//  Created by zenwise technologies on 23/03/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewProfileTableViewCell.h"

@implementation NewProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.oLabel.layer.cornerRadius=12.5;
    self.oLabel.clipsToBounds = YES;
    
    self.stLabel.layer.cornerRadius=12.5;
    self.stLabel.clipsToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
