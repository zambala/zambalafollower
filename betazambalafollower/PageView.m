//
//  PageView.m
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PageView.h"
#import "TabBar.h"


#define SCROLLWIDTH 375

@interface PageView ()
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollview_ContentView_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollview_ContentView_Width;

@end

@implementation PageView


- (void)viewDidLoad
{
    
    scrollView.contentSize=CGSizeMake(SCROLLWIDTH*3, scrollView.frame.size.height);
    scrollView.delegate = self;
    
    for (int i =0; i<3; i++)
    {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(SCROLLWIDTH*i, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        if (i==0)
        {
//            imageView.backgroundColor = [UIColor redColor];
            imageView.image = [UIImage imageNamed:@"image1.jpg"];
            
        
            
        }
        else if (i==1)
        {
//            imageView.backgroundColor = [UIColor greenColor];
            imageView.image = [UIImage imageNamed:@"image2.jpg"];

            
        }
        else
        {
//            imageView.backgroundColor = [UIColor yellowColor];
            imageView.image = [UIImage imageNamed:@"kiteimg.png"];

        }
        [scrollView addSubview:imageView];
    }
    
    
       [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)viewWillAppear:(BOOL)animated
{
    [self performAdjustScrollviewHeightWidth];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if(prefs!=nil)
    {
        @try {
            NSString * local=[prefs stringForKey:@"logintype"];
            
            if([local isEqualToString:@"OTP1"])
            {
                
                TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
                
                [self presentViewController:tabPage animated:YES completion:nil];
                
                
                
                
            }
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        
        
        
        
        
    }
    

    
    
}
//- (void)viewDidAppear:(BOOL)animated;     // Called when the view has been fully transitioned onto the screen. Default does nothing
//- (void)viewWillDisappear:(BOOL)animated; // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
- (void)viewDidDisappear:(BOOL)animated
{
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
}



-(void)performAdjustScrollviewHeightWidth
{
    CGFloat windowWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat windowHeight =  [[UIScreen mainScreen] bounds].size.height;
    
    self.scrollview_ContentView_Height.constant = windowHeight - 127;
    self.scrollview_ContentView_Width.constant = windowWidth * 3;
    
    [self.view layoutIfNeeded];
    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark changePage

-(IBAction)changePage:(id)sender
{
    [scrollView scrollRectToVisible:CGRectMake(SCROLLWIDTH*pageControl.currentPage, scrollView.frame.origin.y, SCROLLWIDTH, scrollView.frame.size.height) animated:YES];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    
    [self setIndiactorForCurrentPage];
    
}

-(void)setIndiactorForCurrentPage
{
    uint page = scrollView.contentOffset.x / SCROLLWIDTH;
    [pageControl setCurrentPage:page];
    
}


@end
