//
//  OffersView.m
//  testing
//
//  Created by zenwise mac 2 on 1/24/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "OffersView.h"
#import "OffersCollectionCell.h"
#import "HMSegmentedControl.h"

@interface OffersView ()

@end

@implementation OffersView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    imgArray = [NSMutableArray arrayWithObjects:@"img1.jpeg",@"img2.jpeg",@"image4.jpeg",@"image5.jpeg" ,@"image3.jpg",nil];
    
    [_offerScroll addSubview:_offerCollView];
    [_offerScroll addSubview:_seachCtrl];
    
    _seachCtrl = [[UISearchBar alloc]initWithFrame:CGRectMake(_seachCtrl.frame.origin.x, 64, _seachCtrl.frame.size.width, _seachCtrl.frame.size.height)];
    _seachCtrl.delegate = self;
    
    [self.view addSubview:self.offerScroll];
    
    
    HMSegmentedControl *segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"TRENDING",@"MOBILES",@"RESTAURANTS"]];
    segmentedControl.frame = CGRectMake(0, _seachCtrl.frame.origin.y+_seachCtrl.frame.size.height, 375, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.backgroundColor = [UIColor lightGrayColor];
    [segmentedControl setTintColor:[UIColor whiteColor]];
    //    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
//    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];



}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [imgArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    
    OffersCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.imgView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    
    return cell;
    
}
@end
