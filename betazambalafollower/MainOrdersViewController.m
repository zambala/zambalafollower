//
//  MainOrdersViewController.m
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "MainOrdersViewController.h"
#import "HMSegmentedControl.h"
#import "PendingOrdersCell.h"
#import "CompletedCell.h"
#import "CancelledOrderCell.h"
#import "TabBar.h"
#import "AppDelegate.h"
#import "NoOrderImg.h"
#import "StockOrderView.h"
#import "HomePage.h"
#import "PortfolioView.h"
#import "HomePage.h"
#import "TagEncode.h"
#import "TagDecode.h"
#import "MT.h"
#import "NewLoginViewController.h"
#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Endian.h>
#import "MTORDER.h"
#import <Mixpanel/Mixpanel.h>

@interface MainOrdersViewController ()
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate2;
    //mt//
    
    MT *sharedManager;
    MTORDER * sharedManagerOrder;
    NSMutableArray * securityTokenArray;
    NSMutableArray *symbolResponse;
    NSMutableArray * newSymbolResponseArray;
    BOOL mainorderCheck;
    NSMutableArray * mixpanelCancelledArray;
    NSMutableArray * mixpanelCompletedArray;
    NSMutableArray * mixpanelPendingArray;
    BOOL reload;
    NSTimer * timer;
    NSMutableArray * mtExchangeArray;
//    NSArray *newSymbols;
}

@end

@implementation MainOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    reload=true;
    //orders list//
    
    orderIDArray = [[NSMutableArray alloc]init];
    tradingSymArray = [[NSMutableArray alloc]init];
    orderTimeArray = [[NSMutableArray alloc]init];
    exchangeArray = [[NSMutableArray alloc]init];
    filledArray = [[NSMutableArray alloc]init];
    pendingArray = [[NSMutableArray alloc]init];
    orderTypeArray = [[NSMutableArray alloc]init];
    transactionTypeArray = [[NSMutableArray alloc]init];
    priceArray = [[NSMutableArray alloc]init];
    totalQtyArray = [[NSMutableArray alloc]init];
    
    cancelledOrderIDArray = [[NSMutableArray alloc]init];
    cancelledTradingSymArray = [[NSMutableArray alloc]init];
    cancelledOrderTimeArray = [[NSMutableArray alloc]init];
    cancelledExchangeArray = [[NSMutableArray alloc]init];
    cancelledFilledArray = [[NSMutableArray alloc]init];
    cancelledPendingArray = [[NSMutableArray alloc]init];
    cancelledOrderTypeArray = [[NSMutableArray alloc]init];
    cancelledTransactionTypeArray = [[NSMutableArray alloc]init];
    cancelledPriceArray = [[NSMutableArray alloc]init];
    cancelledTotalQtyArray = [[NSMutableArray alloc]init];
    
    
    pendingOrderIDArray = [[NSMutableArray alloc]init];
    pendingTradingSymArray = [[NSMutableArray alloc]init];
    pendingOrderTimeArray = [[NSMutableArray alloc]init];
    pendingExchangeArray = [[NSMutableArray alloc]init];
    pendingFilledArray = [[NSMutableArray alloc]init];
    pendingPendingArray = [[NSMutableArray alloc]init];
    pendingOrderTypeArray = [[NSMutableArray alloc]init];
    pendingTransactionTypeArray = [[NSMutableArray alloc]init];
    pendingPriceArray = [[NSMutableArray alloc]init];
    pendingTotalQtyArray = [[NSMutableArray alloc]init];
    pendingFilledArrayy = [[NSMutableArray alloc]init];
    pendingLabelArray = [[NSMutableArray alloc]init];
    instrumentTokenn = [[NSMutableArray alloc]init];
    pendingMessageArray=[[NSMutableArray alloc]init];

    
       
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"PENDING",@"COMPLETED",@"CANCELLED"]];
    segmentedControl.frame = CGRectMake(0,88, self.view.frame.size.width, 54.1);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
        segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    
    [self.view addSubview:segmentedControl];
    
    self.pendingView.hidden=NO;
    self.completedView.hidden=YES;
    self.cancelView.hidden=YES;
    
    NSLog(@"%@",delegate2.orderStatusCheck);
    
    //mt//
    sharedManager = [MT Mt1];
    sharedManagerOrder = [MTORDER Mt2];
   
    self.pendingOrdersTableView.hidden=YES;
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    mixpanelCancelledArray=[[NSMutableArray alloc]init];
    mixpanelCompletedArray=[[NSMutableArray alloc]init];
    mixpanelPendingArray=[[NSMutableArray alloc]init];
    
   timer=  [NSTimer scheduledTimerWithTimeInterval:2.0f
                                                      target:self selector:@selector(reloadData) userInfo:nil repeats:YES];
    securityTokenArray=[[NSMutableArray alloc]init];
    exchangeArray=[[NSMutableArray alloc]init];

    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                   selector:@selector(showMainMenu:)
                                                    name:@"loginComplete" object:nil];
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    newSymbolResponseArray=[[NSMutableArray alloc]init];
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    delegate2.mainorderCheck=true;
    
    delegate2.allOrderHistory=[[NSMutableArray alloc]init];
    
    if([delegate2.orderStatusCheck isEqualToString:@"COMPLETED"])
    {
        [segmentedControl setSelectedSegmentIndex:1];
        [self segmentedControlChangedValue];
        
        delegate2.orderStatusCheck=@"";
        
        
    }
    
    else if([delegate2.orderStatusCheck isEqualToString:@"REJECTED"])
    {
        [segmentedControl setSelectedSegmentIndex:2];
        [self segmentedControlChangedValue];
        delegate2.orderStatusCheck=@"";
        
        
    }
    
    else if([delegate2.orderStatusCheck isEqualToString:@"OPEN"])
    {
        [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue];
        delegate2.orderStatusCheck=@"";
        
    }
    
    else
    {
        segmentedControl.selectedSegmentIndex=0;
        
  if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
  {
      [self RetrieveAllOrders];
  }
        
else
{
    
    delegate2.mtPendingArray=[[NSMutableArray alloc]init];
    TagEncode * tag = [[TagEncode alloc]init];
    delegate2.mtCheck=true;
    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
    [tag GetBuffer];
    
    [self newMessage];
    
}
    
        
        
        
}
    

    
}

-(void)viewWillDisappear:(BOOL)animated
{
    delegate2.mainorderCheck=false;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
       return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(segmentedControl.selectedSegmentIndex==0 && (pendingOrderIDArray.count>0||delegate2.mtPendingArray.count>0))
    {

        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
             return [pendingOrderIDArray count];
        }
        
        else
        {
            return [delegate2.mtPendingArray count];
        }

    
    }
    else if(segmentedControl.selectedSegmentIndex==1 && (orderIDArray.count>0||delegate2.mtCompletedArray.count>0))
        
    {
       if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            return [orderIDArray count];
        }
        
        else
        {
            return [delegate2.mtCompletedArray count];
        }
        
        
        
    }
    
    else if(segmentedControl.selectedSegmentIndex==2 && (cancelledOrderIDArray.count>0||delegate2.mtCancelArray.count>0))
    {
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            return [cancelledOrderIDArray count];
        }
        
        else
        {
            return [delegate2.mtCancelArray count];
        }
        
    }
    
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * orderText=@"ORDER";
    @try {
        if(segmentedControl.selectedSegmentIndex==0 && (pendingOrderIDArray.count>0||delegate2.mtPendingArray.count>0))
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
            PendingOrdersCell * cell2=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            cell2.orderIdLbl.text = [NSString stringWithFormat:@"%@", [pendingOrderIDArray objectAtIndex:indexPath.row]];
            cell2.timeLbl.text = [NSString stringWithFormat:@"%@", [pendingOrderTimeArray objectAtIndex:indexPath.row]];
            cell2.tradingSymLbl.text = [NSString stringWithFormat:@"%@", [pendingTradingSymArray objectAtIndex:indexPath.row]];
            
            
            
            cell2.exchangeLbl.text = [NSString stringWithFormat:@"%@ %@ %@", [pendingExchangeArray objectAtIndex:indexPath.row],[pendingOrderTypeArray objectAtIndex:indexPath.row],orderText];
            cell2.transactionTypeLbl.text = [NSString stringWithFormat:@"%@", [pendingTransactionTypeArray objectAtIndex:indexPath.row]];
            float finalPrice=[[pendingPriceArray objectAtIndex:indexPath.row] floatValue];
            cell2.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
            
            cell2.qtyLbl.text = [NSString stringWithFormat:@"%@", [pendingTotalQtyArray objectAtIndex:indexPath.row]];
            
            cell2.pendingLabel.text=[NSString stringWithFormat:@"%@",[pendingLabelArray objectAtIndex:indexPath.row]];
            cell2.filledLabel.text=[NSString stringWithFormat:@"%@",[pendingFilledArrayy objectAtIndex:indexPath.row]];
            
            
            [cell2.editBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchDown];
            
            [cell2.closeBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchDown];
            
            return cell2;
        }
            
            else
            {
                
                
                PendingOrdersCell * cell2=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
                cell2.orderIdLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"stExchangeOrderID"]];
                cell2.timeLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"stOrderTime"]];
//                cell2.tradingSymLbl.text = @"AXISBANK";
                
                NSString * string=[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"btOrderType"];
                
                int orderType=[string intValue];
                
                if(orderType==1)
                {
                    string=@"MARKET ORDER";
                }
                
                else if(orderType==2)
                {
                    string=@"LIMIT ORDER";
                }
                
                else if(orderType==4)
                {
                    string=@"SL ORDER";
                }

                
                
                
                cell2.exchangeLbl.text = [NSString stringWithFormat:@"%@ %@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"stExchange"],string ];
                
                 NSString * string1=[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"btSide"];
                
                int orderType1=[string1 intValue];
                
                if(orderType1==1)
                {
                    string1=@"BUY";
                }
                
                else if(orderType1==2)
                {
                    string1=@"SELL";
                }
                
                

                
                cell2.transactionTypeLbl.text = [NSString stringWithFormat:@"%@",string1];
                
                int securityId=[[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"stSecurityID"] intValue];
                
                for(int i=0;i<delegate2.scripsMt.count;i++)
                {
                    int securityId2=[[[delegate2.scripsMt objectAtIndex:i] objectForKey:@"exchange_token"] intValue];
                    
                    if(securityId==securityId2)
                    {
                        cell2.tradingSymLbl.text=[[delegate2.scripsMt objectAtIndex:i] objectForKey:@"tradingsymbol"];
                    }
                }
                
               
                
                
                float finalPrice=[[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"dbPrice"] floatValue];
                cell2.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
                
                cell2.qtyLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"inQty"]];
                
                NSString * pendingStr=[NSString stringWithFormat:@"%@", [[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"inPendingQty"]];
                
                if([pendingStr isEqualToString:@"(null)"])
                {
                   cell2.pendingLabel.text=@"0";
                }
                
                else
                {
                    cell2.pendingLabel.text= pendingStr;
                    
                }
                
                
                cell2.filledLabel.text=@"0";
                
                
                [cell2.editBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchDown];
                
                [cell2.closeBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchDown];
                
                
                return cell2;
                
                
            }
        
           
        }
        else if(segmentedControl.selectedSegmentIndex==1 && (orderIDArray.count>0||delegate2.mtCompletedArray.count>0))    {
            
            
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
            
            
            
            CompletedCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            cell.orderIdLbl.text = [NSString stringWithFormat:@"%@", [orderIDArray objectAtIndex:indexPath.row]];
            cell.timeLbl.text = [NSString stringWithFormat:@"%@", [orderTimeArray objectAtIndex:indexPath.row]];
            cell.tradingSymLbl.text = [NSString stringWithFormat:@"%@", [tradingSymArray objectAtIndex:indexPath.row]];
            cell.exchangeLbl.text = [NSString stringWithFormat:@"%@ %@ %@", [exchangeArray objectAtIndex:indexPath.row],[orderTypeArray objectAtIndex:indexPath.row],orderText];
            cell.transactionLbl.text = [NSString stringWithFormat:@"%@", [transactionTypeArray objectAtIndex:indexPath.row]];
            
            float finalPrice=[[priceArray objectAtIndex:indexPath.row] floatValue];
            cell.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
            cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
            cell.fillQtyLbl.text = [NSString stringWithFormat:@"%@", [filledArray objectAtIndex:indexPath.row]];
            cell.pendingQtyLbl.text = [NSString stringWithFormat:@"%@", [pendingArray objectAtIndex:indexPath.row]];
            cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [totalQtyArray objectAtIndex:indexPath.row]];
            
            
            
            return cell;
                
            }
            
            else
            {
                
                CompletedCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                cell.orderIdLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"stExchangeOrderID"]];
                cell.timeLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"stOrderTime"]];
                cell.tradingSymLbl.text = @"AXISBANK";
                
                NSString * string=[[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"btOrderType"];
                
                int orderType=[string intValue];
                
                if(orderType==1)
                {
                    string=@"MARKET ORDER";
                }
                
                else if(orderType==2)
                {
                    string=@"LIMIT ORDER";
                }
                
                else if(orderType==4)
                {
                    string=@"SL ORDER";
                }
                
                
                cell.exchangeLbl.text = [NSString stringWithFormat:@"%@ %@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"stExchange"],string];
                
                NSString * string1=[[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"btSide"];
                
                int orderType1=[string1 intValue];
                
                if(orderType1==1)
                {
                    string1=@"BUY";
                }
                
                else if(orderType1==2)
                {
                    string1=@"SELL";
                }
                
                int securityId=[[[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"stSecurityID"] intValue];
                
                for(int i=0;i<delegate2.scripsMt.count;i++)
                {
                    int securityId2=[[[delegate2.scripsMt objectAtIndex:i] objectForKey:@"exchange_token"] intValue];
                    
                    if(securityId==securityId2)
                    {
                        cell.tradingSymLbl.text=[[delegate2.scripsMt objectAtIndex:i] objectForKey:@"tradingsymbol"];
                    }
                }
                

                cell.transactionLbl.text = [NSString stringWithFormat:@"%@",string1];
                
               float finalPrice=[[[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"dbPrice"] floatValue];
                cell.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
                
//                cell.orderTypeLbl.text = [NSString stringWithFormat:@"%@", [orderTypeArray objectAtIndex:indexPath.row]];
                cell.fillQtyLbl.text =[NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"inExeQty"]];
             
                
                NSString * pendingStr=[NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"inPendingQty"]];
                
                if([pendingStr isEqualToString:@"(null)"])
                {
                   
                    
                    cell.pendingQtyLbl.text=@"0";
                }
                
                else
                {
                     cell.pendingQtyLbl.text= pendingStr;
                }
                
                cell.qtyLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCompletedArray objectAtIndex:indexPath.row] objectForKey:@"inQty"]];
                
                
                return cell;
               
            }
            
        }
        
        else if(segmentedControl.selectedSegmentIndex==2 && (cancelledOrderIDArray.count>0||delegate2.mtCancelArray.count>0))
        {
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
            
            CancelledOrderCell * cell1=[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
            
            cell1.orderIDLbl.text = [NSString stringWithFormat:@"%@", [cancelledOrderIDArray objectAtIndex:indexPath.row]];
            cell1.timeLbl.text = [NSString stringWithFormat:@"%@", [cancelledOrderTimeArray objectAtIndex:indexPath.row]];
            cell1.companyLbl.text = [NSString stringWithFormat:@"%@", [cancelledTradingSymArray objectAtIndex:indexPath.row]];
                
            cell1.exchangeLbl.text = [NSString stringWithFormat:@"%@ %@ %@", [cancelledExchangeArray objectAtIndex:indexPath.row],[cancelledOrderTypeArray objectAtIndex:indexPath.row],orderText];
            cell1.transactionLbl.text = [NSString stringWithFormat:@"%@", [cancelledTransactionTypeArray objectAtIndex:indexPath.row]];
            
            float finalPrice=[[cancelledPriceArray objectAtIndex:indexPath.row] floatValue];
            cell1.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
            
            cell1.qtyLbl.text = [NSString stringWithFormat:@"%@", [cancelledTotalQtyArray objectAtIndex:indexPath.row]];
                
                 return cell1;
            
            }
            
            else
            {
                CancelledOrderCell * cell1=[tableView dequeueReusableCellWithIdentifier:@"CELL2" forIndexPath:indexPath];
                
                cell1.orderIDLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"stExchangeOrderID"]];
                cell1.timeLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"stOrderTime"]];
                cell1.companyLbl.text = @"IDFC";
                
                NSString * string=[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"btOrderType"];
                
                int orderType=[string intValue];
                
                if(orderType==1)
                {
                    string=@"MARKET ORDER";
                }
                
                else if(orderType==2)
                {
                    string=@"LIMIT ORDER";
                }
                
                else if(orderType==4)
                {
                    string=@"SL ORDER";
                }
                
                
                int securityId=[[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"stSecurityID"] intValue];
                
                for(int i=0;i<delegate2.scripsMt.count;i++)
                {
                    int securityId2=[[[delegate2.scripsMt objectAtIndex:i] objectForKey:@"exchange_token"] intValue];
                    
                    if(securityId==securityId2)
                    {
                        cell1.companyLbl.text=[[delegate2.scripsMt objectAtIndex:i] objectForKey:@"tradingsymbol"];
                    }
                }
                
                
                
                cell1.exchangeLbl.text = [NSString stringWithFormat:@"%@ %@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"stExchange"],string ];
                
                NSString * string1=[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"btSide"];
                
                int orderType1=[string1 intValue];
                
                if(orderType1==1)
                {
                    string1=@"BUY";
                }
                
                else if(orderType1==2)
                {
                    string1=@"SELL";
                }
                
                
                
                cell1.transactionLbl.text = [NSString stringWithFormat:@"%@",string1];
                
                float finalPrice=[[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"dbPrice"] floatValue];
                cell1.priceLbl.text =[NSString stringWithFormat:@"%.2f",finalPrice];
                
                cell1.qtyLbl.text = [NSString stringWithFormat:@"%@", [[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"inQty"]];
                
                return cell1;
                
            }
            
            
           
        }
        
        else
        {
            NoOrderImg * cell1=[tableView dequeueReusableCellWithIdentifier:@"CELL3" forIndexPath:indexPath];
            
            return cell1;
        }

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
       return 0;
    
             }


- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
    
    if(indexPath.row % 2 == 0)
        
       
    
     cell.backgroundColor = [UIColor whiteColor];
    else
    {
        cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
        
    }
    
}

-(void)segmentedControlChangedValue
{
    self.pendingOrdersTableView.hidden=YES;
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    if(segmentedControl.selectedSegmentIndex==0)
    {
        
        
        self.pendingView.hidden=NO;
        self.completedView.hidden=YES;
        self.cancelView.hidden=YES;
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
           
            [self RetrieveAllOrders];
        }
        
        else
        {
            newSymbolResponseArray=[[NSMutableArray alloc]init];
            delegate2.mtPendingArray=[[NSMutableArray alloc]init];
            TagEncode * tag = [[TagEncode alloc]init];
            delegate2.mtCheck=true;
            [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
            [tag GetBuffer];
            
            [self newMessage];
            
           
            
        }
    
//        [self.pendingOrdersTableView reloadData];
    }
    
    else if(segmentedControl.selectedSegmentIndex==1)
    {
       if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            [self RetrieveAllOrders];
        }
        
        else
        {
            newSymbolResponseArray=[[NSMutableArray alloc]init];
            delegate2.mtCompletedArray=[[NSMutableArray alloc]init];
            TagEncode * tag = [[TagEncode alloc]init];
            delegate2.mtCheck=true;
            [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
            [tag GetBuffer];
            
            [self newMessage];
            
        }

        self.pendingView.hidden=YES;
        self.completedView.hidden=NO;
        self.cancelView.hidden=YES;
        
//        [self.pendingOrdersTableView reloadData];

    }
    
    else if(segmentedControl.selectedSegmentIndex==2)
    {
        
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
             [self RetrieveAllOrders];
        }
        
        else
        {newSymbolResponseArray=[[NSMutableArray alloc]init];
            delegate2.mtCancelArray=[[NSMutableArray alloc]init];
            TagEncode * tag = [[TagEncode alloc]init];
            delegate2.mtCheck=true;
            [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
            [tag GetBuffer];
            
            [self newMessage];
            
        }
        
       

        self.pendingView.hidden=YES;
        self.completedView.hidden=YES;
        self.cancelView.hidden=NO;
        
//        [self.pendingOrdersTableView reloadData];
    }
    
    
   }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0&& (pendingOrderIDArray.count>0||delegate2.mtPendingArray.count>0))
    {
        return 104;
    }
   else  if(segmentedControl.selectedSegmentIndex==1&&(orderIDArray.count>0||delegate2.mtCompletedArray.count>0))
    {
        return 104;
    }
    
   else  if(segmentedControl.selectedSegmentIndex==2&&(cancelledOrderIDArray.count>0||delegate2.mtCancelArray.count>0))
    {
        return 104;
    }
    return 408;
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backToMainView:(id)sender {
//    

    
    
    
    
  
//
    //now present this navigation controller modally
//    [self presentViewController:tabPage
//                       animated:YES
//                     completion:^{
//                         
//                     }];
    
//    [self showDetailViewController:tabPage sender:nil];
//    if([delegate2.holdingCheck isEqualToString:@"hold"])
//    {
//        delegate2.holdingCheck=@"";
//        delegate2.orderToPort=@"ordercheck";
//
//            PortfolioView * port=[self.storyboard instantiateViewControllerWithIdentifier:@"port"];
//
//            [self presentViewController:port animated:YES completion:nil];
//    }
//
//    else if([delegate2.holdingCheck isEqualToString:@"position"])
//    {
//        delegate2.holdingCheck=@"";
//        delegate2.orderToPort=@"positionCheck";
//
//        PortfolioView * port=[self.storyboard instantiateViewControllerWithIdentifier:@"port"];
//
//        [self presentViewController:port animated:YES completion:nil];
//    }
    
   if([delegate2.modifyCheck isEqualToString:@"check"])
    {
        delegate2.modifyCheck=@"";
        
            TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
        
            [self presentViewController:tabPage animated:YES completion:nil];
    }
    
    else
    {
    
    delegate2.dismissCheck=@"dismissed";
   
   [self dismissViewControllerAnimated:true completion:^{
       
   }];
        
    }
//    StockOrderView * sov = [self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
//    
//    [sov.navigationController popViewControllerAnimated:YES];
//    if([delegate2.navigationCheck isEqualToString:@"Marketwatch"])
//    {
//        
//            StockOrderView * sov = [self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
//        
//        [sov navigationMethod];
//        
//    }
    
   
    
   
    
 
    
    
//    [self navigationController].viewControllers = [[self navigationController].viewControllers arrayByAddingObject:tabPage];
    
   
//

    
    
    
    
    
    
}

- (IBAction)searchAction:(id)sender {
}

//get all orders//

-(void)RetrieveAllOrders
{
    
     mixpanelCancelledArray=[[NSMutableArray alloc]init];
    mixpanelCompletedArray=[[NSMutableArray alloc]init];
    mixpanelPendingArray=[[NSMutableArray alloc]init];

    @try {
        [cancelledOrderIDArray removeAllObjects];
        [cancelledTradingSymArray removeAllObjects];
        [cancelledOrderTimeArray removeAllObjects];
        [cancelledExchangeArray removeAllObjects];
        [cancelledFilledArray removeAllObjects];
        [cancelledPendingArray removeAllObjects];
        [cancelledOrderTypeArray removeAllObjects];
        [cancelledTransactionTypeArray removeAllObjects];
        [cancelledPriceArray removeAllObjects];
        [cancelledTotalQtyArray removeAllObjects];
        
        [orderIDArray removeAllObjects];
        [tradingSymArray removeAllObjects];
        [orderTimeArray removeAllObjects];
        [exchangeArray removeAllObjects];
        [filledArray removeAllObjects];
        [pendingArray removeAllObjects];
        [orderTypeArray removeAllObjects];
        [transactionTypeArray removeAllObjects];
        [priceArray removeAllObjects];
        [totalQtyArray removeAllObjects];
        
        [ pendingOrderIDArray removeAllObjects];
        [ pendingTradingSymArray removeAllObjects];
        [ pendingOrderTimeArray removeAllObjects];
        [ pendingExchangeArray removeAllObjects];
        [ pendingFilledArray removeAllObjects];
        [ pendingPendingArray removeAllObjects];
        [ pendingOrderTypeArray removeAllObjects];
        [ pendingTransactionTypeArray removeAllObjects];
        [ pendingPriceArray removeAllObjects];
        [ pendingTotalQtyArray removeAllObjects];
        [pendingLabelArray removeAllObjects];
        [pendingFilledArrayy removeAllObjects];
        [instrumentTokenn removeAllObjects];
        [pendingMessageArray removeAllObjects];
        NSURL *url;
        
          NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
        NSDictionary * headers;

        NSURLSession *session = [NSURLSession sharedSession];
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
       url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/orders?api_key=xt2q1dpbwzu69n97&access_token=%@",delegate2.accessToken]];
            
        }
        
        else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/orders"]];
            
          headers  = @{ @"cache-control": @"no-cache",
                 @"Content-Type" : @"application/json",
                 @"authorization":access,
                 @"x-api-key":@"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1"
                 };
        }
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
   
        [request setHTTPMethod:@"GET"];
        if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
        [request setAllHTTPHeaderFields:headers];
        }
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(data!=nil)
            {
            
            NSDictionary *orderResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"orderDict----%@",orderResponse);
            
            orderArray = [orderResponse valueForKey:@"data"];
            
            orderArray=[[[orderArray reverseObjectEnumerator] allObjects] mutableCopy];
            
            for (int i = 0; i<[orderArray count]; i++)
            {
                NSString * status=[[orderArray objectAtIndex:i]valueForKey:@"status"];
                
                if([status isEqualToString:@"COMPLETE"]||[status isEqualToString:@"complete"])
                {
                    
                    
                    
                    
                    orderIdStr = [[orderArray objectAtIndex:i]valueForKey:@"order_id"];
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                       tradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"tradingsymbol"];
                          orderTimeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_timestamp"];
                          exchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                         fillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
                         pendingQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
                        orderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        transactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                    }
                    
                    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                        tradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"symbol"];
                        orderTimeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange_time"];;
                        
                        
                          exchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        
                        if([exchangeStr isEqualToString:@"NSE_EQ"])
                        {
                            exchangeStr=@"NSE";
                        }
                        
                        else if([exchangeStr isEqualToString:@"BSE_EQ"])
                        {
                            exchangeStr=@"BSE";
                        }
                        
                        else if([exchangeStr isEqualToString:@"NSE_FO"])
                        {
                            exchangeStr=@"NFO";
                        }
                        else if([exchangeStr isEqualToString:@"BSE_FO"])
                        {
                            exchangeStr=@"BFO";
                        }
                         fillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"traded_quantity"]stringValue];
                        pendingQtyStr = @"---";
                        
                        orderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        if([orderTypeStr isEqualToString:@"M"])
                        {
                            orderTypeStr=@"MARKET";
                        }
                        else if([orderTypeStr isEqualToString:@"L"])
                        {
                            orderTypeStr=@"LIMIT";
                        }
                        
                        else if([orderTypeStr isEqualToString:@"SL"])
                        {
                            orderTypeStr=@"SL";
                        }
                        
                        transactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        
                        if([transactionTypeStr isEqualToString:@"B"])
                        {
                            transactionTypeStr=@"BUY";
                        }
                        
                        else if([transactionTypeStr isEqualToString:@"S"])
                        {
                            transactionTypeStr=@"SELL";
                        }
                        
                        
                        
                    }
                    
                
                    priceStr = [[[orderArray objectAtIndex:i]valueForKey:@"price"]stringValue];
                    totalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                    
                    
                    [orderIDArray addObject:orderIdStr];
                    [tradingSymArray addObject:tradingSymStr];
                    [orderTimeArray addObject:orderTimeStr];
                    [exchangeArray addObject:exchangeStr];
                    [filledArray addObject:fillQtyStr];
                    [pendingArray addObject:pendingQtyStr];
                    [orderTypeArray addObject:orderTypeStr];
                    [transactionTypeArray addObject:transactionTypeStr];
                    [priceArray addObject:priceStr];
                    [totalQtyArray addObject:totalQtyStr];
                }
                
                else if([status isEqualToString:@"CANCELLED"]||[status isEqualToString:@"REJECTED"]||[status isEqualToString:@"cancelled"]||[status isEqualToString:@"rejected"])
                {
                    
                   
                   
                    cancelledOrderIdStr = [[orderArray objectAtIndex:i]valueForKey:@"order_id"];
                    
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                        cancelledTradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"tradingsymbol"];
                        cancelledOrderTimeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_timestamp"];
                        cancelledExchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        cancelledFillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
                        cancelledPendingQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
                        cancelledOrderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        cancelledTransactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        
                        pendingMessageStr = [[orderArray objectAtIndex:i]objectForKey:@"status_message"];
                        
                        NSLog(@"%@",[[orderArray objectAtIndex:i]objectForKey:@"status_message"]);
                    }
                    
                    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                        cancelledTradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"symbol"];
                        cancelledOrderTimeStr =[[orderArray objectAtIndex:i]valueForKey:@"exchange_time"];
                        
                       
                        cancelledExchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        
                        if([cancelledExchangeStr isEqualToString:@"NSE_EQ"])
                        {
                            cancelledExchangeStr=@"NSE";
                        }
                        
                        else if([cancelledExchangeStr isEqualToString:@"BSE_EQ"])
                        {
                            cancelledExchangeStr=@"BSE";
                        }
                        
                        else if([cancelledExchangeStr isEqualToString:@"NSE_FO"])
                        {
                            cancelledExchangeStr=@"NFO";
                        }
                        else if([cancelledExchangeStr isEqualToString:@"BSE_FO"])
                        {
                            cancelledExchangeStr=@"BFO";
                        }
                        cancelledFillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"traded_quantity"]stringValue];
                        cancelledPendingQtyStr = @"---";
                        
                        cancelledOrderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        if([cancelledOrderTypeStr isEqualToString:@"M"])
                        {
                            cancelledOrderTypeStr=@"MARKET";
                        }
                        else if([cancelledOrderTypeStr isEqualToString:@"L"])
                        {
                            cancelledOrderTypeStr=@"LIMIT";
                        }
                        
                        else if([cancelledOrderTypeStr isEqualToString:@"SL"])
                        {
                            cancelledOrderTypeStr=@"SL";
                        }
                        
                        cancelledTransactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        
                        if([cancelledTransactionTypeStr isEqualToString:@"B"])
                        {
                            cancelledTransactionTypeStr=@"BUY";
                        }
                        
                        else if([cancelledTransactionTypeStr isEqualToString:@"S"])
                        {
                            cancelledTransactionTypeStr=@"SELL";
                        }
                        
                        pendingMessageStr = [[orderArray objectAtIndex:i]objectForKey:@"message"];
                        
                        NSLog(@"%@",[[orderArray objectAtIndex:i]objectForKey:@"message"]);
                        
                    }
                    
                    
                    cancelledPriceStr = [[[orderArray objectAtIndex:i]valueForKey:@"price"]stringValue];
                    cancelledTotalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                  
                    
                    
                    
                    [cancelledOrderIDArray addObject:cancelledOrderIdStr];
                    [cancelledTradingSymArray addObject:cancelledTradingSymStr];
                    [cancelledOrderTimeArray addObject:cancelledOrderTimeStr];
                    [cancelledExchangeArray addObject:cancelledExchangeStr];
                    [cancelledFilledArray addObject:cancelledFillQtyStr];
                    [cancelledPendingArray addObject:cancelledPendingQtyStr];
                    [cancelledOrderTypeArray addObject:cancelledOrderTypeStr];
                    [cancelledTransactionTypeArray addObject:cancelledTransactionTypeStr];
                    [cancelledPriceArray addObject:cancelledPriceStr];
                    [cancelledTotalQtyArray addObject:cancelledTotalQtyStr];
                    [ pendingMessageArray addObject: pendingMessageStr];
                    
                   
                    
                   
                    
                        
                   
                    
                    
                  
                    
                    
                    
                   
                    
                }
                else if([status isEqualToString:@"OPEN"]||[status isEqualToString:@"open"])
                {
                    int filledint ;
                    int pending;
                    int instrument;
                    
                    pendingOrderIdStr = [[orderArray objectAtIndex:i]valueForKey:@"order_id"];
                   
                    
                    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                    {
                        pendingTradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"tradingsymbol"];
                        pendingOrderTimeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_timestamp"];
                        pendingExchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        pendingFillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"filled_quantity"]stringValue];
                        pendingPendingQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"pending_quantity"]stringValue];
                        pendingOrderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        pendingTransactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        pendingPriceStr = [[[orderArray objectAtIndex:i]valueForKey:@"price"]stringValue];
                        pendingTotalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                        NSString * filledString = [[orderArray objectAtIndex:i] valueForKey:@"filled_quantity"];
                        filledint = [filledString intValue];
                        NSString * pendingString = [[orderArray objectAtIndex:i] valueForKey:@"pending_quantity"];
                       pending  = [pendingString intValue];
                        
                        NSString * instrumentString = [[orderArray objectAtIndex:i] valueForKey:@"instrument_token"];
                         instrument = [instrumentString intValue];
                        
                        NSNumber *filledNumber = [NSNumber numberWithInt:filledint];
                        NSNumber *pendingNumber = [NSNumber numberWithInt:pending];
                        NSNumber *instrumentNumber = [NSNumber numberWithInt:instrument];
                        [pendingFilledArrayy addObject:filledNumber];
                        [pendingLabelArray addObject:pendingNumber];
                        [instrumentTokenn addObject:instrumentNumber];
                        
                        
                    }
                    
                    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                    {
                        pendingTradingSymStr = [[orderArray objectAtIndex:i]valueForKey:@"symbol"];
                        pendingOrderTimeStr =[[orderArray objectAtIndex:i]valueForKey:@"exchange_time"];
                        
                        
                        pendingExchangeStr = [[orderArray objectAtIndex:i]valueForKey:@"exchange"];
                        
                        if([pendingExchangeStr isEqualToString:@"NSE_EQ"])
                        {
                            pendingExchangeStr=@"NSE";
                        }
                        
                        else if([pendingExchangeStr isEqualToString:@"BSE_EQ"])
                        {
                            pendingExchangeStr=@"BSE";
                        }
                        
                        else if([pendingExchangeStr isEqualToString:@"NSE_FO"])
                        {
                            pendingExchangeStr=@"NFO";
                        }
                        else if([pendingExchangeStr isEqualToString:@"BSE_FO"])
                        {
                            pendingExchangeStr=@"BFO";
                        }
                        pendingFillQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"traded_quantity"]stringValue];
                        pendingPendingQtyStr = @"---";
                        
                        pendingOrderTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"order_type"];
                        
                        if([pendingOrderTypeStr isEqualToString:@"M"])
                        {
                            pendingOrderTypeStr=@"MARKET";
                        }
                        else if([pendingOrderTypeStr isEqualToString:@"L"])
                        {
                            pendingOrderTypeStr=@"LIMIT";
                        }
                        
                        else if([pendingOrderTypeStr isEqualToString:@"SL"])
                        {
                            pendingOrderTypeStr=@"SL";
                        }
                        
                        pendingTransactionTypeStr = [[orderArray objectAtIndex:i]valueForKey:@"transaction_type"];
                        
                        if([pendingTransactionTypeStr isEqualToString:@"B"])
                        {
                            pendingTransactionTypeStr=@"BUY";
                        }
                        
                        else if([pendingTransactionTypeStr isEqualToString:@"S"])
                        {
                            pendingTransactionTypeStr=@"SELL";
                        }
                        
                        NSString * instrumentString = [[orderArray objectAtIndex:i] valueForKey:@"token"];
                        instrument = [instrumentString intValue];
                        
                        NSString * filledString = [[orderArray objectAtIndex:i] valueForKey:@"traded_quantity"];
                        filledint = [filledString intValue];
                        NSString * pendingString = @"--";
                        pending  = [pendingString intValue];
                        
                        NSNumber *filledNumber = [NSNumber numberWithInt:filledint];
                        NSNumber *pendingNumber = [NSNumber numberWithInt:pending];
                        NSNumber *instrumentNumber = [NSNumber numberWithInt:instrument];
                        [pendingFilledArrayy addObject:filledNumber];
                        [pendingLabelArray addObject:pendingNumber];
                        [instrumentTokenn addObject:instrumentNumber];
                        
                    }
                    
                    
                     pendingPriceStr = [[[orderArray objectAtIndex:i]valueForKey:@"price"]stringValue];
                    
                    pendingTotalQtyStr = [[[orderArray objectAtIndex:i]valueForKey:@"quantity"]stringValue];
                    
                    
                    [ pendingOrderIDArray addObject: pendingOrderIdStr];
                    [ pendingTradingSymArray addObject: pendingTradingSymStr];
                    [ pendingOrderTimeArray addObject: pendingOrderTimeStr];
                    [ pendingExchangeArray addObject: pendingExchangeStr];
                    [ pendingFilledArray addObject: pendingFillQtyStr];
                    [ pendingPendingArray addObject: pendingPendingQtyStr];
                    [ pendingOrderTypeArray addObject: pendingOrderTypeStr];
                    [ pendingTransactionTypeArray addObject: pendingTransactionTypeStr];
                    [ pendingPriceArray addObject: pendingPriceStr];
                    [ pendingTotalQtyArray addObject: pendingTotalQtyStr];
                    
                    
                 
                    
                    
                }
                
            }
                
                for(int i=0;i<cancelledTradingSymArray.count;i++)
                {
                NSDictionary * tmp_Dict=@{@"Symbol":[cancelledTradingSymArray objectAtIndex:i],@"Qty":[cancelledTotalQtyArray objectAtIndex:i],@"Transaction time":[cancelledOrderTimeArray objectAtIndex:i],@"Transaction type":[cancelledTransactionTypeArray objectAtIndex:i],@"Exchange":[cancelledExchangeArray objectAtIndex:i],@"Price":[cancelledPriceArray objectAtIndex:i]};
                
                [mixpanelCancelledArray addObject:tmp_Dict];
                    
                }
                
                for(int i=0;i<pendingOrderIDArray.count;i++)
                {
                    NSDictionary * tmp_Dict=@{@"Symbol":[pendingTradingSymArray objectAtIndex:i],@"Qty":[pendingTotalQtyArray objectAtIndex:i],@"Transaction time":[pendingOrderTimeArray objectAtIndex:i],@"Transaction type":[pendingTransactionTypeArray objectAtIndex:i],@"Exchange":[pendingExchangeArray objectAtIndex:i],@"Price":[pendingPriceArray objectAtIndex:i]};
                    
                    [mixpanelPendingArray addObject:tmp_Dict];
                    
                }
                
              
                for(int i=0;i<orderIDArray.count;i++)
                {
                    NSDictionary * tmp_Dict=@{@"Symbol":[tradingSymArray objectAtIndex:i],@"Qty":[totalQtyArray objectAtIndex:i],@"Transaction time":[orderTimeArray objectAtIndex:i],@"Transaction type":[transactionTypeArray objectAtIndex:i],@"Exchange":[exchangeArray objectAtIndex:i],@"Price":[priceArray objectAtIndex:i]};
                    
                    [mixpanelCompletedArray addObject:tmp_Dict];
                    
                }
                
                
                Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
                
                [mixpanel1 identify:delegate2.userID];
                
                [mixpanel1.people set:@{@"MarketCancelledOrders":mixpanelCancelledArray}];
                
                [mixpanel1.people set:@{@"MarketCompletedOrders":mixpanelCompletedArray}];
                [mixpanel1.people set:@{@"MarketPendingOrders":mixpanelPendingArray}];
            
            NSLog(@"cance; %@",pendingMessageArray);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.pendingOrdersTableView.hidden=NO;
                self.activityInd.hidden=YES;
                [self.activityInd stopAnimating];
                
                self.pendingOrdersTableView.delegate=self;
                self.pendingOrdersTableView.dataSource=self;
                
                [self.pendingOrdersTableView reloadData];
                
            });
            }
            
        }];
        
        [postDataTask resume];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
   
    
   
    
    
}


//edit action//
-(void)editAction:(UIButton*)sender
{
    delegate2.editOrderBool=true;
    delegate2.modifyCheck=@"check";


    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.pendingOrdersTableView];



    NSIndexPath *indexPath = [self.pendingOrdersTableView indexPathForRowAtPoint:buttonPosition];
    PendingOrdersCell *cell = [self.pendingOrdersTableView cellForRowAtIndexPath:indexPath];

    @try {

        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
        delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[instrumentTokenn objectAtIndex:indexPath.row]];

        delegate2.symbolDepthStr=cell.tradingSymLbl.text;

        //    FollowCell *cell = [self.followerTableView cellForRowAtIndexPath:indexPath];
        delegate2.orderSegment=cell.exchangeLbl.text;

        NSLog(@"%@",delegate2.orderSegment);

        delegate2.orderID=[NSString stringWithFormat:@"%@",[pendingOrderIDArray objectAtIndex:indexPath.row]];

        delegate2.qtyStr=[NSString stringWithFormat:@"%@",[pendingTotalQtyArray objectAtIndex:indexPath.row]];

        NSLog(@"%@",delegate2.qtyStr);

        delegate2.transaction=[NSString stringWithFormat:@"%@",[pendingTransactionTypeArray objectAtIndex:indexPath.row]];

        delegate2.editPrice=[NSString stringWithFormat:@"%@",[pendingPriceArray objectAtIndex:indexPath.row]];


        StockOrderView * sov = [self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
        [self presentViewController:sov animated:YES completion:nil];
        }

        else
        {
            delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"stSecurityID"]];

            delegate2.modifyATINORDERNO=[[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"ATINOrderId"]intValue];


            delegate2.symbolDepthStr=cell.tradingSymLbl.text;
            delegate2.orderSegment=[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"stExchange"];

            delegate2.qtyStr=[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"inQty"];

            int btside=[[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"btSide"] intValue];

            NSString * string;
            if(btside==1)
            {
                string=@"BUY";
            }

            else if(btside==2)
            {
                string=@"SELL";
            }

            delegate2.transaction=string;

            delegate2.editPrice=[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"dbPrice"];

            StockOrderView * sov = [self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            [self presentViewController:sov animated:YES completion:nil];


        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
   
}

//cancel action//

-(void)cancelAction:(UIButton*)sender
{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Cancel Order"
                                 message:@"Do you want to cancel the order"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    
                                    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.pendingOrdersTableView];
                                    
                                    
                                    
                                    NSIndexPath *indexPath = [self.pendingOrdersTableView indexPathForRowAtPoint:buttonPosition];
//                                    PendingOrdersCell *cell = [self.pendingOrdersTableView cellForRowAtIndexPath:indexPath];
                                    
                                    @try {
                                        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                        {
                                        
                                        NSString * orderId=[pendingOrderIDArray objectAtIndex:indexPath.row];
                                            
                                            NSDictionary *headers;
                                            NSString * urlString;
                                            
                                            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                            {
                                                headers = @{ @"cache-control": @"no-  cache",
                                                           };
                                                
                                                  urlString=[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/%@?api_key=%@&access_token=%@",orderId,delegate2.APIKey,delegate2.accessToken];
                                            }
                                            
                                            else if ([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                            {
                                                 NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
                                                headers = @{ @"cache-control": @"no-cache",
                                                             @"Content-Type" : @"application/json",
                                                             @"authorization":access,
                                                             @"x-api-key":@"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1"
                                                             };
                                                
                                                 urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/orders/%@",orderId];
                                            }
                                        
                                       
                                        
                                      
                                        
                                        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                                           timeoutInterval:10.0];
                                        [request setHTTPMethod:@"DELETE"];
                                        [request setAllHTTPHeaderFields:headers];
                                        
                                        NSURLSession *session = [NSURLSession sharedSession];
                                        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                                        if (error) {
                                                                                            NSLog(@"%@", error);
                                                                                        } else {
                                                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                                            NSLog(@"%@", httpResponse);
                                                                                            
                                                                                            NSMutableDictionary *orderDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                                            
                                                                                            NSLog(@"%@",
                                                                                                  orderDict);
                                                                                            
                                                                                            
                                                                                        }
                                                                                        
                                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                                            
                                                                                            [self RetrieveAllOrders];
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                        });
                                                                                        
                                                                                        
                                                                                    }];
                                        [dataTask resume];

                                        }
                                        
                                       
                                        
                                        else
                                        {
                                            reload=true;
                                            int ATIONORDERNO=[[[delegate2.mtPendingArray objectAtIndex:indexPath.row] objectForKey:@"ATINOrderId"] intValue];
                                            
                                            TagEncode * tag = [[TagEncode alloc]init];
                                            delegate2.mtCheck=true;
                                            [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.cancelOrder];
                                            [tag TagData:sharedManager.inATINOrderNo intMethod:ATIONORDERNO];
                                            [tag GetBuffer];
                                            [self newMessage];
                                            
                                            [segmentedControl setSelectedSegmentIndex:0];
                                            
                                            
                                            
                                            [self segmentedControlChangedValue];
                                            
                                        }
                                    }
                                    @catch (NSException * e) {
                                        NSLog(@"Exception: %@", e);
                                    }
                                    @finally {
                                        NSLog(@"finally");
                                    }
                                                                   }
                                
                                
                                ];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==2)
    {
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
    if(pendingMessageArray.count>0)
    {
    NSString * messageStr=[NSString stringWithFormat:@"%@",[pendingMessageArray objectAtIndex:indexPath.row]];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Message"
                                 message:messageStr
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    
                                 
                                }];
    
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
        
    }
            
        }
        
        else
        {
            
            NSString * messageStr=[NSString stringWithFormat:@"%@",[[delegate2.mtCancelArray objectAtIndex:indexPath.row] objectForKey:@"stErrorText"]];
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Message"
                                         message:messageStr
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Ok"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            
                                            
                                        }];
            
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }

}

//reachability//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}

//mt//

-(void)newMessage
{
    
    
    
    //    [delegate2.outputStream setDelegate:self];
    Byte UUID[delegate2.btOutBuffer.count];
    
    for (int i = 0; i < delegate2.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate2.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp1;
        [scanner scanHexInt:&temp1];
        NSLog(@"%d",temp1);
        
        UUID[i]=temp1;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate2.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate2.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate2.outputStream write:bytes maxLength:delegate2.btOutBuffer.count];
    
}

- (void)showMainMenu:(NSNotification *)note
{
    Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
    
    [mixpanel1 identify:delegate2.userID];
    
   
    self.pendingOrdersTableView.hidden=NO;
    self.activityInd.hidden=YES;
    [self.activityInd stopAnimating];
    
    for(int i=0;i<delegate2.allOrderHistory.count;i++)
    {
        NSString * string=[[delegate2.allOrderHistory objectAtIndex:i] objectForKey:@"btOrderStatus"];
        
        int status=[string intValue];
    
        if(status==4||status==3)
        {
            [delegate2.mtCancelArray addObject:[delegate2.allOrderHistory objectAtIndex:i]];
            [securityTokenArray addObject:[NSString stringWithFormat:@"%@",[[delegate2.allOrderHistory objectAtIndex:i] objectForKey:@"stSecurityID"]]];
             [mtExchangeArray addObject:[NSString stringWithFormat:@"%@",[[delegate2.allOrderHistory objectAtIndex:i] objectForKey:@"stExchange"]]];
            
            [mixpanel1.people set:@{@"MarketCancelledOrders":delegate2.mtCancelArray}];
            
           
        
            [delegate2.allOrderHistory removeObjectAtIndex:i];
        }
        
//        else if(status==0)
//        {
//
//            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Unknown Response" preferredStyle:UIAlertControllerStyleAlert];
//
//            [self presentViewController:alert animated:YES completion:^{
//
//            }];
//
//            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//            }];
//
//            [alert addAction:okAction];
//
//        }
        
       else  if(status==1)
        {
            
            [delegate2.mtPendingArray addObject:[delegate2.allOrderHistory objectAtIndex:i]];
            
            NSArray * userArray=[delegate2.mtPendingArray mutableCopy];
            NSSet *set = [NSSet setWithArray:userArray];
            NSArray *uniqueArray = [set allObjects];
            delegate2.mtPendingArray =[uniqueArray mutableCopy];
             [securityTokenArray addObject:[NSString stringWithFormat:@"%@",[[delegate2.allOrderHistory objectAtIndex:i] objectForKey:@"stSecurityID"]]];
            
          
            [mixpanel1.people set:@{@"MarketPendingOrders":delegate2.mtPendingArray}];
            [delegate2.allOrderHistory removeObjectAtIndex:i];
        }
        
       else  if(status==2)
        {
            [delegate2.mtCompletedArray addObject:[delegate2.allOrderHistory objectAtIndex:i]];
             [securityTokenArray addObject:[NSString stringWithFormat:@"%@",[[delegate2.allOrderHistory objectAtIndex:i] objectForKey:@"stSecurityID"]]];
              [mixpanel1.people set:@{@"MarketCompletedOrders":delegate2.mtCompletedArray}];
            
            
            [delegate2.allOrderHistory removeObjectAtIndex:i];
        }
    }
    
//    [delegate2.allOrderHistory removeAllObjects];
    
    

            [self symbolMethod];

    
//
    
    
    
    
//         NSLog(@"Received Notification - Someone seems to have logged in");
}


-(void)symbolMethod
{
    if(securityTokenArray.count>0)
    {
    NSArray * userArray=[securityTokenArray mutableCopy];
    NSSet *set = [NSSet setWithArray:userArray];
    NSArray *uniqueArray = [set allObjects];
        
        NSArray * userArray1=[mtExchangeArray mutableCopy];
        NSSet *set1 = [NSSet setWithArray:userArray1];
        NSArray *uniqueArray1 = [set1 allObjects];
        
       
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               };
    NSDictionary *parameters = @{ @"exchangetokens":uniqueArray,
                                  @"exchangevalue":uniqueArray1
                                  };

    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@stock/tokens",delegate2.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);

                                             symbolResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                                                        for(int i=0;i<symbolResponse.count;i++)
                                                        {
                                                            [newSymbolResponseArray addObject:[symbolResponse objectAtIndex:i]];

                                                        }


                                                        NSArray * symbols=[newSymbolResponseArray mutableCopy];
                                                        NSSet *set = [NSSet setWithArray:symbols];
                                                       delegate2.scripsMt = [set allObjects];

                                                    }

                                                    }

                                                    dispatch_async(dispatch_get_main_queue(), ^{

if(reload==true)
{
  
    reload=false;                                               self.pendingOrdersTableView.delegate=self;
                                                        self.pendingOrdersTableView.dataSource=self;

                                                        [self.pendingOrdersTableView reloadData];
    
    
}

                                                    });

                                                }];
    [dataTask resume];

    }



}

-(void)reloadData
{
    
    self.pendingOrdersTableView.delegate=self;
    self.pendingOrdersTableView.dataSource=self;
    
    [self.pendingOrdersTableView reloadData];

    
}

@end
