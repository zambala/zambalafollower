//
//  MarketTableViewCell.m
//  testing
//
//  Created by zenwise technologies on 18/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "MarketTableViewCell.h"

@implementation MarketTableViewCell

@synthesize timeLbl, priceLbl, percentLbl, companyNameLbl;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
