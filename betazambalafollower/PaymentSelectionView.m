//
//  PaymentSelectionView.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/20/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "PaymentSelectionView.h"
#import "ViewController.h"
#import "AppDelegate.h"
@import Tune;

@interface PaymentSelectionView ()
{
    AppDelegate * delegate1;
    NSArray * allKeys;
    NSMutableArray * eventItem;
}

@end

@implementation PaymentSelectionView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString * string=[NSString stringWithFormat:@"₹%@",delegate1.amountForPayment];
    
    self.grandTotal.text=string;
    self.amountPayable.text=string;
    
    self.navigationItem.title = @"Make Payment";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    

    
    [self.cardBtn addTarget:self action:@selector(cardAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.redeemSelectionBtn addTarget:self action:@selector(redeemAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.redeemSelectionBtn setImage:[UIImage imageNamed:@"checkUnselected"] forState:UIControlStateNormal];
    
    [self.redeemSelectionBtn setImage:[UIImage imageNamed:@"checkSelected"] forState:UIControlStateSelected];
    
    self.redeemSelectionBtn.selected=NO;
    
    
    
    self.redeemView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.redeemView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.redeemView.layer.shadowOpacity = 1.0f;
    self.redeemView.layer.shadowRadius = 1.0f;
    self.redeemView.layer.cornerRadius=2.1f;
    self.redeemView.layer.masksToBounds = NO;
    
    self.amountView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.amountView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.amountView.layer.shadowOpacity = 1.0f;
    self.amountView.layer.shadowRadius = 1.0f;
    self.amountView.layer.cornerRadius=6.2f;
    self.amountView.layer.masksToBounds = NO;


    
    
    
    [self.cardBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    
    [self.cardBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self cardAction];
    
    [self.netBankingBtn addTarget:self action:@selector(netBankingAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.netBankingBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    
    [self.netBankingBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.upiBtn addTarget:self action:@selector(upiAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.upiBtn setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    
    [self.upiBtn setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.makePaymentBtn addTarget:self action:@selector(paymentAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Do any additional setup after loading the view.
}

-(void)redeemAction
{
    if(self.redeemSelectionBtn.selected==NO)
    {
        self.redeemSelectionBtn.selected=YES;
    }
    
    else if(self.redeemSelectionBtn.selected==YES)
    {
        self.redeemSelectionBtn.selected=NO;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)cardAction
{
    self.cardBtn.selected=YES;
    self.netBankingBtn.selected=NO;
    self.upiBtn.selected=NO;
    delegate1.paymentString=@"CARD";
     self.selectionViewHgt.constant=145;
    self.enterVPALbl.hidden=YES;
    self.upiInputTxt.hidden=YES;
    self.brokerUpi.hidden=YES;
    self.targetVPALbl.hidden=YES;
    
    
}

-(void)netBankingAction
{
    self.cardBtn.selected=NO;
    self.netBankingBtn.selected=YES;
    self.upiBtn.selected=NO;
    delegate1.paymentString=@"NET";
     self.selectionViewHgt.constant=145;
    self.enterVPALbl.hidden=YES;
    self.upiInputTxt.hidden=YES;
    self.brokerUpi.hidden=YES;
    self.targetVPALbl.hidden=YES;
}


-(void)upiAction
{
    self.cardBtn.selected=NO;
    self.netBankingBtn.selected=NO;
    self.upiBtn.selected=YES;
    delegate1.paymentString=@"UPI";
//    self.selectionViewHgt.constant=270;
    self.enterVPALbl.hidden=YES;
    self.upiInputTxt.hidden=YES;
    self.brokerUpi.hidden=YES;
    self.targetVPALbl.hidden=YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)paymentAction
{
    eventItem=[[NSMutableArray alloc]init];
    allKeys=[[NSArray alloc]init];
   
    allKeys=[delegate1.cartDict allKeys];
    
    
    for(int i=0;i<allKeys.count;i++)
    {
        NSString * name=[NSString stringWithFormat:@"%@",[allKeys objectAtIndex:i]];
        
        for(int j=0;j<[[delegate1.cartDict objectForKey:name] count];j++)
        {
            TuneEventItem * item = [TuneEventItem eventItemWithName:[[[delegate1.cartDict objectForKey:name] objectAtIndex:j] objectForKey:@"offername"] unitPrice:[[[[delegate1.cartDict objectForKey:name] objectAtIndex:j] objectForKey:@"amount"] floatValue] quantity:1 revenue:0.00 attribute1:[allKeys objectAtIndex:i] attribute2:nil attribute3:nil attribute4:nil attribute5:nil];
            [eventItem addObject:item];

        }
   
        
    }
    
    if(delegate1.emailId.length>0)
    {
        [Tune setUserEmail:delegate1.emailId];
    }
    if(delegate1.userName.length>0)
    {
        [Tune setUserName:delegate1.userName];
    }
    
    [Tune setUserId:delegate1.userID];

    
    
   
    
    TuneLocation *loc = [TuneLocation new];
    loc.latitude = @(9.142276);
    loc.longitude = @(-79.724052);
    loc.altitude = @(15.);
    [Tune setLocation:loc];
    
    TuneEvent *event = [TuneEvent eventWithName:TUNE_EVENT_ADD_TO_CART];
    event.eventItems = eventItem;
    event.revenue = 0.00;
    event.currencyCode = @"INR";
    
    [Tune measureEvent:event];
    

    
    if(delegate1.paymentString.length>0)
    {
        if([delegate1.paymentString isEqualToString:@"UPI"])
        {
            
            
                delegate1.UPIDetails=self.upiInputTxt.text;
                NSString *newStr = [self.amountPayable.text substringFromIndex:1];
                delegate1.amountAfterRedeem=newStr;
                ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
                
                [self.navigationController pushViewController:view animated:YES];
            
            
//            else
//            {
//                
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please fill the empty fields" preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
//                    // Ok action example
//                }];
//                [alert addAction:okAction];
//                
//                [self presentViewController:alert animated:YES completion:nil];
// 
//            }
            
        }
        
        else
        {
            NSString *newStr = [self.amountPayable.text substringFromIndex:1];
            delegate1.amountAfterRedeem=newStr;
            ViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"view1"];
            
            [self.navigationController pushViewController:view animated:YES];
            
        }
        
    }
}

@end
