//
//  ViewController.m
//  Payment
//
//  Created by zenwise mac 2 on 7/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "ViewController.h"
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>
#import "AppDelegate.h"
#import "TabBar.h"
#import <Mixpanel/Mixpanel.h>

@interface ViewController ()
{
    
    NSString * merchantID;
    NSString * merchantTxnNo;
    NSString * amount;
    NSString * currencyCode;
    NSString * customerEmailId;
    NSString * transactionType;
    NSString * txnDate;
    NSString * customerMobileNo;
    NSString* newStr;
    NSString * payType;
    NSString * paymentMode;
   
    
    NSString *cardNo;
    NSString *cardExpiry;
    NSString *cvv;
    NSString *nameOnCard;
    AppDelegate * delegate1;
    NSMutableString *HMAC;
    NSString * customerUPIAlias;
    NSString * returnUrl;
    NSString * aggregatorId;
    NSMutableString *randomString;
    NSDictionary * secureHashDict;
    NSString * secureHash;
    NSMutableURLRequest * request;
    NSMutableData * postData1;
    NSMutableData *phiPostData;
    NSDictionary *parameters;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view, typically from a nib.
    
    //SHA
    
    [self webLoad];
    
    

   
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    [self webLoad];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    //self.url = self.webView.request.mainDocumentURL;
    
//    https://qa.phicommerce.com/pg/api/pgresponse
//    https://qa.phicommerce.com/pg/api/merchant
    
    self.myString=[[NSString alloc]init];
    
    self.myString = self.webView.request.URL.absoluteString;
    
    NSError *error;
    
    if([self.myString isEqualToString:@"https://qa.phicommerce.com/pg/api/merchant"])
    {
        NSURL * url=[NSURL URLWithString:self.myString];
        NSString *content = [NSString stringWithContentsOfURL:url
                                                        encoding:NSASCIIStringEncoding
                                                           error:&error];
        
        NSString *html = [self.webView stringByEvaluatingJavaScriptFromString:
                          @"document.body.innerHTML"];
        
        
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Cart Checked Out"
             properties:@{ @"Order Status":@"Success"}];
        
       
        
        [mixpanel identify:delegate1.userID];
        
        [mixpanel.people set:@{@"Premium Leaders":delegate1.premiumLeaders}];
        
        
        
        TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
        
        [self presentViewController:tabPage animated:YES completion:nil];
    }
    
}

-(void)webLoad
{
    self.webView.delegate=self;
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    int len=6;
    
    randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    
    merchantID=@"T_00030";
    merchantTxnNo=randomString;
    amount=delegate1.amountAfterRedeem;
    currencyCode=@"356";
    customerEmailId=delegate1.emailId;
    transactionType=@"SALE";
    txnDate=@"20170717101010";
    customerMobileNo=delegate1.mobileNumber;
    payType=@"0";
    
    cardNo=@"4111111111111111";
    cardExpiry=@"202207";
    cvv=@"123";
    nameOnCard=@"test";
    customerUPIAlias=delegate1.UPIDetails;
    returnUrl=[NSString stringWithFormat:@"%@",delegate1.baseUrl];
    aggregatorId=@"J_00002";
    
    
    if([delegate1.paymentString isEqualToString:@"CARD"])
    {
       paymentMode=@"CARD";
        
        parameters = @{@"addlParam1":paymentMode,
                       @"aggregatorId":aggregatorId,
                       @"amount":amount,
                       @"currencyCode": @356,
                       @"customerEmailId":customerEmailId,
                       @"customerMobileNo":customerMobileNo,
                       @"merchantID": @"T_00030",
                       @"merchantTxnNo":randomString,
                       @"payType": @"0",
                       @"paymentMode":paymentMode,
                       @"returnUrl": @"http://52.66.159.178:8013/api/phicommerce/app/get/ptgResponse",
                       @"transactionType": @"SALE",
                       @"txnDate": @"20111108011101"
                       };
        [self serverHit];
        
       
        


    }
    
    else if([delegate1.paymentString isEqualToString:@"UPI"]){
        paymentMode=@"UPI";
        
        parameters = @{@"addlParam1":paymentMode,
                       @"aggregatorId":aggregatorId,
                       @"amount":amount,
                       @"currencyCode": @356,
                       @"customerEmailId":customerEmailId,
                       @"customerMobileNo":customerMobileNo,
                       @"customerUPIAlias":@"railwaytest@icici",
                       @"merchantID": @"T_00030",
                       @"merchantTxnNo":randomString,
                       @"payType": @"0",
                       @"paymentMode":paymentMode,
                       @"returnUrl": @"http://52.66.159.178:8013/api/phicommerce/app/get/ptgResponse",
                       @"transactionType": @"SALE",
                       @"txnDate": @"20111108011101"
                       };
        [self serverHit];
    }
    
    else if([delegate1.paymentString isEqualToString:@"NET"]){
        paymentMode=@"NB";
        parameters = @{@"addlParam1":paymentMode,
                       @"aggregatorId":aggregatorId,
                       @"amount":amount,
                       @"currencyCode": @356,
                       @"customerEmailId":customerEmailId,
                       @"customerMobileNo":customerMobileNo,
                       @"merchantID": @"T_00030",
                       @"merchantTxnNo":randomString,
                       @"payType": @"0",
                       @"paymentMode":paymentMode,
                       @"returnUrl": @"http://52.66.159.178:8013/api/phicommerce/app/get/ptgResponse",
                       @"transactionType": @"SALE",
                       @"txnDate": @"20111108011101"
                       };
        [self serverHit];
        
        
    }
}


-(void)serverHit

{
   

    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                             };
//    parameters = @{@"addlParam1":paymentMode,
//                                 @"aggregatorId":aggregatorId,
//                                  @"amount":amount,
//                                  @"currencyCode": @356,
//                                  @"customerEmailId":customerEmailId,
//                                  @"customerMobileNo":customerMobileNo,
//                                  @"merchantID": @"T_00030",
//                                  @"merchantTxnNo":randomString,
//                                  @"payType": @"0",
//                                  @"paymentMode":paymentMode,
//                                  @"returnUrl": @"http://52.66.159.178:8013/api/phicommerce/app/get/ptgResponse",
//                                  @"transactionType": @"SALE",
//                                  @"txnDate": @"20111108011101"
//                                  };
    
   
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString * urlStr=[NSString stringWithFormat:@"http://52.66.159.178:8013/api/clienttrade/phicommerce/gethash"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                         secureHashDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"Server hash:%@",secureHashDict);
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        secureHash=[secureHashDict objectForKey:@"hash"];
                                                        
                                                       
                                                        if(secureHash.length>0)
                                                        {
                                                               NSString * hash=[NSString stringWithFormat:@"&secureHash=%@",secureHash];
                                                             NSString * addParam=[NSString stringWithFormat:@"addlParam1=%@",paymentMode];
                                                            NSString * merchantTx=[NSString stringWithFormat:@"&merchantTxnNo=%@",randomString];
                                                            NSString * amountStr=[NSString stringWithFormat:@"&amount=%@",amount];
                                                            NSString * aggregator=[NSString stringWithFormat:@"&aggregatorID=%@",aggregatorId];
                                                            
                                                            NSString * paymentmode1=[NSString stringWithFormat:@"&paymentMode=%@",paymentMode];
                                                            
                                                               phiPostData = [[NSMutableData alloc] initWithData:[addParam dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                         //  phiPostData = [[NSMutableData alloc] initWithData:[aggregator dataUsingEncoding:NSUTF8StringEncoding]];
                                                            [phiPostData appendData:[aggregator dataUsingEncoding:NSUTF8StringEncoding]];
                                                          
                                                            
                                                            [phiPostData appendData:[amountStr dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            [phiPostData appendData:[@"&currencyCode=356" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            NSString * email=[NSString stringWithFormat:@"&customerEmailId=%@",customerEmailId];
                                                            [phiPostData appendData:[email dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                           
                                                            
                                                            NSString * mobile=[NSString stringWithFormat:@"&customerMobileNo=%@",customerMobileNo];
                                                            [phiPostData appendData:[mobile dataUsingEncoding:NSUTF8StringEncoding]];
                                                            if([delegate1.paymentString isEqualToString:@"UPI"])
                                                            {
                                                                NSString * VPA = [NSString stringWithFormat:@"&customerUPIAlias=%@",@"railwaytest@icici"];
                                                                [phiPostData appendData:[VPA dataUsingEncoding:NSUTF8StringEncoding]];
                                                            }
                                                            
                                                            [phiPostData appendData:[@"&merchantID=T_00030" dataUsingEncoding:NSUTF8StringEncoding]];
                                                    [phiPostData appendData:[merchantTx dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            [phiPostData appendData:[@"&payType=0" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            [phiPostData appendData:[paymentmode1 dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            NSString * returnUrl1=[NSString stringWithFormat:@"&returnUrl=http://52.66.159.178:8013/api/phicommerce/app/get/ptgResponse"];
                                                            [phiPostData appendData:[returnUrl1 dataUsingEncoding:NSUTF8StringEncoding]];
                                          
                                                            [phiPostData appendData:[@"&transactionType=SALE" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            [phiPostData appendData:[@"&txnDate=20111108011101" dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            [phiPostData appendData:[hash dataUsingEncoding:NSUTF8StringEncoding]];
                                                            
                                                            NSString * str = [[NSString alloc]initWithData:phiPostData encoding:NSASCIIStringEncoding];
                                                            NSLog(@"Final String%@",str);
                                                            [self phiCommerceServer];
                                           
                                                        }
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    
    
    
    
    
}

-(void)phiCommerceServer
{
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://qa.phicommerce.com/pg/api/sale"]];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: phiPostData];
    [_webView loadRequest: request];

}



@end
