//
//  ReferralView.m
//  testing
//
//  Created by zenwise technologies on 24/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "ReferralView.h"
#import "GuestViewController.h"
#import "AppDelegate.h"

@interface ReferralView ()
{
    AppDelegate * delegate1;
}

@end

@implementation ReferralView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
//    [self outFocusTextField];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard
{
    [self.referralTxtField resignFirstResponder];
}


//-(void)inFocusTextField
//{
//    self.referralTxtField.leftViewMode = UITextFieldViewModeAlways;
//    self.referralTxtField.font=[UIFont fontWithName:@"OpenSans" size:13];
//    self.referralTxtField.textColor = [UIColor blackColor];
//    self.referralTxtField.backgroundColor = [UIColor clearColor];
//    
//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0.0f, self.referralTxtField.frame.size.height - 1, self.referralTxtField.frame.size.width, 1.0f);
//    bottomBorder.backgroundColor = [UIColor darkGrayColor].CGColor;
//    [self.referralTxtField.layer addSublayer:bottomBorder];
//}
//
//-(void)outFocusTextField
//{
//    self.referralTxtField.leftViewMode = UITextFieldViewModeAlways;
//    self.referralTxtField.font=[UIFont fontWithName:@"OpenSans" size:13];
//    self.referralTxtField.textColor = [UIColor blackColor];
//    self.referralTxtField.backgroundColor = [UIColor clearColor];
//    
//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.frame = CGRectMake(0.0f, self.referralTxtField.frame.size.height - 1, self.referralTxtField.frame.size.width, 1.0f);
//    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
//    [self.referralTxtField.layer addSublayer:bottomBorder];
//}
//
//
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//
//{
//    [self inFocusTextField];
//    return YES;
//}
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [self outFocusTextField];
//    return YES;
//}
//


- (IBAction)continueAction:(id)sender {
    
    if(self.referralTxtField.text.length>0)
    {
        delegate1.referralString=self.referralTxtField.text;
        GuestViewController * guest=[self.storyboard instantiateViewControllerWithIdentifier:@"GuestViewController"];
        [self.navigationController pushViewController:guest animated:YES];
    }
    
    else{
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter the referral code to continue" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        

    }
}
@end
