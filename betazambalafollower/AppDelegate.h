//
//  AppDelegate.h
//  testing
//
//  Created by zenwise mac 2 on 11/14/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@property NSMutableArray * urlArray;;
@property NSString * requestToken;
@property NSString * APIKey;
@property NSString * secret;
@property NSMutableDictionary * dict;
@property NSDictionary * dict1;
@property NSString * userID;
@property NSString * publicToken;
@property NSString * accessToken;
@property NSString *lastPriceStr;
@property NSString *lastTimeStr;
@property NSString *changePercentageStr;
@property NSMutableArray * companyArray;
@property NSMutableArray * companyNameArray;
@property NSMutableArray * lastPriceArray;
@property NSMutableArray * percentageChangeArray;
@property NSMutableArray * timeArray;
@property NSMutableDictionary * NIFTYdict;
@property NSMutableDictionary * dict123;
@property NSString * message;
@property UINavigationController * navigationController;


@property NSMutableDictionary * SENSEXdict;

@property NSString * NIFTYLastPrice;
@property NSString * NIFTYChangePercentage;
@property NSString * SENSEXLastPrice;
@property NSString * SENSEXChangePercentage;

@property NSString * openString;
@property NSString * highString;
@property NSString * lowString;
@property NSString * changeString;
@property NSString * volumeString;
@property NSString * instrumentDepthStr;


@property UITabBarController * tabBarController;

@property NSMutableDictionary * cashDic;
@property NSString * closeStr;
@property NSString * bidSizeStr;
@property NSString * askSizeStr;
@property NSMutableArray * instrumentNameArr;
@property NSMutableArray * instrumentToken;
@property BOOL launchCheck;

@property NSMutableArray * imageArray;
@property NSMutableArray * filteredCompanyName;
@property NSString * symbolDepthStr;
@property NSString * exchange;
@property BOOL editOrderBool;
//clientinfo//
@property NSString * referralString;
@property NSString * numberStr;
@property NSMutableString *bankBranchString,*ifscCodeString,*bankNameString,*mcirString,*accountNumberString;
@property NSString * referralCode;
@property NSMutableDictionary * searchSymbolDict;
@property NSString * exchaneLblStr;

@property NSString * orderBuySell;
@property NSString * orderinstrument;
@property NSString * orderSegment;
@property BOOL firstTime;
@property BOOL orderBool;
@property BOOL tradeBtnBool;
@property NSString * orderID;
@property NSString * LTPStr;
@property NSString * qtyStr;
@property NSString * transaction;
@property NSString * editPrice;
@property NSMutableArray * leaderDetailArray;
@property NSMutableArray * optionOrderArray;
@property NSMutableArray * optionOrderTransactionArray;
@property NSString * lotSize;
@property NSString * depth;
@property BOOL test;
@property BOOL test1;
@property NSString * holdingsQty;
@property BOOL  portfolioBackBool;
@property NSString *buttonTitle;
@property NSString * orderStatusCheck;
@property NSMutableArray * depthLotSize;
@property NSMutableArray * sampleDepthLot;
@property NSMutableArray * optionLimitPriceArray;
@property NSMutableArray * leaderAdviceDetails;
@property NSString *phoneNumberSessionID;
@property NSString * orderStr;
@property NSMutableArray * optionSymbolArray;
@property NSString * brokerNameStr;
@property NSString * userName;
@property NSString * mainViewBrokerStr;
@property NSString * mainTickerStr;
@property NSMutableDictionary * filterResponseDictionary;
@property NSString * filterCheckString;
@property NSMutableArray * filterMonksArray;
//monks filter//
@property NSString * segment3;
@property NSString * duration3;
@property NSString * rating;
@property NSMutableArray * homeInstrumentToken;
@property NSMutableArray * homeinstrumentName;
@property NSString * homeExchange;
@property NSMutableArray * homeLotDepth;
@property NSMutableArray * homeFilterCompanyName;

@property NSInteger anIndex;
@property NSInteger anIndex1;
@property NSInteger anIndex2;
@property NSInteger anIndex3;
@property NSString * portFolioIDString;
@property BOOL wisdomCheck;
@property NSString * leaderIDWhoToFollow;
@property NSString * leaderIDFollowing;
@property NSString * tradingSecurityDes;
@property NSString * navigationCheck;
@property NSString * dismissCheck;
@property NSString * holdingCheck;
@property NSString * orderToPort;
@property NSString * logoutCheck;
@property NSString * clientPhoneNumber;
@property NSString * loginActivityStr;
@property NSString * protFolioUserID;
@property NSString * wisdomGardemTickIDString;
@property NSString * adviceSecurityId;
@property NSString * wisdomDuration;
@property NSString * wisdomBuySell;
@property BOOL wisdomTopAdvices;
@property BOOL wisdomBool;
@property NSString * modifyCheck;
@property NSString * profileImg;
@property NSString * fincode;
@property NSMutableArray * btnCountArray;
@property NSMutableArray * selectLeadersID;
@property NSMutableDictionary * detailNewsDict;
@property NSMutableArray * selectLeadersName;
@property BOOL marketmonksHint;
@property BOOL wisdomHint;
@property BOOL stockHint;
@property BOOL marketwatchHinT;
@property BOOL feedsHint;
@property BOOL stream;
@property NSString * subscriptionId;
@property NSString * leaderid;
@property NSMutableDictionary * cartDict;
@property NSMutableArray * cartArray;
@property NSString * paymentString;
@property NSString * amountForPayment;
@property NSString * userName1;
@property NSString * emailId;
@property NSString * mobileNumber;
@property NSString * amountAfterRedeem;
@property NSString * cartLeaderName;
@property NSString * UPIDetails;
@property NSString * profileTOFeeds;
@property BOOL firstTimeCheck;
@property NSString * region;
@property NSString * country;
@property NSString *locality;
@property NSString *name;
@property NSString *ocean;
@property NSString *postalCode;
@property NSString * subLocality;
@property NSString * location;
@property NSString * locatedAt;
@property NSArray * premiumLeaders;
@property NSString * token;
@property BOOL upStockCheck;
@property NSString * expirySeriesValue;


//multi trade//

@property NSMutableArray * btOutBuffer;
@property BOOL mtCheck;
@property int referanceOrderNo;
@property NSOutputStream  *outputStream;
@property NSOutputStream  *broadCastOutputstream;
@property BOOL messageCheck;
@property NSInputStream   *inputStream;
@property NSInputStream   *broadCastInputStream;
@property int orderStatus;
@property NSMutableArray * allOrderHistory;
@property NSMutableArray * allTradeArray;
@property BOOL  orderCheckMt;
@property NSMutableArray * mtCancelArray;
@property NSMutableArray * mtPendingArray;
@property NSMutableArray * mtCompletedArray;
@property int modifyATINORDERNO;
@property NSMutableArray * exchangeToken;
@property NSString * mtExchangeToken;
@property NSMutableArray * homeExchangeToken;
@property NSMutableArray * mtExchange;
@property NSMutableArray * homeMtExchange;
@property NSMutableArray * marketWatch1;
@property BOOL depthMt;
@property NSMutableArray * mtpositionsArray;
@property NSMutableArray * homeNewCompanyArray;
@property NSMutableArray * expiryDateArray;
@property NSMutableArray * homeExpiryDate;
@property NSMutableArray * strikeArray;
@property NSMutableArray * homeStrkeArray;
@property NSMutableArray * optionArray;
@property NSMutableArray * homeOptionArray;
@property NSMutableArray * lotsizeArray;
@property NSMutableArray * homeLotSizeArray;
@property NSMutableArray * userMarketWatch;
@property BOOL broadcastOpen;
@property NSString * notificationToneName;
@property BOOL  searchToWatch;
@property NSMutableArray * localExchageToken;
@property NSMutableArray * tradeCompletedArray;
@property BOOL mtOrderCheck;
@property  NSData * mixpanelDeviceToken;
@property BOOL mainorderCheck;
@property NSString * mtClientId;
@property NSString * mtPassword;
@property NSMutableArray * questionArray;
@property NSMutableDictionary * brokerInfoDict;
@property NSArray * scripsMt;
@property NSString * POSITIONtransctionType;
@property NSString * currentDeviceId;
@property NSMutableArray * stdealerId;
@property NSMutableArray * mtHoldingArray;
@property NSString * baseUrl;
@property NSMutableArray * allTradeOrderArray;
@property BOOL marketWatchBool;
@property NSMutableArray *mtIndexWatchArray;
@property BOOL  tickerCheck;




@end



