//
//  NewOTPViewController.m
//  OTP
//
//  Created by zenwise mac 2 on 5/23/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewOTPViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "TradingAccountView.h"
#import "TabBar.h";
#import "NewLoginViewController.h"

@interface NewOTPViewController ()

@end

@implementation NewOTPViewController
{
    NSString * string1,*string2,*string3,*string4,*string5,*string6;
    NSString * appendString1,*appendString2,*appendString3,*appendString4,*appendString5,*appendString6;
    AppDelegate * delegate;
    NSMutableDictionary *  responseDict;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.navigationItem.leftBarButtonItem.title=@"Cancel";
    self.navigationController.title=@"ENTER OTP";
    
    self.responseDictionary = [[NSMutableDictionary alloc]init];
    
    [self.TF1 becomeFirstResponder];
    self.TF1.delegate=self;
    self.TF2.delegate=self;
    self.TF3.delegate=self;
    self.TF4.delegate=self;
    self.TF5.delegate=self;
    self.TF6.delegate=self;
     [self.navigationController setNavigationBarHidden:NO animated:YES];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [self settingBorderColor];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)settingBorderColor
{
   // self.TF1.layer.cornerRadius=8.0f;
    
    self.TF1.layer.borderColor=[[UIColor colorWithRed:(5/255.0) green:(76/255.0) blue:(104/255.0) alpha:1]CGColor];
    self.TF1.layer.borderWidth= 1.0f;
    
  //  self.TF1.layer.cornerRadius=8.0f;
   
    self.TF2.layer.borderColor=[[UIColor colorWithRed:(5/255.0) green:(76/255.0) blue:(104/255.0) alpha:1]CGColor];
    self.TF2.layer.borderWidth= 1.0f;
    
  //  self.TF3.layer.cornerRadius=8.0f;
    
    self.TF3.layer.borderColor=[[UIColor colorWithRed:(5/255.0) green:(76/255.0) blue:(104/255.0) alpha:1]CGColor];
    self.TF3.layer.borderWidth= 1.0f;
    
  //  self.TF4.layer.cornerRadius=8.0f;
   
    self.TF4.layer.borderColor=[[UIColor colorWithRed:(5/255.0) green:(76/255.0) blue:(104/255.0) alpha:1]CGColor];
    self.TF4.layer.borderWidth= 1.0f;
    
 //   self.TF5.layer.cornerRadius=8.0f;
    
    self.TF5.layer.borderColor=[[UIColor colorWithRed:(5/255.0) green:(76/255.0) blue:(104/255.0) alpha:1]CGColor];
    self.TF5.layer.borderWidth= 1.0f;
    
  //  self.TF6.layer.cornerRadius=8.0f;
   
    self.TF6.layer.borderColor=[[UIColor colorWithRed:(5/255.0) green:(76/255.0) blue:(104/255.0) alpha:1]CGColor];
    self.TF6.layer.borderWidth= 1.0f;

}


//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    
//}
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    return YES;
//}
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if(textField==self.TF1)
//    {
//        string1=[self.TF1.text stringByReplacingCharactersInRange:range withString:string];
//        NSLog(@"%@",string1);
//    if(string1.length>0)
//    {
//        [self.TF1 resignFirstResponder];
//        [self.TF2 becomeFirstResponder];
//        self.TF1.text= string1;
//        self.TF2.text=@"";
//    }else
//    {
//        return NO;
//    }
//    }
//    
//    
//    if(textField==self.TF2)
//    {
//        string2=[self.TF2.text stringByReplacingCharactersInRange:range withString:string];
//        NSLog(@"%@",string2);
//
//    if(self.TF2.text.length>0)
//    {
//        [self.TF2 resignFirstResponder];
//        [self.TF3 becomeFirstResponder];
//        self.TF2.text= string2;
//        self.TF3.text = @"";
//    }else
//    {
//        return NO;
//    }
//    }
//    if(textField==self.TF3)
//    {
//        string3=[self.TF3.text stringByReplacingCharactersInRange:range withString:string];
//    if(self.TF3.text.length>0)
//    {
//        [self.TF3 resignFirstResponder];
//        [self.TF4 becomeFirstResponder];
//        self.TF3.text= string3;
//        self.TF4.text = @"";
//    }else
//    {
//        return NO;
//    }
//    }
//    
//    
//    if(textField==self.TF4)
//    {
//        string4=[self.TF4.text stringByReplacingCharactersInRange:range withString:string];
//    if(self.TF4.text.length>0)
//    {
//        [self.TF4 resignFirstResponder];
//        [self.TF5 becomeFirstResponder];
//        self.TF4.text= string4;
//        self.TF5.text = @"";
//    }else
//    {
//        return NO;
//    }
//    }
//    
//    if(textField==self.TF5)
//    {
//        string5=[self.TF5.text stringByReplacingCharactersInRange:range withString:string];
//
//    if(self.TF5.text.length>0)
//    {
//        [self.TF5 resignFirstResponder];
//        [self.TF6 becomeFirstResponder];
//        self.TF5.text= string5;
//        self.TF6.text = @"";
//    }else
//    {
//        return NO;
//    }
//    }
//    
//    if(textField==self.TF6)
//    {
//        string6=[self.TF6.text stringByReplacingCharactersInRange:range withString:string];
//
//    if(self.TF6.text.length>0)
//    {
//        [self.TF6 resignFirstResponder];
//        self.TF6.text= string6;
//    }else
//    {
//        return NO;
//    }
//    }
//    
//    
//    return YES;
//}

-(void)dismissKeyboard
{
    [self.TF1 resignFirstResponder];
    [self.TF2 resignFirstResponder];
    [self.TF3 resignFirstResponder];
    [self.TF4 resignFirstResponder];
    [self.TF5 resignFirstResponder];
    [self.TF6 resignFirstResponder];

}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if (textField == self.TF1)
    {
        [self.TF2 becomeFirstResponder];
    }
    else if (textField == self.TF2)
    {
        [self.TF3 becomeFirstResponder];
    }
    else if (textField == self.TF3)
    {
        [self.TF4 becomeFirstResponder];
    }else if (textField == self.TF4)
    {
        [self.TF5 becomeFirstResponder];
    }else if (textField == self.TF5)
    {
        [self.TF6 becomeFirstResponder];
    }
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // This allows numeric text only, but also backspace for deletes
    if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
        return NO;
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength == 1) {
        if (textField == self.TF1)
        {
            string1=[self.TF1.text stringByReplacingCharactersInRange:range withString:string];

            [self performSelector:@selector(setNextResponder:) withObject:self.TF2 afterDelay:0.2];
        }
        else if (textField == self.TF2)
        {
            string2=[self.TF2.text stringByReplacingCharactersInRange:range withString:string];
            appendString1 = [string1 stringByAppendingString:string2];
            NSLog(@"%@",appendString1);
            [self performSelector:@selector(setNextResponder:) withObject:self.TF3 afterDelay:0.2];
        }
        else if (textField == self.TF3)
        {
            string3=[self.TF3.text stringByReplacingCharactersInRange:range withString:string];
            appendString2 = [appendString1 stringByAppendingString:string3];
            NSLog(@"%@",appendString2);
            [self performSelector:@selector(setNextResponder:) withObject:self.TF4 afterDelay:0.2];
        }
        else if (textField == self.TF4)
        {
            string4=[self.TF4.text stringByReplacingCharactersInRange:range withString:string];
            appendString3 = [appendString2 stringByAppendingString:string4];
            NSLog(@"%@",appendString3);

            [self performSelector:@selector(setNextResponder:) withObject:self.TF5 afterDelay:0.2];
        }
        else if (textField == self.TF5)
        {
            string5=[self.TF5.text stringByReplacingCharactersInRange:range withString:string];
            appendString4 = [appendString3 stringByAppendingString:string5];
            NSLog(@"%@",appendString4);

            [self performSelector:@selector(setNextResponder:) withObject:self.TF6 afterDelay:0.2];
        }
        else if (textField ==self.TF6)
        {
            string6=[self.TF6.text stringByReplacingCharactersInRange:range withString:string];
            appendString5 = [appendString4 stringByAppendingString:string6];
            NSLog(@"Working!");
            NSLog(@"%@",appendString5);
            [self otpVerify];
           // TestViewController * test=[self.storyboard instantiateViewControllerWithIdentifier:@"test"];
           // [self presentViewController:test animated:YES completion:nil];
            
        }


        
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField == self.TF6)
        {
            [self performSelector:@selector(setNextResponder:) withObject:self.TF5 afterDelay:0.1];
        }
        else if (textField == self.TF5)
        {
            [self performSelector:@selector(setNextResponder:) withObject:self.TF4 afterDelay:0.1];
        }
        else if (textField == self.TF4)
        {
            [self performSelector:@selector(setNextResponder:) withObject:self.TF3 afterDelay:0.1];
        }
        else if (textField == self.TF3)
        {
            [self performSelector:@selector(setNextResponder:) withObject:self.TF2 afterDelay:0.1];
        }
        else if (textField == self.TF2)
        {
            [self performSelector:@selector(setNextResponder:) withObject:self.TF1 afterDelay:0.1];
        }
        
    }
    
    return newLength <= 1;
}

-(void)otpVerify
{
    if(string1.length>0&&string2.length>0&&string3.length>0&&string4.length>0&&string5.length>0&&string6.length>0)
        
    {
        
        self.OTPString=appendString5;
        
        NSLog(@"OTP String:%@",self.OTPString);
        
        
        
        // NSData *postData = [[NSData alloc] initWithData:[@"{}" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   
                                   };
        
        
        
        NSString * url=[NSString stringWithFormat:@"http://2factor.in/API/V1/13ffd06a-2fe3-11e7-8473-00163ef91450/SMS/VERIFY/%@/%@",delegate.phoneNumberSessionID,self.OTPString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                        
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                        
                                                           timeoutInterval:10.0];
        
        [request setHTTPMethod:@"GET"];
        
        [request setAllHTTPHeaderFields:headers];
        
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                          
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        
                                                        if (error) {
                                                            
                                                            NSLog(@"%@", error);
                                                            
                                                        } else {
                                                            
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            
                                                            
                                                            self.responseDictionary= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSLog(@"OTP dict%@",self.responseDictionary);
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            
                                                            if([[self.responseDictionary objectForKey:@"Details"]isEqualToString:@"OTP Matched"])
                                                                
                                                            {
                                                                
//                                                                TradingAccountView * trading=[self.storyboard instantiateViewControllerWithIdentifier:@"TradingAccountView"];
//                                                                [self.navigationController pushViewController:trading animated:YES];
//
//                                                                NSLog(@"Success");
                                                                
                                                                
//                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Message" message:@"OTP verified." preferredStyle:UIAlertControllerStyleAlert];
//                                                                
//                                                                
//                                                                
//                                                                [self presentViewController:alert animated:YES completion:^{
//                                                                    
//                                                                    
//                                                                    
//                                                                }];
//                                                                
//                                                                
//                                                                
//                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                                                                    
//                                                                   
//                                                                    
//                                                                }];
//                                                                
//                                                                
//                                                                
//                                                                [alert addAction:okAction];
                                                                
                                                                delegate.userID=delegate.clientPhoneNumber;
                                                                
                                                                delegate.loginActivityStr=@"OTP1";
                                                                
                                                                NewLoginViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"NewLoginViewController"];
                                                                [self presentViewController:view animated:YES completion:nil];
                                                                
                                                            }
                                                            
                                                            else
                                                                
                                                            {
                                                                
                                                                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Invalid OTP. Try Again." preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                
                                                                
                                                                [self presentViewController:alert animated:YES completion:^{
                                                                    
                                                                    
                                                                    
                                                                }];
                                                                
                                                                
                                                                
                                                                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                    
                                                                    
                                                                }];
                                                                
                                                                
                                                                
                                                                [alert addAction:okAction];
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        });
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    }];
        
        [dataTask resume];
        
        
        
        
        
    }
    
    else
        
    {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Please enter OTP" preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        [self presentViewController:alert animated:YES completion:^{
            
            
            
        }];
        
        
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            
        }];
        
        
        
        [alert addAction:okAction];
        
        
        
        
        
    }
    
}




- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}

-(void)createClientMethod
{
    
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   };
        NSDictionary *parameters = @{ @"clientmemberid":delegate.clientPhoneNumber };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/client",delegate.baseUrl]]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                             responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            
                                                            if([[responseDict objectForKey:@"message"]isEqualToString:@"client info updated"])
                                                                
                                                                
                                                            {
                                                                delegate.userID=delegate.clientPhoneNumber;
                                                                
                                                                delegate.loginActivityStr=@"OTP1";
                                                        
                                                                NewLoginViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"NewLoginViewController"];
                                                             [self presentViewController:view animated:YES completion:nil];
                                                            }
                                                                //                                                                [self.navigationController pushViewController:view animated:YES];
//                                                                                                                      }
                                                            
                                                        });

                                                    }];
        [dataTask resume];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)resendOTPAction:(id)sender {
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               
                               };
    
    //        NSData *postData = [[NSData alloc] initWithData:[@"{}" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString * url = [NSString stringWithFormat:@"http://2factor.in/API/V1/13ffd06a-2fe3-11e7-8473-00163ef91450/SMS/%@/AUTOGEN",delegate.clientPhoneNumber];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                    
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                    
                                                       timeoutInterval:10.0];
    
    [request setHTTPMethod:@"GET"];
    
    [request setAllHTTPHeaderFields:headers];
    
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                      
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        
                                                        if (error) {
                                                            
                                                            NSLog(@"%@", error);
                                                            
                                                        } else {
                                                            
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            self.responseDictionary1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSLog(@"Response Dict:%@",self.responseDictionary1);
                                                            
                                                            
                                                            
                                                            delegate.phoneNumberSessionID= [self.responseDictionary1 objectForKey:@"Details"];
                                                            
                                                            NSLog(@"Session id:%@",delegate.phoneNumberSessionID);
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        });
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                    
                                                    
                                                }];
    
    [dataTask resume];
    




}
@end
