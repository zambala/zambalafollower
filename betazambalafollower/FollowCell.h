//
//  FollowCell.h
//  testing
//
//  Created by zenwise technologies on 20/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface FollowCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *profileimage;


@property (weak, nonatomic) IBOutlet UILabel *profileName;

@property (strong, nonatomic) IBOutlet UIView *starView;
@property (strong, nonatomic) IBOutlet UILabel *followeCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *followBtn;
@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (strong, nonatomic) IBOutlet UILabel *aboutLbl;
@property (strong, nonatomic) IBOutlet UILabel *leaderActiveSts;
@property (strong, nonatomic) IBOutlet UILabel *gainPer;

@end
