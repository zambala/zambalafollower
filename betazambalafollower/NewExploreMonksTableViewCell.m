//
//  NewExploreMonksTableViewCell.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewExploreMonksTableViewCell.h"

@implementation NewExploreMonksTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.profileImg.layer.cornerRadius=self.profileImg.frame.size.width/2;
    self.profileImg.clipsToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
