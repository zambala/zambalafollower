//
//  NewPortOrderView.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/22/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSWebSocket.h"

@interface NewPortOrderView : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *portTblView;

@property (weak, nonatomic) IBOutlet UIButton *marketBtn;
@property (weak, nonatomic) IBOutlet UIButton *limitBtn;
- (IBAction)marketBtnAction:(id)sender;
- (IBAction)limitBtnAction:(id)sender;
@property NSMutableDictionary * portfolioDepthResponseDictionary;
@property NSMutableArray * countArray;

- (IBAction)portOrderSubmit:(id)sender;
@property (nonatomic, strong) PSWebSocket *socket;
@property NSString * message;
@end
