//
//  OpenAccountView.m
//  testing
//
//  Created by zenwise technologies on 28/02/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "OpenAccountView.h"
#import "BrokerCell.h"
#import "BrokerDetailPage.h"
#import "AppDelegate.h"

@interface OpenAccountView ()
{
    
    
    AppDelegate * delegate1;
}

@end

@implementation OpenAccountView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    
     [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                             };
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/recommendedbrokers?clientid=%@",delegate1.baseUrl,delegate1.userID]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                      
                                                        self.responseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"Open account responseArray:%@",self.responseArray);

                                                        

                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        self.brokerTbl.delegate=self;
                                                        self.brokerTbl.dataSource=self;
                                                        [self.brokerTbl reloadData];
                                                    });
                                                }];
    [dataTask resume];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//tableview//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self.responseArray objectForKey:@"results"] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
        BrokerCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
    cell.brokerTitle.text = [[[self.responseArray objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"name"];
//    cell.brokerDetail.text = [[[self.responseArray objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"description"];
//    

//    
        return cell;
 }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BrokerDetailPage * broker=[self.storyboard instantiateViewControllerWithIdentifier:@"BrokerDetailPage"];
    [self.navigationController pushViewController:broker animated:YES];
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//   
//}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
