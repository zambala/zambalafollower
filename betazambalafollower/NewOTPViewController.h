//
//  NewOTPViewController.h
//  OTP
//
//  Created by zenwise mac 2 on 5/23/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewOTPViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *TF1;
@property (strong, nonatomic) IBOutlet UITextField *TF2;
@property (strong, nonatomic) IBOutlet UITextField *TF3;
@property (strong, nonatomic) IBOutlet UITextField *TF4;
@property (strong, nonatomic) IBOutlet UITextField *TF5;
@property (strong, nonatomic) IBOutlet UITextField *TF6;
@property NSString *OTPString;
@property NSMutableDictionary *responseDictionary;
- (IBAction)resendOTPAction:(id)sender;
@property NSMutableDictionary * responseDictionary1;

@end
