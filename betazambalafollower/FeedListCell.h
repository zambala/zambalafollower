//
//  FeedListCell.h
//  testing
//
//  Created by zenwise mac 2 on 12/19/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *desLbl;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;



@end
