//
//  LinkAccount.h
//  partofZambalaFollower
//
//  Created by zenwise mac 2 on 3/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkAccount : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
- (IBAction)onChooseBankTap:(id)sender;
- (IBAction)onChooseBranchTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UIButton *skipButton;
@property UIPickerView *chooseBank,*chooseBranch;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIButton *bankButton;
@property (strong, nonatomic) IBOutlet UIButton *bankBranchButton;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UITextField *ifscCodeTF;
@property (strong, nonatomic) IBOutlet UITextField *micsCodeTF;
@property (strong, nonatomic) IBOutlet UITextField *acoountNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *reEnterAccountNumberTF;


@end
