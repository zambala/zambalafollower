//
//  WealthTreeView.h
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WealthTreeView : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *referralCodeLbl;

- (IBAction)shareBtnAction:(id)sender;

- (IBAction)backAction:(id)sender;

@end
