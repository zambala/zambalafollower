//
//  MarketMonksView.m
//  testing
//
//  Created by zenwise mac 2 on 11/23/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "MarketMonksView.h"
#import "FollowView.h"
#import "WhoToFollow.h"
#import "HMSegmentedControl.h"
#import "FollowCell.h"
#import "ProfileViewcontroller.h"
#import "NoMonksCell.h"
#import "AppDelegate.h"
#import "NewProfile.h"
#import "Reachability.h"
#import <Mixpanel/Mixpanel.h>
@import Mixpanel;




@interface MarketMonksView ()
{
    HMSegmentedControl * segmentedControl;/*!<segment controller*/
    AppDelegate * delegate1;/*!<appdelegate reference*/
    NSString * messageStr;/*!<it stores status message (subscribed to leader or not)*/
    NSNumber * isSuccessNumber;/*!<unused*/
    int success;
    NSMutableArray * followingUserId;/*!<followed leaders userid*/
    NSString * userId;/*!<it stores leader id to pass to mixpanel*/
    NSArray * filterArray;/*!<it stores filter data according to user search*/
    BOOL searchCheck;/*!<to check search is active or inactive*/
    NSMutableArray * firstName;/*!<it store first name of leader*/
    NSMutableArray * newFollowArray;/*!<it stores filter data according to user search*/
    NSMutableDictionary * monksDataDict;/*!<it stores data of who to follow leaders list*/
    NSString * segmentFinal;/*!<it stores segments to filter list*/
    NSString * durationFinall;/*!<it stores duration to filter list*/
    NSString * ratingFinal;/*!<it stores rating to filter list*/
    NSDictionary * params;/*!<it stores request params */
    NSMutableArray * detailsArray;/*!<it store first name of leader*/
    NSString * filterString1;/*!<it store filtered data accordind to user search*/
    NSString * leaderUserName;/*!<it store first name of leader*/
    NSMutableArray * specialization;/*!<it store specialization list*/
    NSMutableString * specializationMutString;/*!<it is support string for specialization*/
    UIImageView *hintScreen ;
    UIWindow * window;
    UIImageView *   myImageView;;
}

@property (strong, nonatomic) UISearchController *searchController;


@end

@implementation MarketMonksView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Market Monks";
    self.navigationItem.leftBarButtonItem.title=@" ";
    self.leaderDict=[[NSMutableArray alloc]init];
    self.buttonArray = [[NSMutableArray alloc]init];
    followingUserId=[[NSMutableArray alloc]init];
    self.followPlusArray=[[NSMutableArray alloc]init];
       delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    firstName=[[NSMutableArray alloc]init];
    newFollowArray=[[NSMutableArray alloc]init];
    self.buttonArrayy = [[NSMutableArray alloc]init];
    specialization = [[NSMutableArray alloc]init];
    specializationMutString = [[NSMutableString alloc]init];
    
   
    self.followerTableView.hidden=YES;
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
   
    
    self.followerTableView.delegate=self;
    self.followerTableView.dataSource=self;
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"FOLLOWING",@"WHO TO FOLLOW"]];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    segmentedControl.verticalDividerEnabled = YES;
//    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
        segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    [self.view addSubview:segmentedControl];
    
  
   
    self.profileNameArray=[[NSMutableArray alloc]init];
    self.followersArray = [[NSMutableArray alloc]init];
    self.userIdArray = [[NSMutableArray alloc]init];
//    self.responseDictionary = [[NSMutableDictionary alloc]init];
    
    [segmentedControl setSelectedSegmentIndex:0];
    
    self.controller.delegate=self;
    self.controller.searchResultsUpdater=self;
    
// hintScreen =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
//    hintScreen.image=[UIImage imageNamed:@"2_2_hint"];
//    [self.view addSubview:hintScreen];
//
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissImage)];
//
//
//    [self.view addGestureRecognizer:tap];
//
    
    //star reating//
    
    
   
}
//-(void)dismissImage
//{
//    hintScreen.hidden=YES;
//}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
    [self segmentedControlChangedValue];
}

/*!
 @discussion unused.
 */
-(void)starOneImg1
{
    
    if (starOneFlag1==false)
    {
        starOneImg1.selected = YES;
        starTwoImg1.selected = NO;
        starThreeImg1.selected = NO;
        starFourImg1.selected = NO;
        starFiveImg1.selected = NO;
        starOneFlag1 = true;
    }
    else
    {
        starOneImg1.selected = NO;
        starTwoImg1.selected = NO;
        starThreeImg1.selected = NO;
        starFourImg1.selected = NO;
        starFiveImg1.selected = NO;
        starOneFlag1 = false;
        
    }
}
/*!
 @discussion unused.
 */
-(void)starTwoImg1
{
    starOneImg1.selected = YES;
    starTwoImg1.selected = YES;
    starThreeImg1.selected = NO;
    starFourImg1.selected = NO;
    starFiveImg1.selected = NO;
    
}
/*!
 @discussion unused.
 */

-(void)starThreeImg1
{
    starOneImg1.selected = YES;
    starTwoImg1.selected = YES;
    starThreeImg1.selected = YES;
    starFourImg1.selected = NO;
    starFiveImg1.selected = NO;
    
}
/*!
 @discussion unused.
 */

-(void)starFourImg1
{
    
    starOneImg1.selected = YES;
    starTwoImg1.selected = YES;
    starThreeImg1.selected = YES;
    starFourImg1.selected = YES;
    starFiveImg1.selected = NO;
    
}
/*!
 @discussion unused.
 */

-(void)starFiveImg1
{
    
    
    starOneImg1.selected = YES;
    starTwoImg1.selected = YES;
    starThreeImg1.selected = YES;
    starFourImg1.selected = YES;
    starFiveImg1.selected = YES;
    
}


//- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
//{
//    NSString *searchString = searchController.searchBar.text;
//    self.followerTableView.tableHeaderView = self.searchController.searchBar;
//
//    
//  // [self searchForText:searchString scope:searchController.searchBar.selectedScopeButtonIndex];
//    
//
////    [self.followerTableView reloadData];
//}

/*!
 @discussion When ever there is change in segment selection this method will trigger.
 */

-(void)segmentedControlChangedValue
{
    BOOL toFollow=true;
    
    self.followerTableView.hidden=YES;
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
        
        NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
        if(netStatus==NotReachable)
        {
           
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:^{
                
            }];
            
            UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            
            
            
        }
        else
        {
            if(segmentedControl.selectedSegmentIndex==1)
            {
              
                
                
               
                if(delegate1.marketmonksHint==YES)
                {
                    
                    delegate1.marketmonksHint=NO;
                    
                    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
                    
                    myImageView =[[UIImageView alloc] initWithFrame:CGRectMake(0.0,0.0,currentWindow.frame.size.width,currentWindow.frame.size.height)];
                    
                    myImageView.image=[UIImage imageNamed:@"marketmonks"];
                    
                    
                    [currentWindow addSubview:myImageView ];
                    [currentWindow bringSubviewToFront:myImageView];
                }
                
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideImage)];
                singleTap.numberOfTapsRequired = 1;
                [myImageView setUserInteractionEnabled:YES];
                [myImageView addGestureRecognizer:singleTap];
                
                
                @try {
                    
                    
                    NSDictionary *headers = @{ @"content-type": @"application/json",
                                               @"cache-control": @"no-cache",
                                               @"Accept":@"application/json"
                                               
                                               };
                    
                    //        NSString * user=[NSString stringWithFormat:@"&clientid=%@",userId];
                    //         NSString * limit=[NSString stringWithFormat:@"&limit=1000"];
                    ////        NSString * follow=[NSString stringWithFormat:@"&tofollow=%s",toFollow?"true":"false"];
                    //
                    //
                    //        NSString * follow=[NSString stringWithFormat:@"&tofollow=true"];
                    
                    
                    
                    NSLog(@"%@",delegate1.segment3);
                    NSLog(@"%@",delegate1.rating);
                    NSLog(@"%@",delegate1.duration3);
                    
                    //        if([delegate1.segment3 isEqual:[NSNull null]]||[delegate1.segment3 isEqual:nil])
                    //        {
                    //            delegate1.segment3=@"";
                    //        }
                    //
                    //        if([delegate1.duration3 isEqual:[NSNull null]]||[delegate1.segment3 isEqual:nil])
                    //        {
                    //            delegate1.duration3=@"";
                    //        }
                    //
                    //        if([delegate1.rating isEqual:[NSNull null]]||[delegate1.segment3 isEqual:nil])
                    //        {
                    //            delegate1.rating=@"";
                    //        }
                    //
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    
                    @try {
                        NSString * equityStr=[prefs stringForKey:@"0"];
                        
                        
                        if([equityStr isEqualToString:@"checkSelected"])
                        {
                            
                            @try {
                                durationFinall=[prefs stringForKey:@"dur"];
                                
                            }
                            
                            @catch (NSException * e) {
                                NSLog(@"Exception: %@", e);
                            }
                            @finally {
                                NSLog(@"finally");
                            }
                            
                            @try {
                                segmentFinal=[prefs stringForKey:@"segment"];
                                
                            }
                            
                            @catch (NSException * e) {
                                NSLog(@"Exception: %@", e);
                            }
                            @finally {
                                NSLog(@"finally");
                            }
                            
                            @try {
                                ratingFinal=[prefs stringForKey:@"rating"];
                                
                            }
                            
                            @catch (NSException * e) {
                                NSLog(@"Exception: %@", e);
                            }
                            @finally {
                                NSLog(@"finally");
                            }
                            
                        }
                        
                        
                        else
                        {
                            durationFinall=@"";
                            segmentFinal=@"";
                            ratingFinal=@"";
                        }
                        
                        
                        
                    }
                    @catch (NSException * e) {
                        NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        NSLog(@"finally");
                    }
                    
                    // getting an NSString
                    
                    
                    
                    
                    NSLog(@"%@",durationFinall);
                    NSLog(@"%@",segmentFinal);
                    NSLog(@"%@",ratingFinal);
                    
                    @try {
                        
                        params=@{@"clientid":delegate1.userID,
                                 @"tofollow":@(toFollow),
                                 @"limit":@1000,
                                 @"rating":ratingFinal,
                                 @"durationtype":durationFinall,
                                 @"segment":segmentFinal
                                 
                                 
                                 
                                 };
                        
                        
                    }
                    @catch (NSException * e) {
                        NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        NSLog(@"finally");
                    }
                    
                    
                    
                    NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
                    //        NSMutableData *postData = [[NSMutableData alloc] initWithData:[delegate1.segment3 dataUsingEncoding:NSUTF8StringEncoding]];
                    //        [postData appendData:[delegate1.duration3 dataUsingEncoding:NSUTF8StringEncoding]];
                    //        [postData appendData:[delegate1.rating dataUsingEncoding:NSUTF8StringEncoding]];
                    //        [postData appendData:[user dataUsingEncoding:NSUTF8StringEncoding]];
                    //        [postData appendData:[limit dataUsingEncoding:NSUTF8StringEncoding]];
                    //        [postData appendData:[follow dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    NSString * localUrl=[NSString stringWithFormat:@"%@follower/leaders",delegate1.baseUrl];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"POST"];
                    [request setAllHTTPHeaderFields:headers];
                    [request setHTTPBody:postData];
                    
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    
                                                                    if(data!=nil)
                                                                    {
                                                                        if (error) {
                                                                            NSLog(@"%@", error);
                                                                        } else {
                                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                            NSLog(@"%@", httpResponse);
                                                                            if(self.followPlusArray.count>0)
                                                                            {
                                                                                [self.followPlusArray removeAllObjects];
                                                                            }
                                                                            if(self.buttonArrayy.count>0)
                                                                            {
                                                                                [self.buttonArrayy removeAllObjects];
                                                                            }
                                                                            monksDataDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                            [self.followersArray removeAllObjects];
                                                                            [self.profileNameArray removeAllObjects];
                                                                            [self.userIdArray removeAllObjects];
                                                                            [self.buttonArray removeAllObjects];
                                                                            
                                                                            NSLog(@"Monks list Dict:%@",monksDataDict);
                                                                            
                                                                            for(int i=0;i<[[monksDataDict objectForKey:@"results"]count];i++)
                                                                            {
                                                                                
                                                                                NSLog(@"monksData %@",monksDataDict);
                                                                                [self.profileNameArray addObject:[[[monksDataDict objectForKey:@"results"]objectAtIndex:i] objectForKey:@"firstname"]];
                                                                                
                                                                                [self.userIdArray addObject:[[[monksDataDict objectForKey:@"results"]objectAtIndex:i] objectForKey:@"userid"]];
                                                                                
                                                                                [self.followPlusArray addObject:[[monksDataDict objectForKey:@"results"] objectAtIndex:i]];
                                                                                
                                                                                [self.followersArray addObject:[[[monksDataDict objectForKey:@"results"]objectAtIndex:i] objectForKey:@"followercount"]];
                                                                                
                                                                                NSString * localInt = [[[monksDataDict objectForKey: @"results"] objectAtIndex:i]objectForKey:@"subscribed"];
                                                                                
                                                                                success=[localInt intValue];
                                                                                NSLog(@"Success1:%d",success);
                                                                                if(success==1)
                                                                                {
                                                                                    [self.buttonArray addObject:@"Following"];
                                                                                    delegate1.buttonTitle=@"Following";
                                                                                }else if(success==0)
                                                                                {
                                                                                    [self.buttonArrayy addObject:@"Follow +"];
                                                                                    delegate1.buttonTitle=@"Follow +";
                                                                                    //                                                                    [self.followPlusArray addObject:[[monksDataDict objectForKey:@"results"] objectAtIndex:i]];
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        
                                                                        NSLog(@"%@",self.profileNameArray);
                                                                        NSLog(@"%lu followers array Count",self.followersArray.count);
                                                                        NSLog(@"New Array:%@",self.followPlusArray);
                                                                        NSLog(@"Button Arrayyy:%@",self.buttonArrayy);
                                                                        NSLog(@"Button Arrayyy:%@",self.buttonArrayy);
                                                                        NSLog(@"Button Array:%@",self.buttonArray);
                                                                        
                                                                      
                                                                        
                                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                                            
                                                                            self.followerTableView.hidden=NO;
                                                                            
                                                                            self.activityIndicator.hidden=YES;
                                                                            [self.activityIndicator stopAnimating];
                                                                            self.followerTableView.delegate=self;
                                                                            self.followerTableView.dataSource=self;
                                                                            [self.followerTableView reloadData];
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        });
                                                                        
                                                                    }
                                                                    
                                                                    
                                                                }];
                    [dataTask resume];
                    
                }
                @catch (NSException * e) {
                    NSLog(@"Exception: %@", e);
                }
                @finally {
                    NSLog(@"finally");
                }
                
                
                
            }
            
            else if(segmentedControl.selectedSegmentIndex==0)
            {
                //        [self.followerTableView reloadData];
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                
                @try {
                    NSString * equityStr=[prefs stringForKey:@"0"];
                    
                    
                    if([equityStr isEqualToString:@"checkSelected"])
                    {
                        
                        @try {
                            durationFinall=[prefs stringForKey:@"dur"];
                            
                        }
                        
                        @catch (NSException * e) {
                            NSLog(@"Exception: %@", e);
                        }
                        @finally {
                            NSLog(@"finally");
                        }
                        
                        @try {
                            segmentFinal=[prefs stringForKey:@"segment"];
                            
                        }
                        
                        @catch (NSException * e) {
                            NSLog(@"Exception: %@", e);
                        }
                        @finally {
                            NSLog(@"finally");
                        }
                        
                        @try {
                            ratingFinal=[prefs stringForKey:@"rating"];
                            
                        }
                        
                        @catch (NSException * e) {
                            NSLog(@"Exception: %@", e);
                        }
                        @finally {
                            NSLog(@"finally");
                        }
                        
                    }
                    
                    
                    else
                    {
                        durationFinall=@"";
                        segmentFinal=@"";
                        ratingFinal=@"";
                    }
                    
                    
                    
                }
                @catch (NSException * e) {
                    NSLog(@"Exception: %@", e);
                }
                @finally {
                    NSLog(@"finally");
                }
                

                
                @try {
                    NSDictionary *headers = @{ @"content-type": @"application/json",
                                               @"cache-control": @"no-cache",
                                               };
                    
                    
                
                    @try {
                        
                        params=@{@"clientid":delegate1.userID,
                                 @"limit":@1000,
                                 @"rating":ratingFinal,
                                 @"durationtype":durationFinall,
                                 @"segment":segmentFinal
                                 
                                 
                                 
                                 };
                        
                        
                    }
                    @catch (NSException * e) {
                        NSLog(@"Exception: %@", e);
                    }
                    @finally {
                        NSLog(@"finally");
                    }
                    
                    NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];

                    
                    NSString * localUrl=[NSString stringWithFormat:@"%@follower/leaders/%@",delegate1.baseUrl,delegate1.userID];
                    
                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                       timeoutInterval:10.0];
                    [request setHTTPMethod:@"POST"];
                    [request setAllHTTPHeaderFields:headers];
                    [request setHTTPBody:postData];
                    
                    
                    NSURLSession *session = [NSURLSession sharedSession];
                    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                    if (error) {
                                                                        NSLog(@"%@", error);
                                                                    } else {
                                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                        NSLog(@"%@", httpResponse);
                                                                        
                                                                        if(followingUserId.count>0)
                                                                        {
                                                                            [followingUserId removeAllObjects];
                                                                        }
                                                                        
                                                                        self.followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                        
                                                                       
                                                                        NSLog(@"Response dicttt%@",self.followDict);
                                                                        
                                                                        for(int i=0;i<self.followDict.count;i++)
                                                                        {
                                                                            
                                                                            
                                                                            
                                                                            [followingUserId addObject:[[self.followDict objectAtIndex:i] objectForKey:@"userid"]];
                                                                        }
                                                                        
                                                                        //                                                            NSLog(@"Response Dictionary Count:%lu",[[self.responseDictionary objectForKey:@"results"] count]);
                                                                        
                                                                        
                                                                        //                                                            for(int i=0;i<[[self.responseDictionary objectForKey:@"results"]count];i++)
                                                                        //                                                            {
                                                                        //
                                                                        //                                                            NSString * localInt = [[[self.responseDictionary objectForKey: @"results"] objectAtIndex:i]objectForKey:@"subscribed"];
                                                                        //
                                                                        //                                                            success=[localInt intValue];
                                                                        //                                                            if(success==YES)
                                                                        //                                                            {
                                                                        //                                                                [self.buttonArray addObject:@"Following"];
                                                                        //                                                            }else
                                                                        //                                                            {
                                                                        //                                                                [self.buttonArray addObject:@"Follow +"];
                                                                        //                                                            }
                                                                        //                                                            NSLog(@"Button array:%@",self.buttonArray);
                                                                        //                                                            }
                                                                        
                                                                        Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
                                                                        
                                                                        [mixpanel1 identify:delegate1.userID];
                                                                        
                                                                        NSMutableString * string=[[NSMutableString alloc]init];
                                                                        
                                                                        for(int i=0;i<self.followDict.count;i++)
                                                                        {
                                                                            NSMutableString * string1=[[NSMutableString alloc]init];
                                                                            string1=[NSMutableString stringWithFormat:@"%@,",[[self.followDict objectAtIndex:i] objectForKey:@"firstname"]];
                                                                            
                                                                            [string appendString:string1];                                       }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        [mixpanel1.people set:@{@"LeaderFollowing":string}];
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        self.followerTableView.hidden=NO;
                                                                        
                                                                        self.activityIndicator.hidden=YES;
                                                                        [self.activityIndicator stopAnimating];           self.followerTableView.delegate=self;
                                                                        self.followerTableView.dataSource=self;
                                                                        [self.followerTableView reloadData];
                                                                        
                                                                        
                                                                    });
                                                                }];
                    [dataTask resume];
                    
                    
                }
                @catch (NSException * e) {
                    NSLog(@"Exception: %@", e);
                }
                @finally {
                    NSLog(@"finally");
                }
                
            }
            

        }
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(segmentedControl.selectedSegmentIndex==0)
    {
//        if(newFollowArray.count>0||self.followDict.count>0)
//        {
//            if(self.controller.active==YES&&newFollowArray.count>0)
//            {
//                return [newFollowArray count];
//            }
//            else
//            {
//                return self.followDict.count;
//            }
//        }
//        
//        else
//        {
//            return 1;
//        }
        
        
        if(self.controller.isActive==YES&&newFollowArray.count>0)
        {
            return newFollowArray.count;
        }
        
        
        
        
        else  if(self.followDict.count>0&&self.controller.isActive==NO)
        {
            return self.followDict.count;
        }
        
        else
        {
            return 1;
            
            
        }
        
        
        
    }
    
   else  if(segmentedControl.selectedSegmentIndex==1)
    {
//        if(self.controller.active==YES&&newFollowArray.count>0)
//        {
//            return [newFollowArray count];
//        }
//        else
//        {
//            return self.followPlusArray.count;
//        }
        
        
        if(self.controller.isActive==YES&&newFollowArray.count>0)
        {
            return newFollowArray.count;
        }
        
        
        
        
        else  if(self.followPlusArray.count>0&&self.controller.isActive==NO)
        {
            return self.followPlusArray.count;
        }
        
        else
        {
            return 1;
            
            
        }
        
        
    }
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if(segmentedControl.selectedSegmentIndex==0)
    {
       
        if(self.controller.active==YES&&self.controller.searchBar.text.length>=1)
        {
        //        self.brokerMesssagesTableView.hidden=YES;
        //        self.notoficationstableView.hidden=NO;
//        FollowCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//        
//        cell.profileName.text=@"Ankit Sharma";
//        return cell;
        
        if([filterArray  count]>0)
        {
            FollowCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
//            if(segmentedControl.selectedSegmentIndex==0)
//            {
//            [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
//            [cell.followBtn setUserInteractionEnabled:NO];
//            }
            
            [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
             cell.profileName.text=[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"firstname"]];
            
            
            
           
             cell.followeCountLabel.text=@"0 Followers";
           
              NSString *followersString =[NSString stringWithFormat:@"%@ followers", [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
            
            NSString * testString =[NSString stringWithFormat:@"%@", [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
            
            if([testString isEqual:[NSNull null]])
            {
                cell.followeCountLabel.text=@"0 Followers";
            }
            else
            {
                cell.followeCountLabel.text=followersString;
            }
            
//              cell.aboutLbl.text = @"No info";
            
            @try {
                NSString * aboutString = [NSString stringWithFormat:@"%@",[[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"about"] objectForKey:@"info"]];
                
                
                if([aboutString isEqual:[NSNull null]])
                {
                    cell.aboutLbl.text = @"No info";
                }else
                {
                    cell.aboutLbl.text = aboutString;
                }
            } @catch (NSException *exception) {
                
              
                
            } @finally {
                
            }
            
           

            
            
            NSString * rating = [NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"rating"]];
           
            
//            cell.profileName.text=[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"firstname"];
            cell.leaderActiveSts.text=@"Inactive";
            
            if([[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"created"] isEqual:[NSNull null]])
            {
                cell.leaderActiveSts.text=@"Inactive";
                
            }else
            {
            NSString *dateStr =[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"created"]];
                
                NSLog(@"%@",dateStr);
            
            // Convert string to date object
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
                [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                NSDate *date = [dateFormat dateFromString:dateStr];
            

            NSString *differnce = [self remaningTime:date endDate:[NSDate date]];
                
                cell.leaderActiveSts.text=[NSString stringWithFormat:@"(last active %@ ago)",differnce];
            
            NSLog(@"%@",differnce);
            }
            
            cell.gainPer.text=@"0% gain";
            if([[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"] isEqual:[NSNull null]])
            {
                 cell.gainPer.text=@"0% gain";
                
            }else
            {
                NSString * gain=[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"];
                                 
                float gainFloat=[gain floatValue];
                
                NSString * str=@"%";
                
                cell.gainPer.text=[NSString stringWithFormat:@"%.2f%@ gain",gainFloat,str];
            }
            
            cell.starRatingView.minimumValue=0;
            
            cell.starRatingView.maximumValue=5;
            
             cell.starRatingView.value=0;
            
            if([rating isEqual:[NSNull null]])
            {
                cell.starRatingView.value=0;

            }else
            {
                 float ratingFloat = [rating floatValue];
            
            cell.starRatingView.value=ratingFloat;
            }
            cell.starRatingView.allowsHalfStars=YES;
            
            cell.starRatingView.allowsHalfStars=YES;
            
            cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
            
            cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
            
            cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
            
            [cell.starRatingView setUserInteractionEnabled:NO];
            [cell.followBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
             // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
            
             cell.profileimage.image=[UIImage imageNamed:@"dpDefault"];
            
            NSURL *url;
            @try {
                url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"logourl"]]];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
        
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                           
                            if (cell)
                                cell.profileimage.image = image;
                        });
                    }
                }
            }];
            [task resume];
                
            
            
            return cell;
            
        }
        else
        {
        
        NoMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
        
        
        return cell;
            
        }
            
        }
        
        else
        {
            
            if(self.followDict.count>0)
            {
                FollowCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
                NSString *followersString = [NSString stringWithFormat:@"%@ followers",[[self.followDict  objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                
               
                NSString * testString = [NSString stringWithFormat:@"%@",[[self.followDict  objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
                
                cell.leaderActiveSts.text=@"Inactive";
                
                if([[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"created"] isEqual:[NSNull null]])
                {
                    cell.leaderActiveSts.text=@"Inactive";
                    
                }else
                {
                    NSString *dateStr =[NSString stringWithFormat:@"%@",[[self.followDict  objectAtIndex:indexPath.row ]objectForKey:@"created"]];
                    
                    // Convert string to date object
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
                    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    NSDate *date = [dateFormat dateFromString:dateStr];
                    
                    
                    NSString *differnce = [self remaningTime:date endDate:[NSDate date]];
                    
                    cell.leaderActiveSts.text=[NSString stringWithFormat:@"(last active %@ ago)",differnce];
                    
                    NSLog(@"%@",differnce);
                }
                
                
//                cell.aboutLbl.text = @"No info";
                @try {
                    NSString * aboutString = [NSString stringWithFormat:@"%@",[[[self.followDict  objectAtIndex:indexPath.row ]objectForKey:@"about"]objectForKey:@"info"]];
                    
                    
                    if([aboutString isEqual:[NSNull null]])
                    {
                        cell.aboutLbl.text = @"No info";
                    }else
                    {
                        cell.aboutLbl.text = aboutString;
                    }
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                cell.gainPer.text=@"0% gain";
            
                if([[[self.followDict  objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"] isEqual:[NSNull null]])
                {
                    cell.gainPer.text=@"0% gain";
                    
                }else
                {
                    NSString * gain=[NSString stringWithFormat:@"%@",[[self.followDict  objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"]];
                    
                    float gainFloat=[gain floatValue];
                    
                    NSString * str=@"%";
                    
                    cell.gainPer.text=[NSString stringWithFormat:@"%.2f%@ gain",gainFloat,str];
                }

                cell.followeCountLabel.text=@"0 Followers";
                if([testString isEqual:[NSNull null]])
                {
                    cell.followeCountLabel.text=@"0 Followers";
                }
                else
                {
                    cell.followeCountLabel.text=followersString;
                }
                

                
                
                [cell.followBtn setTitle:@"Following" forState:UIControlStateNormal];
                cell.profileName.text=[NSString stringWithFormat:@"%@",[[self.followDict objectAtIndex:indexPath.row ]objectForKey:@"firstname"]];
                NSString * rating = [NSString stringWithFormat:@"%@",[[self.followDict objectAtIndex:indexPath.row] objectForKey:@"rating"]];
                cell.starRatingView.minimumValue=0;
                
                cell.starRatingView.maximumValue=5;
                
                if([rating isEqual:[NSNull null]])
                {
                    cell.starRatingView.value=0;
                }else
                {
                    float ratingFloat = [rating floatValue];
                cell.starRatingView.value=ratingFloat;
                }
                
                cell.starRatingView.allowsHalfStars=YES;
                
                cell.starRatingView.allowsHalfStars=YES;
                
                cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
                
                cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
                
                cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
                
                [cell.starRatingView setUserInteractionEnabled:NO];
                [cell.followBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                
                 // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
                
                cell.profileimage.image=[UIImage imageNamed:@"dpDefault"];
                
                NSURL *url;
                @try {
                    url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[self.followDict objectAtIndex:indexPath.row] objectForKey:@"logourl"]]];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
                
                
                
               
                
                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (data) {
                        UIImage *image = [UIImage imageWithData:data];
                        if (image) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if (cell)
                                    cell.profileimage.image = image;
                            });
                        }
                    }
                }];
                [task resume];
                
                
                
                return cell;
                
            }
            else
            {
                
                NoMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                
                
                return cell;
                
            }

        }

    }
    else if (segmentedControl.selectedSegmentIndex==1)
    {
        
        
       
        
//        if(segmentedControl.selectedSegmentIndex==1&&[messageStr isEqualToString:@"Subscriber created"])
//        {
//            [cell.followBtn setUserInteractionEnabled:YES];
//            [cell.followBtn setTitle:@"Unfollow" forState:UIControlStateNormal];
//        }else
//        {
//            [cell.followBtn setUserInteractionEnabled:YES];
//            [cell.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
//        }
//        if([[self.followersArray objectAtIndex:indexPath.row]isEqualToString:@""])
//        {
//            cell.followeCountLabel.text=@"0";
//        }else
//        {
//        
//        cell.followeCountLabel.text=followersString;
//        }
        
        //filter
        
        if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
        {
            if(newFollowArray.count>0)
            {
            
            FollowCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            NSString *followersString = [NSString stringWithFormat:@"%@ Followers",[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
            
            
            NSString * testString =[NSString stringWithFormat:@"%@", [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
            cell.profileName.text=[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
            
            NSString * rating = [NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"rating"]];
            
            cell.starRatingView.minimumValue=0;
            
            cell.starRatingView.maximumValue=5;
                
//                cell.aboutLbl.text=@"No info";
                
                @try {
                   
                    
                    specialization=[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"specialization"];
                    
                    // NSMutableString * specializationMutString = [[NSMutableString alloc]init];
                    
                    for(int i=1;i<=3;i++)
                    {
                        
                        //for (int j=0; j<segment.count; j++) {
                        if([specialization containsObject:[NSNumber numberWithInt:i]])
                        {
                            if(i==1)
                            {
                                [specializationMutString appendString:@",DayTrade"];
                            }
                            if(i==2)
                            {
                                [specializationMutString appendString:@",ShortTerm"];
                            }
                            if(i==3)
                            {
                                [specializationMutString appendString:@",LongTerm"];
                            }
                            
                            
                            //  }
                            
                        }
                    }
                    NSLog(@"Specialization Test:%@",specializationMutString);
                    NSString * aboutString=[specializationMutString mutableCopy];
                    
                    
                    aboutString = [aboutString substringFromIndex:1];
                    
                    cell.aboutLbl.text = aboutString;

                    
                } @catch (NSException *exception) {
                     cell.aboutLbl.text=@"No info";
                    
                } @finally {
                    
                }
                
               // NSMutableArray * specialization = [[NSMutableArray alloc]init];
                
                
            
            if([rating isEqual:[NSNull null]])
            {
                cell.starRatingView.value=0;
            }else
            {
                float ratingFloat = [rating floatValue];
                cell.starRatingView.value=ratingFloat;
            }
            
            cell.starRatingView.allowsHalfStars=YES;
            
            cell.starRatingView.allowsHalfStars=YES;
            
            cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
            
            cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
            
            cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
            
            [cell.starRatingView setUserInteractionEnabled:NO];
            
            cell.followeCountLabel.text=@"0 Followers";
            if([testString isEqual:[NSNull null]])
            {
                cell.followeCountLabel.text=@"0 Followers";
            }
            else
            {
                cell.followeCountLabel.text=followersString;
            }
            
                cell.leaderActiveSts.text=@"Inactive";
            
            if([[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"created"] isEqual:[NSNull null]])
            {
                
                cell.leaderActiveSts.text=@"Inactive";
            }else
            {
                NSString *dateStr =[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"created"]];
                
                NSLog(@"%@",dateStr);
                
                // Convert string to date object
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
                [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                NSDate *date = [dateFormat dateFromString:dateStr];
                
                
                NSString *differnce = [self remaningTime:date endDate:[NSDate date]];
                
                cell.leaderActiveSts.text=[NSString stringWithFormat:@"(last active %@ ago)",differnce];
                
                NSLog(@"%@",differnce);
            }
            
            cell.gainPer.text=@"0% gain";
            if([[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"] isEqual:[NSNull null]])
            {
                cell.gainPer.text=@"0% gain";
            }else
            {
                NSString * gain=[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"]];
                
                float gainFloat=[gain floatValue];
                
                NSString * str=@"%";
                
                cell.gainPer.text=[NSString stringWithFormat:@"%.2f%@ gain",gainFloat,str];
            }
            
            
            //        starOneImg1 = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 15, 15)];
            //        [starOneImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starOneImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starOneImg1.selected = YES;
            //        [starOneImg1 addTarget: self action: @selector(starOneImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starOneImg1];
            //
            //        starTwoImg1 = [[UIButton alloc] initWithFrame:CGRectMake(30, 0, 15, 15)];
            //        [starTwoImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starTwoImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starTwoImg1.selected = YES;
            //        [starTwoImg1 addTarget: self action: @selector(starTwoImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starTwoImg1];
            //
            //        starThreeImg1 = [[UIButton alloc] initWithFrame:CGRectMake(50, 0, 15, 15)];
            //        [starThreeImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starThreeImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starThreeImg1.selected = YES;
            //        [starThreeImg1 addTarget: self action: @selector(starThreeImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starThreeImg1];
            //
            //        starFourImg1 = [[UIButton alloc] initWithFrame:CGRectMake(70, 0, 15, 15)];
            //        [starFourImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starFourImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starFourImg1.selected = YES;
            //        [starFourImg addTarget: self action: @selector(starFourImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starFourImg1];
            //
            //        starFiveImg1 = [[UIButton alloc] initWithFrame:CGRectMake(90, 0, 15, 15)];
            //        [starFiveImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starFiveImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starFiveImg1.selected = YES;
            //        [starFiveImg1 addTarget: self action: @selector(starFiveImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starFiveImg1];
            
            cell.followBtn.tag=indexPath.row;
            
            [cell.followBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            //            NSString * localStr=[self.buttonArrayy objectAtIndex:indexPath.row];
            //
            [cell.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
                
                 // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
                cell.profileimage.image=[UIImage imageNamed:@"dpDefault"];
                
                NSURL *url;
                @try {
                    url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"logourl"]]];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
               
                
                
              
                
                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (data) {
                        UIImage *image = [UIImage imageWithData:data];
                        if (image) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if (cell)
                                    cell.profileimage.image = image;
                            });
                        }
                    }
                }];
                [task resume];
                    
                
                
                
                
//                @try {
//                    NSString * aboutString = [[newFollowArray  objectAtIndex:indexPath.row ]objectForKey:@"about"];
//                    
//                    
//                    if([aboutString isEqual:[NSNull null]])
//                    {
//                        cell.aboutLbl.text = @"No info";
//                    }else
//                    {
//                        cell.aboutLbl.text = aboutString;
//                    }
//                } @catch (NSException *exception) {
//                    
//                } @finally {
//                    
//                }
//
            
            return cell;
            
        }
        
        else
        {
            
            NoMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            
            
            return cell;
            
        }

        }
    
        /////
        
        else
        {
            if(self.followPlusArray.count>0)
            {
                   FollowCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
            
            NSString *followersString = [NSString stringWithFormat:@"%@ Followers",[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
            
            
            NSString * testString = [NSString stringWithFormat:@"%@",[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"followercount"]];
            cell.profileName.text=[NSString stringWithFormat:@"%@",[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
            
            NSString * rating = [NSString stringWithFormat:@"%@",[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"rating"]];
                
//                
//                @try {
//                    NSString * aboutString = [[self.followPlusArray  objectAtIndex:indexPath.row ]objectForKey:@"about"];
//                    
//                    
//                    if([aboutString isEqual:[NSNull null]])
//                    {
//                        cell.aboutLbl.text = @"No info";
//                    }else
//                    {
//                        cell.aboutLbl.text = aboutString;
//                    }
//                } @catch (NSException *exception) {
//                    
//                } @finally {
//                    
//                }
            
            cell.starRatingView.minimumValue=0;
            
            cell.starRatingView.maximumValue=5;
//                cell.aboutLbl.text=@"No info";
                @try {
                    
                    
                    specialization=[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"specialization"];
                    
                    // NSMutableString * specializationMutString = [[NSMutableString alloc]init];
                    
                    for(int i=1;i<=3;i++)
                    {
                        
                        //for (int j=0; j<segment.count; j++) {
                        if([specialization containsObject:[NSNumber numberWithInt:i]])
                        {
                            if(i==1)
                            {
                                [specializationMutString appendString:@",DayTrade"];
                            }
                            if(i==2)
                            {
                                [specializationMutString appendString:@",ShortTerm"];
                            }
                            if(i==3)
                            {
                                [specializationMutString appendString:@",LongTerm"];
                            }
                            
                            
                            //  }
                            
                        }
                    }
                    NSLog(@"Specialization Test:%@",specializationMutString);
                    
                    NSString * aboutString=[specializationMutString mutableCopy];
                    
                    
                    aboutString = [aboutString substringFromIndex:1];
                    
                    cell.aboutLbl.text = aboutString;
                    

                    
                } @catch (NSException *exception) {
                    cell.aboutLbl.text=@"No info";
                    
                } @finally {
                    
                }
                
                
                
            if([rating isEqual:[NSNull null]])
            {
                cell.starRatingView.value=0;
            }else
            {
                float ratingFloat = [rating floatValue];
                cell.starRatingView.value=ratingFloat;
            }
            
            cell.starRatingView.allowsHalfStars=YES;
            
            cell.starRatingView.allowsHalfStars=YES;
            
            cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
            
            cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
            
            cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
            
            [cell.starRatingView setUserInteractionEnabled:NO];
            
             cell.followeCountLabel.text=@"0 Followers";
            if([testString isEqual:[NSNull null]])
            {
                cell.followeCountLabel.text=@"0 Followers";
            }
            else
            {
                cell.followeCountLabel.text=followersString;
            }
            
            cell.leaderActiveSts.text=@"Inactive";
            
            if([[[self.followPlusArray objectAtIndex:indexPath.row ]objectForKey:@"created"] isEqual:[NSNull null]])
            {
                cell.leaderActiveSts.text=@"Inactive";
                
            }else
            {
                NSString *dateStr =[NSString stringWithFormat:@"%@",[[self.followPlusArray objectAtIndex:indexPath.row ]objectForKey:@"created"]];
                
                NSLog(@"%@",dateStr);
                
                // Convert string to date object
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
                [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                NSDate *date = [dateFormat dateFromString:dateStr];
                
                
                NSString *differnce = [self remaningTime:date endDate:[NSDate date]];
                
                cell.leaderActiveSts.text=[NSString stringWithFormat:@"(last active %@ ago)",differnce];
                
                NSLog(@"%@",differnce);
            }
            
            cell.gainPer.text=@"0% gain";
            if([[[self.followPlusArray objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"] isEqual:[NSNull null]])
            {
                cell.gainPer.text=@"0% gain";
                
            }else
            {
                NSString * gain=[[self.followPlusArray objectAtIndex:indexPath.row ]objectForKey:@"gainpercent"];
                
                float gainFloat=[gain floatValue];
                NSString * str=@"%";
                cell.gainPer.text=[NSString stringWithFormat:@"%.2f%@ gain",gainFloat,str];
            }
            
            
            //        starOneImg1 = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 15, 15)];
            //        [starOneImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starOneImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starOneImg1.selected = YES;
            //        [starOneImg1 addTarget: self action: @selector(starOneImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starOneImg1];
            //
            //        starTwoImg1 = [[UIButton alloc] initWithFrame:CGRectMake(30, 0, 15, 15)];
            //        [starTwoImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starTwoImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starTwoImg1.selected = YES;
            //        [starTwoImg1 addTarget: self action: @selector(starTwoImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starTwoImg1];
            //
            //        starThreeImg1 = [[UIButton alloc] initWithFrame:CGRectMake(50, 0, 15, 15)];
            //        [starThreeImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starThreeImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starThreeImg1.selected = YES;
            //        [starThreeImg1 addTarget: self action: @selector(starThreeImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starThreeImg1];
            //
            //        starFourImg1 = [[UIButton alloc] initWithFrame:CGRectMake(70, 0, 15, 15)];
            //        [starFourImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starFourImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starFourImg1.selected = YES;
            //        [starFourImg addTarget: self action: @selector(starFourImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starFourImg1];
            //
            //        starFiveImg1 = [[UIButton alloc] initWithFrame:CGRectMake(90, 0, 15, 15)];
            //        [starFiveImg1 setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
            //        [starFiveImg1 setImage:[UIImage imageNamed:@"ratingstarFilled"] forState:UIControlStateSelected];
            //        starFiveImg1.selected = YES;
            //        [starFiveImg1 addTarget: self action: @selector(starFiveImg1) forControlEvents:UIControlEventTouchUpInside];
            //        [cell.starView addSubview: starFiveImg1];
            
            cell.followBtn.tag=indexPath.row;
            
            [cell.followBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
//            NSString * localStr=[self.buttonArrayy objectAtIndex:indexPath.row];
//            
            [cell.followBtn setTitle:@"Follow +" forState:UIControlStateNormal];
                
                 // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
                cell.profileimage.image=[UIImage imageNamed:@"dpDefault"];
                
                NSURL *url;
                @try {
                    url =  [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"logourl"]]];
                } @catch (NSException *exception) {
                    
                    
                    
                } @finally {
                    
                }
                
              
                
                NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (data) {
                        UIImage *image = [UIImage imageWithData:data];
                        if (image) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                               
                                if (cell)
                                    cell.profileimage.image = image;
                            });
                        }
                    }
                }];
                [task resume];
                    
                

            
            return cell;
                
            }
            
            else
            {
                
                NoMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
                
                
                return cell;
                
            }
                
                
        
    }

    
        }
        

        
//        NoMonksCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];

        
    
        
    
    
       return 0;
    
}
// Date

/*!
 @discussion unused.
 @param from date ,to date.
 */

-(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}


// New Date and Time

/*!
 @discussion unused.
 @param startdate,enddate.
 */

-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    if(days>0)
    {
        if(days>1)
            durationString=[NSString stringWithFormat:@"%ld days",(long)days];
        else
            durationString=[NSString stringWithFormat:@"%ld day",(long)days];
        return durationString;
    }
    if(hour>0)
    {
        if(hour>1)
            durationString=[NSString stringWithFormat:@"%ld hrs",(long)hour];
        else
            durationString=[NSString stringWithFormat:@"%ld hrs",(long)hour];
        return durationString;
    }
    if(minutes>0)
    {
        if(minutes>1)
            durationString = [NSString stringWithFormat:@"%ld min",(long)minutes];
        else
            durationString = [NSString stringWithFormat:@"%ld min",(long)minutes];
        
        return durationString;
    }
    return @""; 
}



//follow method//

/*!
 @discussion when user tap on follow button this method will triger and here we subscribe or unsubscribe to leader.
 
 */
-(void)yourButtonClicked:(UIButton*)sender
{
//    if (sender.tag == 0)
//    {
//
//    }
    
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.followerTableView];
    
    
    
    NSIndexPath *indexPath = [self.followerTableView indexPathForRowAtPoint:buttonPosition];
    
    NSLog(@"Index path:%ld",(long)indexPath.row);
    FollowCell *cell = [self.followerTableView cellForRowAtIndexPath:indexPath];
    
    
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
       
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        //mixpanel//
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        
        [mixpanel identify:delegate1.userID];
        
        @try {
            NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
                                       @"cache-control": @"no-cache",
                                       };
            
            
            if(self.controller.active==YES&&self.controller.searchBar.text.length>=1)
            {
                
                if([filterArray  count]>0)
                {
                    if(segmentedControl.selectedSegmentIndex==0)
                    {
                        [mixpanel.people increment:@"point count" by:@-10];
                      userId=[NSString stringWithFormat:@"leaderid=%@", [[newFollowArray objectAtIndex:indexPath.row ]objectForKey:@"userid"]];
                        
                    }
                    
                    else if(segmentedControl.selectedSegmentIndex==1)
                    {
                        [mixpanel.people increment:@"point count" by:@10];
                        NSLog(@"%@",self.followPlusArray);
                        NSLog(@"%@",[NSString stringWithFormat:@"leaderid=%@",[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"userid"]]);
                        userId=[NSString stringWithFormat:@"leaderid=%@",[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"userid"]];
                        leaderUserName=[[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"firstname"];
                    }
                    
                }
                
            }
            
            else
            {
                
                
           
            
                
            if(segmentedControl.selectedSegmentIndex==0)
            {
                 [mixpanel.people increment:@"point count" by:@-10];
                userId=[NSString stringWithFormat:@"leaderid=%@",[followingUserId objectAtIndex:indexPath.row]];
                
            }
            
            else if(segmentedControl.selectedSegmentIndex==1)
            {
                [mixpanel.people increment:@"point count" by:@10];
                NSLog(@"%@",self.followPlusArray);
                NSLog(@"%@",[NSString stringWithFormat:@"leaderid=%@",[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"userid"]]);
                userId=[NSString stringWithFormat:@"leaderid=%@",[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"userid"]];
                leaderUserName=[NSString stringWithFormat:@"%@",[[self.followPlusArray objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
            }
                
            }
            
            NSString * clientId=[NSString stringWithFormat:@"&clientid=%@",delegate1.userID];
            
            
            NSMutableData *postData = [[NSMutableData alloc] initWithData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
            [postData appendData:[clientId dataUsingEncoding:NSUTF8StringEncoding]];
            //    if(segmentedControl.selectedSegmentIndex==0)
            //    {
            //         [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            //    }
            //    else
            //    {
            //    if(self.buttonArray.count>0)
            //    {
            //    if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Follow +"])
            //    {
            //    [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
            //    }else if([[self.buttonArray objectAtIndex:indexPath.row] isEqualToString:@"Following"])
            //    {
            //        [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
            //
            //    }
            //    }
            //    }
            
            
            NSArray * subscribeArray=@[@1];
            
            NSString * subscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",subscribeArray];
            NSString * sub = [subscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
            NSString * sub1 = [sub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
            
            NSArray * unSubscribeArray=@[];
            
            NSString * unSubscribeString=[NSString stringWithFormat:@"&subscriptiontype=%@",unSubscribeArray];
            NSString * unSub = [unSubscribeString stringByReplacingOccurrencesOfString:@"(" withString:@"{"];
            NSString * unSub1 = [unSub stringByReplacingOccurrencesOfString:@")" withString:@"}"];
            
            
            if(segmentedControl.selectedSegmentIndex==0)
            {
                
                
                [postData appendData:[@"&subscribe=false" dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[unSub1 dataUsingEncoding:NSUTF8StringEncoding]];
            }else if (segmentedControl.selectedSegmentIndex==1)
            {
                [postData appendData:[@"&subscribe=true" dataUsingEncoding:NSUTF8StringEncoding]];
                [postData appendData:[sub1 dataUsingEncoding:NSUTF8StringEncoding]];
                
            }
            
     if(segmentedControl.selectedSegmentIndex==1)
     {
         
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                                
                                                                NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                NSLog(@"%@",followDict);
                                                                
                                                                messageStr=[followDict objectForKey:@"message"];
                                                                
                                                                
                                                                
                                                            }
                                                            
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                if([messageStr isEqualToString:@"Subscriber created"])
                                                                {
                                                                    
                                                                    
                                                                    //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                                                                    
//                                                                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//                                                                    [mixpanel track:@"follow" properties:@{
//                                                                                                           @"leadername":leaderUserName
//                                                                                                           }];
                                                                    
                                                                    
                                                                    UIAlertController * alert = [UIAlertController
                                                                                                 alertControllerWithTitle:@""
                                                                                                 message:@"You have started following leader"
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    //Add Buttons
                                                                    
                                                                    UIAlertAction* okButton = [UIAlertAction
                                                                                               actionWithTitle:@"Ok"
                                                                                               style:UIAlertActionStyleDefault
                                                                                               handler:^(UIAlertAction * action) {
                                                                                                   //Handle your yes please button action here
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                               }];
                                                                    //Add your buttons to alert controller
                                                                    
                                                                    [alert addAction:okButton];
                                                                    
                                                                    
                                                                    [self presentViewController:alert animated:YES completion:nil];
                                                                    
                                                                    self.controller.searchBar.hidden=YES;
                                                                    self.controller.active=NO;
                                                                    
                                                                    [self segmentedControlChangedValue];
                                                                }
                                                            });
                                                            
                                                            
                                                            
                                                        }];
            [dataTask resume];
         
     }
            
            else if(segmentedControl.selectedSegmentIndex==0)
            {
                
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to unfollow a leader." preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                        [self presentViewController:alert animated:YES completion:^{
                
                
                
                        }];
                
                
                
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower",delegate1.baseUrl]]
                                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                                               timeoutInterval:10.0];
                            [request setHTTPMethod:@"POST"];
                            [request setAllHTTPHeaderFields:headers];
                            [request setHTTPBody:postData];
                            
                            NSURLSession *session = [NSURLSession sharedSession];
                            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                            if (error) {
                                                                                NSLog(@"%@", error);
                                                                            } else {
                                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                                NSLog(@"%@", httpResponse);
                                                                                
                                                                                NSMutableDictionary *followDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                                
                                                                                NSLog(@"%@",followDict);
                                                                                
                                                                                messageStr=[followDict objectForKey:@"message"];
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            
                                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                                
                                                                                
                                                                                if([messageStr isEqualToString:@"Subscriber created"])
                                                                                {
                                                                                    
                                                                                    
                                                                             //       NSString * leaderName = [NSString stringWithFormat:@"%@",[To be written]];
                                                                                    
                                                                                                                                                       //                                                            [cell.followBtn setTitle:[self.buttonArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
                                                                                    
                                                                                    self.controller.searchBar.hidden=YES;
                                                                                    self.controller.active=NO;
                                                                                    [self segmentedControlChangedValue];
                                                                                }
                                                                            });
                                                                            
                                                                            
                                                                            
                                                                        }];
                            [dataTask resume];

                
                        }];
                        
                        UIAlertAction * noAction=[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            
                            
                        }];

                        
                        
                        
                        [alert addAction:okAction];
                        [alert addAction:noAction];
                        
                    });

                
            }
            
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        

    }
    
    
    
    
    
   }



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // ProfileViewcontroller * profile=[self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
    
    NewProfile * profile1 = [self.storyboard instantiateViewControllerWithIdentifier:@"NewProfile"];
    
    if(segmentedControl.selectedSegmentIndex==0)
    {
        
        if(self.controller.active==YES&&self.controller.searchBar.text.length>=1)
        {
    
    @try {
        if(delegate1.leaderDetailArray.count>0)
        {
            [delegate1.leaderDetailArray removeAllObjects];
            
        }
        
        if(segmentedControl.selectedSegmentIndex==1)
        {
            NSString * local=[[monksDataDict objectForKey:@"results"]objectAtIndex:indexPath.row];
            
            delegate1.leaderIDWhoToFollow = [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"userid"];
            
            
            
            //            [delegate1.leaderDetailArray addObject:local];
            
            
            
            NSLog(@"LeaderID:::%@",delegate1.leaderIDWhoToFollow);
        }else if(segmentedControl.selectedSegmentIndex==0)
        {
            NSString * local = [newFollowArray objectAtIndex:indexPath.row];
            //        [delegate1.leaderDetailArray addObject:local];
            
            delegate1.leaderIDWhoToFollow = [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"userid"];
            
            NSLog(@"Leader ID:%@",delegate1.leaderIDWhoToFollow);
        }
        
        
        
        [self.navigationController pushViewController:profile1 animated:YES];
        
        //    dispatch_async(dispatch_get_main_queue(), ^{
        //
        //
        //
        //
        //        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
        //
        //
        //
        //        [self presentViewController:alert animated:YES completion:^{
        //
        //
        //
        //        }];
        //
        //
        //
        //        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //            
        //            
        //            
        //        }];
        //        
        //        
        //        
        //        [alert addAction:okAction];
        //        
        //    });
        

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
            
        }
        
        else
        {
            @try {
                if(delegate1.leaderDetailArray.count>0)
                {
                    [delegate1.leaderDetailArray removeAllObjects];
                    
                }
                
                if(segmentedControl.selectedSegmentIndex==1)
                {
                    NSString * local=[[monksDataDict objectForKey:@"results"]objectAtIndex:indexPath.row];
                    
                    delegate1.leaderIDWhoToFollow = [[[monksDataDict objectForKey:@"results"] objectAtIndex:indexPath.row] objectForKey:@"userid"];
                    
                    
                    
                    //            [delegate1.leaderDetailArray addObject:local];
                    
                    
                    
                    NSLog(@"LeaderID:::%@",delegate1.leaderIDWhoToFollow);
                }else if(segmentedControl.selectedSegmentIndex==0)
                {
                    NSString * local = [self.followDict objectAtIndex:indexPath.row];
                    //        [delegate1.leaderDetailArray addObject:local];
                    
                    delegate1.leaderIDWhoToFollow = [[self.followDict objectAtIndex:indexPath.row] objectForKey:@"userid"];
                    
                    NSLog(@"Leader ID:%@",delegate1.leaderIDWhoToFollow);
                }
                
                
                
                [self.navigationController pushViewController:profile1 animated:YES];
                
                //    dispatch_async(dispatch_get_main_queue(), ^{
                //
                //
                //
                //
                //        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
                //
                //
                //
                //        [self presentViewController:alert animated:YES completion:^{
                //
                //
                //
                //        }];
                //
                //
                //
                //        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //            
                //            
                //            
                //        }];
                //        
                //        
                //        
                //        [alert addAction:okAction];
                //        
                //    });
                
                
                
            }
            @catch (NSException * e) {
                NSLog(@"Exception: %@", e);
            }
            @finally {
                NSLog(@"finally");
            }
        }
        
    }
    
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        if(self.controller.isActive==YES&&self.controller.searchBar.text.length>=1)
        {
            
            @try {
                if(delegate1.leaderDetailArray.count>0)
                {
                    [delegate1.leaderDetailArray removeAllObjects];
                    
                }
                
                if(segmentedControl.selectedSegmentIndex==1)
                {
                    NSString * local=[[monksDataDict objectForKey:@"results"]objectAtIndex:indexPath.row];
                    
                    delegate1.leaderIDWhoToFollow = [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"userid"];
                    
                    
                    
                    //            [delegate1.leaderDetailArray addObject:local];
                    
                    
                    
                    NSLog(@"LeaderID:::%@",delegate1.leaderIDWhoToFollow);
                }else if(segmentedControl.selectedSegmentIndex==0)
                {
                    NSString * local = [newFollowArray objectAtIndex:indexPath.row];
                    //        [delegate1.leaderDetailArray addObject:local];
                    
                    delegate1.leaderIDWhoToFollow = [[newFollowArray objectAtIndex:indexPath.row] objectForKey:@"userid"];
                    
                    NSLog(@"Leader ID:%@",delegate1.leaderIDWhoToFollow);
                }
                
                
                
                [self.navigationController pushViewController:profile1 animated:YES];
                
                //    dispatch_async(dispatch_get_main_queue(), ^{
                //
                //
                //
                //
                //        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
                //
                //
                //
                //        [self presentViewController:alert animated:YES completion:^{
                //
                //
                //
                //        }];
                //
                //
                //
                //        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //
                //
                //
                //        }];
                //
                //
                //
                //        [alert addAction:okAction];
                //
                //    });
                
                
                
            }
            @catch (NSException * e) {
                NSLog(@"Exception: %@", e);
            }
            @finally {
                NSLog(@"finally");
            }
            
        }
        
        else
        {
            @try {
                if(delegate1.leaderDetailArray.count>0)
                {
                    [delegate1.leaderDetailArray removeAllObjects];
                    
                }
                
                if(segmentedControl.selectedSegmentIndex==1)
                {
                    NSString * local=[_followPlusArray objectAtIndex:indexPath.row];
                    
                    delegate1.leaderIDWhoToFollow = [[_followPlusArray objectAtIndex:indexPath.row] objectForKey:@"userid"];
                    
                    
                    
                    //            [delegate1.leaderDetailArray addObject:local];
                    
                    
                    
                    NSLog(@"LeaderID:::%@",delegate1.leaderIDWhoToFollow);
                }else if(segmentedControl.selectedSegmentIndex==0)
                {
                    NSString * local = [_followPlusArray objectAtIndex:indexPath.row];
                    //        [delegate1.leaderDetailArray addObject:local];
                    
                    delegate1.leaderIDWhoToFollow = [[_followPlusArray objectAtIndex:indexPath.row] objectForKey:@"userid"];
                    
                    NSLog(@"Leader ID:%@",delegate1.leaderIDWhoToFollow);
                }
                
                
                
                [self.navigationController pushViewController:profile1 animated:YES];
                
                //    dispatch_async(dispatch_get_main_queue(), ^{
                //
                //
                //
                //
                //        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Under Development." preferredStyle:UIAlertControllerStyleAlert];
                //
                //
                //
                //        [self presentViewController:alert animated:YES completion:^{
                //
                //
                //
                //        }];
                //
                //
                //
                //        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //            
                //            
                //            
                //        }];
                //        
                //        
                //        
                //        [alert addAction:okAction];
                //        
                //    });
                
                
                
            }
            @catch (NSException * e) {
                NSLog(@"Exception: %@", e);
            }
            @finally {
                NSLog(@"finally");
            }
        }
    }
    }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
//        if(newFollowArray.count>0||self.followDict.count>0)
//        {
//           
//            return 111;
//            
//        }
//        
//        else
//        {
//            return 393;
        //}
        
        
        if(self.controller.isActive==YES&&newFollowArray.count>0)
        {
            return 111;
        }
        
        
        
        
        else  if(self.followDict.count>0&&self.controller.isActive==NO)
        {
           return 111;
        }
        
        else
        {
             return 393;
            
        }

    }
    
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        if(self.controller.isActive==YES&&newFollowArray.count>0)
        {
            return 111;
        }
        
        
        
        
        else  if(self.followPlusArray.count>0&&self.controller.isActive==NO)
        {
            return 111;
        }
        
        else
        {
            return 393;
            
        }
        

    }
    
    return 393;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*!
 @discussion search controller become active.
 */



-(IBAction)searchAction:(id)sender
{
  
    
    self.controller = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.controller.searchResultsUpdater = self;
    self.controller.hidesNavigationBarDuringPresentation = NO;
    self.controller.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
    //    self.controller.searchBar.scopeButtonTitles = @[@"Posts", @"Users", @"Subreddits"];
    
   
    

   
    [self presentViewController:self.controller animated:YES
                     completion:nil];
    
    
}

/*!
 @discussion update the list according to user search .
 */
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        
        @try {
            if(newFollowArray.count>0)
            {
                [newFollowArray removeAllObjects];
                
            }
            if(firstName.count>0)
            {
                [firstName removeAllObjects];
            }
            
            if(self.followDict.count>0)
            {
            for(int i=0;i<self.followDict.count;i++)
            {
                
                [firstName addObject:[[self.followDict objectAtIndex:i]objectForKey:@"firstname"]];
            }
                
            
            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchController.searchBar.text];
            NSLog(@"%@",predicate);
            if(self.controller.searchBar.text.length>0)
            {
                filterArray =[[NSArray alloc]init];
                
                
                filterArray=[firstName  filteredArrayUsingPredicate:predicate];
                
                

                
                for(int i=0;i<filterArray.count;i++)
                {
                    detailsArray=[[NSMutableArray alloc]init];
                    
                    if(newFollowArray.count>0&&filterArray.count>0)
                    {
                        
                        
                        filterString1=[filterArray objectAtIndex:i];
                        for(int k=0;k<newFollowArray.count;k++)
                        {
                            [detailsArray addObject:[[newFollowArray objectAtIndex:k]objectForKey:@"firstname"]];
                            
                        }
                        
                    }

                    for(int j=0;j<self.followDict.count;j++)
                    {
                        NSString * filterString=[filterArray objectAtIndex:i];
                        NSString * responseString=[[self.followDict  objectAtIndex:j]objectForKey:@"firstname"];
                
                        
                        if(detailsArray.count>0)
                        {
                            
                            if([detailsArray containsObject:filterString1])
                            {
                                
                                
                            }
                            
                            
                            else
                            {
                                if([filterString isEqualToString:responseString])
                                {
                                    [newFollowArray addObject:[self.followDict objectAtIndex:j]];
                                }
                            }
                            
                            
                        }
                        
                        else
                        {
                            if([filterString isEqualToString:responseString])
                            {
                                [newFollowArray addObject:[self.followDict objectAtIndex:j]];
                            }
                        }

                    ///
                        
                    }
                }
            }
           
                
                if(self.controller.searchBar.text.length>=1)
                {
                    
                    [self.followerTableView reloadData];
                }
                
                if(self.controller.isActive==NO)
                {
                     [self.followerTableView reloadData];
                }
                
                
               
                

                
            }
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        
 
    }
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        @try {
            if(newFollowArray.count>0)
            {
                [newFollowArray removeAllObjects];
                
            }
            if(firstName.count>0)
            {
                [firstName removeAllObjects];
            }
            for(int i=0;i<[[monksDataDict objectForKey:@"results"]count];i++)
            {
                
                [firstName addObject:[[[monksDataDict objectForKey:@"results" ] objectAtIndex:i]objectForKey:@"firstname"]];
            }
            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self beginswith[c] %@",searchController.searchBar.text];
            NSLog(@"%@",predicate);
            if(self.controller.searchBar.text.length>0)
            {
                filterArray =[[NSArray alloc]init];
                
                
                filterArray=[firstName  filteredArrayUsingPredicate:predicate];
                
                
                
                
                for(int i=0;i<filterArray.count;i++)
                {
                    detailsArray=[[NSMutableArray alloc]init];
                    
                    if(newFollowArray.count>0&&filterArray.count>0)
                    {
                        
                        filterString1=[filterArray objectAtIndex:i];
                        for(int k=0;k<newFollowArray.count;k++)
                        {
                            [detailsArray addObject:[[newFollowArray objectAtIndex:k]objectForKey:@"firstname"]];
                            
                        }
                        
                    }

                    for(int j=0;j<[[monksDataDict objectForKey:@"results"]count];j++)
                    {
                        NSString * filterString=[filterArray objectAtIndex:i];
                        NSString * responseString=[[[monksDataDict objectForKey:@"results" ] objectAtIndex:j]objectForKey:@"firstname"];
                        
                        
                                               
                        
                        
                        
                        if(detailsArray.count>0)
                        {
                            
                            if([detailsArray containsObject:filterString1])
                            {
                                
                                
                            }
                            
                            
                            else
                            {
                                if([filterString isEqualToString:responseString])
                                {
                                    [newFollowArray addObject:[[monksDataDict objectForKey:@"results" ] objectAtIndex:j]];
                                }
                            }
                            
                            
                        }
                        
                        else
                        {
                            if([filterString isEqualToString:responseString])
                            {
                                [newFollowArray addObject:[[monksDataDict objectForKey:@"results" ] objectAtIndex:j]];
                            }
                        }

                        
                       
                    }
                }
            }
            if(self.controller.searchBar.text.length>=1)
            {
                
                [self.followerTableView reloadData];
            }
            
            if(self.controller.isActive==NO)
            {
                [self.followerTableView reloadData];
            }
            ;
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
    }
}

/*!
 @discussion filter button tapped.
 */
-(IBAction)filterAction:(id)sender
{
   
    if (filterFlag == false)
    {

    
    

        
        filterFlag = true;
    }
    else
    {
        filterFlag = false;

    }
    
}

//REACHABILITY//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    if(self.controller.isActive==YES)
    {
    [self dismissViewControllerAnimated:self.controller completion:nil];
    }
}

/*!
 @discussion used to hide hint screen image.
 */

-(void)hideImage
{
    
    [myImageView removeFromSuperview];
    
}



@end
