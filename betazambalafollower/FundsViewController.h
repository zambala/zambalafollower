//
//  FundsViewController.h
//  Funds
//
//  Created by Zenwise Technologies on 27/11/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FundsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *fundsView;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@property (weak, nonatomic) IBOutlet UILabel *netCashAvailable;
@property (weak, nonatomic) IBOutlet UILabel *amountTodayLabel;
@property (weak, nonatomic) IBOutlet UILabel *accontCashLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashTodayLabel;
@property NSMutableDictionary * zerodhaResponseDictionary;
@property NSMutableDictionary * upstoxResponseDicitionary;

- (IBAction)backAction:(id)sender;

@end
