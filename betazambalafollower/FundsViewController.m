//
//  FundsViewController.m
//  Funds
//
//  Created by Zenwise Technologies on 27/11/17.
//  Copyright © 2017 Zenwise Technologies. All rights reserved.
//

#import "FundsViewController.h"
#import "AppDelegate.h"

@interface FundsViewController ()
{
    
    AppDelegate * delegate1;
    
}

@end

@implementation FundsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
      delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.payButton.hidden=YES;
    
    self.fundsView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.fundsView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.fundsView.layer.shadowOpacity = 1.0f;
    self.fundsView.layer.shadowRadius = 1.0f;
    self.fundsView.layer.cornerRadius=1.0f;
    self.fundsView.layer.masksToBounds = NO;
    
    
    self.payButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.payButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.payButton.layer.shadowOpacity = 1.0f;
    self.payButton.layer.shadowRadius = 1.0f;
    self.payButton.layer.cornerRadius=1.0f;
    self.payButton.layer.masksToBounds = NO;
    
    [self.payButton addTarget:self action:@selector(onPayButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    if([delegate1.brokerNameStr isEqualToString:@"Zerodha"])
    {
    [self zerodhaFundsServer];
    }else if([delegate1.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self upStoxSever];
    }
    
    else
    {
        self.netCashAvailable.text=@"0.00";
        self.amountTodayLabel.text= @"0.00";
        self.accontCashLabel.text=@"0.00";
        self.cashTodayLabel.text=@"0.00";
        
        
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)upStoxSever
{
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate1.accessToken];
    NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                               @"authorization": access,
                               };
    NSString * urlStr=[NSString stringWithFormat:@"https://api.upstox.com/live/profile/balance"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.upstoxResponseDicitionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"upstox balance%@",self.upstoxResponseDicitionary);
                                                    }      
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        self.netCashAvailable.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"available_margin"]];
                                                        self.amountTodayLabel.text= [NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"payin_amount"]];
                                                        self.accontCashLabel.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"available_margin"]];
                                                        self.cashTodayLabel.text=[NSString stringWithFormat:@"%@",[[[self.upstoxResponseDicitionary objectForKey:@"data"]objectForKey:@"equity"] objectForKey:@"used_margin"]];
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
}

-(void)zerodhaFundsServer
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    NSString * urlStr=[NSString stringWithFormat:@"https://api.kite.trade/user/margins/equity?api_key=%@&access_token=%@",delegate1.APIKey,delegate1.accessToken];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.zerodhaResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        self.netCashAvailable.text=[NSString stringWithFormat:@"%@",[[self.zerodhaResponseDictionary objectForKey:@"data"] objectForKey:@"net"]];
                                                        self.amountTodayLabel.text= [NSString stringWithFormat:@"%@",[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"available"]objectForKey:@"intraday_payin"]];
                                                        self.accontCashLabel.text=[NSString stringWithFormat:@"%@",[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"available"]objectForKey:@"cash"]];
                                                        self.cashTodayLabel.text=[NSString stringWithFormat:@"%@",[[[self.zerodhaResponseDictionary objectForKey:@"data"]objectForKey:@"utilised"]objectForKey:@"debits"]];
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
}


-(void)onPayButtonTap
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
