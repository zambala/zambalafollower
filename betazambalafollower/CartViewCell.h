//
//  CartViewCell.h
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLbl, *monthLbl, *subLbl;

@property (strong, nonatomic) IBOutlet UIButton *removeBtn;

@end
