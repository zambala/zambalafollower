//
//  PortfolioOrderView.m
//  
//
//  Created by zenwise technologies on 20/03/17.
//
//

#import "PortfolioOrderView.h"
#import "PortfolioAdviceOrdersCell.h"

@interface PortfolioOrderView ()

@end

@implementation PortfolioOrderView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.orderTableView.delegate=self;
    self.orderTableView.dataSource=self;
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
            PortfolioAdviceOrdersCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
    
       cell.marketBtn.tag=indexPath.row;
    cell.limitBtn.tag=indexPath.row;
    cell.stopLossBtn.tag=indexPath.row;
    cell.buySellSegment.tag=indexPath.row;
    
    [cell.buySellSegment addTarget:self action:@selector(yourButtonClicked3:) forControlEvents:UIControlEventValueChanged];
    
    [cell.marketBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.limitBtn addTarget:self action:@selector(yourButtonClicked1:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.stopLossBtn addTarget:self action:@selector(yourButtonClicked2:) forControlEvents:UIControlEventTouchUpInside];
    
    
        return cell;
   
    
}

-(void)yourButtonClicked:(UIButton*)sender
{
    //    if (sender.tag == 0)
    //    {
    //
    //    }
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.orderTableView];
    
    
    
    NSIndexPath *indexPath = [self.orderTableView indexPathForRowAtPoint:buttonPosition];
    PortfolioAdviceOrdersCell *cell = [self.orderTableView cellForRowAtIndexPath:indexPath];
    
    cell.marketView.hidden=YES;
    cell.marketBtn.selected=YES;
    cell.limitBtn.selected=NO;
    cell.stopLossBtn.selected=NO;
    cell.marketBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    cell.limitBtn.backgroundColor=[UIColor clearColor];
    cell.stopLossBtn.backgroundColor=[UIColor clearColor];
    

}

-(void)yourButtonClicked1:(UIButton*)sender
{
    //    if (sender.tag == 0)
    //    {
    //
    //    }
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.orderTableView];
    
    
    
    NSIndexPath *indexPath = [self.orderTableView indexPathForRowAtPoint:buttonPosition];
    PortfolioAdviceOrdersCell *cell = [self.orderTableView cellForRowAtIndexPath:indexPath];
    
    cell.marketView.hidden=NO;
    cell.stopLossView.hidden=YES;
    cell.marketBtn.selected=NO;
    cell.limitBtn.selected=YES;
    cell.stopLossBtn.selected=NO;
    cell.limitLbl.hidden=NO;
    cell.limitTxt.hidden=NO;
    cell.limitTextFieldView.hidden=NO;
    cell.limitBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    cell.marketBtn.backgroundColor=[UIColor clearColor];
    cell.stopLossBtn.backgroundColor=[UIColor clearColor];


    
}

-(void)yourButtonClicked2:(UIButton*)sender
{
    //    if (sender.tag == 0)
    //    {
    //
    //    }
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.orderTableView];
    
    
    
    NSIndexPath *indexPath = [self.orderTableView indexPathForRowAtPoint:buttonPosition];
    PortfolioAdviceOrdersCell *cell = [self.orderTableView cellForRowAtIndexPath:indexPath];
    cell.marketView.hidden=NO;
    cell.stopLossView.hidden=NO;
    cell.marketBtn.selected=NO;
    cell.limitBtn.selected=NO;
    cell.stopLossBtn.selected=YES;
    cell.limitLbl.hidden=YES;
    cell.limitTxt.hidden=YES;
    cell.limitTextFieldView.hidden=YES;
    cell.stopLossBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    cell.limitBtn.backgroundColor=[UIColor clearColor];
    cell.marketBtn.backgroundColor=[UIColor clearColor];
}


-(void)yourButtonClicked3:(UISegmentedControl*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.orderTableView];
    
    
    
    NSIndexPath *indexPath = [self.orderTableView indexPathForRowAtPoint:buttonPosition];
    PortfolioAdviceOrdersCell *cell = [self.orderTableView cellForRowAtIndexPath:indexPath];
    
    if(cell.buySellSegment.selectedSegmentIndex==0)
    {
        cell.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
    }
    
    else if(cell.buySellSegment.selectedSegmentIndex==1)
    {
        cell.buySellSegment.tintColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
//    self.view.translatesAutoresizingMaskIntoConstraints = NO;
////    PortfolioAdviceOrdersCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    
//    if(cell.marketBtn.selected==YES)
//    {
//        return 306;
//    }
//    
//    else
//    {
//        return 447;
//    }
//
   return 306;
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
