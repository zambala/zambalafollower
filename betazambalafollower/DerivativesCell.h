//
//  DerivativesCell.h
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DerivativesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *derivativesImg1;

@property (weak, nonatomic) IBOutlet UIImageView *derivativesImg2;

@property (weak, nonatomic) IBOutlet UIImageView *profileImg;

@property (strong, nonatomic) IBOutlet UILabel *oLbl;
@property (strong, nonatomic) IBOutlet UILabel *stLbl;

@end
