//
//  OffersView.h
//  testing
//
//  Created by zenwise mac 2 on 1/24/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersView : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource,UISearchBarDelegate, UISearchControllerDelegate>
{
    NSMutableArray *imgArray;
}

@property (strong, nonatomic) IBOutlet  UICollectionView *offerCollView;

@property (strong, nonatomic) IBOutlet UIScrollView *offerScroll;

@property (strong, nonatomic) IBOutlet UISearchBar *seachCtrl;

@end
