//
//  TopPicks.h
//  PayPage
//
//  Created by zenwise mac 2 on 12/22/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface TopPicks : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *topPicksTbl;



@property (strong, nonatomic) IBOutlet UIImageView *profileImg;

@property NSMutableDictionary * topPicksResponseArray;
@property Reachability * reach;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end
