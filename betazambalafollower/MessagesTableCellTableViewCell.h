//
//  MessagesTableCellTableViewCell.h
//  testing
//
//  Created by zenwise technologies on 16/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesTableCellTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *messageLable;

@end
