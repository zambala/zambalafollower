//
//  FeedsView.h
//  testing
//
//  Created by zenwise mac 2 on 12/14/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "HHSlideView.h"
#import "Reachability.h"
#import <TwitterKit/TwitterKit.h>




@interface FeedsView : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource,NSURLConnectionDelegate, NSXMLParserDelegate, UIGestureRecognizerDelegate,UIActionSheetDelegate>
{
    NSMutableArray *imgArray, *imgArray1, *imgArray2, *arrImage2;
    NSMutableArray *descriptionArray, *descriptionArray1, *detailNameArray, *captionNameArray, *subSecNameArray, *headingNameArray, *timeArray;
    
    NSMutableArray *detailNameArray2, *timeArray2, *descriptionArray2;
    
    NSMutableArray *detailNameArray3, *timeArray3, *descriptionArray3;
    
    NSMutableArray *detailNameArray4, *timeArray4, *descriptionArray4;
    NSMutableArray *detailNameArray5, *timeArray5, *descriptionArray5;
    NSMutableArray *detailNameArray6, *timeArray6, *descriptionArray6;
    
    NSString *dateStr1,  *dateStr2, *dateStr3, *dateStr4, *dateStr5, *dateStr6;
    
    NSString  *nameString2, *timeString2;
    
    UIActionSheet *actionSheet;


  

    
    NSString *subSecNameStr2,  *detailStr2, *captionStr2, *headingNameStr2, *timeStr2;
    
    NSMutableArray *videoImageArray, *desArray;
    
    NSDictionary *xmlDictionary1, *xmlDictionary2, *xmlDictionary3, *xmlDictionary4, *xmlDictionary5, *xmlDictionary6;
    
    NSXMLParser *parser;
    NSMutableArray *feeds;
    
    
    NSString *subSecNameStr, *detailStr, *captionStr, *headingNameStr, *timeStr1;
    
    NSString *subSecNameStr3, *detailStr3, *captionStr3, *headingNameStr3, *timeStr3;
    NSString *subSecNameStr4, *detailStr4, *captionStr4, *headingNameStr4, *timeStr4;
    NSString *subSecNameStr5, *detailStr5, *captionStr5, *headingNameStr5, *timeStr5;
    NSString *subSecNameStr6, *detailStr6, *captionStr6, *headingNameStr6, *timeStr6;


    
}

@property (strong, nonatomic) IBOutlet UIView *cellTitleView1, *cellTitleView2, *feedContentView, *monksContentView;
//@property (strong, nonatomic) IBOutlet UILabel *titleLbl;

@property (strong, nonatomic) IBOutlet UITableView *monksTbl;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UILabel *headingLbl, *headingLbl2, *headingLbl3, *headingLbl4, *headingLbl5, *headingLbl6;


@property Reachability * reach;

@property (strong, nonatomic) NSString *descriptionStr;

@property (strong, nonatomic) IBOutlet  UICollectionView *collectionView1, *collectionView2, *collectionView3, *collectionView4, *collectionView5, *collectionView6;

@property (strong, nonatomic) IBOutlet UIScrollView *collectionViewScroll;

@property (strong, nonatomic) NSString *desStr2;
@property NSMutableDictionary *monksFeedResponseArray;
@property NSMutableArray *imageTextFilterArray;

- (IBAction)moreBtn7:(id)sender;

- (IBAction)moreBtn8:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView7;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView8;

@property (weak, nonatomic) IBOutlet UIView *socialFeedView;


@end
