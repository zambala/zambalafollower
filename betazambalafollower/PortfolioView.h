//
//  PortfolioView.h
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface PortfolioView : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *positionsTableView;

- (IBAction)backBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *bottomProfitAndLoss;

@property (strong, nonatomic) IBOutlet UIView *dummyBottomView;

@property (weak, nonatomic) IBOutlet UIView *postionView;
@property (weak, nonatomic) IBOutlet UIView *noPortfolioView;

@property (weak, nonatomic) IBOutlet UIView *dummyPotfolioView;
@property (strong, nonatomic) IBOutlet UILabel *netProfitAndLoss;
@property (strong, nonatomic) IBOutlet UILabel *assumptionsLabel;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;
@property (weak, nonatomic) IBOutlet UITableView *dummyTableView;


@property (weak, nonatomic) IBOutlet UIView *dematView;

@property (weak, nonatomic) IBOutlet UITableView *dematTableView;
@property Reachability * reach;


@end
