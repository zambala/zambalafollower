//
//  MTNetPosition.h
//  
//
//  Created by zenwise mac 2 on 9/4/17.
//
//

#import <Foundation/Foundation.h>

@interface MTNetPosition : NSObject

@property int       inRecordNo;
@property NSString*stExchange;
@property NSString*stSecurityID;
@property NSString*stClientID;
@property NSString*    stSymbol;
@property NSString*    stExpiryDate;
@property int       inBuyQty;
@property double    dbBuyAvg;
@property double    dbBuyAmount;
@property int       inSellQty;
@property double    dbSellAvg;
@property double    dbSellAmount;
@property int       inNetQty;
@property double    dbNetAvg;
@property double    dbNetAmount;
@property double    dbLTP;
@property double    dbMTM;
@property double    dbMultiplier; //LotSize
@property NSString*    stDateTime; //time_t
@property NSString*    stUserID;
@property double    dbLastMTM; //for Servailance
@property double    dbSettleMTM;
@property NSString*    stBaseCurrency;
@property NSString*    stSettleCurrency;
@property double    dbBuyAmountWithExp;
@property double    dbSellAmountWithExp;

+ (id)Mt7;

@end
