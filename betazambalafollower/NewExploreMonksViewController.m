//
//  NewExploreMonksViewController.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 7/13/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "NewExploreMonksViewController.h"
#import "NewExploreMonksTableViewCell.h"
#import "NewOffersByMonksViewController.h"
#import "AppDelegate.h"
#import <Mixpanel/Mixpanel.h>

@interface NewExploreMonksViewController ()
{
    NSMutableArray * leaderListArray;
    AppDelegate * delegate1;
}

@end

@implementation NewExploreMonksViewController
{
    NSString * urlStr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.navigationItem.title = @"Explore Monks";
    self.navigationController.navigationItem.leftBarButtonItem.title=@"";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    //mixpanel
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Premium Leaders Page"];
    
    
    // Do any additional setup after loading the view.
    
    
    [self serverHit];
}

-(void)serverHit
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                             };
    
      urlStr=[NSString stringWithFormat:@"%@subscriptionoffer/leaders/all?subscriptionid=%@",delegate1.baseUrl,delegate1.subscriptionId];
   
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        leaderListArray=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.exploreMonksTableView.delegate=self;
                                                        self.exploreMonksTableView.dataSource=self;
                                                        
                                                        [self.exploreMonksTableView reloadData];
                                                    });
                                                    

                                                }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return leaderListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewExploreMonksTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewExploreMonksTableViewCell" forIndexPath:indexPath];
    
    cell.leaderName.text=[[leaderListArray objectAtIndex:indexPath.row] objectForKey:@"firstname"];
    
    NSString * string=[NSString stringWithFormat:@"%@ total subscribers",[[leaderListArray objectAtIndex:indexPath.row] objectForKey:@"subscribercount"]];
    
    cell.subscriBerCount.text=string;
    
//    cell.subscriBerCount.text=[[leaderListArray objectAtIndex:indexPath.row] objectForKey:@"firstname"];
    NSString * local=[[leaderListArray objectAtIndex:indexPath.row] objectForKey:@"logourl"];
    
//    if([local isEqualToString:@"http://test.com"])
//       {
//    
//           cell.profileImg.image=[UIImage imageNamed:@"userimage.png"];
//       }
//    
//    else
//    {
//
    
    
    @try {
        cell.profileImg.image = nil; // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
        
        NSURL *url = [NSURL URLWithString:[[leaderListArray objectAtIndex:indexPath.row] objectForKey:@"logourl"]];
        
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NewExploreMonksTableViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            updateCell.profileImg.image = image;
                    });
                }
            }
        }];
        [task resume];
        

        
    } @catch (NSException *exception) {
        cell.profileImg.image=[UIImage imageNamed:@"userimage.png"];
        
        
    } @finally {
        
    }
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    delegate1.cartLeaderName=[NSString stringWithFormat:@"%@",[[leaderListArray objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
    
    delegate1.leaderid=[[leaderListArray objectAtIndex:indexPath.row] objectForKey:@"userid"];
    NewOffersByMonksViewController * offers = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOffersByMonksViewController"];
    [self.navigationController pushViewController:offers animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 87;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionView:(id)sender {
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Filer by:" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    UIAlertAction * All=[UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
         delegate1.subscriptionId=@"1";
          [self serverHit];
    }];
    
    UIAlertAction * adviceClosed=[UIAlertAction actionWithTitle:@"Premium Advices" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
          delegate1.subscriptionId=@"2";
        
        [self serverHit];
        
        
    }];
    
    UIAlertAction * followedOrSubscribed=[UIAlertAction actionWithTitle:@"Signals" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        delegate1.subscriptionId=@"3";
        
        [self serverHit];
    }];
    UIAlertAction * alerts = [UIAlertAction actionWithTitle:@"Dealer Mode" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        delegate1.subscriptionId=@"4";
        
        [self serverHit];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
        
    }];
    
    
    
    
    
    [alert addAction:All];
    [alert addAction:adviceClosed];
    [alert addAction:followedOrSubscribed];
    [alert addAction:alerts];
    [alert addAction:cancel];
    
}
@end
