//
//  AggrementViewController.m
//  betazambalafollower
//
//  Created by Zenwise Technologies on 15/12/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import "AggrementViewController.h"

@interface AggrementViewController ()

@end

@implementation AggrementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.title =@"Terms & Conditions";
    NSString *urlAddress = @"https://s3-ap-southeast-1.amazonaws.com/elasticbeanstalk-ap-southeast-1-507357262371/docs/ZenwiseAggrement.pdf";
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [self.aggrementWebView loadRequest:requestObj];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
