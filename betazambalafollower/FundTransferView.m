//
//  FundTransferView.m
//  
//
//  Created by zenwise technologies on 19/12/16.
//
//

#import "FundTransferView.h"
#import "TabBar.h"
#import "AppDelegate.h"

@interface FundTransferView ()
{
    AppDelegate * delegate1;
    NSMutableArray * segmentsArray;
}

@end

@implementation FundTransferView

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate1=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    segmentsArray=[[NSMutableArray alloc]init];
    [segmentsArray addObject:@"Equities"];
    [segmentsArray addObject:@"Commodities"];
    // Do any additional setup after loading the view.
//    self.fundTransferView1.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.fundTransferView1.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.fundTransferView1.layer.shadowOpacity = 1.0f;
//    self.fundTransferView1.layer.shadowRadius = 1.0f;
//    self.fundTransferView1.layer.cornerRadius=2.1f;
//    self.fundTransferView1.layer.masksToBounds = YES;
    
       
//    self.payBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
//    self.payBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
//    self.payBtn.layer.shadowOpacity = 1.0f;
//    self.payBtn.layer.shadowRadius = 1.0f;
//    self.payBtn.layer.cornerRadius=2.1f;
//    self.payBtn.layer.masksToBounds = YES;
    self.segmentPickerView.hidden=YES;
    [self.netBankingBtn addTarget:self action:@selector(netBankingAction) forControlEvents:UIControlEventTouchUpInside];
    [self netBankingAction];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showingMainView:(id)sender {
    
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
    [self presentViewController:tabPage animated:YES completion:nil];
}

- (IBAction)segmentBtn:(id)sender {
    self.segmentPickerView.hidden=NO;
    
    self.segmentPickerView.delegate=self;
    self.segmentPickerView.dataSource=self;
    
   
}
- (IBAction)upiBtnAction:(id)sender {
    
   
    self.netBankingBtn.selected=NO;
    self.upiBtn.selected=YES;
    delegate1.paymentString=@"UPI";
    //    self.selectionViewHgt.constant=270;
    self.enterVpaLbl.hidden=YES;
    self.vpaFld.hidden=YES;
    self.destinationVpa.hidden=YES;
    self.targetVpaLbl.hidden=YES;
}
- (IBAction)makePayementBtn:(id)sender {
    
    
}

-(void)netBankingAction
{
    self.netBankingBtn.selected=YES;
    self.upiBtn.selected=NO;
    delegate1.paymentString=@"NET";
    self.paymentViewHgt.constant=145;
    self.enterVpaLbl.hidden=YES;
    self.vpaFld.hidden=YES;
    self.destinationVpa.hidden=YES;
    self.targetVpaLbl.hidden=YES;
}

//picker view//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
        return segmentsArray.count;
    
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    
        return segmentsArray[row];
    
    
   
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
        NSInteger selectRow = [self.segmentPickerView selectedRowInComponent:0];
    
    NSString * title=segmentsArray[row];
        
    [self.segmentBtnOutlet setTitle:title forState:UIControlStateNormal];
    
}

@end
