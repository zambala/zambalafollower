//
//  EquitiesCell2.h
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EquitiesCell2 : UITableViewCell<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *portfolioAdviceOrdersBtn;

@property (weak, nonatomic) IBOutlet UILabel *oLabel;
@property (weak, nonatomic) IBOutlet UILabel *ltLabel;

@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UILabel *actedByLabel;
@property (strong, nonatomic) IBOutlet UILabel *sharesBroughtLabel;
@property (strong, nonatomic) IBOutlet UIButton *buySellButton;

@property (strong, nonatomic) IBOutlet UITableView *portfolioTblView;
@property NSMutableArray * portfolioArray;
@property (strong, nonatomic) IBOutlet UILabel *profileName;
@property (strong, nonatomic) IBOutlet UILabel *dateAndTime;
@property (strong, nonatomic) IBOutlet UILabel *assumedInvestment;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewheightConstraint;

@property (strong, nonatomic) IBOutlet UILabel *adivceName;

@end
