//
//  StockOrderView.m
//  testing
//
//  Created by zenwise technologies on 21/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "StockOrderView.h"
#import "PSWebSocket.h"
#import "AppDelegate.h"
#import "PortfolioView.h"
#import "TabBar.h"
#import "MainOrdersViewController.h"
#import "HomePage.h"
#import "TagEncode.h"
#import "TagDecode.h"
#import "MT.h"
#import "NewLoginViewController.h"
#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Endian.h>
#import "MTORDER.h"
#import<malloc/malloc.h>
#import <Mixpanel/Mixpanel.h>
@interface StockOrderView ()<PSWebSocketDelegate,NSStreamDelegate>

{
    AppDelegate * delegate2;
    

    
    NSDictionary * dict1;
    NSString * string1;
    NSMutableArray * LtpArray;
    NSMutableArray * changePerArray;
    NSMutableArray * shareListArray;
    NSMutableArray * shareListArrayCopy;
    NSMutableArray * instrumentTokens;
    NSMutableArray * filteredArray;
    NSMutableDictionary *instrumentTokensDict;
    NSMutableArray *allKeysList;
    bool temp;
    int value;
    int packetsLength;
    int packetsNumber;
    int instrumentToken;
    NSDictionary *upStoxOrderParms;
    NSData *postData;
    NSURL * url12;
    NSString * orderTypeStr;
    NSString * transTypeStr;
    NSString * productStr;
    NSString * method;
    NSString * segmentNew;
    BOOL detailViewCheck;
    NSString * buyCheck;
    NSString * sellCheck;
    NSString * orderId;
    NSString * statusCheck;
    NSString * orderTypeString;
    BOOL firstTimeCheck;
    NSString * quantityCheck;
    NSString * allQty;
    NSUserDefaults *prefs;
    NSString * valueStr;
    BOOL ltpCheck;
    NSString * quantityStr;
    BOOL incrementCheck;
    BOOL decrementCheck;
    int incrementInt;
    NSString * exchange;
    
    int decrement;
    int localInt;
    NSDictionary *headers;
    BOOL orderNvCheck;
    
    //mt
    int buySellInt;
//    NSOutputStream  *outputStream;
    MT *sharedManager;
    MTORDER * sharedManagerOrder;
    
    //    MTORDER *sharedManagerOrder;
    //    MTMaster *sharedManagerMaster;
    
    NSMutableArray * btPendingBuffer;
    
    uint8_t * buffer1;
    TagDecode * tagDecode;
    BOOL blWrite_;
    NSMutableArray * dataBuffer;
    NSInteger len;
    BOOL check;
    NSNumber *bytesRead;
    TagEncode * tag1;
    short shTag1;
    short shDataType;
    short shDataSize;
    NSMutableArray * testBtOutBuffer;
    
    BOOL blDataType;
    BOOL blDataSize;
    
    short shPendingBufferSize1;
    
    
    NSData *data123;
    short shStart1;
    
    NSUInteger count;
    
    NSString * swithStr;
    
    BOOL byteCheck;
    BOOL messageCheck;
    
    NSUInteger byteIndex;
    
    
    NSMutableData *data;
    int modifyRefferanceOrderNo;

    NSNumber * segmentNum;
    UIImageView * myImageView;
    
    
}

@end

@implementation StockOrderView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    //mt//
     sharedManager = [MT Mt1];
     sharedManagerOrder = [MTORDER Mt2];
    if(delegate2.stockHint==YES)
    {
        
        delegate2.stockHint=NO;
        
        UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
        
        myImageView =[[UIImageView alloc] initWithFrame:CGRectMake(0.0,0.0,currentWindow.frame.size.width,currentWindow.frame.size.height)];
        
        myImageView.image=[UIImage imageNamed:@"stockorder"];
        
        
        [currentWindow addSubview:myImageView ];
        [currentWindow bringSubviewToFront:myImageView];
    }
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [myImageView setUserInteractionEnabled:YES];
    [myImageView addGestureRecognizer:singleTap];
    
    
    
    NewLoginViewController *control = [[NewLoginViewController alloc]init];
    control.delegate = self;
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
       // [self.socket close];
        [self upStoxSocket];
    }
    
    
//    NewLoginViewController *control = [[NewLoginViewController alloc]init];
//    control.delegate = self;
//    [control OrderDecode];
    
    self.activityInd.hidden=YES;
    
    self.limitTxtView.hidden=YES;
    self.stopLossView.hidden=YES;
    self.marketView.hidden=YES;
   
    
     //[self upStoxSocket];
    delegate2.dismissCheck=@"";
    ltpCheck=true;
    firstTimeCheck=true;
    incrementCheck=true;
    decrementCheck=true;
    
    //adviceview//
    self.adviceView.hidden=YES;
    self.adviceViewHgt.constant=0;
    
    //opt view//
    
    self.optView.hidden=YES;
    
    //detail view 2//
    detailViewCheck=false;
    self.view2_Hgt.constant=0;
    self.view2.hidden=YES;

    
    self.limitTxtFld.delegate=self;
    self.stopLossPriceTxt.delegate=self;
    self.trigerPriceTxt.delegate=self;
    self.qtyTxt.delegate=self;
    
    self.headerView.hidden=YES;
    self.headerViewHgt.constant=0;
    self.port.hidden=YES;
    self.back.hidden=YES;
//    self.futureView.hidden=YES;
    
    //view 1 action//
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap)];
    [self.view1 addGestureRecognizer:singleFingerTap];
    
    if([delegate2.orderSegment containsString:@"FUT"]||[delegate2.orderSegment containsString:@"OPT"]||[delegate2.orderSegment containsString:@"CDS-FUT"]||[delegate2.orderSegment containsString:@"MCX"]||[delegate2.orderSegment containsString:@"CDS-OPT"])
    {
        
        NSDictionary *headers1 = @{ @"content-type": @"application/json",
                                    @"cache-control": @"no-cache",
                                    };
        
        NSString * segment;
        
        if([delegate2.orderSegment containsString:@"NFO-FUT"])
        {
            
            segment=@"NFO-FUT";
        }
        
        else if([delegate2.orderSegment containsString:@"BFO-FUT"])
        {
            
            segment=@"BFO-FUT";
        }
        
        else if([delegate2.orderSegment containsString:@"NFO-OPT"])
        {
            
            segment=@"NFO-OPT";
        }
        
        else if([delegate2.orderSegment containsString:@"BFO-OPT"])
        {
            
            segment=@"BFO-OPT";
        }
        
        else if([delegate2.orderSegment containsString:@"CDS-OPT"])
        {
            
            segment=@"CDS-OPT";
        }
        
        else if([delegate2.orderSegment containsString:@"CDS-FUT"])
        {
            
            segment=@"CDS-FUT";
        }
        
        else if([delegate2.orderSegment containsString:@"MCX"])
        {
            
            segment=@"MCX";
        }
        
        
        NSString * depthStr1=[NSString stringWithFormat:@"%@stock/fao/%@?segment=%@&limit=150&orderby=ASC",delegate2.baseUrl,delegate2.symbolDepthStr,segment];
        
        NSLog(@"%@",depthStr1);
        
        NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:depthStr1]
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:10.0];
        [request1 setHTTPMethod:@"GET"];
        [request1 setAllHTTPHeaderFields:headers1];
        
        @try {
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request1
                                                        completionHandler:^(NSData *data1, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                                
                                                                NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
                                                                
                                                                
                                                                @try {
                                                                       NSString * string=[[array  objectAtIndex:0]valueForKey:@"lotsize"];
                                                                    if(delegate2.depthLotSize.count>0)
                                                                    {
                                                                        [delegate2.depthLotSize removeAllObjects];
                                                                    }
                                                                    
                                                                    [delegate2.depthLotSize addObject:string];
                                                                } @catch (NSException *exception) {
                                                                    
                                                                } @finally {
                                                                    
                                                                }
                                                             
                                                               
                                                                
                                                                NSLog(@"%@",array);
                                                                
                                                            }
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                
                                                               [self futQuantityMethod];
                                                                
                                                                
                                                                
                                                                
                                                            });
                                                            
                                                            
                                                        }];
            [dataTask resume];
            
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        
    }

        
    else
    {
        
        [self futQuantityMethod];
    }
    
    //The event handling method
    
    
    if(delegate2.orderBool==true)
    {
        delegate2.orderBool=false;
//        self.headingLbl.text=@"ORDER";
        
        [self.buySellSegment setSelectedSegmentIndex:0];
        [self segmentAction];
        if([delegate2.POSITIONtransctionType isEqualToString:@"BUY"])
        {
            delegate2.POSITIONtransctionType=@"";
            sellCheck=@"Sellacted";
            [self sellBtn:_sellBtnOutlet];
        }
        
        else if([delegate2.POSITIONtransctionType isEqualToString:@"SELL"])
        {
           delegate2.POSITIONtransctionType=@"";
            
            sellCheck=@"Buyacted";
            [self butBtn:_buyBtnOutlet];
        }
        
        else
        {
             sellCheck=@"Sellacted";
             [self sellBtn:_sellBtnOutlet];
        }
       
       
        self.qtyTxt.text=delegate2.holdingsQty;
        allQty=delegate2.holdingsQty;
        self.headerView.hidden=YES;
        self.headerViewHgt.constant=0;
        self.port.hidden=YES;
        self.back.hidden=YES;
        self.companyLbl.text=delegate2.symbolDepthStr;
        
            self.segment.text=delegate2.orderSegment;
            
            segmentNew=self.segment.text;
        
    		if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
        
        @try {
            NSDictionary *headers = @{ @"content-type": @"application/json",
                                       @"cache-control": @"no-cache",
                                       @"postman-token": @"0db3995e-20e2-ba24-4fa2-fe574ca58153" };
            
            NSString * urlStr=[NSString stringWithFormat:@"%@stock/%@?exchange=%@",delegate2.baseUrl,delegate2.symbolDepthStr, delegate2.orderSegment];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"GET"];
            [request setAllHTTPHeaderFields:headers];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                                
                                                                NSMutableDictionary * instrumet=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                NSLog(@"dict %@",instrumet);
                                                                
                                                                NSString * portfoilioinstrumetToken=[NSString stringWithFormat:@"%@",[instrumet valueForKey:@"instrument_token"]];
                                                                
                                                                instrumentToken=[portfoilioinstrumetToken intValue];
                                                                
                                                                
                                        
                                                                delegate2.orderBool=false;
                                                                
                                                            }
                                                        }];
            [dataTask resume];
            
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
                
            }
        
        else
        {
            
             [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
////            delegate2.marketWatch1=[[NSMutableArray alloc]init];
            instrumentToken=[delegate2.orderinstrument intValue];


            if([delegate2.orderSegment containsString:@"NSE"]||[delegate2.orderSegment containsString:@"NSECM"])
            {
                exchange=@"NSECM";

            }

            else if([delegate2.orderSegment containsString:@"BSE"]||[delegate2.orderSegment containsString:@"BSECM"])
            {
                exchange=@"BSECM";

            }

            else if([delegate2.orderSegment containsString:@"NFO"]||[delegate2.orderSegment containsString:@"NSEFO"])
            {
                exchange=@"NSEFO";

            }

            else if([delegate2.orderSegment containsString:@"BFO"]||[delegate2.orderSegment containsString:@"BSEFO"])
            {
                exchange=@"BSEFO";

            }

            TagEncode * tag1 = [[TagEncode alloc]init];
            delegate2.mtCheck=true;


            //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];


            [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Broadcast];
            //        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
            [tag1 TagData:sharedManager.stSecurityID stringMethod:delegate2.orderinstrument];


            [tag1 TagData:sharedManager.stExchange stringMethod:exchange];


            [tag1 GetBuffer];


            [self newMessage1];

//                             [[NSNotificationCenter defaultCenter] postNotificationName:@"ordertowatch" object:nil];

            
            
        }
        [self limitAction];
          }
    
    else if(delegate2.tradeBtnBool==true)
    {
         [self.submitButton setTitle:@"SUBMIT ORDER" forState:UIControlStateNormal];
        sellCheck=@"";
        
        if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            
          //  [self subMethod];
        }
        
        [self futQuantityMethod];
        if([delegate2.orderSegment containsString:@"NFO-FUT"]||[delegate2.orderSegment containsString:@"NSEFO"])
        {
            
            
            self.optView.hidden=YES;
            self.segment.text=@"NFO-FUT";
           
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
             self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            
            
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"NFO";
            
        }
        else if([delegate2.orderSegment containsString:@"BFO-FUT"]||[delegate2.orderSegment containsString:@"BSEFO"])
        {
            self.optView.hidden=YES;
             self.segment.text=@"BFO-FUT";
         
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
               self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"BFO";
            
        }
        else if([delegate2.orderSegment containsString:@"CDS-FUT"])
        {
            self.segment.text=@"CDS-FUT";
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
              self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"CDS";
            
        }
        else if([delegate2.orderSegment containsString:@"CDS-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
        {
            self.segment.text=@"CDS-OPT CE";
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
              self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"CDS";
        }
        
        else if([delegate2.orderSegment containsString:@"CDS-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
        {
            self.segment.text=@"CDS-OPT PE";
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
              self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"CDS";
        }
        
        else if([delegate2.orderSegment containsString:@"NFO-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
        {
            self.optView.hidden=NO;
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"NFO";
        }
        
        else if([delegate2.orderSegment containsString:@"NFO-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
        {
            self.optView.hidden=NO;
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"NFO";
        }
        
        else if([delegate2.orderSegment containsString:@"BFO-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
        {
            self.optView.hidden=NO;
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"BFO";
        }
        
        else if([delegate2.orderSegment containsString:@"BFO-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
        {
            self.optView.hidden=NO;
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"BFO";
        }
        
        else if([delegate2.orderSegment containsString:@"MCX"])
        {
            self.optView.hidden=NO;
            delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            self.companyLbl.text=delegate2.symbolDepthStr;
            segmentNew=@"MCX";
            
            self.segment.text=@"MCS";
        }
        
        else
        {
            
            self.segment.text=delegate2.orderSegment;
            self.companyLbl.text=delegate2.symbolDepthStr;
            
        }


        instrumentToken=[delegate2.instrumentDepthStr intValue];
        delegate2.tradeBtnBool=false;
        [self.buySellSegment setSelectedSegmentIndex:0];
        [self segmentAction];
        self.headerView.hidden=YES;
        self.headerViewHgt.constant=0;
        self.port.hidden=YES;
        self.back.hidden=YES;
       
        
        
        [self.sellBtnOutlet.layer setBorderWidth:1.0f];
        [self.sellBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7]CGColor]];
        
        [self.sellBtnOutlet setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
        
        
        [self.buyBtnOutlet.layer setBorderWidth:1.0f];
        [self.buyBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
        
        [self.buyBtnOutlet setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
        
       
        [self tradeSettingsMethod];
    }
    else if(delegate2.editOrderBool==true)
    {
        NSLog(@"%@",delegate2.orderSegment);
        self.headingLbl.text=@"MODIFY ORDER";
        [self.submitButton setTitle:@"MODIFY ORDER" forState:UIControlStateNormal];
        [self futQuantityMethod];
        NSLog(@"%@",delegate2.qtyStr);
        
        self.qtyTxt.text=delegate2.qtyStr;
        self.futQtyTextField.text=delegate2.qtyStr;
        self.limitTxtFld.text=delegate2.editPrice;
    
        self.headerView.hidden=NO;
               self.headerViewHgt.constant=58.3;
        self.port.hidden=NO;
        self.back.hidden=NO;
        self.companyLbl.text=delegate2.symbolDepthStr;
       
        [self limitAction];
//        self.limitTxtFld.text=delegate2.editPrice;
        
        
            
            self.segment.text=delegate2.orderSegment;
        
        segmentNew=self.segment.text;
            
        
        

        instrumentToken=[delegate2.instrumentDepthStr intValue];
        
        if([delegate2.transaction isEqualToString:@"BUY"])
        {
            [self.sellBtnOutlet.layer setBorderWidth:1.0f];
            
            [self.sellBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
            
            [self.sellBtnOutlet setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
            
            
            [self.buyBtnOutlet setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
            
            buySellInt=1;
        }
        else if([delegate2.transaction isEqualToString:@"SELL"])
        {
            [self.sellBtnOutlet setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
            
            
            
            
            
            [self.buyBtnOutlet.layer setBorderWidth:1.0f];
            [self.buyBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
            
            [self.buyBtnOutlet setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
            
            buySellInt=2;
            
        }
        [self segmentAction];
        
        
       
       
    }
    
     else if(delegate2.wisdomCheck==true)
     {
         
         NSString * logo=[delegate2.leaderAdviceDetails objectAtIndex:5];
         
        
             self.profileImg.image = nil;
         
         
         
             
             NSURL *url = [NSURL URLWithString:logo];
             
             NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                 if (data) {
                     UIImage *image = [UIImage imageWithData:data];
                     if (image) {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             self.profileImg.image = image;
                             
                         });
                     }
                 }
             }];
             [task resume];
             
         
         
         
         
         
         
         
         
         
             [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
//             delegate2.marketWatch1=[[NSMutableArray alloc]init];
         
         
         if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
         {
             
             
         }
         
         else
         {
             if([delegate2.orderSegment containsString:@"NSE"]||[delegate2.orderSegment containsString:@"NSECM"])
             {
                 exchange=@"NSECM";
                 
             }
             
             else if([delegate2.orderSegment containsString:@"BSE"]||[delegate2.orderSegment containsString:@"BSECM"])
             {
                 exchange=@"BSECM";
                 
             }
             
             else if([delegate2.orderSegment containsString:@"NFO"]||[delegate2.orderSegment containsString:@"NSEFO"])
             {
                 exchange=@"NSEFO";
                 
             }
             
             else if([delegate2.orderSegment containsString:@"BFO"]||[delegate2.orderSegment containsString:@"BSEFO"])
             {
                 exchange=@"BSEFO";
                 
             }
             
             TagEncode * tag1 = [[TagEncode alloc]init];
             delegate2.mtCheck=true;
             
             
             //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];
             
             
             [tag1 TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Broadcast];
             
     [tag1 TagData:sharedManager.stSecurityID stringMethod:delegate2.orderinstrument];
             
             
             [tag1 TagData:sharedManager.stExchange stringMethod:exchange];
             
             
             [tag1 GetBuffer];
             

            
             
             [self newMessage1];
                 
             
             
             
         }
         
         self.adviceView.hidden=NO;
         self.adviceViewHgt.constant=125;
         self.localWisdomIDString = delegate2.wisdomGardemTickIDString;
         localInt=[self.localWisdomIDString intValue];
         
         
         self.profileName.text=[delegate2.leaderAdviceDetails objectAtIndex:0];
        
         
         NSString * dateStr=[delegate2.leaderAdviceDetails objectAtIndex:1];
         
         NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
         dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
         NSDate *yourDate = [dateFormatter dateFromString:dateStr];
         dateFormatter.dateFormat = @"dd MMM yyyy HH:mm:ss.SSS";
         NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
         
         NSString * finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
         
         self.date.text=finalDate;
         
         
         NSString * message=[NSString stringWithFormat:@"%@",[delegate2.leaderAdviceDetails objectAtIndex:4]];
         if([message containsString:@"@"])
         {
             
             self.detailAdvice.text=[message uppercaseString];
         }
         
         else
         {
         NSArray * mainArray=[[delegate2.leaderAdviceDetails objectAtIndex:4] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
         
         
         
         NSLog(@"array  %@",[mainArray objectAtIndex:0]);
         
         NSLog(@"array  %@",[mainArray objectAtIndex:1]);
         
         NSString * buySell=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:0]];
         
         if([buySell containsString:@"Buy"]||[buySell containsString:@"BUY"])
         {
             buySell=@"BUY";
             buySellInt=1;
         }
         else if([buySell containsString:@"Sell"]||[buySell containsString:@"SELL"])
         {
             buySell=@"SELL";
             buySellInt=2;
         }
         
         NSString * string=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
         
         
         
         
         
         NSArray * companyArray=[string componentsSeparatedByString:@";"];
         
         
         
         NSLog(@" array1 %@",companyArray);
         
         
         
         //               for (int i=1; i<[companyArray count];i++) {
         
         
         
         //                   NSString * string11=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
         //
         //
         //
         //                   NSArray * array2=[string11 componentsSeparatedByString:@";"];
         //
         //                   NSLog(@"%@",array2);
         
         NSString * companyName=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:0]];
         
         
         NSString * EP1=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:1]];
         NSString * TP=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:2]];
         NSString * SL=[NSString stringWithFormat:@"%@",[companyArray objectAtIndex:3]];
         
         
         
         
         
         
         
         
         
         
         NSString * sell = [NSString stringWithFormat:@"<html><body><center><div style='display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>%@ <b>&nbsp;%@</b></div></center> </body></html>",buySell,companyName];
         //  NSString * muthootfin =[NSString stringWithFormat:@"<html><body><center><b><div style='display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></center></body></html>",companyName];
         
         
         
         
         NSString * msg=[NSString stringWithFormat:@"%@",sell];
         
         NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[msg dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
         
         
         
         self.companyName.attributedText=attrStr;
         
         NSString * EPFinal =[EP1 stringByReplacingOccurrencesOfString:@"EP=" withString:@" "];
         NSString * TPFinal =[TP stringByReplacingOccurrencesOfString:@"TP=" withString:@" "];
         NSString * SLFinal =[SL stringByReplacingOccurrencesOfString:@"SL=" withString:@" "];
         
         NSString * htmlString = [NSString stringWithFormat:@"<html><body><center><div style='display:inline-block;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>@&nbsp;EP<b>%@</b>&nbsp;TP<b>%@</b>&nbsp;SL<b>%@</b></div><center></html></body>",EPFinal,TPFinal,SLFinal];
         
         //        NSString * ep = @"<html><body><center><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>@ EP </div><center></html></body>";
         //        NSString * value1 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",EPFinal];
         //        NSString * tp = @"<html><body><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>   TP</div></html></body>";
         //        NSString * value2 = [NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",TPFinal];
         //        NSString * sl = @"<html><body><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#4b4d52'>   SL</div></html></body>";
         //        NSString * value3 =[NSString stringWithFormat:@"<html><body><b><div style='display:inline-block;text-align:center;font-size:12.5px;font-family:Ubuntu;color:#1f2022'>  %@</div></b></html></body>",SLFinal] ;
         
         
         //NSString * msg1=[NSString stringWithFormat:@"%@%@  %@%@  %@%@",ep,value1,tp,value2,sl,value3];
         
         
         NSAttributedString * attrStr1 = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
         
         self.detailAdvice.attributedText=attrStr1;
             
         }
         
         
         if([delegate2.orderSegment containsString:@"NFO"]||[delegate2.orderSegment containsString:@"BFO"]||[delegate2.orderSegment containsString:@"CDS"]||[delegate2.orderSegment containsString:@"MCX"])
         {
             self.optView.hidden=NO;
             self.optCompany.text=delegate2.tradingSecurityDes;
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
             segmentNew=delegate2.orderSegment;
         }
         
         else
         {
             self.segment.text=delegate2.orderSegment;
             self.companyLbl.text=delegate2.symbolDepthStr;
             segmentNew=delegate2.orderSegment;
         }
         
         if([delegate2.orderBuySell containsString:@"BUY"])
         {
             transTypeStr=@"BUY";
             sellCheck=@"Buyacted";
             [self.buySellSegment setSelectedSegmentIndex:0];
             
             self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
             
             
             
             
             [self.sellBtnOutlet.layer setBorderWidth:1.0f];
             
             [self.sellBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
             
             [self.sellBtnOutlet setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
             
             
             [self.buyBtnOutlet setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
             
         }
         else if([delegate2.orderBuySell containsString:@"SELL"])
         {
             transTypeStr=@"SELL";
             sellCheck=@"Sellacted";
             
             [self.buySellSegment setSelectedSegmentIndex:1];
             
             
             self.buySellSegment.tintColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
             
             [self.sellBtnOutlet setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
             
             
             
             
             
             [self.buyBtnOutlet.layer setBorderWidth:1.0f];
             [self.buyBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
             
             [self.buyBtnOutlet setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
             
         }
         
         
         
         
         self.headerView.hidden=YES;
         self.headerViewHgt.constant=0;
         self.port.hidden=YES;
         self.back.hidden=YES;
         
         self.marketBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
         
         instrumentToken=[delegate2.orderinstrument intValue];
         
         [self tradeSettingsMethod];
         
      
         
//
//             else if([delegate2.depth isEqualToString:@"MARKETWATCH"])
//             {
//
//
//                 [[NSNotificationCenter defaultCenter] postNotificationName:@"ordertowatch" object:nil];
//             }
         
         
//         }
         
    

         
         delegate2.wisdomCheck=false;
         
         
     }
    
    else
    {
         [self.submitButton setTitle:@"SUBMIT ORDER" forState:UIControlStateNormal];
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
             instrumentToken=[delegate2.orderinstrument intValue];
        }
        
        else
        {
            instrumentToken=[delegate2.orderinstrument intValue];
        }
       
        
        
        
        [self futQuantityMethod];
        
        if([delegate2.orderSegment containsString:@"NFO-FUT"])
        {
            self.optView.hidden=NO;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.tradingSecurityDes];
            delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"NFO";
            self.companyLbl.text=delegate2.symbolDepthStr;
             self.segment.text=delegate2.orderSegment;
            
        }
        else if([delegate2.orderSegment containsString:@"BFO-FUT"])
        {
            self.optView.hidden=NO;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.tradingSecurityDes];
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"BFO";
            self.companyLbl.text=delegate2.symbolDepthStr;
             self.segment.text=delegate2.orderSegment;
        }
        else if([delegate2.orderSegment containsString:@"CDS-FUT"])
        {
            self.segment.text=@"CDS-FUT";
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"CDS";
            self.companyLbl.text=delegate2.symbolDepthStr;
            
            
        }
        else if([delegate2.orderSegment containsString:@"CDS-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
        {
            self.segment.text=@"CDS-OPT CE";
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"CDS";
            self.companyLbl.text=delegate2.symbolDepthStr;
        }
        
        else if([delegate2.orderSegment containsString:@"CDS-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
        {
            self.segment.text=@"CDS-OPT PE";
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"CDS";
            self.companyLbl.text=delegate2.symbolDepthStr;
        }
        
        else if([delegate2.orderSegment containsString:@"NFO-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
        {
            self.optView.hidden=NO;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.tradingSecurityDes];
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"NFO";
            self.companyLbl.text=delegate2.symbolDepthStr;
        }
        
        else if([delegate2.orderSegment containsString:@"NFO-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
        {
            self.optView.hidden=NO;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.tradingSecurityDes];
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"NFO";
            self.companyLbl.text=delegate2.symbolDepthStr;
        }
        
        else if([delegate2.orderSegment containsString:@"BFO-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
        {
            self.optView.hidden=NO;
            self.optCompany.text=[NSString stringWithFormat:@"%@ ",delegate2.tradingSecurityDes];
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"BFO";
            self.companyLbl.text=delegate2.symbolDepthStr;
        }
        
        else if([delegate2.orderSegment containsString:@"BFO-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
        {
            self.optView.hidden=NO;
            
            self.optCompany.text=[NSString stringWithFormat:@"%@ ",delegate2.tradingSecurityDes];
             delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            segmentNew=@"BFO";
            self.companyLbl.text=delegate2.symbolDepthStr;
        }
        else if([delegate2.orderSegment containsString:@"MCX"])
        {
            self.optView.hidden=NO;
            delegate2.symbolDepthStr=delegate2.tradingSecurityDes;
            self.optCompany.text=[NSString stringWithFormat:@"%@",delegate2.symbolDepthStr];
            segmentNew=@"MCX";
            
            self.segment.text=@"MCX";
        }
        
        
        
        else
        {

        self.segment.text=delegate2.orderSegment;
            segmentNew=delegate2.orderSegment;
            self.companyLbl.text=delegate2.symbolDepthStr;
            
        }
        
        if([delegate2.orderBuySell containsString:@"BUY"])
        {
            transTypeStr=@"BUY";
            sellCheck=@"Buyacted";
            [self.buySellSegment setSelectedSegmentIndex:0];
            
            self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
    
            buySellInt=1;
            
            
            [self.sellBtnOutlet.layer setBorderWidth:1.0f];
            
             [self.sellBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
            
            [self.sellBtnOutlet setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
            
            
            [self.buyBtnOutlet setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
            
        }
        else if([delegate2.orderBuySell containsString:@"SELL"])
        {
            transTypeStr=@"SELL";
            sellCheck=@"Sellacted";
            
            [self.buySellSegment setSelectedSegmentIndex:1];
            
            
            self.buySellSegment.tintColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
            
            [self.sellBtnOutlet setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
            
           
            buySellInt=2;
            
            
            [self.buyBtnOutlet.layer setBorderWidth:1.0f];
            [self.buyBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
            
             [self.buyBtnOutlet setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
            
        }

        
          
        
        self.headerView.hidden=YES;
        self.headerViewHgt.constant=0;
        self.port.hidden=YES;
        self.back.hidden=YES;
        
          self.marketBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
        
        [self tradeSettingsMethod];
        
        
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    
    
    [self.view addGestureRecognizer:tap];
    
    
    NSLog(@"%@",delegate2.symbolDepthStr);
//    transTypeStr=@"BUY";
//    orderTypeStr=@"MARKET";
//    self.marketBtn.selected=YES;
   
    
    
    
    
    
    //instrument token//
    
   
    
   
    
    

    
    [self.marketBtn addTarget:self action:@selector(marketAction) forControlEvents:UIControlEventTouchUpInside];
    
     [self.limitBtn addTarget:self action:@selector(limitAction) forControlEvents:UIControlEventTouchUpInside];
    
     [self.stopLossBtn addTarget:self action:@selector(stopLossAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buySellSegment addTarget:self action:@selector(segmentAction) forControlEvents:UIControlEventValueChanged];
    
//    self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void)marketAction
{
    self.marketView.hidden=YES;
    self.marketBtn.selected=YES;
    self.limitBtn.selected=NO;
    self.stopLossBtn.selected=NO;
    self.marketBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.limitBtn.backgroundColor=[UIColor clearColor];
    self.stopLossBtn.backgroundColor=[UIColor clearColor];
    orderTypeStr=@"MARKET";
   
    
    
}

-(void)limitAction
{
    self.marketView.hidden=NO;
    self.stopLossView.hidden=YES;
    self.marketBtn.selected=NO;
    self.limitBtn.selected=YES;
    self.stopLossBtn.selected=NO;
    self.limitLbl.hidden=NO;
        self.limitTxtView.hidden=NO;
    self.limitBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.marketBtn.backgroundColor=[UIColor clearColor];
    self.stopLossBtn.backgroundColor=[UIColor clearColor];
    orderTypeStr=@"LIMIT";
    
   self.limitTxtFld.hidden=NO;
    
   
    
   
    if([sellCheck containsString:@"Buyacted"])
    {
        NSString * localStr=self.askPriceLbl.text;
        self.limitTxtFld.text=localStr;
    }
    else if([sellCheck containsString:@"Sellacted"])
    {
        NSString * localStr=self.bidPriceLbl.text;
        self.limitTxtFld.text=localStr;
    }
     }

-(void)stopLossAction
{
    self.marketView.hidden=NO;
    self.stopLossView.hidden=NO;
    self.marketBtn.selected=NO;
    self.limitBtn.selected=NO;
    self.stopLossBtn.selected=YES;
    self.limitLbl.hidden=YES;
    self.limitTxtFld.hidden=YES;
    
    
    self.limitTxtView.hidden=YES;
    self.stopLossBtn.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.limitBtn.backgroundColor=[UIColor clearColor];
    self.marketBtn.backgroundColor=[UIColor clearColor];
    orderTypeStr=@"SL";
}

-(void)segmentAction
{
    if(_buySellSegment.selectedSegmentIndex==0)
    {
        self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
        
        transTypeStr=@"BUY";
    }
    
    else if(_buySellSegment.selectedSegmentIndex==1)
    {
        self.buySellSegment.tintColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
        
        transTypeStr=@"SELL";
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)handleSingleTap
{
    
    if(detailViewCheck==true)
    {
        self.view2_Hgt.constant=0;
        self.view2.hidden=YES;
       
         self.dropDownImgView.image=[UIImage imageNamed:@"expandCard.png"];
        detailViewCheck=false;
    }
    
    else if(detailViewCheck==false)
    {
        self.view2_Hgt.constant=277;
        self.view2.hidden=NO;
        
         self.dropDownImgView.image=[UIImage imageNamed:@"collapseCard.png"];
        
        detailViewCheck=true;
    }
    
    
    
    //Do stuff here...
}

- (IBAction)submitBtn:(id)sender {
    
//    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
//    if([delegate2.symbolDepthStr containsString:@"&"])
//    {
//        
//        delegate2.symbolDepthStr=[delegate2.symbolDepthStr stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
//        
//    }
    
    
     if([delegate2.orderSegment containsString:@"NSE"]||[delegate2.orderSegment containsString:@"BSE"])
     {
         segmentNum=[NSNumber numberWithInt:1];
         
     }
    
    else  if([delegate2.orderSegment containsString:@"NFO"]||[delegate2.orderSegment containsString:@"BFO"])
    {
        segmentNum=[NSNumber numberWithInt:2];
        
    }
     else  if([delegate2.orderSegment containsString:@"CDS"])
     {
         segmentNum=[NSNumber numberWithInt:3];
     }
    
     else  if([delegate2.orderSegment containsString:@"MCX"])
     {
         segmentNum=[NSNumber numberWithInt:4];
     }
    
    
    
    if([delegate2.loginActivityStr isEqualToString:@"OTP1"])
    {
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Open E-account" message:@"Please have a account with us to trade" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
        
    }else if ([delegate2.loginActivityStr isEqualToString:@"CLIENT"])
    {
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self.socket close];
        [self segmentCheck];
        
        if(self.qtyTxt.text.length>0||self.futQtyTextField.text.length>0)
        {
            NSLog(@"%@",delegate2.orderSegment);
            
            if([delegate2.orderSegment containsString:@"NSE"]||[delegate2.orderSegment containsString:@"BSE"])
            {
                productStr=@"CNC";
            }
            else
            {
                productStr=@"NRML";
            }
            
            if([delegate2.orderSegment containsString:@"CDS"])
            {
                delegate2.orderSegment=@"CDS";
                
            }
            
            if([delegate2.orderSegment containsString:@"FUT"]||[delegate2.orderSegment containsString:@"NFO"]||[delegate2.orderSegment containsString:@"BFO"]||[delegate2.orderSegment containsString:@"CDS"]||[delegate2.orderSegment containsString:@"MCX"])
            {
                allQty=self.futQtyTextField.text;
                
            }
            
            else
            {
                allQty=self.qtyTxt.text;
            }
            
           
//            if([delegate2.symbolDepthStr containsString:@"&"])
//            {
//                //              \"order_id\":\"%@\"}"
//
//                delegate2.symbolDepthStr=[delegate2.symbolDepthStr stringByReplacingOccurrencesOfString:@"&" withString: @"\"&\""];
//
//            }
            
     if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
     {
         
//         delegate2.symbolDepthStr=[delegate2.symbolDepthStr stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
//
//         NSString * test = [delegate2.symbolDepthStr stringb]
//
//         NSMutableCharacterSet *chars = NSCharacterSet.URLQueryAllowedCharacterSet.mutableCopy; [chars removeCharactersInRange:NSMakeRange('&', 1)]; // %26
//         delegate2.symbolDepthStr = [delegate2.symbolDepthStr stringByAddingPercentEncodingWithAllowedCharacters:chars];
         
     CFStringRef newString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)delegate2.symbolDepthStr, NULL, CFSTR("!*'();:@&=+@,/?#[]"), kCFStringEncodingUTF8);
         
         delegate2.symbolDepthStr=[NSString stringWithFormat:@"%@",newString];

       
            if ([orderTypeStr isEqualToString:@"MARKET"])
            {
                if(delegate2.editOrderBool==true)
                {
                    
                    orderTypeString=[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/%@?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=%@&validity=DAY",delegate2.orderID,delegate2.APIKey,delegate2.accessToken,delegate2.symbolDepthStr,segmentNew, transTypeStr, orderTypeStr, allQty,productStr];
                    
                    
                    method=@"PUT";
                    
                    
                }
                
                else
                {
                   
                  
                    
                    

                    
                    orderTypeString=[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=%@&validity=DAY",delegate2.APIKey,delegate2.accessToken,delegate2.symbolDepthStr,segmentNew, transTypeStr, orderTypeStr, allQty,productStr];
                    
                    
                    method=@"POST";
                }
            }
            
            else if ([orderTypeStr isEqualToString:@"LIMIT"])
            {
                if(delegate2.editOrderBool==true)
                {
                    
                    
                    
                    
                    orderTypeString=[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/%@?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=%@&validity=DAY&price=%@",delegate2.orderID,delegate2.APIKey,delegate2.accessToken,delegate2.symbolDepthStr,segmentNew, transTypeStr, orderTypeStr, allQty,productStr,self.limitTxtFld.text];
                    method=@"PUT";
                    
                }
                else
                {
                    
                 
                    
                    
    orderTypeString=[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=%@&validity=DAY&price=%@&tradingsymbol=%@",delegate2.APIKey,delegate2.accessToken,segmentNew,transTypeStr, orderTypeStr, allQty,productStr,self.limitTxtFld.text,delegate2.symbolDepthStr];
                    
      
                    
                    method=@"POST";
                }
            }
            
            
            else if([orderTypeStr isEqualToString:@"SL"])
            {
                if(delegate2.editOrderBool==true)
                {
                    orderTypeString=[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/%@?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=%@&validity=DAY&trigger_price=%@&stopLossPrice=%@",delegate2.orderID,delegate2.APIKey,delegate2.accessToken,delegate2.symbolDepthStr,segmentNew,transTypeStr,orderTypeStr,allQty,productStr,self.trigerPriceTxt.text,self.stopLossPriceTxt.text];
                    
                    
                    
                    method=@"PUT";
                    
                    
                }
                else
                {
                    orderTypeString=[NSString stringWithFormat:@"https://api.kite.trade/orders/regular/?api_key=%@&access_token=%@&tradingsymbol=%@&exchange=%@&transaction_type=%@&order_type=%@&quantity=%@&product=%@&validity=DAY&trigger_price=%@&stopLossPrice=%@",delegate2.APIKey,delegate2.accessToken,delegate2.symbolDepthStr,segmentNew,transTypeStr,orderTypeStr, allQty,productStr,self.trigerPriceTxt.text,self.stopLossPriceTxt.text];
                    
                    
                    method=@"POST";
                }
            }
         
         headers = @{ @"cache-control": @"no-cache",
                       @"content-type": @"application/x-www-form-urlencoded"
                      
                     };
         
        
        
     }
            
            else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
                
                if(delegate2.editOrderBool==true)
                {
               orderTypeString=[NSString stringWithFormat:@"https://api.upstox.com/live/orders/%@",delegate2.orderID];
                    
                    method=@"PUT";
                    
                    
                }
                else
                {
                orderTypeString=[NSString stringWithFormat:@"https://api.upstox.com/live/orders"];
                    
                    
                    method=@"POST";
                }
               
                
                NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
                
                headers = @{ @"cache-control": @"no-cache",
                               @"Content-Type" : @"application/json",
                              @"authorization":access,
                             @"x-api-key":@"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1"
                                           };
                
                NSString * segment;
                
                if([transTypeStr isEqualToString:@"BUY"])
                {
                    transTypeStr=@"b";
                }
                
                else if([transTypeStr isEqualToString:@"SELL"])
                {
                    transTypeStr=@"s";
                    
                }
                
                if([delegate2.orderSegment containsString:@"NSE"])
                {
                    segment=@"NSE_EQ";
                }
                
                else if([delegate2.orderSegment containsString:@"BSE"])
                {
                   segment=@"BSE_EQ";
                }
                
                else if([delegate2.orderSegment containsString:@"NFO"])
                {
                    segment=@"NSE_FO";
                }
                
                else if([delegate2.orderSegment containsString:@"BFO"])
                {
                    segment=@"BSE_FO";
                }
                
                else if([delegate2.orderSegment containsString:@"CDS"])
                {
                    segment=@"NCD_FO";
                }
                
                if([orderTypeStr isEqualToString:@"MARKET"])
                {
                    orderTypeStr=@"m";
                }
                
                else if([orderTypeStr isEqualToString:@"LIMIT"])
                {
                    orderTypeStr=@"l";
                }
                
                else if([orderTypeStr isEqualToString:@"SL"])
                {
                    orderTypeStr=@"sl";
                }
                NSString * productType=@"D";
                
                if(delegate2.editOrderBool==true)
                {
                    if([orderTypeStr isEqualToString:@"m"])
                    {
                        
                        upStoxOrderParms = @{ @"order_id":delegate2.orderID,
                                              @"quantity":allQty,
                                              @"order_type":orderTypeStr,
                                             
                                              };
                        
                        
                    }
                    
                    else if([orderTypeStr isEqualToString:@"l"])
                    {
                        
                        upStoxOrderParms = @{
                                             @"order_id":delegate2.orderID,
                                              @"quantity":allQty,
                                              @"order_type":orderTypeStr,
                                              @"price":self.limitTxtFld.text
                  
                                              };
                        
                        
                    }
                    
                }
                
                else
                {
                    
                    if([orderTypeStr isEqualToString:@"m"])
                    {
                        
                        upStoxOrderParms = @{ @"transaction_type":transTypeStr,
                                              @"exchange":segment,
                                              @"symbol":delegate2.symbolDepthStr,
                                              @"quantity":allQty,
                                              @"order_type":orderTypeStr,
                                              @"product":productType
                                             
                                              };
                        
                        
                    }
                    
                    else if([orderTypeStr isEqualToString:@"l"])
                    {
                        
                        upStoxOrderParms = @{ @"transaction_type":transTypeStr,
                                              @"exchange":segment,
                                              @"symbol":delegate2.symbolDepthStr,
                                              @"quantity":allQty,
                                              @"order_type":orderTypeStr,
                                              @"product":productType,
                                              @"price":self.limitTxtFld.text
                                             
                                              
                                              };
                        
                        
                    }
                    
                }
                
                postData = [NSJSONSerialization dataWithJSONObject:upStoxOrderParms options:0 error:nil];
                
              
         
            }
            
           
            
            
            
            
            
            
            @try {
                
                NSLog(@"%@",orderTypeString);
                
                [self orderPlacingMethod];
                
            }
            @catch (NSException *exception) {
                NSLog(@"Something missing...");
            }
            @finally {
                
            }
         
     
            
            //           MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
            //        [self.navigationController pushViewController:nextPage animated:YES];
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Order placed"];
            
//            MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
//            [self presentViewController:nextPage animated:YES completion:nil];
//
//
            

    }
        
    }
    
    else
    {
        if(self.qtyTxt.text.length>0||self.futQtyTextField.text.length>0)
        {
            if(delegate2.editOrderBool==true)
            {
                [self modifyBuyRequest];
            }
            
            else
            {
            
            if([delegate2.orderSegment containsString:@"NSE"])
            {
                
                delegate2.orderSegment=@"NSECM";
            }
            
            else if([delegate2.orderSegment containsString:@"BSE"])
            {
                
                delegate2.orderSegment=@"BSECM";
            }
            
            else if([delegate2.orderSegment containsString:@"NFO"])
            {
                
                delegate2.orderSegment=@"NSEFO";
            }
                
            else if([delegate2.orderSegment containsString:@"BFO"])
            {
                
                delegate2.orderSegment=@"BSEFO";
            }
            
            [self SendSocketBuyRequest];
                
            }
            
            
        }
    
        
    
    }
        
    }
}

//websockets//


- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    NSLog(@"The websocket handshake completed and is now open!");
    
    
    
    
    
    //     instrumentTokens=[[NSMutableArray alloc]init];
    
    
    
    //   [instrumentTokens addObject:[NSNumber numberWithInt:11547906]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:4911105]];
    //   [instrumentTokens addObject:[NSNumber numberWithInt:11508226]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:11535618]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:24026370]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:12344066]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:4954113]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:20517634]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:20517890]];
    //    [instrumentTokens addObject:[NSNumber numberWithInt:217138949]];
    
 if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
 {
    if(instrumentToken!=0)
    {
        
        NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":@[[NSNumber numberWithInt:instrumentToken]]};
        
        
        NSData *json;
        
        NSError * error;
        
        
        
        
        // Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                //
                
                NSLog(@"JSON: %@",string1);
                
            }
        }
        
        
        [self.socket send:string1];
        
        
        
        
        
        
        NSArray * mode=@[@"full",@[[NSNumber numberWithInt:instrumentToken]]];
        
        subscribeDict=@{@"a": @"mode", @"v": mode};
        
        
        
        //     Dictionary convertable to JSON ?
        if ([NSJSONSerialization isValidJSONObject:subscribeDict])
        {
            // Serialize the dictionary
            json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
            
            
            // If no errors, let's view the JSON
            if (json != nil && error == nil)
            {
                _message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                
                //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                
                NSLog(@"JSON: %@",_message);
                
            }
        }
        [self.socket send:_message];
        
    }
     
 }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
//        [self subMethod];
        
    }
    
   
    
}

-(void)unSubMethod
{
    
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                               @"authorization": access,
                               };
    
    NSString * urlString;
    if([delegate2.orderSegment containsString:@"NSE"])
    {
        NSString * exchange=@"nse_eq";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    
    else if([delegate2.orderSegment containsString:@"BSE"])
    {
        
        NSString * exchange=@"bse_eq";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    else if([delegate2.orderSegment containsString:@"CDS"])
    {
        
        NSString * exchange=@"ncd_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    else if([delegate2.orderSegment containsString:@"MCX"])
    {
        
        NSString * exchange=@"mcx_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
  
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",json);
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                       
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    
}
-(void)unSubFutMethod
{
    
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                               @"authorization": access,
                               };
    
    NSString * urlString;
    if([delegate2.orderSegment containsString:@"NFO"])
    {
        NSString * exchange=@"nse_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    
    else if([delegate2.orderSegment containsString:@"BFO"])
    {
        
        NSString * exchange=@"bse_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    else if([delegate2.orderSegment containsString:@"CDS"])
    {
        
        NSString * exchange=@"ncd_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    else if([delegate2.orderSegment containsString:@"MCX"])
    {
        
        NSString * exchange=@"mcx_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    
    
   
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",json);
                                                    }
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    
}



-(void)futSubMethod
{
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                               @"authorization": access,
                               };
    
    
    NSString * urlString;
    if([delegate2.orderSegment containsString:@"NFO"])
    {
        NSString * exchange=@"nse_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    
    else if([delegate2.orderSegment containsString:@"BSE"])
    {
        
        NSString * exchange=@"bse_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    else if([delegate2.orderSegment containsString:@"CDS"])
    {
        
        NSString * exchange=@"ncd_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    else if([delegate2.orderSegment containsString:@"MCX"])
    {
        
        NSString * exchange=@"mcx_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",json);
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        
                                                        //                                                        [self.socket close];
                                                        //                                                        [self upStoxSocket];
                                                        
                                                    });
                                                    
                                                    
                                                    
                                                }];
    [dataTask resume];
    
    
}






-(void)subMethod
{
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                               @"authorization": access,
                               };
    
    NSString * urlString;
    if([delegate2.orderSegment containsString:@"NSE"])
    {
        NSString * exchange=@"nse_eq";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    
    else if([delegate2.orderSegment containsString:@"BSE"])
    {
        
        NSString * exchange=@"bse_eq";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    else if([delegate2.orderSegment containsString:@"CDS"])
    {
        
        NSString * exchange=@"ncd_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    else if([delegate2.orderSegment containsString:@"MCX"])
    {
        
        NSString * exchange=@"mcx_fo";
        urlString=[NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,delegate2.symbolDepthStr];
    }
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",json);
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        
//                                                        [self.socket close];
//                                                        [self upStoxSocket];
                                                        
                                                    });
                                                    
                                                    
                                                    
                                                }];
    [dataTask resume];
    
    
}





- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    
    @try {
        
        NSLog(@"The websocket received a message: %@",message);
        
        
      if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
      {
        NSData * data=[NSKeyedArchiver archivedDataWithRootObject:message];
        
        
        
        
        //    NSLog(@"size of Object: %zd",malloc_size((__bridge const void *)(message)));
        
        if(data.length > 247)
        {
            
            
            NSLog(@"The websocket received a message: %@",message);
            
            
            id packets = [message subdataWithRange:NSMakeRange(0,2)];
            packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
            NSLog(@" number of packets %i",packetsNumber);
            
            int startingPosition = 2;
            
            for( int i =0; i< packetsNumber ; i++)
            {
                
                
                NSData * packetsLengthData = [message subdataWithRange:NSMakeRange(startingPosition,2)];
                packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
                startingPosition = startingPosition + 2;
                
                id packetQuote = [message subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
                
                
                
                
                id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0, 4)];
                int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
                
                id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
                int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
                
                
                float lastPriceFlaotValue = (float)lastPriceValue/100;
                
                
                
                id volume = [packetQuote subdataWithRange:NSMakeRange(16,4)];
                int32_t volumeValue = CFSwapInt32BigToHost(*(int32_t*)([volume bytes]));
                
                
                
                
                float volumeFloatValue = (float)volumeValue;
                
                
                id BID = [packetQuote subdataWithRange:NSMakeRange(20,4)];
                int32_t BIDValue = CFSwapInt32BigToHost(*(int32_t*)([BID bytes]));
                
                
                int BIDFloatValue = (int)BIDValue;
                
                id ASK = [packetQuote subdataWithRange:NSMakeRange(24,4)];
                int32_t ASLValue = CFSwapInt32BigToHost(*(int32_t*)([ASK bytes]));
                
                
                int ASKFloatValue = (int)ASLValue;
                
                id open = [packetQuote subdataWithRange:NSMakeRange(28,4)];
                int32_t openValue = CFSwapInt32BigToHost(*(int32_t*)([open bytes]));
                
                
                float openFloatValue = (float)openValue/100;
                
                id high = [packetQuote subdataWithRange:NSMakeRange(32,4)];
                int32_t highValue = CFSwapInt32BigToHost(*(int32_t*)([high bytes]));
                
                
                float highFloatValue = (float)highValue/100;
                
                id low = [packetQuote subdataWithRange:NSMakeRange(36,4)];
                int32_t lowValue = CFSwapInt32BigToHost(*(int32_t*)([low bytes]));
                
                
                float lowFloatValue = (float)lowValue/100;
                
                
                id closingPrice = [packetQuote subdataWithRange:NSMakeRange(40,4)];
                int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
                
                
                float closePriceFlaotValue = (float)closePriceValue/100;
                
                float changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                
                float changeFloatPercentageValue = ((lastPriceFlaotValue-closePriceFlaotValue) *100/closePriceFlaotValue);
                
                
                id marketDepthBID = [packetQuote subdataWithRange:NSMakeRange(44,60)];
                int32_t marketDepthValueBID = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBID bytes]));
                
                NSLog(@"%d",marketDepthValueBID);
                
                id marketDepthBIDQuantity = [marketDepthBID subdataWithRange:NSMakeRange(0,4)];
                int32_t marketDepthValueBIDQuantity = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDQuantity bytes]));
                
                NSLog(@"%i",marketDepthValueBIDQuantity);
                
                
                
                
                
                id marketDepthBIDPrice = [marketDepthBID subdataWithRange:NSMakeRange(4,4)];
                int32_t marketDepthValueBIDPriceInt = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDPrice bytes]));
                
                float marketDepthValueBIDPrice = (float)marketDepthValueBIDPriceInt/100;
                
                NSLog(@"%.2f",marketDepthValueBIDPrice);
                
                id marketDepthBIDQuantity2 = [marketDepthBID subdataWithRange:NSMakeRange(12,4)];
                int32_t marketDepthValueBIDQuantity2 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDQuantity2 bytes]));
                
                NSLog(@"%i",marketDepthValueBIDQuantity2);
                
                
                
                id marketDepthBIDPrice2 = [marketDepthBID subdataWithRange:NSMakeRange(16,4)];
                int32_t marketDepthValueBIDPriceInt2 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDPrice2 bytes]));
                
                float marketDepthValueBIDPrice2 = (float)marketDepthValueBIDPriceInt2/100;
                
                NSLog(@"%.2f",marketDepthValueBIDPrice2);
                
                id marketDepthBIDQuantity3 = [marketDepthBID subdataWithRange:NSMakeRange(24,4)];
                int32_t marketDepthValueBIDQuantity3 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDQuantity3 bytes]));
                
                NSLog(@"%i",marketDepthValueBIDQuantity3);
                
                
                
                id marketDepthBIDPrice3 = [marketDepthBID subdataWithRange:NSMakeRange(28,4)];
                int32_t marketDepthValueBIDPriceInt3 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDPrice3 bytes]));
                
                float marketDepthValueBIDPrice3 = (float)marketDepthValueBIDPriceInt3/100;
                
                NSLog(@"%.2f",marketDepthValueBIDPrice3);
                
                id marketDepthBIDQuantity4 = [marketDepthBID subdataWithRange:NSMakeRange(36,4)];
                int32_t marketDepthValueBIDQuantity4 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDQuantity4 bytes]));
                
                NSLog(@"%i",marketDepthValueBIDQuantity4);
                
                
                
                id marketDepthBIDPrice4 = [marketDepthBID subdataWithRange:NSMakeRange(40,4)];
                int32_t marketDepthValueBIDPriceInt4 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDPrice4 bytes]));
                
                float marketDepthValueBIDPrice4 = (float)marketDepthValueBIDPriceInt4/100;
                
                NSLog(@"%.2f",marketDepthValueBIDPrice4);
                
                id marketDepthBIDQuantity5 = [marketDepthBID subdataWithRange:NSMakeRange(48,4)];
                int32_t marketDepthValueBIDQuantity5 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDQuantity5 bytes]));
                
                NSLog(@"%i",marketDepthValueBIDQuantity5);
                
                
                
                id marketDepthBIDPrice5 = [marketDepthBID subdataWithRange:NSMakeRange(52,4)];
                int32_t marketDepthValueBIDPriceInt5 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthBIDPrice5 bytes]));
                
                float marketDepthValueBIDPrice5 = (float)marketDepthValueBIDPriceInt5/100;
                
                NSLog(@"%.2f",marketDepthValueBIDPrice5);
                
                
                
                
                id marketDepthASK = [packetQuote subdataWithRange:NSMakeRange(104,60)];
                int32_t marketDepthValueASK = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASK bytes]));
                
                NSLog(@"%d",marketDepthValueASK);
                
                id marketDepthASKQuantity = [marketDepthASK subdataWithRange:NSMakeRange(0,4)];
                int32_t marketDepthValueASKQuantity = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKQuantity bytes]));
                
                
                NSLog(@"%i",marketDepthValueASKQuantity);
                
                
                
                
                
                
                id marketDepthASKPrice = [marketDepthASK subdataWithRange:NSMakeRange(4,4)];
                int32_t marketDepthValueASKPriceInt = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKPrice bytes]));
                
                float marketDepthValueASKPrice = (float)marketDepthValueASKPriceInt/100;
                
                NSLog(@"%.2f",marketDepthValueASKPrice);
                //2
                
                id marketDepthASKQuantity2 = [marketDepthASK subdataWithRange:NSMakeRange(12,4)];
                int32_t marketDepthValueASKQuantity2 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKQuantity2 bytes]));
                
                
                NSLog(@"%i",marketDepthValueASKQuantity2);
                
                
                
                
                
                
                id marketDepthASKPrice2 = [marketDepthASK subdataWithRange:NSMakeRange(16,4)];
                int32_t marketDepthValueASKPriceInt2 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKPrice2 bytes]));
                
                float marketDepthValueASKPrice2 = (float)marketDepthValueASKPriceInt2/100;
                
                NSLog(@"%.2f",marketDepthValueASKPrice2);
                
                //3//
                
                id marketDepthASKQuantity3 = [marketDepthASK subdataWithRange:NSMakeRange(24,4)];
                int32_t marketDepthValueASKQuantity3 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKQuantity3 bytes]));
                
                
                NSLog(@"%i",marketDepthValueASKQuantity3);
                
                
                
                
                
                
                id marketDepthASKPrice3 = [marketDepthASK subdataWithRange:NSMakeRange(28,4)];
                int32_t marketDepthValueASKPriceInt3 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKPrice3 bytes]));
                
                float marketDepthValueASKPrice3 = (float)marketDepthValueASKPriceInt3/100;
                
                NSLog(@"%.2f",marketDepthValueASKPrice3);
                
                //4
                
                id marketDepthASKQuantity4 = [marketDepthASK subdataWithRange:NSMakeRange(36,4)];
                int32_t marketDepthValueASKQuantity4 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKQuantity4 bytes]));
                
                
                NSLog(@"%i",marketDepthValueASKQuantity4);
                
                
                
                
                
                
                id marketDepthASKPrice4 = [marketDepthASK subdataWithRange:NSMakeRange(40,4)];
                int32_t marketDepthValueASKPriceInt4 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKPrice4 bytes]));
                
                float marketDepthValueASKPrice4 = (float)marketDepthValueASKPriceInt4/100;
                
                NSLog(@"%.2f",marketDepthValueASKPrice4);
                
                
                //5
                
                
                id marketDepthASKQuantity5 = [marketDepthASK subdataWithRange:NSMakeRange(48,4)];
                int32_t marketDepthValueASKQuantity5 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKQuantity5 bytes]));
                
                
                NSLog(@"%i",marketDepthValueASKQuantity5);
                
                
                
                
                
                
                id marketDepthASKPrice5 = [marketDepthASK subdataWithRange:NSMakeRange(52,4)];
                int32_t marketDepthValueASKPriceInt5 = CFSwapInt32BigToHost(*(int32_t*)([marketDepthASKPrice5 bytes]));
                
                float marketDepthValueASKPrice5 = (float)marketDepthValueASKPriceInt5/100;
                
                NSLog(@"%.2f",marketDepthValueASKPrice5);
                
                
                
                
                startingPosition = startingPosition + packetsLength;
                
                
                
                
                @autoreleasepool {
                    
                    NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc]init];
                    [tmpDict setValue:[NSString stringWithFormat:@"%d",instrumentTokenValue] forKey:@"InstrumentToken"];
                    
                    NSString *tmpInstrument = [NSString stringWithFormat:@"%d",instrumentTokenValue];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",closePriceFlaotValue] forKey:@"ClosePriceValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue] forKey:@"LastPriceValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatValue] forKey:@"ChangeValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] forKey:@"ChangePercentageValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueBIDQuantity] forKey:@"BIDValue"];
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueBIDQuantity2] forKey:@"BIDValue1"];
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueBIDQuantity3] forKey:@"BIDValue2"];
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueBIDQuantity4] forKey:@"BIDValue3"];
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueBIDQuantity5] forKey:@"BIDValue4"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueASKQuantity] forKey:@"ASKValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueASKQuantity2] forKey:@"ASKValue1"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueASKQuantity3] forKey:@"ASKValue2"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueASKQuantity4] forKey:@"ASKValue3"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%i",marketDepthValueASKQuantity5] forKey:@"ASKValue4"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",openFloatValue] forKey:@"OpenValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",highFloatValue] forKey:@"HighValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lowFloatValue] forKey:@"LowValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%f",volumeFloatValue] forKey:@"VolumeValue"];
                    
                    
                    
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueBIDPrice] forKey:@"BIDPrice"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueBIDPrice2] forKey:@"BIDPrice2"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueBIDPrice3] forKey:@"BIDPrice3"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueBIDPrice4] forKey:@"BIDPrice4"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueBIDPrice5] forKey:@"BIDPrice5"];
                        
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueASKPrice] forKey:@"ASKPrice"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueASKPrice2] forKey:@"ASKPrice2"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueASKPrice3] forKey:@"ASKPrice3"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueASKPrice4] forKey:@"ASKPrice4"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%.2f",marketDepthValueASKPrice5] forKey:@"ASKPrice5"];
                        
                        [tmpDict setValue:[NSString stringWithFormat:@"%i",BIDFloatValue] forKey:@"TotalBid"];
                        [tmpDict setValue:[NSString stringWithFormat:@"%i",ASKFloatValue] forKey:@"TotalAsk"];
                        
                        
                        instrumentTokensDict=[[NSMutableDictionary alloc]init];
                        
                        [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                        
                        NSLog(@"%@",instrumentTokensDict);
                        
                    
                    
                    
                    
                    //  [shareListArray addObject:tmpDict];
                    
                }
                
                
            }
            
        }
          
          
      }
        
      else if ([delegate2.brokerNameStr isEqualToString:@"Upstox"])
      {
          
          NSString *detailsString = [[NSString alloc] initWithData:message encoding:NSASCIIStringEncoding];
          
          
          NSLog(@"%@",detailsString);
          
          NSArray * completeArray=[[NSArray alloc]init];
          
          completeArray= [detailsString componentsSeparatedByString:@";"] ;
          NSLog(@"%@",completeArray);
          
          
          
          for (int i=0; i<completeArray.count; i++) {
              
              
              
              
              NSMutableArray * individualCompanyArray=[[NSMutableArray alloc]init];
              NSString * innerStr=[NSString stringWithFormat:@"%@",[completeArray objectAtIndex:i]];
              
              individualCompanyArray=[[innerStr componentsSeparatedByString:@","] mutableCopy];
              
              
              NSString * symbolName=[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:2]];
              
              if(individualCompanyArray.count>5)
              {
              
              
              if([symbolName isEqualToString:delegate2.symbolDepthStr])
              {
                  
                  
                  
                  
                  
                  @autoreleasepool {
                      
                      NSString * instrument1=[NSString stringWithFormat:@"%d",instrumentToken];
                      
                      NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc]init];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",instrument1] forKey:@"InstrumentToken"];
                      
                      NSString *tmpInstrument = [NSString stringWithFormat:@"%@",instrument1];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:7]] forKey:@"ClosePriceValue"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:3]] forKey:@"LastPriceValue"];
                      
                      
                      float close=[[individualCompanyArray objectAtIndex:7] floatValue];
                      float ltp=[[individualCompanyArray objectAtIndex:3] floatValue];
                      
                      float change=ltp-close;
                      
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%.2f",change] forKey:@"ChangeValue"];
                      
                      float changePer = ((ltp-close) *100/close);
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changePer] forKey:@"ChangePercentageValue"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:18]] forKey:@"BIDValue"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:21]] forKey:@"BIDValue1"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:24]] forKey:@"BIDValue2"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:27]] forKey:@"BIDValue3"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:30]] forKey:@"BIDValue4"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:33]] forKey:@"ASKValue"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:36]] forKey:@"ASKValue1"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:39]] forKey:@"ASKValue2"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:42]] forKey:@"ASKValue3"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:45]] forKey:@"ASKValue4"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:4]] forKey:@"OpenValue"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:5]] forKey:@"HighValue"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:6]] forKey:@"LowValue"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:8]] forKey:@"VolumeValue"];
                      
                      
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:19]] forKey:@"BIDPrice"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:22]] forKey:@"BIDPrice2"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:25]] forKey:@"BIDPrice3"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:28]] forKey:@"BIDPrice4"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:31]] forKey:@"BIDPrice5"];
                      
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:34]] forKey:@"ASKPrice"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:37]] forKey:@"ASKPrice2"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:40]] forKey:@"ASKPrice3"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:43]] forKey:@"ASKPrice4"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:46]] forKey:@"ASKPrice5"];
                      
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:12]] forKey:@"TotalBid"];
                      [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:13]] forKey:@"TotalAsk"];
                      
                      
                      instrumentTokensDict=[[NSMutableDictionary alloc]init];
                      
                      [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                      
                      NSLog(@"%@",instrumentTokensDict);
                      
                      
                      
                      
                      
                      //  [shareListArray addObject:tmpDict];
                      
                  }
                  
                  
                  
                  
                  NSLog(@"%@",instrumentTokensDict);
                  
                  if(allKeysList.count>0)
                  {
                      [allKeysList removeAllObjects];
                  }
                  allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
                  
                  NSLog(@"%@",allKeysList);
                  
              }
              
          }
              
              
              
          }
          
      }
            
            
        
            
            if(allKeysList.count>0)
            {
                [allKeysList removeAllObjects];
            }
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            
            NSLog(@"%@",[allKeysList objectAtIndex:0]);
            
            NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList objectAtIndex:0]]];
            
            //        self.ltpLbl.text=[tmp objectForKey:@"LastPriceValue"];
            //        self.chngLbl.text=[tmp objectForKey:@"ChangeValue"];
            if(ltpCheck==true)
            {
                ltpCheck=false;
                NSString * ltp=[tmp objectForKey:@"LastPriceValue"];
                
                if([quantityStr isEqualToString:@"value"])
                {
                    valueStr=[prefs stringForKey:@"quantity"];
                    
                    int valueInt=[valueStr intValue];
                    
                    int ltpInt=[ltp intValue];
                    
                    int  quantityInt=valueInt/ltpInt;
                    
                    NSNumber * quantityNum=[NSNumber numberWithInt:quantityInt];
                    
                    NSLog(@"%@",quantityNum);
                    
                    self.qtyTxt.text=[quantityNum stringValue];
                    
                    
                    
                }
            }
            
            
            
            self.bidQtyLbl.text=[tmp objectForKey:@"BIDValue"];
            self.askSizeLbl.text=[tmp objectForKey:@"ASKValue"];
            
            self.bidQty1.text=[tmp objectForKey:@"BIDValue"];
            self.bidQty2.text=[tmp objectForKey:@"BIDValue1"];
            self.bidQty3.text=[tmp objectForKey:@"BIDValue2"];
            self.bidQty4.text=[tmp objectForKey:@"BIDValue3"];
            self.bidQty5.text=[tmp objectForKey:@"BIDValue4"];
            
            self.askQty1.text=[tmp objectForKey:@"ASKValue"];
            self.askQty2.text=[tmp objectForKey:@"ASKValue1"];
            self.askQty3.text=[tmp objectForKey:@"ASKValue2"];
            self.askQty4.text=[tmp objectForKey:@"ASKValue3"];
            self.askQty5.text=[tmp objectForKey:@"ASKValue4"];
            
            self.totalBid.text=[tmp objectForKey:@"TotalBid"];
            self.totalAsk.text=[tmp objectForKey:@"TotalAsk"];
            
            
            //        self.chngperLbl.text=delegate2.changePercentageStr;
            
            NSString * ltpStr=[tmp objectForKey:@"LastPriceValue"];
            if(ltpStr.length>7&&[delegate2.orderSegment containsString:@"CDS"])
            {
                NSString * str=[tmp valueForKey:@"LastPriceValue"];
                float priceValue=[str floatValue];
                
                float priceValue1= (float) priceValue/100000;
                
                NSString * priceStr=[NSString stringWithFormat:@"%.4f",priceValue1];
                
                self.ltpLbl.text=priceStr;
                
                NSString * str1=[tmp valueForKey:@"ChangeValue"];
                float chngPrice=[str1 floatValue];
                
                float changeFloat= (float) chngPrice/100000;
                
                NSString * chngStr=[NSString stringWithFormat:@"%.2f",changeFloat];
                
                self.chngLbl.text=chngStr;
                
                NSString * str2=[tmp valueForKey:@"BIDPrice"];
                float priceValue7=[str2 floatValue];
                
                float priceValue8= (float) priceValue7/100000;
                
                NSString * priceStr2=[NSString stringWithFormat:@"%.4f",priceValue8];
                
                self.bidPriceLbl.text=priceStr2;
                
                NSString * str3=[tmp valueForKey:@"ASKPrice"];
                float priceValue9=[str3 floatValue];
                
                float priceValue10= (float) priceValue9/100000;
                
                NSString * priceStr3=[NSString stringWithFormat:@"%.4f",priceValue10];
                
                
                NSString * bidStr1=[tmp valueForKey:@"BIDPrice"];
                float bidfloat1=[bidStr1 floatValue];
                
                float bidfinalFloat1= (float) bidfloat1/100000;
                
                NSString * bidfinalStr1=[NSString stringWithFormat:@"%.4f",bidfinalFloat1];
                
                 self.bidPrice1.text=bidfinalStr1;
                
                NSString * bidStr2=[tmp valueForKey:@"BIDPrice2"];
                float bidfloat2=[bidStr2 floatValue];
                
                float bidfinalFloat2= (float) bidfloat2/100000;
                
                NSString * bidfinalStr2=[NSString stringWithFormat:@"%.4f",bidfinalFloat2];
                
                self.bidPrice2.text=bidfinalStr2;
                
                NSString * bidStr3=[tmp valueForKey:@"BIDPrice3"];
                float bidfloat3=[bidStr3 floatValue];
                
                float bidfinalFloat3= (float) bidfloat3/100000;
                
                NSString * bidfinalStr3=[NSString stringWithFormat:@"%.4f",bidfinalFloat3];
                
                self.bidPrice3.text=bidfinalStr3;
                
                NSString * bidStr4=[tmp valueForKey:@"BIDPrice4"];
                float bidfloat4=[bidStr4 floatValue];
                
                float bidfinalFloat4= (float) bidfloat4/100000;
                
                NSString * bidfinalStr4=[NSString stringWithFormat:@"%.4f",bidfinalFloat4];
                
                self.bidprice4.text=bidfinalStr4;
                
                NSString * bidStr5=[tmp valueForKey:@"BIDPrice5"];
                float bidfloat5=[bidStr5 floatValue];
                
                float bidfinalFloat5= (float) bidfloat5/100000;
                
                NSString * bidfinalStr5=[NSString stringWithFormat:@"%.4f",bidfinalFloat5];
                
                self.bidPrice5.text=bidfinalStr5;
                
                
                NSString * askPrice=[tmp valueForKey:@"ASKPrice"];
                float askfloat=[askPrice floatValue];
                
                float askFinalFloat= (float) askfloat/100000;
                
                NSString * askFinal=[NSString stringWithFormat:@"%.4f",askFinalFloat];
                
                self.askPrice1.text=askFinal;
                
                NSString * askPrice2=[tmp valueForKey:@"ASKPrice2"];
                float askfloat2=[askPrice2 floatValue];
                
                float askFinalFloat2= (float) askfloat2/100000;
                
                NSString * askFinal2=[NSString stringWithFormat:@"%.4f",askFinalFloat2];
                
                self.askPrice2.text=askFinal2;
                
                NSString * askPrice3=[tmp valueForKey:@"ASKPrice3"];
                float askfloat3=[askPrice3 floatValue];
                
                float askFinalFloat3= (float) askfloat3/100000;
                
                NSString * askFinal3=[NSString stringWithFormat:@"%.4f",askFinalFloat3];
                
                self.askPrice3.text=askFinal3;
                
                
                NSString * askPrice4=[tmp valueForKey:@"ASKPrice4"];
                float askfloat4=[askPrice4 floatValue];
                
                float askFinalFloat4= (float) askfloat4/100000;
                
                NSString * askFinal4=[NSString stringWithFormat:@"%.4f",askFinalFloat4];
                
                self.askPrice4.text=askFinal4;
                
                NSString * askPrice5=[tmp valueForKey:@"ASKPrice5"];
                float askfloat5=[askPrice5 floatValue];
                
                float askFinalFloat5= (float) askfloat5/100000;
                
                NSString * askFinal5=[NSString stringWithFormat:@"%.4f",askFinalFloat5];
                
                self.askPrice5.text=askFinal5;


                
                
                self.askPriceLbl.text=priceStr3;
                if(firstTimeCheck==true)
                {
                    if([sellCheck containsString:@"Buyacted"])
                    {
                        NSString * localStr=self.askPriceLbl.text;
                        self.limitTxtFld.text=localStr;
                    }
                    else if([sellCheck containsString:@"Sellacted"])
                    {
                        NSString * localStr=self.bidPriceLbl.text;
                        self.limitTxtFld.text=localStr;
                    }
                    
                    firstTimeCheck=false;
                    
                }
                
                
                
                
            }
            else
            {
                self.ltpLbl.text=[tmp objectForKey:@"LastPriceValue"];
                self.chngLbl.text=[tmp objectForKey:@"ChangeValue"];
                self.bidPriceLbl.text=[tmp objectForKey:@"BIDPrice"];
                self.askPriceLbl.text=[tmp objectForKey:@"ASKPrice"];
                self.bidPrice1.text=[tmp objectForKey:@"BIDPrice"];
                self.bidPrice2.text=[tmp objectForKey:@"BIDPrice2"];
                self.bidPrice3.text=[tmp objectForKey:@"BIDPrice3"];
                self.bidprice4.text=[tmp objectForKey:@"BIDPrice4"];
                self.bidPrice5.text=[tmp objectForKey:@"BIDPrice5"];
                
                self.askPrice1.text=[tmp objectForKey:@"ASKPrice"];
                self.askPrice2.text=[tmp objectForKey:@"ASKPrice2"];
                self.askPrice3.text=[tmp objectForKey:@"ASKPrice3"];
                self.askPrice4.text=[tmp objectForKey:@"ASKPrice4"];
                self.askPrice5.text=[tmp objectForKey:@"ASKPrice5"];
                if(firstTimeCheck==true)
                {
                    if([sellCheck containsString:@"Buyacted"])
                    {
                        NSString * localStr=self.askPriceLbl.text;
                        self.limitTxtFld.text=localStr;
                    }
                    else if([sellCheck containsString:@"Sellacted"])
                    {
                        NSString * localStr=self.bidPriceLbl.text;
                        self.limitTxtFld.text=localStr;
                    }
                    firstTimeCheck=false;
                    
                    
                }
                
                
            }
            
            
            self.companyLbl.text=delegate2.symbolDepthStr;
            
            
            
            NSString * str=@"%";
            NSString * changeStrPer=[tmp valueForKey:@"ChangePercentageValue"];
            
            
            
            
            
            if([changeStrPer containsString:@"-"])
            {
                self.chngPerLbl.text = [NSString stringWithFormat:@"(%@%@)",changeStrPer,str];
                self.chngPerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
            }
            else{
                self.chngPerLbl.text = [NSString stringWithFormat:@"(%@%@)",changeStrPer,str];
                self.chngPerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
            }
            
            
            
            
            
            
            
       
        self.activityInd.hidden=YES;
        [self.activityInd stopAnimating];
        
      
        
        
            

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    
    
    
}

-(void)segmentCheck

{
    
    if([delegate2.orderSegment containsString:@"NFO-FUT"])
    {
        segmentNew=@"NFO";
    }
    else if([delegate2.orderSegment containsString:@"BFO-FUT"])
    {
        segmentNew=@"BFO";
    }
    else if([delegate2.orderSegment containsString:@"CDS-FUT"])
    {
        segmentNew=@"CDS";
    }
    else if([delegate2.orderSegment containsString:@"CDS-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
    {
        segmentNew=@"CDS";
    }
    
    else if([delegate2.orderSegment containsString:@"CDS-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
    {
        segmentNew=@"CDS";
    }
    
    else if([delegate2.orderSegment containsString:@"NFO-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
    {
        segmentNew=@"NFO";
    }
    
    else if([delegate2.orderSegment containsString:@"NFO-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
    {
        segmentNew=@"NFO";
    }
    
    else if([delegate2.orderSegment containsString:@"BFO-OPT"]&&[delegate2.orderSegment containsString:@"CE"])
    {
        segmentNew=@"BFO";
    }
    
    else if([delegate2.orderSegment containsString:@"BFO-OPT"]&&[delegate2.orderSegment containsString:@"PE"])
    {
        segmentNew=@"BFO";
    }
    
    else if([delegate2.orderSegment containsString:@"NSE"])
    {
        
        segmentNew=@"NSE";
    }
    
    else if([delegate2.orderSegment containsString:@"BSE"])
    {
        
        segmentNew=@"BSE";
    }

}


- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
    NSLog(@"The websocket received a message: %@", data);
}
- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}

//keyboard//

-(void)dismissKeyboard
{
    [self.qtyTxt resignFirstResponder];
    [self.limitTxtFld resignFirstResponder];
    [self.stopLossPriceTxt resignFirstResponder];
    [self.trigerPriceTxt resignFirstResponder];
}



- (IBAction)backBtn:(id)sender {
    if(delegate2.portfolioBackBool==true)
    {
//        PortfolioView * portView=[self.storyboard instantiateViewControllerWithIdentifier:@"port"];
//        
//        [self presentViewController:portView animated:YES completion:nil];
        delegate2.holdingCheck=@"";
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        delegate2.portfolioBackBool=false;
    }
    
    else
    {
        MainOrdersViewController * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
        
        [self presentViewController:tabPage animated:YES completion:nil];
        
        delegate2.editOrderBool=false;
        delegate2.modifyCheck=@"check";

    }
    
   
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    
    
    [self.limitTxtFld resignFirstResponder];
    [self.stopLossPriceTxt resignFirstResponder];
    [self.trigerPriceTxt resignFirstResponder];
    [self.qtyTxt resignFirstResponder];

    return YES;
}

-(void)futQuantityMethod
{
    NSLog(@"%@",delegate2.depthLotSize);
    if([delegate2.orderSegment containsString:@"FUT"]||[delegate2.orderSegment containsString:@"NFO"]||[delegate2.orderSegment containsString:@"BFO"]||[delegate2.orderSegment containsString:@"CDS"]||[delegate2.orderSegment containsString:@"MCX"])
    {
        self.futureView.hidden=NO;
        
        self.futQtyTextField.layer.borderWidth=1.0f;
        self.futQtyTextField.layer.borderColor=[[UIColor colorWithRed:(69/255.0) green:(133/255.0) blue:(157/255.0) alpha:1]CGColor];
        
        if(delegate2.depthLotSize.count>0)
        {
            NSString * localStr=[NSString stringWithFormat:@"%@",[delegate2.depthLotSize objectAtIndex:0]];
            self.futQtyTextField.text=localStr;
        }
        
        
        
        
    }
    else
    {
        self.futureView.hidden=YES;
        
        self.futQtyTextField.layer.borderWidth=0.0f;
        self.futQtyTextField.layer.borderColor=[[UIColor clearColor]CGColor];
       
    }
}
- (IBAction)sellBtn:(id)sender {
     transTypeStr=@"SELL";
//    [self.sellBtnOutlet setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
//    
//    [self.buyBtnOutlet setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.1]];
    
    [self.sellBtnOutlet setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
    
    [self.sellBtnOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
     [self.buyBtnOutlet setBackgroundColor:[UIColor clearColor]];
    
    [self.buyBtnOutlet.layer setBorderWidth:1.0f];
    [self.buyBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
    
    [self.buyBtnOutlet setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
    sellCheck=@"Sellacted";
    buySellInt=2;
    [self limitAction];
    
}

- (IBAction)butBtn:(id)sender {
     transTypeStr=@"BUY";
//    [self.sellBtnOutlet setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.1]];
//    
//    [self.buyBtnOutlet setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
    
    [self.sellBtnOutlet.layer setBorderWidth:1.0f];
    
    [self.sellBtnOutlet.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
    
    [self.sellBtnOutlet setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
    
    [self.sellBtnOutlet setBackgroundColor:[UIColor clearColor]];
    
    
    
    [self.buyBtnOutlet setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
    [self.buyBtnOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sellCheck=@"Buyacted";
    buySellInt=1;
     [self limitAction];
}

-(void)individualOrderMethod
{

    if(orderId.length>0)
    {
        @try {
            NSDictionary *headers ;
            NSString * urlStr;
            
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
       headers = @{ @"cache-control": @"no-cache",
                                   };
        
        urlStr=[NSString stringWithFormat:@"https://api.kite.trade/orders/%@?api_key=%@&access_token=%@",orderId,delegate2.APIKey,delegate2.accessToken];
            }
            
            else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
                headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                             @"authorization": access,
                                           };
                
               urlStr=[NSString stringWithFormat:@"https://api.upstox.com/live/orders/%@",orderId];
                
                
            }
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data1, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            NSDictionary *orderDict=[NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
                                                            NSLog(@"orderDict----%@",orderDict);
                                                            NSString *    statusCheck1;
                                                            
                                                            for(int i=0;i<[[orderDict objectForKey:@"data"] count];i++)
                                                            {
                                                                
                                                            statusCheck1=[NSString stringWithFormat:@"%@",[[[orderDict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"status"]];
                                                                
                                                                if([statusCheck1 isEqualToString:@"REJECTED"]||[statusCheck1 isEqualToString:@"rejected"])
                                                                {
                    
                                                                    
                                                                    statusCheck=statusCheck1;
                                                                    
                                                                }
                                                                
                                                                else if ([statusCheck1 isEqualToString:@"COMPLETE"]||[statusCheck1 isEqualToString:@"complete"])
                                                                {
                                                                    
                                                                    statusCheck=statusCheck1;
                                                                }
                                                                
                                                                else if ([statusCheck1 isEqualToString:@"OPEN"]||[statusCheck1 isEqualToString:@"open"])
                                                                {
                                                                    
                                                                   statusCheck=statusCheck1;
                                                                }
                                                                
                                                            }
                                                            
                                                           
                                                            
                                                            if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                            {
                                                                
                                                                statusCheck=[statusCheck uppercaseString];
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if([statusCheck isEqualToString:@"REJECTED"])
                                                            {
                                                                
                                                                delegate2.orderStatusCheck=@"REJECTED";
                                                                [self alertMethod];
                                                                
                                                                
                                                            }
                                                            
                                                            else if ([statusCheck isEqualToString:@"COMPLETE"])
                                                            {
                                                                delegate2.orderStatusCheck=@"COMPLETED";
                                                                [self alertMethod];
                                                                
                                                                
                                                            }
                                                            
                                                            else if ([statusCheck isEqualToString:@"OPEN"])
                                                            {
                                                                delegate2.orderStatusCheck=@"OPEN";
                                                                [self alertMethod];
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                        });
                                                    }];
        [dataTask resume];
        
    }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
    
   
    }
}

-(void)alertMethod
{
    self.activityInd.hidden=YES;
    [self.activityInd stopAnimating];
    
    if(delegate2.editOrderBool==true)
    {
        
        delegate2.editOrderBool=false;
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Order Status"
                                 message:@"Order was modified successfully"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
//                                    delegate2.holdingCheck=@"hold";
                                   //Handle your yes please button action here
                                  
                                       MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
                                       [self presentViewController:nextPage animated:YES completion:nil];
                                  
                                   
                                   
                               }];
    //Add your buttons to alert controller
    
    [alert addAction:okButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    }
    
    else
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Order Status"
                                     message:@"Order was placed successfully"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                      
                                           MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
                                           [self presentViewController:nextPage animated:YES completion:nil];
                                      
                                       
                                      
//                                       delegate2.holdingCheck=@"hold";
                                       //Handle your yes please button action here
                                      
                                       
                                       
                                   }];
        //Add your buttons to alert controller
        
        [alert addAction:okButton];
        
        
        [self presentViewController:alert animated:YES completion:nil];

    }
}
- (IBAction)incrementBtn:(id)sender {
    @try {
        id i= [delegate2.depthLotSize objectAtIndex:0];
        
        int j=[i intValue];
        
        int k=[i intValue];
        //    if(incrementCheck==true)
        //    {
        //        incrementInt=2;
        //
        //        incrementCheck=false;
        //    }
        
        
        if (j>=k) {
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber * myNumber = [f numberFromString:[NSString stringWithFormat:@"%@",self.futQtyTextField.text]];
            
            myNumber = @([myNumber intValue] + [i intValue]);
            
            
            
            j =(int)[myNumber integerValue];
            NSLog(@"i %d",j);
            NSString *convertNumber = [f stringForObjectValue:myNumber];
            
            //        incrementInt=incrementInt+1;
            
            
            
            self.futQtyTextField.text=convertNumber;
            
            decrementCheck=true;
        }
        

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    

}

- (IBAction)decrementBtn:(id)sender {
    
    @try {
        id i= [delegate2.depthLotSize objectAtIndex:0];
        if(self.futQtyTextField.text.length>0)
        {
            quantityCheck=self.futQtyTextField.text;
        }
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber * myNumber = [f numberFromString:[NSString stringWithFormat:@"%@",quantityCheck]];
        
        
        
        
        int  j=[myNumber intValue];
        
        NSLog(@"%i",j);
        
        NSNumberFormatter *f1 = [[NSNumberFormatter alloc] init];
        f1.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber * myNumber1 = [f1 numberFromString:[NSString stringWithFormat:@"%@",i]];
        
        
        
        
        int  k=[myNumber1 intValue];
        
        NSLog(@"%i",k);
        
        //    if(decrementCheck==true)
        //    {
        //        decrement=incrementInt;
        //
        //        decrementCheck=false;
        //    }
        
        
        
        if (j>k) {
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber * myNumber = [f numberFromString:[NSString stringWithFormat:@"%@",self.futQtyTextField.text]];
            
            myNumber = @([myNumber intValue] -k);
            j =(int)[myNumber integerValue];
            NSLog(@"i %i",j);
            NSString *convertNumber = [f stringForObjectValue:myNumber];
            
            //        decrement=decrement+1;
            
            self.futQtyTextField.text=convertNumber;
        }
        

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
   
}

-(void)tradeSettingsMethod
{
    prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    
    
    delegate2.orderStr=[prefs stringForKey:@"order"];
    
    if([delegate2.orderStr isEqualToString:@"Market"])
    {
        [self marketAction];
    }
    else if([delegate2.orderStr isEqualToString:@"Limit"])
    {
        [self limitAction];
    }
    else if([delegate2.orderStr isEqualToString:@"StoppLoss"])
    {
        [self stopLossAction];
    }
    
    else
    {
        [self marketAction];
    }
    
    quantityStr=[prefs stringForKey:@"quantityCheck"];
    
    if([quantityStr isEqualToString:@"quantity"])
    {
        self.qtyTxt.text=[prefs stringForKey:@"quantity"];
    }
    
    else if([quantityStr isEqualToString:@"value"])
    {
        valueStr=[prefs stringForKey:@"quantity"];
        
    }
    

    
}




-(void)viewWillAppear:(BOOL)animated
{
    if([delegate2.dismissCheck isEqualToString:@"dismissed"])
    {
        
    delegate2.dismissCheck=@"";
        
        if([delegate2.navigationCheck isEqualToString:@"TOP"])
        {
            delegate2.navigationCheck=@"";
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else
        {
             [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
        }
    
//    [self.navigationController popToViewController:homePage animated:YES];
        
        
        
    
            
            
            //    [self.navigationController popToViewController:homePage animated:YES];
        
        }
        
    
}

-(void)wisdomGardenTickCheckMethod
{
    @try {
        float totalPrice=[allQty floatValue]*[self.limitTxtFld.text floatValue];
        
        NSString * totalPriceString=[NSString stringWithFormat:@"%.2f",totalPrice];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                                };
    NSDictionary *parameters = @{ @"clientid": delegate2.userID,
                                  @"symbol": self.companyLbl.text,
                                  @"messageid":self.localWisdomIDString,
                                  @"qty":allQty,
                                  @"totalprice":totalPriceString,
                               @"tradeprice":self.limitTxtFld.text,
                                  @"exchangeorderid":orderId,
                                  @"segment":segmentNum,
                                  @"market":@"INDIA"
                                  };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@clienttrade",delegate2.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.localWisdomIDString=nil;
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                        {
                                                        
                                                        [self individualOrderMethod];
                                                        }
                                                        else
                                                        {
                                                            [self alertMethod];
                                                        }
                                                        
                                                    });

                                                }];
    [dataTask resume];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
}

-(void)watchMethod
{
   
    @try {
        float totalPrice=[allQty floatValue]*[self.limitTxtFld.text floatValue];
        
        NSString * totalPriceString=[NSString stringWithFormat:@"%.2f",totalPrice];
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   };
        NSDictionary *parameters = @{ @"clientid": delegate2.userID,
                                      @"symbol": self.companyLbl.text,
                                      @"qty":allQty,
                                      @"totalprice":totalPriceString,
                                      @"tradeprice":self.limitTxtFld.text,
                                      @"exchangeorderid":orderId,
                                      @"segment":segmentNum,
                                      @"market":@"INDIA"
                                      };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@clienttrade",delegate2.baseUrl]]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            self.localWisdomIDString=nil;
                                                        }
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                            {
                                                                
                                                                [self individualOrderMethod];
                                                            }
                                                            else
                                                            {
                                                                [self alertMethod];
                                                            }
                                                            
                                                        });
                                                        
                                                    }];
        [dataTask resume];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    
}

//MULTI TRADE//

-(void)SendSocketBuyRequest
{
    
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    
//    [delegate2.outputStream setDelegate:self];
//    
//    [delegate2.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
//    
//   
//    [delegate2.outputStream open];
    NSString* stExchage;
    NSString* stSecurityID;
    delegate2.referanceOrderNo=delegate2.referanceOrderNo+1;
    int orderno = delegate2.referanceOrderNo;
    
    //    stExchage =@"NSECM";
    //    stSecurityID = @"11957";//IDFC
    stExchage =@"BSECM";
    stSecurityID = @"532215";
//    14537 DSIHTV
    int orderTypeInt = 1;
    
    if([orderTypeStr isEqualToString:@"MARKET"])
    {
        orderTypeInt=1;
    }
    
    else if([orderTypeStr isEqualToString:@"LIMIT"])
    {
        orderTypeInt=2;
    }
    
    else if([orderTypeStr isEqualToString:@"SL"])
    {
        orderTypeInt=4;
    }
    NSString * orderQty;
    if([delegate2.orderSegment containsString:@"NSEFO"]||[delegate2.orderSegment containsString:@"BSEFO"])
    {
    orderQty=self.futQtyTextField.text;
        allQty=self.futQtyTextField.text;
    }
    
    else
    {
         orderQty=self.qtyTxt.text;
        allQty=self.qtyTxt.text;
        
    }
   
    int inOrderqty =[orderQty intValue];
    int inPendingqty= inOrderqty;
    int btSide =buySellInt;
    int btOrderType =orderTypeInt;
//  NSString *stClientID=@"P202230";
    NSString *stClientID=[NSString stringWithFormat:@"%@",[delegate2.stdealerId objectAtIndex:0]];
    //    NSString  * stClientID=@"OWN";
    int btTimeinForce = 0;
    int  inProductType = 2;
    NSString * localLimit=self.limitTxtFld.text;
    NSString * slString;
    double slPrice = 0.0;
    if(orderTypeInt==4)
    {
       localLimit=self.stopLossPriceTxt.text;
     slString=self.trigerPriceTxt.text;
        slPrice=[slString doubleValue];
        
    }
    
    double price=[localLimit doubleValue];
    //    double dbTriggerPirce=84;
    
    NSString * security=[NSString stringWithFormat:@"%d",instrumentToken];
    
    TagEncode * tag = [[TagEncode alloc]init];
    delegate2.mtCheck=true;
    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Order];
    [tag TagData:sharedManager.inOrderNo intMethod:orderno];
    [tag TagData:sharedManager.nlTagFooter];
    [tag TagData:sharedManager.stExchange stringMethod:delegate2.orderSegment];
    [tag TagData:sharedManager.stSecurityID stringMethod:security];
    [tag TagData:sharedManager.inOrderQty intMethod:inOrderqty];
    [tag TagData:sharedManager.inPendingQty intMethod:inPendingqty];
    [tag TagData:sharedManager.dbPrice doubleMethod:price];
    
    [tag TagData:sharedManager.btSide byteMethod:btSide];
    [tag TagData:sharedManager.btOrderType byteMethod:btOrderType];
    
    if(orderTypeInt==4)
    {
    
        [tag TagData:sharedManager.dbTriggerPrice doubleMethod:slPrice];
    }
    
    [tag TagData:sharedManager.stClientID stringMethod:stClientID];
    [tag TagData:sharedManager.btTimeinForce byteMethod:btTimeinForce];
    
    [tag TagData:sharedManager.inProductType intMethod:inProductType];
    [tag TagData:sharedManager.nlTagFooter];
    
    [tag GetBuffer];
    
//    NewLoginViewController * new = [[NewLoginViewController alloc]init];
//    [new newMessage];
    
    delegate2.mtOrderCheck=true;
    
    delegate2.allOrderHistory=[[NSMutableArray alloc]init];
    
    [self newMessage];
    
//    delegate2.messageCheck=true;
    
    // [self open];
    
    
    
}

-(void)newMessage
{
   

    
//    [delegate2.outputStream setDelegate:self];
    Byte UUID[delegate2.btOutBuffer.count];
    
    for (int i = 0; i < delegate2.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate2.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp1;
        [scanner scanHexInt:&temp1];
        NSLog(@"%d",temp1);
        
        UUID[i]=temp1;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate2.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate2.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
   
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    
    
    [delegate2.outputStream write:bytes maxLength:delegate2.btOutBuffer.count];
    
}

-(void)newMessage1
{
    
    
    
    //    [delegate2.outputStream setDelegate:self];
    Byte UUID[delegate2.btOutBuffer.count];
    
    for (int i = 0; i < delegate2.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate2.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp1;
        [scanner scanHexInt:&temp1];
        NSLog(@"%d",temp1);
        
        UUID[i]=temp1;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate2.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate2.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    
    
    [delegate2.broadCastOutputstream write:bytes maxLength:delegate2.btOutBuffer.count];
    
}
-(void)orderStatusAlertMethod
{
    NSString * orderStatusString;
    
    if(delegate2.orderStatus==0)
    {
        orderStatusString=@"Unknown";
    }
    
    else if(delegate2.orderStatus==1)
    {
         orderStatusString=@"Pending";
    }
    
    else if(delegate2.orderStatus==2)
    {
         orderStatusString=@"Execute";
    }
    
    else if(delegate2.orderStatus==3)
    {
         orderStatusString=@"Cancel";
    }
    
    else if(delegate2.orderStatus==4)
    {
         orderStatusString=@"Reject";
    }
    
    [self viewDidAppear:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Order Status"
                                     message:orderStatusString
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       
                                   }];
        //Add your buttons to alert controller
        
        [alert addAction:okButton];
        
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    });

   
}


-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMainMenu:)
                                                 name:@"ordersuccess" object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(depthMenu:)
                                                 name:@"orderwatch" object:nil];
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"orderwatch" object:nil];
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
        allKeysList=[[NSMutableArray alloc]init];
        
        //pocket socket//
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://websocket.kite.trade/?api_key=%@&user_id=%@&public_token=%@",delegate2.APIKey,delegate2.userID,delegate2.publicToken]];
        
        NSLog(@"user %@",delegate2.userID);
        NSLog(@"user %@",delegate2.publicToken);
        
        
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request];
        self.socket.delegate = self;
        //
        //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //     open socket
        [self.socket open];
        delegate2.broadcastOpen=true;
        
        self.activityInd.hidden=NO;
        [self.activityInd startAnimating];
        
    }
    
    else  if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self upStoxSocket];
        if([self.upstoxCheckString isEqualToString:@"fut"])
        {
            [self futSubMethod];
        }
        [self subMethod];
        
        
        
    }
    
    else
    {
        
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
        self.activityInd.hidden=NO;
        [self.activityInd startAnimating];
        orderNvCheck=true;
        
        NSString * localExchange=self.segment.text;
        
        if([localExchange containsString:@"NSE"])
        {
            localExchange=@"NSECM";
        }
        
       else if([localExchange containsString:@"BSE"])
        {
            localExchange=@"BSECM";
        }
       else if([localExchange containsString:@"NFO"])
       {
           localExchange=@"NSEFO";
       }
       else if([localExchange containsString:@"BFO"])
       {
           localExchange=@"BSEFO";
       }
       
        
        TagEncode * tag = [[TagEncode alloc]init];
        delegate2.mtCheck=true;
        
        
        //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];
        NSString * security=[NSString stringWithFormat:@"%d",instrumentToken];
        
        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Broadcast];
        //        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
        [tag TagData:sharedManager.stSecurityID stringMethod:security];
        [tag TagData:sharedManager.stExchange stringMethod:localExchange];
        [tag GetBuffer];
//        [self newMessage];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ordertowatch" object:nil];
        
        
    }

    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(showMainMenu1:)
//                                                 name:@"watchCheck" object:nil];
    
}

- (void)showMainMenu:(NSNotification *)note
{
    
    if(orderNvCheck==true&&delegate2.allOrderHistory.count>0)
    {
        orderNvCheck=false;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:@"Order Status"
//                                     message:@"Order placed successfully"
//                                     preferredStyle:UIAlertControllerStyleAlert];
//        
//        //Add Buttons
//        
//        UIAlertAction* okButton = [UIAlertAction
//                                   actionWithTitle:@"Ok"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       
                                       int status=[[[delegate2.allOrderHistory objectAtIndex:0] objectForKey:@"btOrderStatus"] intValue];
        
                                  orderId=[[delegate2.allOrderHistory objectAtIndex:0] objectForKey:@"stExchangeOrderID"];
                                       
                                       if(status==1)
                                       {
                                           delegate2.orderStatusCheck=@"OPEN";
                                           
                                       }
                                       
                                       else if(status==2)
                                       {
                                           delegate2.orderStatusCheck=@"COMPLETED";
                                           
                                       }
                                       
                                       else if(status==3)
                                       {
                                           delegate2.orderStatusCheck=@"REJECTED";
                                          
                                       }
                                       
                                       else if(status==4)
                                       {
                                           delegate2.orderStatusCheck=@"REJECTED";
                                           
                                       }
        
//                                       else if(status==0)
//                                       {
//
//                                           UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"Unknown Response" preferredStyle:UIAlertControllerStyleAlert];
//
//                                           [self presentViewController:alert animated:YES completion:^{
//
//                                           }];
//
//                                           UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//                                           }];
//
//                                           [alert addAction:okAction];
//
//                                       }
//
      
    
    
    if(localInt!=0)
    {
        
        [self wisdomGardenTickCheckMethod];
        
    }else
    {
        [self watchMethod];
    }
        
        
    }
   
                                       
//                                       
//                                   }];
//        //Add your buttons to alert controller
//        
//        [alert addAction:okButton];
//        
//        
//        [self presentViewController:alert animated:YES completion:nil];
    
        
//    });

    
}

- (void)depthMenu:(NSNotification *)note
{
    
//    dispatch_async(dispatch_get_main_queue(), ^{
    if(delegate2.marketWatch1.count>0)
    {
        NSLog(@"%@",delegate2.marketWatch1);
    
    NSString * security=[NSString stringWithFormat:@"%d",instrumentToken];
    NSMutableDictionary * depthDetails=[[NSMutableDictionary alloc]init];
    
    
    for(int i=0;i<delegate2.marketWatch1.count;i++)
    {
        NSString * watchId=[NSString stringWithFormat:@"%@",[[delegate2.marketWatch1 objectAtIndex:i]objectForKey:@"stSecurityID"]];
        if([watchId containsString:security])
        {
            depthDetails=[delegate2.marketWatch1 objectAtIndex:i];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(depthDetails.count>0)
        {

        
        
        
        self.bidQtyLbl.text=[depthDetails objectForKey:@"buyQty"];
        self.askSizeLbl.text=[depthDetails objectForKey:@"sellQty"];
        
        self.bidQty1.text=[depthDetails objectForKey:@"buyQty"];
        self.bidQty2.text=[depthDetails objectForKey:@"buyQty1"];
        self.bidQty3.text=[depthDetails objectForKey:@"buyQty2"];
        self.bidQty4.text=[depthDetails objectForKey:@"buyQty3"];
        self.bidQty5.text=[depthDetails objectForKey:@"buyQty4"];
        
        self.askQty1.text=[depthDetails objectForKey:@"sellQty"];
        self.askQty2.text=[depthDetails objectForKey:@"sellQty1"];
        self.askQty3.text=[depthDetails objectForKey:@"sellQty2"];
        self.askQty4.text=[depthDetails objectForKey:@"sellQty3"];
        self.askQty5.text=[depthDetails objectForKey:@"sellQty4"];
        
        self.totalBid.text=[depthDetails objectForKey:@"totalBuy"];
        self.totalAsk.text=[depthDetails objectForKey:@"totalSell"];
        
        
        
        self.bidPriceLbl.text=[depthDetails objectForKey:@"buy1"];
        self.askPriceLbl.text=[depthDetails objectForKey:@"sellPrice"];
        self.bidPrice1.text=[depthDetails objectForKey:@"buy1"];
        self.bidPrice2.text=[depthDetails objectForKey:@"buyPrice1"];
        self.bidPrice3.text=[depthDetails objectForKey:@"buyPrice2"];
        self.bidprice4.text=[depthDetails objectForKey:@"buyPrice3"];
        self.bidPrice5.text=[depthDetails objectForKey:@"buyPrice4"];
        
        self.askPrice1.text=[depthDetails objectForKey:@"sellPrice"];
        self.askPrice2.text=[depthDetails objectForKey:@"sellPrice1"];
        self.askPrice3.text=[depthDetails objectForKey:@"sellPrice2"];
        self.askPrice4.text=[depthDetails objectForKey:@"sellPrice3"];
        self.askPrice5.text=[depthDetails objectForKey:@"sellPrice4"];
            
            self.activityInd.hidden=YES;
            [self.activityInd stopAnimating];
            

        }
        
    });
    
    
    

    
//    });
        
    }
    
    
}


-(void)modifyBuyRequest
{
    
    
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    
   
    
    NSString* stExchage;
    NSString* stSecurityID;
   
    stExchage =segmentNew;
    stSecurityID = @"532121";
    
    NSString * orderQty;
    if([delegate2.orderSegment containsString:@"NSEFO"]||[delegate2.orderSegment containsString:@"BSEFO"])
    {
        orderQty=self.futQtyTextField.text;
        allQty=self.futQtyTextField.text;
    }
    
    else
    {
        orderQty=self.qtyTxt.text;
        allQty=self.qtyTxt.text;
        
    }
    
    
    int inOrderqty =[orderQty intValue];
    int inPendingqty= inOrderqty;
    int btSide =buySellInt;
    
     int orderTypeInt = 2;
    
    if([orderTypeStr isEqualToString:@"MARKET"])
    {
        orderTypeInt=1;
    }
    
    else if([orderTypeStr isEqualToString:@"LIMIT"])
    {
        orderTypeInt=2;
    }
    
    if([orderTypeStr isEqualToString:@"SL"])
    {
        orderTypeInt=4;
    }
    
    
    
    int btOrderType =orderTypeInt;
    
    int btTimeinForce = 0;
    
    NSString * localLimit=self.limitTxtFld.text;
    NSString * slString;
    double slPrice = 0.0;
    if(orderTypeInt==4)
    {
        localLimit=self.stopLossPriceTxt.text;
        slString=self.trigerPriceTxt.text;
        slPrice=[slString doubleValue];
        
    }
    
    double price=[localLimit doubleValue];
    
    TagEncode * tag = [[TagEncode alloc]init];
    delegate2.mtCheck=true;
    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.modifyOrder];
    [tag TagData:sharedManager.inATINOrderNo intMethod:delegate2.modifyATINORDERNO];
    [tag TagData:sharedManager.btTimeinForce intMethod:btTimeinForce];
    
    
    //    [tag TagData:sharedManager.nlTagFooter];
    //    [tag TagData:sharedManager.stExchange stringMethod:stExchage];
    //    [tag TagData:sharedManager.stSecurityID stringMethod:stSecurityID];
    //
    
    [tag TagData:sharedManager.btSide byteMethod:btSide];
    [tag TagData:sharedManager.inOrderQty intMethod:inOrderqty];
    [tag TagData:sharedManager.inPendingQty intMethod:inPendingqty];
    [tag TagData:sharedManager.dbPrice doubleMethod:price];
    
    [tag TagData:sharedManager.btOrderType byteMethod:btOrderType];
    
    if(orderTypeInt==4)
    {
        [tag TagData:sharedManager.dbTriggerPrice doubleMethod:slPrice];
    }
    
    //    [tag TagData:sharedManager.stClientID stringMethod:stClientID];
    //    [tag TagData:sharedManager.btTimeinForce byteMethod:btTimeinForce];
    //
    //    [tag TagData:sharedManager.inProductType intMethod:inProductType];
    //    [tag TagData:sharedManager.nlTagFooter];
    
    [tag GetBuffer];
    
     delegate2.allOrderHistory=[[NSMutableArray alloc]init];
    
    [self newMessage];
    
    // [self open];
    
}

-(void)orderPlacingMethod
{
    
    
  
    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[orderTypeString stringByAddingPercentEncodingWithAllowedCharacters:nsur]]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy                                                        timeoutInterval:10.0];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:orderTypeString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    NSLog(@"%@",request);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [request setHTTPMethod:method];
    [request setAllHTTPHeaderFields:headers];
    
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
         [request setHTTPBody:postData];
    }
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data1, NSURLResponse *response, NSError *error) {
        
        NSDictionary *orderDict=[NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
        
        if(data1!=nil)
        {
            NSString * status;
            NSLog(@"orderDict----%@",orderDict);
            
            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
            {
            
            status=[NSString stringWithFormat:@"%@",[orderDict objectForKey:@"status"]];
            
            orderId=[NSString stringWithFormat:@"%@",[[orderDict objectForKey:@"data"]objectForKey:@"order_id"]];
                
            }
            
            else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
            {
                status=[NSString stringWithFormat:@"%@",[orderDict objectForKey:@"message"]];
                
                orderId=[NSString stringWithFormat:@"%@",[[orderDict objectForKey:@"data"]objectForKey:@"order_id"]];
            }
            
            
            
            NSLog(@"%@",status);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([status isEqualToString:@"success"])
                {
                    @try {
                        if(localInt!=0)
                        {
                            
                            [self wisdomGardenTickCheckMethod];
                            
                        }else
                        {
                            [self watchMethod];
                        }
                        
                    }
                    
                    @catch (NSException *exception) {
                        NSLog(@"Something missing...");
                    }
                    @finally {
                        
                    }
                    
                    
                    
                }
                
                else
                {
                    UIAlertController * alert = [UIAlertController
                                                 alertControllerWithTitle:@"Order Status"
                                                 message:@"Please try again"
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    //Add Buttons
                    
                    UIAlertAction* okButton = [UIAlertAction
                                               actionWithTitle:@"Ok"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Handle your yes please button action here
                                                   
                                               }];
                    //Add your buttons to alert controller
                    
                    [alert addAction:okButton];
                    
                    
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
                
                
                
            });
            
        }
        
        
        
    }];
    
    [postDataTask resume];
    
    
    
}

-(void)symbolMethod
{
    NSString * instrument=[NSString stringWithFormat:@"%d",instrumentToken];
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               };
    NSDictionary *parameters = @{ @"exchangetokens":instrument};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@stock/tokens",delegate2.baseUrl]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableArray * symbolResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                          
                                                            
                                                            
                                                            NSArray * symbols=[symbolResponse mutableCopy];
                                                            NSSet *set = [NSSet setWithArray:symbols];
                                                            delegate2.scripsMt = [set allObjects];
                                                            
                                                        }
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        
                                                        MainOrdersViewController *nextPage = [self.storyboard instantiateViewControllerWithIdentifier:@"orders"];
                                                        [self presentViewController:nextPage animated:YES completion:nil];
                                                        
                                                    });
                                                    
                                                }];
    [dataTask resume];
    
}


-(void)upStoxSocket
{
    allKeysList=[[NSMutableArray alloc]init];
    
    //pocket socket//
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"https://ws-api.upstox.com/?apiKey=QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1&token=%@",delegate2.accessToken]];
    
    
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    //     create the socket and assign delegate
    self.socket = [PSWebSocket clientSocketWithRequest:request];
    self.socket.delegate = self;
    //
    //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    //     open socket
    [self.socket open];
    delegate2.broadcastOpen=true;
    
   
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{ if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
{
    [self unSubMethod];
    if([self.upstoxCheckString isEqualToString:@"fut"])
    {
        [self unSubFutMethod];
    }
}
    
}

-(void)tapDetected{
    NSLog(@"single Tap on imageview");
    [myImageView removeFromSuperview];
}


@end
