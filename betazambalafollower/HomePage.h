//
//  HomePage.h
//  testing
//
//  Created by zenwise mac 2 on 11/15/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>
#import "PSWebSocket.h"
#import "Reachability.h"

/*!
 @author Sanka Revanth
 @class HomePage
 @discussion This class reffers to market watch screen.Here we can see live stream of market prices of symbols.
 @superclass SuperClass: UIViewController\n
 @classdesign    Designed using UIView,Button,BarButton,Label,UITableview.
 @coclass    AppDelegate
 
 */


@interface HomePage : UIViewController<UITableViewDelegate, UITableViewDataSource>

- (IBAction)addBtn:(id)sender;/*!<add symbol button*/




@property (nonatomic, strong) PSWebSocket *socket;/*!<kite websocket*/

@property (strong, nonatomic) IBOutlet UIView *ifEmptyView;/*!<empty view*/


@property (weak, nonatomic) IBOutlet UITableView *marketTableView;/*!<tableview to show list of symbols*/

- (IBAction)editTableViewBtn:(id)sender;/*!<edit button*/



@property (strong, nonatomic) IBOutlet UIView *headerView;
@property Reachability * reach;

@property id message;
//@property SRWebSocket * webSocket;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
