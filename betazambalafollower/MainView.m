//
//  MainView.m
//  testing
//
//  Created by zenwise technologies on 21/11/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#ifndef INMOBI_BANNER_PLACEMENT
#define INMOBI_BANNER_PLACEMENT     1487645804150
#endif

#import "MainView.h"
#import "HomePage.h"
#import "AppDelegate.h"
#import "VCFloatingActionButton.h"
#import <QuartzCore/QuartzCore.h>
#import "DGActivityIndicatorView.h"
#import "NewProfileSettings.h"
#import "Reachability.h"
#import "MessageView.h"
#import "NewMessages.h"
#import "TagEncode.h"
#import "MT.h"
#import "PSWebSocket.h"
#import<malloc/malloc.h>
#import "MarketMonksView.h"
#import "HelpshiftCampaigns.h"
@import Mixpanel;
@import Tune;




@interface MainView () <PSWebSocketDelegate>
{
    AppDelegate * delegate2;/*!<appdelegate instance*/
    DGActivityIndicatorView * activityIndicatorView;/*!<unused*/
    UIVisualEffectView * blurEffectView;/*!<used for blureffect for view*/
    float niftyFloat;/*!<used to store nifty ltp*/
    float niftyChg;/*!<used to store nifty change percentage*/
    float sensexChg;/*!<used to store sensex change percentage*/
    float sensexPrice;/*!<used to store sensex ltp*/
    NSMutableDictionary * clientCreatResponse;/*!<it stores data of user creation response*/
    NSMutableDictionary * brokerResponseDict;/*!<it stores data of broker information*/
    MT * sharedManager;/*!<MT class referance*/
    
    BOOL check;/*!<unused*/
    NSString * brokerid;/*!<it stores brokerid*/
    int packetsNumber;/*!<it stores number of packets coming from socket */
    int packetsLength;/*!<it stores length of packets */
    
    NSString * string1;/*!<message sending to socket in zerodha*/
    NSString * message;/*!<message sending to socket in zerodha*/
   
}

@end

@implementation MainView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
     sharedManager = [MT Mt1];
    
    self.mutualFundsLbl.hidden=YES;
    self.usaLbl.hidden=YES;
    
    if(delegate2.firstTimeCheck==true)
    {
       
        NSString * localStr=[NSString stringWithFormat:@"(%@)",delegate2.brokerNameStr];
        
        [self.brokerName setTitle:localStr];
        
      
     };
        
        
        if([delegate2.loginActivityStr isEqualToString:@"OTP1"])
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            [prefs setObject:delegate2.loginActivityStr forKey:@"logintype"];
           
            
            [prefs setObject:delegate2.userID forKey:@"userid"];
             [prefs setObject:delegate2.brokerNameStr forKey:@"brokername"];
            
            
            [prefs synchronize];
            
            [self.brokerName setTitle:@"(GUEST)"];
            
            self.openEAccountView.hidden = NO;
            self.openEAccountViewHeightConstraint.constant = 60;
            
           
            
          
            
            
            
            
            
        }else if ([delegate2.loginActivityStr isEqualToString:@"CLIENT"])
        {
            self.openEAccountView.hidden=YES;
            self.openEAccountViewHeightConstraint.constant = 0;
            NSString * localStr=[NSString stringWithFormat:@"(%@)",delegate2.brokerNameStr];
            
            [self.brokerName setTitle:localStr];   
            
           
            
            NSString * mutualFundCheck=[NSString stringWithFormat:@"%@",[delegate2.brokerInfoDict objectForKey:@"mutualfund"]];
             brokerid=[NSString stringWithFormat:@"%@",[delegate2.brokerInfoDict objectForKey:@"brokerid"]];
            
            if([mutualFundCheck isEqual:[NSNull null]])
            {
                
                mutualFundCheck=@"";
            }
            
           
            
            
            NSString * usmarketCheck=[NSString stringWithFormat:@"%@",[delegate2.brokerInfoDict objectForKey:@"usmarket"]];
            
            if([usmarketCheck isEqual:[NSNull null]])
            {
                
                usmarketCheck=@"";
            }
        if([mutualFundCheck isEqualToString:@"1"])
        {
            self.mutualFundsLbl.hidden=YES;
            self.mutualFundsLbl.text=@"Mutual Funds";
            
        }
            
            else
            {
                self.mutualFundsLbl.hidden=YES;
                
            }
            
            if([usmarketCheck isEqualToString:@"1"])
            {
                self.usaLbl.hidden=YES;
                self.usaLbl.text=@"US Market";
                
            }
            
            else
            {
                self.usaLbl.hidden=YES;
                
            }
                
                
            
            
        }
        
        @try {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            NSString * verify=[prefs stringForKey:@"ID"];
            
            if(verify.length>0)
            {
                
               
            }
            
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
    
        
        self.tickerImageView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
        self.tickerImageView.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.tickerImageView.layer.shadowOpacity = 1.0f;
        self.tickerImageView.layer.shadowRadius = 1.0f;
        self.tickerImageView.layer.cornerRadius=4.2f;
        self.tickerImageView.layer.masksToBounds = YES;
        
        self.marketmonksBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.marketmonksBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.marketmonksBtn.layer.shadowOpacity = 1.0f;
        self.marketmonksBtn.layer.shadowRadius = 1.0f;
        self.marketmonksBtn.layer.cornerRadius=1.0f;
        self.marketmonksBtn.layer.masksToBounds = NO;
        
        self.wisdomGardenButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.wisdomGardenButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.wisdomGardenButton.layer.shadowOpacity = 1.0f;
        self.wisdomGardenButton.layer.shadowRadius = 1.0f;
        self.wisdomGardenButton.layer.cornerRadius=1.0f;
        self.wisdomGardenButton.layer.masksToBounds = NO;
        
        self.marketWtchButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.marketWtchButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.marketWtchButton.layer.shadowOpacity = 1.0f;
        self.marketWtchButton.layer.shadowRadius = 1.0f;
        self.marketWtchButton.layer.cornerRadius=1.0f;
        self.marketWtchButton.layer.masksToBounds = NO;
        
        self.mutualFundsBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.mutualFundsBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.mutualFundsBtn.layer.shadowOpacity = 1.0f;
        self.mutualFundsBtn.layer.shadowRadius = 1.0f;
        self.mutualFundsBtn.layer.cornerRadius=1.0f;
        self.mutualFundsBtn.layer.masksToBounds = NO;
        
        self.premiumServiceBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.premiumServiceBtn.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.premiumServiceBtn.layer.shadowOpacity = 1.0f;
        self.premiumServiceBtn.layer.shadowRadius = 1.0f;
        self.premiumServiceBtn.layer.cornerRadius=1.0f;
        self.premiumServiceBtn.layer.masksToBounds = NO;
        
        self.openAccountButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        self.openAccountButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
        self.openAccountButton.layer.shadowOpacity = 1.0f;
        self.openAccountButton.layer.shadowRadius = 1.0f;
        self.openAccountButton.layer.cornerRadius=1.0f;
        self.openAccountButton.layer.masksToBounds = NO;
        
        //    self.niftyView.layer.cornerRadius = 23.4;
        //    self.niftyView.layer.masksToBounds = YES;
        self.niftyView.backgroundColor=[UIColor colorWithRed:(251/255.0) green:(237/255.0) blue:(224/255.0) alpha:1];
        self.niftyView.layer.borderColor=[UIColor colorWithRed:(162/255.0) green:(150/255.0) blue:(154/255.0) alpha:1].CGColor;
        self.niftyView.layer.borderWidth=1.0f;
        
        
        //    self.sensexView.layer.cornerRadius = 23.4;
        //    self.sensexView.layer.masksToBounds = YES;
        self.sensexView.backgroundColor=[UIColor colorWithRed:(251/255.0) green:(237/255.0) blue:(224/255.0) alpha:5];
        self.sensexView.layer.borderColor=[UIColor colorWithRed:(162/255.0) green:(150/255.0) blue:(154/255.0) alpha:1].CGColor;
        self.sensexView.layer.borderWidth=1.0f;
        
        self.marketmonksBtn.layer.cornerRadius=1.0f;
        self.wisdomGardenButton.layer.cornerRadius=1.0f;
        self.marketWtchButton.layer.cornerRadius=1.0f;
        self.mutualFundsBtn.layer.cornerRadius=1.0f;
        self.premiumServiceBtn.layer.cornerRadius=1.0f;
        
        [self.mutualFundsBtn addTarget:self action:@selector(onClickingMutualBtn) forControlEvents:UIControlEventTouchUpInside];
    }

-(void)viewWillAppear:(BOOL)animated
{
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self upstoxSocket];
        [self subMethod];
    }else if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
    }else
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(philipIndex:)
                                                     name:@"indexWatch" object:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated

{
    delegate2.tickerCheck=true;
    if(delegate2.firstTimeCheck==true)
    {
    [self createClientMethod];
    delegate2.firstTimeCheck=false;
        
    }
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
       [self kiteWebSocket];
    }
    
    else
    {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(showMainMenu:)
                                                     name:@"orderwatch" object:nil];
    }
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    
   

    [super viewDidAppear:animated];
    
    check=true;
    
    
    
    @try
    {
        if(delegate2.emailId.length>0)
        {
            [Tune setUserEmail:delegate2.emailId];
        }
        if(delegate2.userName.length>0)
        {
            [Tune setUserName:delegate2.userName];
        }
        
        [Tune setUserId:delegate2.userID];
        
        
        TuneLocation *loc = [TuneLocation new];
        loc.latitude = @(9.142276);
        loc.longitude = @(-79.724052);
        loc.altitude = @(15.);
        [Tune setLocation:loc];
        
        [Tune measureEventName:TUNE_EVENT_LOGIN];
        
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
    //mixpanel//
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    [mixpanel identify:delegate2.userID];
    
    [mixpanel.people set:@{@"first_name": delegate2.userName,@"email":delegate2.emailId,@"brokername":delegate2.brokerNameStr,@"logintime":[NSDate date],@"sublocality":delegate2.subLocality,@"location":delegate2.locality,@"postalcode":delegate2.postalCode}];
        
  
   if(delegate2.mixpanelDeviceToken.length>0)
   {
     [mixpanel.people addPushDeviceToken:delegate2.mixpanelDeviceToken];
       
   }
      
}
        
        
    
    else
    {
        
        
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        
        [mixpanel identify:delegate2.userID];
        
        [mixpanel.people set:@{@"brokername":delegate2.brokerNameStr,@"logintime":[NSDate date],@"sublocality":delegate2.subLocality,@"location":delegate2.locality,@"postalcode":delegate2.postalCode,@"first_name":delegate2.userID}];
        
        
        if(delegate2.mixpanelDeviceToken.length>0)
        {
            [mixpanel.people addPushDeviceToken:delegate2.mixpanelDeviceToken];
            
        }
    }
    
   
    }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
if(delegate2.stream==YES)
{
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
            
         // [self liveStreaming];
        }
    
        else
        {
//        [self  broadRequest];
        
        }
}
    

    if([delegate2.mainViewBrokerStr isEqualToString:@"Show"])
    {
        NSString * localStr=[NSString stringWithFormat:@"(%@)",delegate2.brokerNameStr];
        
        [self.brokerName setTitle:localStr];
        
    }
    
    else if([delegate2.mainViewBrokerStr isEqualToString:@"DontShow"])
    {
        
        [self.brokerName setTitle:@""];
        
    }
    
    if([delegate2.mainTickerStr isEqualToString:@"Show"])
    {
        self.tickerImageView.hidden=NO;
        self.tickerImgHeightConstraint.constant=56;
        self.mainTickerView.hidden=NO;
        self.mainTickerViewHeightConstraint.constant=56;
        
    }
    
    else if([delegate2.mainTickerStr isEqualToString:@"DontShow"])
    {
        
        self.tickerImageView.hidden=YES;
        self.tickerImgHeightConstraint.constant=0;
        self.mainTickerView.hidden=YES;
        self.mainTickerViewHeightConstraint.constant=0;
        
    }
    
    

}



/*!
 @discussion It is used to create client.
 */

-(void)createClientMethod
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
   
    if(delegate2.currentDeviceId.length!=0)
    {
        
        if(delegate2.referralCode.length==0)
        {
            delegate2.referralCode=@"";
            
        }
        
      if(delegate2.emailId.length!=0&&delegate2.userName.length!=0)
      {
          
      }
        else
        {
            delegate2.emailId=@"";
            delegate2.userName=@"";
            
        }
        
        if(brokerid.length==0)
        {
            brokerid=@"";
        }
        
       
       
    @try {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   };
        NSDictionary *parameters = @{ @"clientmemberid":delegate2.userID,
                                      
                                    @"referralcode":delegate2.referralCode,
                                    @"brokerid":brokerid,
                                    @"email":delegate2.emailId,
                                    @"firstname":delegate2.userName,
                                    @"lastname":@"",
                                    @"notificationinfo": @{ @"deviceid": delegate2.currentDeviceId, @"devicetoken":[prefs objectForKey:@"devicetoken"],
                                                               @"devicetype":@1
                                                               }
                                      
                                      
                                      
                                    };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSString * requestStr=[NSString stringWithFormat:@"%@follower/client",delegate2.baseUrl];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestStr]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            clientCreatResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            NSString * meesage=[NSString stringWithFormat:@"%@",[clientCreatResponse objectForKey:@"message"]];
                                                            if([delegate2.loginActivityStr isEqualToString:@"OTP1"])
                                                            {
                                                                 
                                                                [self guestVisibility];
                                                                
                                                                
                                                                
                                                            }
                                                           
                                                            
                                          if([meesage containsString:@"created"])
                                                                
                                                                
                                                            {
                                                                
                                                               
                                                                
                                                        delegate2.marketmonksHint=true;
                                                                delegate2.wisdomHint=true;
                                                                delegate2.feedsHint=true;
                                                        delegate2.marketwatchHinT=true;
                                                           delegate2.stockHint=true;
                                                                
                                                            }
                                                            
                                                            else
                                                            {
                                                                delegate2.marketmonksHint=false;
                                                                delegate2.wisdomHint=false;
                                                                delegate2.feedsHint=false;
                                                                delegate2.marketwatchHinT=false;
                                                                delegate2.stockHint=false;
                                                                
                                                                
                                                            }
                                                            
                                                            delegate2.stream=YES;
                                                            if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                                            {
                                                                
                                                               // [self liveStreaming];
                                                                
                                                            }
                                                            
                                                            else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                                            {
                                                                [self subMethod];
                                                                
                                                                    [self.socket close];
                                                                    [self upstoxSocket];
                                                                    
                                                                
                                                            
                                                                
                                                            }
                                                            
                                                        });
                                                        
                                                    }];
        [dataTask resume];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
        
    }
    
    
    else
    {
//        [self brokerInfo];
        
    }
}
/*!
 @discussion It is used to subscribe for nifty and sensex(to get live data in upstox).
 */
-(void)subMethod
{
    NSArray * exchangeArray = @[@"nse_index",@"bse_index"];
    NSArray * symbolsArray = @[@"NIFTY_50",@"SENSEX"];
    
    for(int i=0;i<exchangeArray.count;i++)
    {
        NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
        NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                                   @"authorization": access,
                                   };
        
        NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",[exchangeArray objectAtIndex:i],[symbolsArray objectAtIndex:i]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:30.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            
                                                            NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSLog(@"%@",json);
                                                        }
                                                       
                                                        
                                                    }];
        [dataTask resume];
        
    }
    
}




/*!
 @discussion It is used to unsubscribe for nifty(to stop getting live data in upstox).
 */

-(void)unSubMethodNifty
{
    NSArray * exchangeArray = @[@"nse_index",@"bse_index"];
    NSArray * symbolsArray = @[@"NIFTY_50",@"SENSEX"];
    //for(int i=0;i<exchangeArray.count;i++)
  //  {
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                               @"authorization": access,
                               };
    //NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",[exchangeArray objectAtIndex:i],[symbolsArray objectAtIndex:i]];
        NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/nse_index/?symbol=NIFTY_50"];
        
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",json);
                                                    }
                                                    
                                                    
                                                    
//                                                    dispatch_async(dispatch_get_main_queue(), ^{
//
//
//                                                    });
                                                }];
    [dataTask resume];
//}
}

/*!
 @discussion It is used to unsubscribe for sensex(to stop getting live data in upstox).
 */
-(void)unSubSensex
{
    NSArray * exchangeArray = @[@"nse_index",@"bse_index"];
    NSArray * symbolsArray = @[@"NIFTY_50",@"SENSEX"];
    //for(int i=0;i<exchangeArray.count;i++)
    //  {
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                               @"authorization": access,
                               };
    //NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",[exchangeArray objectAtIndex:i],[symbolsArray objectAtIndex:i]];
    NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/bse_index/?symbol=SENSEX"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",json);
                                                    }
                                                    
                                                    
                                                    
                                                    //                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                    //
                                                    //
                                                    //                                                    });
                                                }];
    [dataTask resume];
    //}
}

-(void)philipIndex:(NSNotification *)note
{
    NSLog(@"IndexWatch:%@",delegate2.mtIndexWatchArray);
    
    for (int i=0; i<delegate2.mtIndexWatchArray.count; i++) {
        
        NSString * niftySymbol = [[delegate2.mtIndexWatchArray objectAtIndex:i] objectForKey:@"symbol"];
        niftySymbol = [niftySymbol stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString * ltp =[NSString stringWithFormat:@"%.2f",[[[delegate2.mtIndexWatchArray objectAtIndex:i]objectForKey:@"LTP"]floatValue]];
        NSString * closePrice = [NSString stringWithFormat:@"%.2f",[[[delegate2.mtIndexWatchArray objectAtIndex:i]objectForKey:@"PrevClose"]floatValue]];
        
        float ltpFloat = [ltp floatValue];
        float closePriceFloat = [closePrice floatValue];
        
        
        float changePercentage = ((ltpFloat-closePriceFloat) *100/closePriceFloat);
        NSString * percent = @"%";
        dispatch_async(dispatch_get_main_queue(), ^{
        if([niftySymbol isEqualToString:@"Nifty50"])
        {
            self.lastPrice.text=[NSString stringWithFormat:@"%.2f",ltpFloat];
            self.changePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changePercentage,percent];
            
        }else if ([niftySymbol isEqualToString:@"SENSEX"])
        {
            self.sensexLastPrice.text=[NSString stringWithFormat:@"%.2f",ltpFloat];
            self.sensexChangePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changePercentage,percent];
        }
       });
        }
}

- (void)viewWillDisappear:(BOOL)animated
{
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self unSubMethodNifty];
        [self unSubSensex];
        [self.socket close];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidDisappear:(BOOL)animated
{
    delegate2.tickerCheck=false;
    check=false;
    }
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
/*!
 @discussion It is used to navigate to mutual fund section.
 */

-(void)onClickingMutualBtn
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Messege" message:@"Coming Soon" preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:okAction];
    

    
}




//-(void)onChatbuttonTap
//{
//
//
//    NewProfileSettings *profileVieww=[self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileSettings"];
//
//    [self presentViewController:profileVieww animated:YES completion:nil];
//}


//REACHABILITY//

/*!
 @discussion It is used to check network status.
 @param notice notifies when net is disconnected
 */


-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

/*!
 @discussion It is used to check network status.
 */


-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}

/*!
 @discussion It is used to connect to zerodha websocket.
 */


-(void)kiteWebSocket
{
     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://websocket.kite.trade/?api_key=%@&user_id=%@&public_token=%@",delegate2.APIKey,delegate2.userID,delegate2.publicToken]];
    
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    //     create the socket and assign delegate
    self.socket = [PSWebSocket clientSocketWithRequest:request];
    self.socket.delegate = self;
    //
    //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    //     open socket
    [self.socket open];
}

/*!
 @discussion It is used to connect to upstox socket.
 */


-(void)upstoxSocket
{
    
        
        //pocket socket//
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"https://ws-api.upstox.com/?apiKey=QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1&token=%@",delegate2.accessToken]];
        
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    
      //  NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request];
        self.socket.delegate = self;
        //
        //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //     open socket
        [self.socket open];
}

/*!
 @discussion It notifies socket is opend.
 @param webSocket socket instance.
 */


- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    NSLog(@"Handshake complete");
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
         NSMutableArray* localInstrumentToken=[[NSMutableArray alloc]initWithObjects:[NSNumber numberWithInt:265],[NSNumber numberWithInt:256265], nil];
            
//            localInstrumentToken=delegate2.homeInstrumentToken;
        
            localInstrumentToken=[[[localInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
            
            if(localInstrumentToken.count>0)
            {
                
                NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":localInstrumentToken};
                
                
                NSData *json;
                
                NSError * error;
                
                
                
                
                // Dictionary convertable to JSON ?
                if ([NSJSONSerialization isValidJSONObject:subscribeDict])
                {
                    // Serialize the dictionary
                    json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
                    
                    // If no errors, let's view the JSON
                    if (json != nil && error == nil)
                    {
                        string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                        
                        //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                        //
                        
                        NSLog(@"JSON: %@",string1);
                        
                    }
                }
                
                
                [self.socket send:string1];
                
                
                
                
                
                
                NSArray * mode=@[@"full",localInstrumentToken];
                
                subscribeDict=@{@"a": @"mode", @"v": mode};
                
                
                
                //     Dictionary convertable to JSON ?
                if ([NSJSONSerialization isValidJSONObject:subscribeDict])
                {
                    // Serialize the dictionary
                    json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
                    
                    
                    // If no errors, let's view the JSON
                    if (json != nil && error == nil)
                    {
                        message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                        
                        //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                        
                        NSLog(@"JSON: %@",message);
                        
                    }
                }
                [self.socket send:message];
                
            }
            
            
        }
    }

/*!
 @discussion It notifies socket recieved message.
 @param message data from socket.
 */


- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    
    
if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
{
    NSData * data=[NSKeyedArchiver archivedDataWithRootObject:message];
    NSLog(@"size of Object: %zd",malloc_size((__bridge const void *)(message)));
    if(data.length > 247)
    {
        NSLog(@"The websocket received a message: %@",message);
        id packets = [message subdataWithRange:NSMakeRange(0,2)];
        packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
        NSLog(@" number of packets %i",packetsNumber);
        int startingPosition = 2;
        //            if([instrumentTokensDict allKeys])
        //                [instrumentTokensDict removeAllObjects];
        
        for( int i =0; i< packetsNumber ; i++)
        {
            
            
            NSData * packetsLengthData = [message subdataWithRange:NSMakeRange(startingPosition,2)];
            packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
            startingPosition = startingPosition + 2;
            
            id packetQuote = [message subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
            
            
            id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0,4)];
            int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
            
            id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
            int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
            
            id closingPrice = [packetQuote subdataWithRange:NSMakeRange(20,4)];
            int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
            
           float lastPriceFlaotValue = (float)lastPriceValue/100;
            
            float closePriceFlaotValue = (float)closePriceValue/100;
            
            NSLog(@"CLOSE %f",closePriceFlaotValue);
            
            float changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
            
           float changeFloatPercentageValue = ((lastPriceFlaotValue-closePriceFlaotValue) *100/closePriceFlaotValue);
            startingPosition = startingPosition + packetsLength;
            
            NSString * instrumentToken = [NSString stringWithFormat:@"%d",instrumentTokenValue];
            NSString * percent=@"%";
            if([instrumentToken isEqualToString:@"265"])
            {
                self.sensexLastPrice.text=[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue];
                self.sensexChangePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changeFloatPercentageValue,percent];
            }
            if ([instrumentToken isEqualToString:@"256265"])
            {
                self.lastPrice.text=[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue];
                self.changePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changeFloatPercentageValue,percent];
            }
            
        }
    }
    
}else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
{
    NSString *detailsString = [[NSString alloc] initWithData:message encoding:NSASCIIStringEncoding];
        
        
        NSLog(@"%@",detailsString);
        
        NSArray * completeArray=[[NSArray alloc]init];
        
        completeArray= [detailsString componentsSeparatedByString:@";"] ;
        NSLog(@"%@",completeArray);
    
        for (int i=0; i<completeArray.count; i++) {

            NSMutableArray * individualCompanyArray=[[NSMutableArray alloc]init];
            NSString * innerStr=[NSString stringWithFormat:@"%@",[completeArray objectAtIndex:i]];

            individualCompanyArray=[[innerStr componentsSeparatedByString:@","] mutableCopy];
            
            NSString * percent =@"%";
            NSLog(@"indi:%@",individualCompanyArray);
            if([[individualCompanyArray objectAtIndex:2]isEqualToString:@"NIFTY_50"])
            {
                NSString* niftyLTP = [NSString stringWithFormat:@"%.2f",[[individualCompanyArray objectAtIndex:3]floatValue]];
                self.lastPrice.text = niftyLTP;
                NSString * closingNifty = [NSString stringWithFormat:@"%.2f",[[individualCompanyArray objectAtIndex:7]floatValue]];
                
                float niftyFloat = [niftyLTP floatValue];
                float closingFloat = [closingNifty floatValue];
                float changeFloatPercentageValue;
                
                changeFloatPercentageValue = ((niftyFloat-closingFloat) *100/closingFloat);
                self.changePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changeFloatPercentageValue,percent];
            }
            
            
            else if([[individualCompanyArray objectAtIndex:2]isEqualToString:@"SENSEX"])
            {
                NSString* niftyLTP = [NSString stringWithFormat:@"%.2f",[[individualCompanyArray objectAtIndex:3]floatValue]];
                self.sensexLastPrice.text = niftyLTP;
                NSString * closingNifty = [NSString stringWithFormat:@"%.2f",[[individualCompanyArray objectAtIndex:7]floatValue]];
                
                float niftyFloat = [niftyLTP floatValue];
                float closingFloat = [closingNifty floatValue];
                float changeFloatPercentageValue;
                
                changeFloatPercentageValue = ((niftyFloat-closingFloat) *100/closingFloat);
                self.sensexChangePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changeFloatPercentageValue,percent];
            }
        }

}
}

/*!
 @discussion It notifies socket connection failed.
 @param error reason for interuption of socket connection.
 */



- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}


/*!
 @discussion It notifies socket recieved data.
 @param data data from socket.
 */

- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
    NSLog(@"The websocket received a message: %@", data);
}


- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}
    




/*!
 @discussion It navigates to messages view.
 @param sender button instance.
 */

- (IBAction)onNotificationButtonTap:(id)sender {
    
    NewMessages * messageView = [self.storyboard instantiateViewControllerWithIdentifier:@"NewMessages"];
    [self presentViewController:messageView animated:YES completion:nil];
    
}

/*!
 @discussion We request multitrade server to get nifty and sensex prices.
 */

-(void)broadRequest
{
TagEncode * tag = [[TagEncode alloc]init];
delegate2.mtCheck=true;


//  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];


[tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Broadcast];
//        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
[tag TagData:sharedManager.stSecurityID stringMethod:@"53580"];
[tag TagData:sharedManager.stExchange stringMethod:@"NSEFO"];
[tag GetBuffer];
[self newMessage];

//        [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];



}

/*!
 @discussion It is used to send request multitrade server.
 
 */

-(void)newMessage
{
    //    delegate1.outputStream = (__bridge NSOutputStream *)writeStream;
    //
    //    [delegate1.outputStream setDelegate:self];
    //
    //    [delegate1.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    //
    //
    //    [delegate1.outputStream open];
    //
    
    Byte UUID[delegate2.btOutBuffer.count];
    
    for (int i = 0; i < delegate2.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate2.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp;
        [scanner scanHexInt:&temp];
        NSLog(@"%d",temp);
        
        UUID[i]=temp;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate2.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate2.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate2.broadCastOutputstream write:bytes maxLength:delegate2.btOutBuffer.count];
    
}

/*!
 @discussion We will get live stream for nifty and sensex.

 */


- (void)showMainMenu:(NSNotification *)note


{
    
    for(int i=0;i<delegate2.marketWatch1.count;i++)
    {
        NSString * tmpInt=[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"];
        
        
      
        NSString * niftyStr=@"Nifty 50";
        
        if([tmpInt isEqualToString:niftyStr])
        {
            float  priceString=[[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"dbLTP"]floatValue];
            
            float close=[[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"dbClose"]floatValue];;
            self.lastPrice.text=[NSString stringWithFormat:@"%.2f",priceString];
            float changePercentage=((priceString-close) *100/close);
            NSString * str=@"%";
            self.changePercent.text = [NSString stringWithFormat:@"(%.2f%@)",changePercentage,str];
            
            
            self.sensexLastPrice.text=@"0.00";
            self.sensexChangePercent.text=@"(0.00%)";
        }
        
    
    
  }
    
}

/*!
 @discussion we will get the guest visibilty rules.
 
 */

-(void)guestVisibility
{
    
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               };
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/recommendedbrokers?clientid=%@",delegate2.baseUrl,delegate2.userID]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        self.responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"Open account responseArray:%@",self.responseDict);
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                     if([[self.responseDict  objectForKey:@"results"]count]==1)
                     {
                                                        
    NSString * mutualFundCheck=[NSString stringWithFormat:@"%@",[[[self.responseDict  objectForKey:@"results"]objectAtIndex:0]objectForKey:@"guestmutualfund"]];
                                                        
                                                        if([mutualFundCheck isEqual:[NSNull null]])
                                                        {
                                                            
                                                            mutualFundCheck=@"";
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        NSString * usmarketCheck=[NSString stringWithFormat:@"%@",[[[self.responseDict  objectForKey:@"results"]objectAtIndex:0]objectForKey:@"guestusmarket"]];
                                                        
                                                        if([usmarketCheck isEqual:[NSNull null]])
                                                        {
                                                            
                                                            usmarketCheck=@"";
                                                        }
                                                        if([mutualFundCheck isEqualToString:@"1"])
                                                        {
                                                            self.mutualFundsLbl.hidden=YES;
                                                            self.mutualFundsLbl.text=@"Mutual Funds";
                                                            
                                                        }
                                                        
                                                        else
                                                        {
                                                            self.mutualFundsLbl.hidden=YES;
                                                            
                                                        }
                                                        
                                                        if([usmarketCheck isEqualToString:@"1"])
                                                        {
                                                            self.usaLbl.hidden=YES;
                                                            self.usaLbl.text=@"US Market";
                                                            
                                                        }
                                                        
                                                        else
                                                        {
                                                            self.usaLbl.hidden=YES;
                                                            
                                                        }
                                                        
                     }
                                                        
                                                        else
                                                        {
                                                            self.usaLbl.hidden=YES;
                                                            self.usaLbl.text=@"US Market";
                                                            
                                                            self.mutualFundsLbl.hidden=YES;
                                                            self.mutualFundsLbl.text=@"Mutual Funds";
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    
    
    
}


//-(void)viewWillDisappear:(BOOL)animated
//{
//    
//    delegate2.searchToWatch=true;
//}
@end
