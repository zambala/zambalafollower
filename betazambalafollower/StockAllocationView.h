//
//  StockAllocationView.h
//  testing
//
//  Created by zenwise technologies on 27/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

@interface StockAllocationView : UIViewController<UITableViewDelegate,UITableViewDataSource,XYPieChartDelegate, XYPieChartDataSource>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIImageView *profileimage;
@property (strong, nonatomic) IBOutlet UITableView *depthNameTableView;

@property (strong, nonatomic) IBOutlet UITableView *portFolioTableView;
@property NSMutableArray *portfolioDetailArray;
@property NSMutableDictionary * portfolioDepthResponseDictionary;
@property (strong, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *leaderNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *assumedInvestmentLabel;
@property (strong, nonatomic) IBOutlet UILabel *actedByLabel;
@property (strong, nonatomic) IBOutlet UILabel *sharesBoughtLabel;
@property NSMutableArray * countArray;
@property (strong, nonatomic) IBOutlet XYPieChart *pieChartOutlet;
@property NSMutableArray *sliceColors;
@property NSMutableArray * pieChartPercentageArray;

@end
