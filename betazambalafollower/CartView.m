//
//  CartView.m
//  PremiumServices
//
//  Created by zenwise mac 2 on 12/30/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "CartView.h"

#import "CartViewCell.h"


@interface CartView ()

@end

@implementation CartView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.paymentBtn.layer.cornerRadius=3.0f;
    self.paymentBtn.clipsToBounds = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (CartViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *CellIdentifier = @"Cell";
    
    CartViewCell *cell = [self.cartTbl dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.removeBtn.layer.cornerRadius=3.0f;
    cell.removeBtn.clipsToBounds = YES;
   
    
    return cell;
    
}



@end
