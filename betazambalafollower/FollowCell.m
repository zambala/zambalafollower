//
//  FollowCell.m
//  testing
//
//  Created by zenwise technologies on 20/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "FollowCell.h"

@implementation FollowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.profileimage.layer.cornerRadius=self.profileimage.frame.size.width / 2;
   self.profileimage.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
