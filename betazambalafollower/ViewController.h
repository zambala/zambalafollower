//
//  ViewController.h
//  Payment
//
//  Created by zenwise mac 2 on 7/17/17.
//  Copyright © 2017 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIWebViewDelegate>

- (IBAction)backAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property NSString * myString;
@end

