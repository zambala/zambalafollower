//
//  DerivativesCell.m
//  testing
//
//  Created by zenwise technologies on 26/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "DerivativesCell.h"

@implementation DerivativesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.oLbl.layer.cornerRadius=12.5;
    self.oLbl.clipsToBounds = YES;
    
    self.stLbl.layer.cornerRadius=12.5;
    self.stLbl.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
