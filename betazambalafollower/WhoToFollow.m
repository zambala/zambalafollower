//
//  WhoToFollow.m
//  testing
//
//  Created by zenwise mac 2 on 12/9/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "WhoToFollow.h"
#import "WhoToFollowCell.h"

@interface WhoToFollow ()

@end

@implementation WhoToFollow

@synthesize whoToFollowTable;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (WhoToFollowCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    WhoToFollowCell *cell = [whoToFollowTable dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[WhoToFollowCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}



@end
