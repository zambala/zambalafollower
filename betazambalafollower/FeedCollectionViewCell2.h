//
//  FeedCollectionViewCell2.h
//  testing
//
//  Created by zenwise mac 2 on 12/21/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCollectionViewCell2 : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *descripLbl2;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl2;



@end
