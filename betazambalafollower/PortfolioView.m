//
//  PortfolioView.m
//  testing
//
//  Created by zenwise technologies on 23/12/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "PortfolioView.h"
#import "HMSegmentedControl.h"
#import "TabBar.h"
#import "PortfolioCell.h"
#import "DummyTableCell.h"
#import "DematCell.h"
#import "AppDelegate.h"
#import "StockOrderView.h"
#import <Mixpanel/Mixpanel.h>
#import "TagEncode.h"
#import "MT.h"
#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import "MTORDER.h"
@interface PortfolioView ()<NSStreamDelegate>
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate;
    NSDictionary * positionDictionary;
    NSDictionary * holdingDictionary;
    NSDictionary *headers;
    
    NSMutableURLRequest *request1;
    
    NSDictionary *parameters;
    
    NSData *postData;
    NSMutableDictionary* dummyResponseArray;
    MT * sharedManager;
    BOOL viewCheck;
    NSMutableArray * securityTokenArray;
    MTORDER * sharedManagerOrder;
    NSMutableArray * exchangeTokenArray;
    NSMutableArray * newMtPositionsArray;
     NSMutableArray *un_array;
    NSArray * segmentsArray;
    NSMutableArray * mtExchangeArray;
    NSMutableDictionary * instrumentTokensDict;

}

@end

@implementation PortfolioView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    delegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    delegate.outputStream.delegate=self;
    delegate.broadCastOutputstream.delegate=self;
    sharedManager=[[MT alloc]init];
     sharedManagerOrder = [MTORDER Mt2];
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"POSITIONS",@"DUMMY POSITIONS",@"DEMAT HOLDINGS"]];
    segmentedControl.frame = CGRectMake(0,0, self.view.frame.size.width,40);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    segmentedControl.verticalDividerEnabled = YES;
//    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    //    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
    positionDictionary =[[NSDictionary alloc]init];
    holdingDictionary=[[NSDictionary alloc]init];
    self.dummyBottomView.hidden=YES;
    
   
   
    
    
   
   
//    self.dummyTableView.hidden=YES;
}

-(void)newMessage
{
    
    
    
    //    [delegate2.outputStream setDelegate:self];
    Byte UUID[delegate.btOutBuffer.count];
    
    for (int i = 0; i < delegate.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp1;
        [scanner scanHexInt:&temp1];
        NSLog(@"%d",temp1);
        
        UUID[i]=temp1;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate
    .btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate.outputStream write:bytes maxLength:delegate.btOutBuffer.count];
    
}

-(void)newMessage1
{
    
    
    
    //    [delegate2.outputStream setDelegate:self];
    Byte UUID[delegate.btOutBuffer.count];
    
    for (int i = 0; i < delegate.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp1;
        [scanner scanHexInt:&temp1];
        NSLog(@"%d",temp1);
        
        UUID[i]=temp1;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate
    .btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
    [delegate.broadCastOutputstream write:bytes maxLength:delegate.btOutBuffer.count];
    
}



-(void)viewDidAppear:(BOOL)animated
{
   
 viewCheck=true;
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    
    
    
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mtpositions:)
                                                 name:@"positions" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mtHoldings:)
                                                 name:@"holdingsuccess" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMainMenu:)
                                                 name:@"orderwatch" object:nil];
    
   
    
    if([delegate.orderToPort isEqualToString:@"ordercheck"])
    {
        delegate.orderToPort=@"";
        [segmentedControl setSelectedSegmentIndex:2];
        [self segmentedControlChangedValue];
    }
    
    else if([delegate.orderToPort isEqualToString:@"position"])
    {
        delegate.orderToPort=@"";
        [segmentedControl setSelectedSegmentIndex:0];
        [self segmentedControlChangedValue];
    }
    
    else
    {
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
            if(viewCheck==true)
            {
                viewCheck=false;
                
                if(segmentedControl.selectedSegmentIndex==0)
                {
                    
            [self positionMethode];
                    
                }

                
            
            
            else  if(segmentedControl.selectedSegmentIndex==2)
            {
                [segmentedControl setSelectedSegmentIndex:2];
                [self segmentedControlChangedValue];
                
                
            }
                
            }
            
            
        }
        
        else
        {
            if(viewCheck==true)
            {
                viewCheck=false;
                
               if(segmentedControl.selectedSegmentIndex==0)
               {
                   
                [self mtPositionsRequest];
                   
               }
                
                else  if(segmentedControl.selectedSegmentIndex==2)
                {
                    [self mtHoldingsRequest];
                    
                }
                
            }
            
        }
        
        
    }
    
    

    
    
    
  
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)positionMethode
{
    self.dummyPotfolioView.hidden=YES;
    self.dummyBottomView.hidden=YES;
    self.postionView.hidden=NO;
    self.dematView.hidden=YES;
    
    @try {
        NSURL *url1;
        NSDictionary * headers;
        
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
        {
        
        url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/portfolio/positions/?api_key=%@&access_token=%@",delegate.APIKey,delegate.accessToken ]];
        
       
            
        }
        
        else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
            url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/profile/positions"]];
            
             NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate.accessToken];
            
            headers  = @{ @"cache-control": @"no-cache",
                          @"Content-Type" : @"application/json",
                          @"authorization":access,
                          @"x-api-key":@"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1"
                          };
            
           
        }
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url1];
        
        
        
        [request setHTTPMethod:@"GET"];
        if([delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
              [request setAllHTTPHeaderFields:headers];
        }
        
       
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            positionDictionary=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            
                                                            NSMutableArray * positionArray=[[NSMutableArray alloc]init];
                                                            NSLog(@"dict %@",positionDictionary);
                                                            
                                                           
                                                            
                                                            Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
                                                            
                                                            [mixpanel1 identify:delegate.userID];
                                                            
                                                            if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
                                                            {
                                                           
                                                            
                                                            for(int i=0;i<[[[positionDictionary objectForKey:@"data"]objectForKey:@"net"]count];i++)
                                                            {
                                                                NSString * symbolString=[[NSString alloc]init];
                                                                symbolString=[NSString stringWithFormat:@"%@", [[[[positionDictionary objectForKey:@"data"]objectForKey:@"net"]objectAtIndex:i]objectForKey:@"tradingsymbol"]];
                                                                
                                                        float qtyFloat=[[NSString stringWithFormat:@"%@", [[[[positionDictionary objectForKey:@"data"]objectForKey:@"net"]objectAtIndex:i]objectForKey:@"quantity"]] floatValue];
                                                                
                                                                NSString * qtyString=[NSString stringWithFormat:@"%.2f",qtyFloat];
                                                                
                                                                NSString * qtyStr;
                                                                
                                                                if(qtyFloat>0)
                                                                {
                                                                    qtyStr=@"Square off";
                                                                }
                                                                
                                                                else
                                                                {
                                                                    qtyStr=@"Flat";
                                                                }
                                                                
                                                                
                                                                NSDictionary * postionDetailsDict=@{@"symbol":symbolString,@"QTY":qtyString ,@"Position":qtyStr};
                                                                
                                                                [positionArray addObject:postionDetailsDict];
                                                                
                                                                                                    }
                                                                
                                                                
                                                            }
                                                            
                                                            else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
                                                            {
                                                                for(int i=0;i<[[positionDictionary objectForKey:@"data"]count];i++)
                                                                {
                                                                    NSString * symbolString=[[NSString alloc]init];
                                                                    symbolString=[NSString stringWithFormat:@"%@", [[[positionDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"symbol"]];
                                                                    
                                                                    
                                                                    
                                                                    float qtyFloat=[[NSString stringWithFormat:@"%@", [[[positionDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]] floatValue];
                                                                    
                                                                    NSString * qtyString=[NSString stringWithFormat:@"%.2f",qtyFloat];
                                                                    
                                                                    NSString * qtyStr;
                                                                    
                                                                    if(qtyFloat>0)
                                                                    {
                                                                        qtyStr=@"Square off";
                                                                    }
                                                                    
                                                                    else
                                                                    {
                                                                        qtyStr=@"Flat";
                                                                    }
                                                                    
                                                                    
                                                                    NSDictionary * postionDetailsDict=@{@"symbol":symbolString,@"QTY":qtyString ,@"Position":qtyStr};
                                                                    
                                                                    [positionArray addObject:postionDetailsDict];
                                                                    
                                                                }
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                            [mixpanel1.people set:@{@"Positions":positionArray}];
                                                            
                                                            
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                          if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
                                                          {
                                                                               
                                                                               if([[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] count]>0)
                                                                               {
                                                                                   self.noPortfolioView.hidden=YES;
                                                                                   self.positionsTableView.hidden=NO;
                                                                                   self.positionsTableView.delegate=self;
                                                                                   self.positionsTableView.dataSource=self;
                                                                                   [self.
                                                                                    positionsTableView reloadData];
                                                                               }else
                                                                               {
                                                                                   self.noPortfolioView.hidden=NO;
                                                                                   self.positionsTableView.hidden=YES;
                                                                                   
                                                                               }
                                                              
                                                              
                                                          }
                                                                               
                                                                               
                                                                               else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
                                                                               {
                                                                                   if([[positionDictionary objectForKey:@"data"] count]>0)
                                                                                   {
                                                                                       self.noPortfolioView.hidden=YES;
                                                                                       self.positionsTableView.hidden=NO;
                                                                                       self.positionsTableView.delegate=self;
                                                                                       self.positionsTableView.dataSource=self;
                                                                                       [self.
                                                                                        positionsTableView reloadData];
                                                                                   }else
                                                                                   {
                                                                                       self.noPortfolioView.hidden=NO;
                                                                                       self.positionsTableView.hidden=YES;
                                                                                       
                                                                                   }
                                                                                   
                                                                                   
                                                                               }
                                                                       
                                                                              
                                                                               
                                                                               
                                                                           });
                                                            
                                                            
                                                        }
                                                    }];
        [dataTask resume];
        [self.positionsTableView reloadData];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    

 
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    
//    if([delegate.holdingCheck isEqualToString:@"hold"])
//    {
    
    TabBar * tabPage=[self.storyboard instantiateViewControllerWithIdentifier:@"tab"];
    
    [self presentViewController:tabPage animated:YES completion:nil];
        
//    }
//
//    else
//
//    {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
    
//
}

-(void)segmentedControlChangedValue
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
         [self positionMethode];
        }
        else
        {
           
            [self mtPositionsRequest];
            
        }
    }
    
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        self.dummyPotfolioView.hidden=NO;
        self.dummyBottomView.hidden=NO;
        
        self.postionView.hidden=YES;
        self.dematView.hidden=YES;
        
        @try {
            
            segmentsArray=[[NSArray alloc]init];
            
            segmentsArray=[delegate.brokerInfoDict objectForKey:@"segment"];
           
            NSArray * segmentArray=@[@1,@2];
            NSString * segmentString=[NSString stringWithFormat:@"%@",segmentArray];
            NSString * segmentF = [segmentString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
            NSString * segment1 = [segmentF stringByReplacingOccurrencesOfString:@"(" withString:@""];
            NSString * segment2 = [segment1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            NSString * segment3 = [segment2 stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSLog(@"SegmentString:%@",segment3);
            
            

            headers = @{ @"cache-control": @"no-cache",
                         @"content-type": @"application/json"
                         };
            
            request1 = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@follower/advices",delegate.baseUrl]]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:10.0];
            
           // NSString * dateStr=[NSString stringWithFormat:@"%@",[NSDate date]];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateStyle: NSDateFormatterShortStyle];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSString *date = [dateFormat stringFromDate:[NSDate date]];
            
            parameters = @{
                           @"clientid":delegate.userID,
                           @"active":@(true),
                           @"issubscribed":@(true),
                           @"messagetypeid":@1,
                           @"limit":@3000,
                           @"sortby":@"DESC",
                           @"orderby":@"createdon",
                           @"closed":@(true),
                           @"fromdate":date,
                           @"segment":segmentsArray

                           };
            postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

            
            [request1 setHTTPMethod:@"POST"];
            [request1 setAllHTTPHeaderFields:headers];
            [request1 setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request1
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                                
                                                                dummyResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                NSLog(@"Reponse array:%@",dummyResponseArray);
                                                                
                                                                
                                                                
                                                               
                                                                //                                                        if([self.advicesArray count]>0)
                                                                //                                                        {
                                                                //                                                            [self.advicesArray removeAllObjects];
                                                                //                                                        }
                                                                //                                                        for(int i=0;i<[self.responseArray count];i++)
                                                                //                                                        {
                                                                //                                                        self.test=[[self.responseArray objectAtIndex:i] objectForKey:@"isactive"];
                                                                //
                                                                //                                                        self.check=[[[self.responseArray objectAtIndex:i] objectForKey:@"isactive"] boolValue];
                                                                //                                                        if(self.check==1)
                                                                //                                                        {
                                                                //                                                            [self.advicesArray addObject:[self.responseArray objectAtIndex:i]];
                                                                //
                                                                //                                                        }
                                                                //                                                        }
                                                                //                                                        NSLog(@"Advices:%@",self.advicesArray);
                                                                
                                                            }
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                if([[dummyResponseArray objectForKey:@"data"] count]>0)
                                                                {
                                                                    self.dummyBottomView.hidden=NO;
                                                                    self.noPortfolioView.hidden=YES;
                                                                    self.dummyTableView.hidden=NO;
                                                                    
                                                                    NSString * profitAndLoss=[[dummyResponseArray valueForKey:@"totalprofitloss"] stringValue];
                                                                    if([profitAndLoss containsString:@"-"])
                                                                    {
                                                                         self.netProfitAndLoss.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                                                    }
                                                                    
                                                                    else
                                                                    {
                                                                         self.netProfitAndLoss.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                                                        
                                                                    }
                                                                    self.netProfitAndLoss.text =profitAndLoss;
                                                                    self.dummyTableView.delegate=self;
                                                                    self.dummyTableView.dataSource=self;
                                                                    
                                                                    [self.dummyTableView reloadData];
                                                                }else
                                                                {
                                                                    self.noPortfolioView.hidden=NO;
                                                                    self.dummyBottomView.hidden=YES;
                                                                    self.dummyTableView.hidden=YES;
                                                                }

                                                                
                                                                
                                                                
                                                                
                                                            });
                                                            
                                                        }];
            [dataTask resume];
            
            
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }

        
           }
    
    else if(segmentedControl.selectedSegmentIndex==2)
    {
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
        @try {
            self.dummyPotfolioView.hidden=YES;
            self.dummyBottomView.hidden=YES;
            self.postionView.hidden=YES;
           
            NSURL *url1;
            if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
            {
            url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.kite.trade/portfolio/holdings/?api_key=%@&access_token=%@",delegate.APIKey,delegate.accessToken ]];
            
            NSLog(@"url %@",url1);
                
            }
            
            else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
            {
                 url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.upstox.com/live/profile/holdings"]];
                 NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate.accessToken];
                headers  = @{ @"cache-control": @"no-cache",
                              @"Content-Type" : @"application/json",
                              @"authorization":access,
                              @"x-api-key":@"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1"
                              };
                
            }
            
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url1];
            
            
            
            [request setHTTPMethod:@"GET"];
            if([delegate.brokerNameStr isEqualToString:@"Upstox"])
            {
                [request setAllHTTPHeaderFields:headers];
            }
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                                holdingDictionary=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
                                                                {
                                                                NSMutableArray * totalHoldingValue=[[NSMutableArray alloc]init];
                                                                
                                                                 NSMutableArray * holdingScrips=[[NSMutableArray alloc]init];
                                                                
                                                                NSLog(@"dict %@",holdingDictionary);
                                                                
                                                                
                                                                Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
                                                                
                                                                [mixpanel1 identify:delegate.userID];
                                                                
                                                              
                                                                
                                                                for(int i=0;i<[[holdingDictionary objectForKey:@"data"]count];i++)
                                                                                                                  {
                                                                                                                      NSString * string1=[[NSString alloc]init];
                                                                                                                       string1=[NSString stringWithFormat:@"%@", [[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"tradingsymbol"]];
                                                                                                                      
                                                                                                                      [holdingScrips addObject:string1];
                                                                                                                      
                                                                                              float value=[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"last_price"]floatValue]* [[[[holdingDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]floatValue];
                                                                                                                      
                                                                                                                      
//                                                                                                                      NSString * stringValue=[NSString stringWithFormat:@"%.2f",value];
//
                                                                                                                      [totalHoldingValue addObject:[NSNumber  numberWithFloat:value]];
                                                                                                                                                    }
                                                                
                                                                float sum = 0;
                                                                for (NSNumber *num in totalHoldingValue)
                                                                {
                                                                    sum += [num floatValue];
                                                                }
                                                                
                                                                NSString * totalSum=[NSString stringWithFormat:@"%.2f",sum];
                                                        
                                                                
                                                                [mixpanel1.people set:@{@"Holdings":holdingScrips,@"Totalholdingsvalue":totalSum}];
                                                                
                                                                }
                                                                
                                                                dispatch_async(dispatch_get_main_queue(),
                                                                               ^{
                                                                                   
                                                                    if([[holdingDictionary objectForKey:@"data"] count]>0)
                                                                    {
                                                                        self.noPortfolioView.hidden=YES;
                                                                        self.dematTableView
                                                                        .hidden=NO;
                                                                        self.dematView.hidden=NO;
                                                                        self.dematTableView.delegate=self;
                                                                        self.dematTableView.dataSource=self;
                                                                        
                                                                        [self.dematTableView reloadData];
                                                                    }else
                                                                    {
                                                                        
                                                                        self.noPortfolioView.hidden=NO;
                                                                        self.dematTableView.hidden=YES;
                                                                    
                                                                    }
                                                                                   
                                                                               });
                                                                
                                                                
                                                            }
                                                        }];
            [dataTask resume];
            

            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
            
            
            
        }
        
        
        else
        {
            instrumentTokensDict=[[NSMutableDictionary alloc]init];
            [self mtHoldingsRequest];
        }
       
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(tableView==self.positionsTableView)
    {
        
        return 1;
    }
    else if(tableView==self.dummyTableView)
    {
        
        return 1;
    }
    
    else if(tableView==self.dematTableView)
    {
        
        return 1;
    }
    return 0;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView==self.positionsTableView)
    {

        if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
        {
     return [[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] count];
            
        }
        
        else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
             return [[positionDictionary objectForKey:@"data"]count];
            
        }
        
        else
        {
            
            return un_array.count;
        }
        
       
    }
    else if(tableView==self.dummyTableView)
    {
        
        return [[dummyResponseArray objectForKey:@"data"]count];
    }
    
    else if(tableView==self.dematTableView)
    {
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"]||[delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
        
        return [[holdingDictionary objectForKey:@"data"]count];
        }
        
        else
        {
            return delegate.mtHoldingArray.count;
            
        }
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        if(tableView==self.positionsTableView)
        {
            if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
            {
            if([[[positionDictionary objectForKey:@"data"] objectForKey:@"net"]count]>0)
            {
                
                PortfolioCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
                cell.companyName.text=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"tradingsymbol"];
                
                NSLog(@"%@",cell.companyName.text);
                
                NSString * str1=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"quantity"];
                int finalQty=[str1 intValue];
                if(finalQty==0)
                {
                    [cell.squareoffBtn setTitle:@"FLAT" forState:UIControlStateNormal];
                }
                
                cell.qtyTypeLbl.text=[NSString stringWithFormat:@"%i",finalQty];
                NSLog(@"%@",cell.qtyTypeLbl.text);
                
                NSString * str=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"average_price"];
                float finalPrice=[str floatValue];
                
                
                
                cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",finalPrice];
                
                NSLog(@"%@",cell.priceLbl.text);
                
                NSString * plValue=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"pnl"];
                
                float finalValue=[plValue floatValue];
                
                NSString * string=[NSString stringWithFormat:@"%.2f",finalValue];
                
                if([string containsString:@"-"])
                {
                    cell.openLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                
                else
                {
                    cell.openLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                }
                
                cell.openLbl.text=string;
                
                NSLog(@"%@",cell.openLbl.text);
                cell.segmentLbl.text=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"exchange"];
                
                NSLog(@"%@",cell.segmentLbl.text);
                
                NSString * buyStr=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"buy_price"];
                
                int butInt=[buyStr intValue];
                
                if(butInt==0)
                {
                    cell.transactionTypeLbl.text=@"SELL";
                }
                else
                {
                    cell.transactionTypeLbl.text=@"BUY";
                }
                
                [cell.squareoffBtn addTarget:self action:@selector(squareoffBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
                
            }
                
            }
            
            else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
            {
                
                if([[positionDictionary objectForKey:@"data"]count]>0)
                {
                    
                    PortfolioCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                    
                    cell.companyName.text=[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
                    
                    NSLog(@"%@",cell.companyName.text);
                    
                    NSString * str1=[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"net_quantity"];
                    int finalQty=[str1 intValue];
                    if(finalQty==0)
                    {
                        [cell.squareoffBtn setTitle:@"FLAT" forState:UIControlStateNormal];
                        cell.openLbl.text=[NSString stringWithFormat:@"%.2f",[[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"realized_profit"]floatValue]];
                    }else
                    {
                        [cell.squareoffBtn setTitle:@"SQUARE OFF" forState:UIControlStateNormal];
                        cell.openLbl.text=[NSString stringWithFormat:@"%.2f",[[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"unrealized_profit"]floatValue]];
                    }
                    
                    cell.qtyTypeLbl.text=[NSString stringWithFormat:@"%i",finalQty];
                    NSLog(@"%@",cell.qtyTypeLbl.text);
                    
//                    NSString * str=[[[positionDictionary objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"average_price"];
//                    float finalPrice=[str floatValue];
//
//
//
//                    cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",finalPrice];
//
//                    NSLog(@"%@",cell.priceLbl.text);
//
//                    NSString * plValue=[[[[positionDictionary objectForKey:@"data"] objectForKey:@"net"] objectAtIndex:indexPath.row] objectForKey:@"pnl"];
//
//                    float finalValue=[plValue floatValue];
//
//                    cell.openLbl.text=[NSString stringWithFormat:@"%.2f",finalValue];
//
//                    NSLog(@"%@",cell.openLbl.text);
                    cell.segmentLbl.text=[[[positionDictionary objectForKey:@"data"]objectAtIndex:indexPath.row] objectForKey:@"exchange"];
                    
                    NSLog(@"%@",cell.segmentLbl.text);
                    
                    NSString * buyStr=[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buy_amount"];
                    
                    int butInt=[buyStr intValue];
                    
                    if(butInt==0)
                    {
                        cell.transactionTypeLbl.text=@"SELL";
                        NSString * str=[[[positionDictionary objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"avg_sell_price"];
                                            float finalPrice=[str floatValue];
                        
                        
                        
                                            cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",finalPrice];
                        
//                        NSString * ltp=[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"avg_sell_price"]];
                        
                        
                    }
                    else
                    {
                        cell.transactionTypeLbl.text=@"BUY";
                        
                        NSString * str=[[[positionDictionary objectForKey:@"data"]  objectAtIndex:indexPath.row] objectForKey:@"avg_buy_price"];
                        float finalPrice=[str floatValue];
                        
                        
                        
                        cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",finalPrice];
                        
                        
                    }
                    
                    [cell.squareoffBtn addTarget:self action:@selector(squareoffBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                    return cell;
                    
                }
                
                
            }
            
            else
            {
                
                PortfolioCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL" forIndexPath:indexPath];
                
//                cell.companyName.text=[[delegate.mtpositionsArray objectAtIndex:indexPath.row] objectForKey:@"stSymbol"];
                
                int securityId=[[[un_array objectAtIndex:indexPath.row] objectForKey:@"stSecurityID"] intValue];
                
                for(int i=0;i< delegate.scripsMt.count;i++)
                {
                    int securityId2=[[[ delegate.scripsMt objectAtIndex:i] objectForKey:@"exchange_token"] intValue];
                    
                    if(securityId==securityId2)
                    {
                        cell.companyName.text=[NSString stringWithFormat:@"%@",[[ delegate.scripsMt objectAtIndex:i] objectForKey:@"tradingsymbol"]];
                    }
                }
                
                
                NSLog(@"%@",cell.companyName.text);
                
               
                
//                if(buyQty==0)
//                {
//                     cell.qtyTypeLbl.text=[[delegate.mtpositionsArray objectAtIndex:indexPath.row] objectForKey:@"inSellQty"];
//
//                    cell.priceLbl.text=[[delegate.mtpositionsArray objectAtIndex:indexPath.row] objectForKey:@"dbSellAmount"];
//
//                    cell.transactionTypeLbl.text=@"SELL";
//
//
//
//                }
//
//                else if(sellQty==0)
//                {
                    cell.qtyTypeLbl.text=[[un_array objectAtIndex:indexPath.row] objectForKey:@"inQty"];
                
                if([cell.qtyTypeLbl.text isEqualToString:@"0"])
                {
                    [cell.squareoffBtn setTitle:@"FLAT" forState:UIControlStateNormal];
                }
                
                else
                {
                    [cell.squareoffBtn setTitle:@"SQUARE OFF" forState:UIControlStateNormal];
                    
                }
                    
                    cell.priceLbl.text=@"---";
//                NSString * string1=[[un_array objectAtIndex:indexPath.row] objectForKey:@"btSide"];
//
//                int orderType1=[string1 intValue];
//
//                if(orderType1==1)
//                {
//                    string1=@"BUY";
//                }
//
//                else if(orderType1==2)
//                {
//                    string1=@"SELL";
//                }
                
                if([cell.qtyTypeLbl.text isEqualToString:@"0"])
                {
                     cell.transactionTypeLbl.text=@"B/S";
                }
                
               else  if([cell.qtyTypeLbl.text containsString:@"-"])
                {
                    cell.transactionTypeLbl.text=@"SELL";
                }
                
                else
                {cell.transactionTypeLbl.text=@"BUY";
                    
                }
                    
                
//                }
                
                
                
              
                
            
               float finalValue=0;
                
                cell.openLbl.text=[NSString stringWithFormat:@"%.2f",finalValue];
                
                NSLog(@"%@",cell.openLbl.text);
                cell.segmentLbl.text=[[un_array objectAtIndex:indexPath.row] objectForKey:@"stExchange"];
                
                NSLog(@"%@",cell.segmentLbl.text);
                
                
                [cell.squareoffBtn addTarget:self action:@selector(squareoffBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                return cell;

                
            }
            
        }
        
        
        else if(tableView==self.dummyTableView)
        {
            
            DummyTableCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            
            
            cell.companyNameLabel.text = [[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"companyname"];
            cell.adviceLabel.text = [[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"firstname"];
            cell.LTPLabel.text = [[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"lasttradedprice"];
            
            NSString * entryPrice = [[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"entryprice"];
            
            if([entryPrice isEqual:[NSNull null]])
            {
                cell.priceLabel.text = @"0";
            }else
            {
                cell.priceLabel.text = entryPrice;
            }
            cell.NSECashLabel.text = [[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"segmenttype"];
            int buySell = [[[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"buysell"] intValue];
            if(buySell==1)
            {
                cell.boughtLabel.text = @"BOUGHT";
            }else if(buySell==2)
            {
                cell.boughtLabel.text = @"SOLD";
            }
            
            NSString * profitAndLoss = [[[dummyResponseArray objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"profitloss"];
            
            if([profitAndLoss isEqual:[NSNull null]])
            {
                cell.profitAndLossLabel.text= @"0";
            }else
            {
                
                if([profitAndLoss containsString:@"-"])
                {
                    cell.profitAndLossLabel.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                }
                
                else
                {
                    cell.profitAndLossLabel.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                    
                }
                cell.profitAndLossLabel.text=profitAndLoss;
            }
            
            
            
            
            return cell;
        }
        
        else if(tableView==self.dematTableView)
        {
            
            
            
            DematCell * cell=[tableView dequeueReusableCellWithIdentifier:@"CELL1" forIndexPath:indexPath];
            if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
            {
            if([[holdingDictionary objectForKey:@"data"]count]>0)
            {
                cell.companyLabel.text= [NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"tradingsymbol"]];
                cell.QTYLabel.text=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
                cell.LPTLabel.text=[NSString stringWithFormat:@"%.2f",[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"last_price"] floatValue]];
                
                //        NSString * local=[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"average_price"];
                
                float value=[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"last_price"]floatValue]* [[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]floatValue];
                
                //        float localFloat=[local floatValue];
                cell.valueLabel.text=[NSString stringWithFormat:@"%.2f",value];
                cell.exchaneLbl.text=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"exchange"]];
                
                [cell.tradeBtn addTarget:self action:@selector(tradeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                 return cell;
                
            }
                
            }else if ([delegate.brokerNameStr isEqualToString:@"Upstox"])
            {
                
                cell.companyLabel.text = [NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"symbol"]];
                cell.QTYLabel.text=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
                cell.LPTLabel.text=@"0.0";
                
                //        NSString * local=[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"average_price"];
                
              //  float value=[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"last_price"]floatValue]* [[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]floatValue];
                
                //        float localFloat=[local floatValue];
                cell.valueLabel.text=@"0.0";
                cell.exchaneLbl.text=[NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"exchange"]];
                
                [cell.tradeBtn addTarget:self action:@selector(tradeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
                }
            
            
            else
            {
                
                cell.companyLabel.text= [NSString stringWithFormat:@"%@",[[delegate.mtHoldingArray objectAtIndex:indexPath.row]objectForKey:@"stSymbolHolding"]];
                cell.QTYLabel.text=[NSString stringWithFormat:@"%@",[[delegate.mtHoldingArray objectAtIndex:indexPath.row]objectForKey:@"inNetQtyHolding"]];
               
                
                //        NSString * local=[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"average_price"];
                
//                float value=[[[delegate.mtHoldingArray objectAtIndex:indexPath.row]objectForKey:@"inNetQtyHolding"] floatValue]*[[[delegate.mtHoldingArray objectAtIndex:indexPath.row]objectForKey:@"dbClosingPrice"]floatValue];
//
//                //        float localFloat=[local floatValue];
//                cell.valueLabel.text=[NSString stringWithFormat:@"%.2f",value];
                
                int securityId=[[[delegate.mtHoldingArray objectAtIndex:indexPath.row]objectForKey:@"stSecurity"] intValue];
                
                 cell.LPTLabel.text=@"0.00";
                
                NSArray * allKeys=[instrumentTokensDict allKeys];
                
                cell.valueLabel.text=@"0.00";
                
                for(int i=0;i<allKeys.count;i++)
                {
                    NSString *  localStr=[allKeys objectAtIndex:i];
                    
                    
                    
                    if([localStr intValue]==securityId)
                    {
                        
                        NSString * ltp=[NSString stringWithFormat:@"%.2f",[[[instrumentTokensDict objectForKey:localStr]objectForKey:@"dbLTP"]floatValue]];
                        
                         cell.LPTLabel.text=ltp;
                        
                        float value=[cell.LPTLabel.text floatValue] * [cell.QTYLabel.text floatValue];
                        
                         cell.valueLabel.text=[NSString stringWithFormat:@"%.2f",value];
                    
                    }
                   
                }
                
                for(int i=0;i< delegate.scripsMt.count;i++)
                {
                    int securityId2=[[[ delegate.scripsMt objectAtIndex:i] objectForKey:@"exchange_token"] intValue];
                    
                    if(securityId==securityId2)
                    {
                        NSString * str=[NSString stringWithFormat:@"%@",[[ delegate.scripsMt objectAtIndex:i] objectForKey:@"exchangevalue"]];
                        
                        if([str isEqual:[NSNull null]])
                        {
                            cell.exchaneLbl.text=@"No exchage";
                        }
                        
                        else
                        {
                            cell.exchaneLbl.text=str;
                        }
                        
                    }
                }
                  [cell.tradeBtn addTarget:self action:@selector(tradeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                
                return cell;
            }

        
    }
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
            
        
    
    


    
    
   
    
}

- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    
    
    if(indexPath.row % 2 == 0)
        
        cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.positionsTableView || tableView==self.dematTableView)
    {
         return 70;
    }
    if(tableView==self.dummyTableView)
    {
        return 83;
    }
    return 0;
   
}

-(void)tradeBtnAction:(UIButton*)sender
{
    
    delegate.navigationCheck=@"TOP";
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.dematTableView];
    
    delegate.holdingCheck=@"hold";
    
    NSIndexPath *indexPath = [self.dematTableView indexPathForRowAtPoint:buttonPosition];
   DematCell *cell = [self.dematTableView cellForRowAtIndexPath:indexPath];
    
    @try {
        
        if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
        {
        delegate.orderSegment=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"exchange"]];
        
        delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"tradingsymbol"]];
        
        delegate.holdingsQty=[NSString stringWithFormat:@"%@",[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
        
        
       
            
            
        }else if ([delegate.brokerNameStr isEqualToString:@"Upstox"])
        {
            delegate.orderSegment=[NSString stringWithFormat:@"%@",cell.exchaneLbl.text];
            
            delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",cell.companyLabel.text];
            
            delegate.holdingsQty=[NSString stringWithFormat:@"%@",cell.QTYLabel.text];
            
            
            delegate.orderinstrument=[NSString stringWithFormat:@"%@",[[[[[holdingDictionary objectForKey:@"data"]objectAtIndex:indexPath.row]objectForKey:@"instrument"]objectAtIndex:0]objectForKey:@"token"]];
        }
        
        
        else
        {
            
            
            delegate.orderSegment=[NSString stringWithFormat:@"%@",cell.exchaneLbl.text];
            
            delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",cell.companyLabel.text];
            
            delegate.holdingsQty=[NSString stringWithFormat:@"%@",cell.QTYLabel.text];
            
           
            delegate.orderinstrument=[[delegate.mtHoldingArray objectAtIndex:indexPath.row]objectForKey:@"stSecurity"];
            
            
        }
        
        
        if(delegate.orderSegment.length>0 && delegate.symbolDepthStr.length>0)
        {
            
            StockOrderView * stockOrder=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            delegate.orderBool=true;
            delegate.portfolioBackBool=true;
              [self.navigationController pushViewController:stockOrder animated:YES];
//            [self presentViewController:stockOrder animated:self completion:nil];
        }
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
  }

-(void)squareoffBtnAction:(UIButton*)sender
{
    
    delegate.holdingCheck=@"position";
     delegate.navigationCheck=@"TOP";
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.positionsTableView];
    
    
    
    NSIndexPath *indexPath = [self.positionsTableView indexPathForRowAtPoint:buttonPosition];
    //    FollowCell *cell = [self.followerTableView cellForRowAtIndexPath:indexPath];
    
    
   if([delegate.brokerNameStr isEqualToString:@"Zerodha"])
   {
    PortfolioCell  *cell = [self.positionsTableView cellForRowAtIndexPath:indexPath];
       delegate.orderSegment=[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"]objectForKey:@"net" ] objectAtIndex:indexPath.row]objectForKey:@"exchange"]];
       
       delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"]objectForKey:@"net" ] objectAtIndex:indexPath.row]objectForKey:@"tradingsymbol"]];
       
      NSString * string=[NSString stringWithFormat:@"%@",[[[[positionDictionary objectForKey:@"data"]objectForKey:@"net" ] objectAtIndex:indexPath.row]objectForKey:@"quantity"]];
       
       if([string containsString:@"-"])
       {
           
           delegate.holdingsQty=[string stringByReplacingOccurrencesOfString:@"-" withString:@""];
       }
       
       else
       {
           delegate.holdingsQty=string;
       }
       
       
       delegate.POSITIONtransctionType=cell.transactionTypeLbl.text;
   }else if([delegate.brokerNameStr isEqualToString:@"Upstox"])
   {
       PortfolioCell  *cell = [self.positionsTableView cellForRowAtIndexPath:indexPath];
       delegate.orderSegment=[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"exchange"]];
       
       delegate.symbolDepthStr=[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
       
       NSString * string=[NSString stringWithFormat:@"%@",[[[positionDictionary objectForKey:@"data"] objectAtIndex:indexPath.row]objectForKey:@"net_quantity"]];
       
       if([string containsString:@"-"])
       {
           
           delegate.holdingsQty=[string stringByReplacingOccurrencesOfString:@"-" withString:@""];
       }
       
       else
       {
           delegate.holdingsQty=string;
       }
       
       
       
       delegate.POSITIONtransctionType=cell.transactionTypeLbl.text;
   }
    
    else
    {
        PortfolioCell  *cell = [self.positionsTableView cellForRowAtIndexPath:indexPath];
        delegate.orderSegment=[[un_array objectAtIndex:indexPath.row] objectForKey:@"stExchange"];
        
        delegate.symbolDepthStr=cell.companyName.text;
        
        NSString * string=[[un_array objectAtIndex:indexPath.row] objectForKey:@"inQty"];
        if([string containsString:@"-"])
        {
            
            delegate.holdingsQty=[string stringByReplacingOccurrencesOfString:@"-" withString:@""];
        }
        
        else
        {
            delegate.holdingsQty=string;
        }
        
       
        
        
        delegate.orderinstrument=[[un_array objectAtIndex:indexPath.row] objectForKey:@"stSecurityID"];
        
        delegate.POSITIONtransctionType=cell.transactionTypeLbl.text;
    }
    
     @try {
   
    
    
    if(delegate.orderSegment.length>0 && delegate.symbolDepthStr.length>0)
    {
        if([delegate.holdingsQty isEqualToString:@"0"])
        {
                dispatch_async(dispatch_get_main_queue(), ^{




                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Squared off" message:@"Your position is already squared" preferredStyle:UIAlertControllerStyleAlert];



                    [self presentViewController:alert animated:YES completion:^{



                    }];



                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {



                    }];



                    [alert addAction:okAction];

                });
        }
        
        else
        {
        StockOrderView * stockOrder=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
        delegate.orderBool=true;
        delegate.portfolioBackBool=true;
            
            [self.navigationController pushViewController:stockOrder animated:YES];
//
//        [self presentViewController:stockOrder animated:self completion:nil];
            
        }
    }
         
     }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }

    
}


//reachability//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}


- (void)mtpositions:(NSNotification *)note
{
    
    Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
    
    [mixpanel1 identify:delegate.userID];
   
    
    newMtPositionsArray=[[NSMutableArray alloc]init];
    for(int i=0;i<delegate.allTradeArray.count;i++)
    {
        NSString * string=[[delegate.allTradeArray objectAtIndex:i] objectForKey:@"btOrderStatus"];
        
        int status=[string intValue];
        
        
        if(status==2)
        {
            [delegate.mtpositionsArray addObject:[delegate.allTradeArray objectAtIndex:i]];
            [securityTokenArray addObject:[NSString stringWithFormat:@"%@",[[delegate.allTradeArray objectAtIndex:i] objectForKey:@"stSecurityID"]]];
              [mtExchangeArray addObject:[NSString stringWithFormat:@"%@",[[delegate.allTradeArray objectAtIndex:i] objectForKey:@"stExchange"]]];
            [mixpanel1.people set:@{@"Positions":delegate.mtpositionsArray}];
            
            
            [delegate.allTradeArray removeObjectAtIndex:i];
        }
    }
    
    
//    for(int i=0;i<delegate.mtpositionsArray.count;i++)
//    {
//
//        [securityTokenArray addObject:[NSString stringWithFormat:@"%@",[[delegate.mtpositionsArray objectAtIndex:i] objectForKey:@"stSecurityID"]]];
//        if(i==0)
//        {
//
//
//        }
//
//
//        else
//        {
//            NSString * securityToken=[NSString stringWithFormat:@"%@",[[delegate.mtpositionsArray objectAtIndex:i] objectForKey:@"stSecurityID"]];
//
//        }
//
//
//    }
   
    
  un_array = [NSMutableArray array];
    
    NSMutableArray * tmpArray=[[NSMutableArray alloc]init];
    for (id obj in delegate.mtpositionsArray)
    {
    if(un_array.count>0)
    {
      for(int j=0;j<un_array.count;j++)
    {
        
        NSString * securityId1=[NSString stringWithFormat:@"%@",[[un_array objectAtIndex:j] objectForKey:@"stSecurityID"]];
                [tmpArray addObject:securityId1];
    }
        NSString * security=[NSString stringWithFormat:@"%@",[obj objectForKey:@"stSecurityID"] ];
        if ([tmpArray containsObject:security])
        {
            
            
//            [dupArray addObject:obj];
            NSMutableDictionary * tmpDict=[[NSMutableDictionary alloc]init];
            
            NSString * transaction1=[obj objectForKey:@"btSide"];
            
            if([transaction1 isEqualToString:@"1"])
            {
                
                [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"inQty"]] forKey:@"inQty"];
            }
            
            else if([transaction1 isEqualToString:@"2"])
            {
                
                [tmpDict setObject:[NSString stringWithFormat:@"-%@",[obj objectForKey:@"inQty"]] forKey:@"inQty"];
            }
            
            [tmpDict setObject:transaction1 forKey:@"btSide"];
            [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"stSecurityID"]] forKey:@"stSecurityID"];
            [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"stExchange"]] forKey:@"stExchange"];
            
            NSUInteger  index = 0;
            
            for(int i=0;i<un_array.count;i++)
            {
                NSString * stId=[NSString stringWithFormat:@"%@",[[un_array objectAtIndex:i]objectForKey:@"stSecurityID"]];
                NSString * stId1=[NSString stringWithFormat:@"%@",[tmpDict objectForKey:@"stSecurityID"]];
                if([stId isEqualToString:stId1])
                {
                    index=i;
                }
                
                
            }
            
            
            int newtransaction=[[[un_array objectAtIndex:index] objectForKey:@"btSide"] intValue];
            
            int qty1=[[[un_array objectAtIndex:index] objectForKey:@"inQty"] intValue];
            
            
            
            int newtransaction1=[[obj objectForKey:@"btSide"] intValue];
            
            int qty2;
            
            if(newtransaction1==2)
            {
                
                NSString * str=[NSString stringWithFormat:@"-%@", [obj objectForKey:@"inQty"]];
                
                  qty2=[str intValue];
            }
            
            else
                
            {
                
                NSString * str=[NSString stringWithFormat:@"%@", [obj objectForKey:@"inQty"]];
                
                qty2=[str intValue];
            }
            
           
            
            
            int finalQty;
            
//            if(newtransaction==newtransaction1)
//            {
                finalQty=qty1+qty2;
//            }
            
//            else
//            {
//                finalQty=qty2+qty1;
//            }
//
            NSMutableDictionary * tmpDict1=[[NSMutableDictionary alloc]init];
            
            
            
            [tmpDict1 setObject:[NSString stringWithFormat:@"%i",finalQty] forKey:@"inQty"];
            
            [tmpDict1 setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"stSecurityID"]] forKey:@"stSecurityID"];
            if(finalQty<0)
            {
                [tmpDict1 setObject:@"2" forKey:@"btSide"];
                
            }
            
            else
            {
                [tmpDict1 setObject:@"1" forKey:@"btSide"];
            }
            
            [tmpDict1 setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"stExchange"]] forKey:@"stExchange"];
            
            [un_array replaceObjectAtIndex:index withObject:tmpDict1];
            
            
        }
        else
        {
//
            
            
            NSMutableDictionary * tmpDict=[[NSMutableDictionary alloc]init];
            
            NSString * transaction1=[obj objectForKey:@"btSide"];
            
            if([transaction1 isEqualToString:@"1"])
            {
                
                [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"inQty"]] forKey:@"inQty"];
            }
            
            else if([transaction1 isEqualToString:@"2"])
            {
                
                [tmpDict setObject:[NSString stringWithFormat:@"-%@",[obj objectForKey:@"inQty"]] forKey:@"inQty"];
            }
            
            [tmpDict setObject:transaction1 forKey:@"btSide"];
            [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"stSecurityID"]] forKey:@"stSecurityID"];
            [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"stExchange"]] forKey:@"stExchange"];
            
            [un_array addObject:tmpDict];
            
        }
            
        
            
        }
        
        else
        {
            
            NSMutableDictionary * tmpDict=[[NSMutableDictionary alloc]init];
            
            NSString * transaction1=[obj objectForKey:@"btSide"];
            
            if([transaction1 isEqualToString:@"1"])
            {
                
                [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"inQty"]] forKey:@"inQty"];
            }
            
            else if([transaction1 isEqualToString:@"2"])
            {
                
                [tmpDict setObject:[NSString stringWithFormat:@"-%@",[obj objectForKey:@"inQty"]] forKey:@"inQty"];
            }
            
            [tmpDict setObject:transaction1 forKey:@"btSide"];
            [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"stSecurityID"]] forKey:@"stSecurityID"];
             [tmpDict setObject:[NSString stringWithFormat:@"%@",[obj objectForKey:@"stExchange"]] forKey:@"stExchange"];
            
            [un_array addObject:tmpDict];
            

            
        }
    }
    

    
    NSLog(@"%@",un_array);
   
    [self symbolMethod];
    
   
 
    
}

-(void)symbolMethod
{
    if(securityTokenArray.count>0)
    {
        NSArray * userArray=[securityTokenArray mutableCopy];
        NSSet *set = [NSSet setWithArray:userArray];
        NSArray *uniqueArray = [set allObjects];
        NSArray * userArray1=[mtExchangeArray mutableCopy];
        NSSet *set1 = [NSSet setWithArray:userArray1];
        NSArray *uniqueArray1 = [set1 allObjects];
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"cache-control": @"no-cache",
                                   };
        NSDictionary *postParameters=[[NSDictionary alloc]init];
        if(segmentedControl.selectedSegmentIndex==2)
        {
           postParameters = @{ @"exchangetokens":uniqueArray,
                               @"symbol":uniqueArray1
                              };
            
        }
        else
        {
            postParameters = @{ @"exchangetokens":uniqueArray,
                                @"exchangevalue":uniqueArray1
                              };
            
        }
      
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:postParameters options:0 error:nil];
        
        NSString * string=[NSString stringWithFormat:@"%@stock/tokens",delegate.baseUrl];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:string]
                                       
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:30.0];
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(data!=nil)
                                                        {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                                NSMutableArray * newSymbolResponseArray=[[NSMutableArray alloc]init];
                                                                
                                                                NSMutableArray * symbolResponse=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                for(int i=0;i<symbolResponse.count;i++)
                                                                {
                                                                    [newSymbolResponseArray addObject:[symbolResponse objectAtIndex:i]];
                                                                    
                                                                }
                                                                
                                                                
                                                                NSArray * symbols=[newSymbolResponseArray mutableCopy];
                                                                NSSet *set = [NSSet setWithArray:symbols];
                                                                delegate.scripsMt = [set allObjects];
                                                                
                                                            }
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.noPortfolioView.hidden=YES;
                                                           
                                                           if(segmentedControl.selectedSegmentIndex==0)
                                                           {self.positionsTableView.hidden=NO;
                                                            
                                                            self.positionsTableView.delegate=self;
                                                            self.positionsTableView.dataSource=self;
                                                            [self.positionsTableView reloadData];
                                                               
                                                           }
                                                            
                                                            else if(segmentedControl.selectedSegmentIndex==2)
                                                            {
                                                                
                                                                if(delegate.scripsMt.count==delegate.mtHoldingArray.count
                                                                   )
                                                                {
                                                                [self brodcastRequest];
                                                                self.dematView.hidden=NO;
                                                                self.dematTableView.delegate=self;
                                                                self.dematTableView.dataSource=self;
                                                                
                                                                [self.dematTableView reloadData];
                                                                    
                                                                }
                                                            }
                                                            
                                                        });
                                                        
                                                    }];
        [dataTask resume];
        
    }
    
    
    
}


-(void)mtPositionsRequest
{
    securityTokenArray=[[NSMutableArray alloc]init];
    mtExchangeArray=[[NSMutableArray alloc]init];
    
    delegate.scripsMt=[[NSMutableArray alloc]init];
    self.dummyPotfolioView.hidden=YES;
    self.dummyBottomView.hidden=YES;
    self.postionView.hidden=NO;
    self.dematView.hidden=YES;
    
    delegate.mtpositionsArray=[[NSMutableArray alloc]init];
    exchangeTokenArray=[[NSMutableArray alloc]init];
   
    delegate.allTradeArray=[[NSMutableArray alloc]init];
    TagEncode * tag = [[TagEncode alloc]init];
    delegate.mtCheck=true;
    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.OrderDownloadRequest];
    [tag GetBuffer];
    
    [self newMessage];//     [[NSNotificationCenter defaultCenter] postNotificationName:@"ordertowatch" object:nil];
    
    
//    delegate.allTradeArray=[[NSMutableArray alloc]init];
//    tag = [[TagEncode alloc]init];
//    delegate.mtCheck=true;
//    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.TradeDownloadRequest];
//    [tag GetBuffer];
//
//    [self newMessage];

}

-(void)mtHoldingsRequest
{
    securityTokenArray=[[NSMutableArray alloc]init];
    mtExchangeArray=[[NSMutableArray alloc]init];
    delegate.scripsMt=[[NSMutableArray alloc]init];
    delegate.mtHoldingArray=[[NSMutableArray alloc]init];
    self.dummyPotfolioView.hidden=YES;
    self.dummyBottomView.hidden=YES;
    self.postionView.hidden=YES;
    self.dematView.hidden=NO;
    
//    delegate.mtpositionsArray=[[NSMutableArray alloc]init];
//    exchangeTokenArray=[[NSMutableArray alloc]init];
//
//    delegate.allTradeArray=[[NSMutableArray alloc]init];
    TagEncode * tag = [[TagEncode alloc]init];
    delegate.mtCheck=true;
    [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.holdingRequest];
    [tag GetBuffer];
    
    [self newMessage];//     [[NSNotificationCenter defaultCenter] postNotificationName:@"ordertowatch" object:nil];
    
}

- (void)mtHoldings:(NSNotification *)note
{
    Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
    
    [mixpanel1 identify:delegate.userID];
    securityTokenArray=[[NSMutableArray alloc]init];
     mtExchangeArray=[[NSMutableArray alloc]init];
    
   
    for(int i=0;i<delegate.mtHoldingArray.count;i++)
    {
       
        
            NSString * security= [NSString stringWithFormat:@"%@",[[delegate.mtHoldingArray objectAtIndex:i] objectForKey:@"stSecurity"]];
         NSString * mtExchage= [NSString stringWithFormat:@"%@",[[delegate.mtHoldingArray objectAtIndex:i] objectForKey:@"stSymbolHolding"]];
        
        if(security.length!=0)
        {
        [securityTokenArray addObject:security];
        
        }
        
        if(mtExchage.length!=0)
        {
            
            [mtExchangeArray addObject:mtExchage];
        }
        
        else
        {
            
            [mtExchangeArray addObject:@""];
        }
        
    }
    
   
    
      [mixpanel1.people set:@{@"Holdings":delegate.mtHoldingArray}];
    
    [self symbolMethod];
}
    
  -(void)brodcastRequest
{
      [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
    
    for(int i=0;i<delegate.scripsMt.count;i++)
    {
        
        TagEncode * tag = [[TagEncode alloc]init];
        delegate.mtCheck=true;
        
        
        //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];
        
        
        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Broadcast];
        //        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
        [tag TagData:sharedManager.stSecurityID stringMethod:[[delegate.scripsMt objectAtIndex:i] objectForKey:@"exchange_token"]];
        [tag TagData:sharedManager.stExchange stringMethod:[[delegate.scripsMt objectAtIndex:i] objectForKey:@"exchangevalue"]];
        [tag GetBuffer];
        [self newMessage1];
        
        
    }
}

- (void)showMainMenu:(NSNotification *)note
{
    
    //     localExchangeTokensArray=[[NSMutableArray alloc]init];
    //
    //    for(int i=0;i<delegate2.localExchageToken.count;i++)
    //    {
    //        NSString * first=[delegate2.localExchageToken objectAtIndex:i];
    //
    //
    //
    //        for(int j=0;j<delegate2.marketWatch1.count;j++)
    //        {
    //            NSString * second=[NSString stringWithFormat:@"%@",[[delegate2.marketWatch1 objectAtIndex:j] objectForKey:@"stSecurityID"]];
    //
    //            if([second containsString:first])
    //            {
    //                [localExchangeTokensArray addObject:[delegate2.marketWatch1 objectAtIndex:j]];
    //
    //            }
    //        }
    //
    //
    //
    //    }
    //
    ///
    
   
        
        
        
        //    instrumentTokensDict=[[NSMutableDictionary alloc]init];
        NSString * newExchangeToken;
        
        
        for(int i=0;i<delegate.marketWatch1.count;i++)
        {
            NSString * tmpInstrument=[NSString stringWithFormat:@"%@",[[delegate.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"]];
            
            for(int j=0;j<delegate.scripsMt.count;j++)
            {
                NSString * exchangeToken=[NSString stringWithFormat:@"%@",[[delegate.scripsMt objectAtIndex:j]objectForKey:@"exchange_token"]];
                
                if([tmpInstrument containsString:exchangeToken])
                {
                    newExchangeToken=exchangeToken;
                    [instrumentTokensDict setValue:[delegate.marketWatch1 objectAtIndex:i] forKeyPath:newExchangeToken];
                }
                
                
            }
            
            
            
            
        }
    
    
        [self.dummyTableView reloadData];
        
        
        
    
        
    }


@end
