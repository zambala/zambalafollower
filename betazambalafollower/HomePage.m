//
//  HomePage.m
//  testing
//
//  Created by zenwise mac 2 on 11/15/16.
//  Copyright © 2016 zenwise mac 2. All rights reserved.
//

#import "HomePage.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonHMAC.h>
#import "PSWebSocket.h"
#import "StockView1.h"

#import "MarketTableViewCell.h"

#import "PSWebSocketDriver.h"

#import<malloc/malloc.h>
#import "StockOrderView.h"
#import "SearchSymbolView.h"
#import "TagDecode.h"
#import "TagEncode.h"
#import "MT.h"
#import <Mixpanel/Mixpanel.h>

@interface HomePage () <PSWebSocketDelegate,NSStreamDelegate>
{
    AppDelegate * delegate2;
    NSDictionary * dict1;
    NSString * string1;/*!<it store message (sending to kite)*/
    NSMutableDictionary *instrumentTokensDict;/*!<it store market price data of symbols*/
    NSMutableArray *allKeysList;/*!<it store all keys of instrumentTokensDict*/
    NSMutableArray *allKeysList1;/*!<it store all keys of instrumentTokensDict*/
    NSString * option;/*!<it store option type of symbol*/
    NSString * finalDate;/*!<it store expiry data of symbol*/
    NSMutableArray * localInstrumentToken;/*!<it store instrument tokens of symbols*/
    NSMutableDictionary * responseDict1;/*!<it store details of symbol from zambala server*/
    NSMutableArray * localExchangeTokensArray;/*!<it store exchange tokens of symbols*/
    NSMutableArray * symbolArray;
    bool temp;
    int value;
    int packetsLength;
    int packetsNumber;
    float lastPriceFlaotValue;
    float closePriceFlaotValue;
    NSMutableDictionary *tmpDict;
    MT * sharedManager;
    TagEncode * tag1;
    BOOL disapperCheck;
    
    float changeFloatValue;
    
    float changeFloatPercentageValue;
    BOOL scripCheck;
    BOOL editCheck;
    NSMutableDictionary * scripDetails;/*!<it store data of added symbols(used to upload to zambala server)*/
    NSMutableArray * scripDetailsArray;/*!<it store data of added symbols(used to upload to zambala server)*/
    NSString * isDelete;
    
//mt//
    
    
    
}

@end

@implementation HomePage

- (void)viewDidLoad {
    [super viewDidLoad];
    temp = YES;
    scripCheck=true;
    //mt//
    sharedManager = [MT Mt1];
    //    sharedManagerMaster = [MTMaster Mt3];
    tag1 = [[TagEncode alloc]init];
    value  =0 ;
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    dict1=[[NSDictionary alloc]init];
    
    allKeysList = [[NSMutableArray alloc]init];
    
   self.navigationItem.title = @"MARKET WATCH";
    _headerView.frame = CGRectMake(_headerView.frame.origin.x,64,_headerView.frame.size.width , _headerView.frame.size.height);
   
   
//    if(delegate2.firstTime==true)
//    {
//    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        NSLog(@"%@",userDefaults);
//    delegate2.instrumentToken = [userDefaults objectForKey:@"token"];
//        
//        delegate2.firstTime=false;
//    
//    }
    
   
    [self.activityIndicator startAnimating];
    
   
    
    
    
    if(delegate2.marketWatchBool==true)
    {
        [self getScripMethod];
    }
    
     [self tmpDictMethod];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    delegate2=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMainMenu:)
                                                 name:@"orderwatch" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderrequest:)
                                                 name:@"ordertowatch" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMainMenu1:)
                                                 name:@"watchCheck" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(backMethod:)
                                                 name:@"back" object:nil];
    
    
    
    
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden=NO;
    
    
    
   
    
//     delegate2.marketWatch1=[[NSMutableArray alloc]init];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Market Watch page"];
    
    [self networkStatus];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    self.reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    self.reach = [Reachability reachabilityForInternetConnection];
    [self.reach startNotifier];
     editCheck=true;
    
   
    
    if(delegate2.marketWatchBool==false)
    {
        if(delegate2.searchToWatch==true)
        {
            
           
               if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
               {
                   
                    [self instruments];
               }
                else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    //[self.socket close];
                    [self webSocket];
                    [self instruments];
                }
                else
                {
            //    if(delegate2.searchToWatch==true)
            //    {
            //        delegate2.searchToWatch=false;
            
            //        [self instruments];
            
            //    }
            //
            //    else
            //    {
//                     [[NSNotificationCenter defaultCenter] postNotificationName:@"orderwatch" object:nil];
                    
            //
            //    }
                    
                    [self instruments];
            
        }
            
//             [self resultCheck];
            
        }
        
        else
        {
        [self tmpDictMethod];
        [self webSocket];
        [self socketHitMethod];
            //[self.socket close];
        
            
        }
        

//
//    }
        
        
    }
    self.activityIndicator.hidden=YES;
    [self.activityIndicator stopAnimating];
}

/*!
 @discussion used to connect socket.
 */

-(void)webSocket
{
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
    @try {
        instrumentTokensDict = [[NSMutableDictionary alloc]init];
        //pocket socket//
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"wss://websocket.kite.trade/?api_key=%@&user_id=%@&public_token=%@",delegate2.APIKey,delegate2.userID,delegate2.publicToken]];
        NSLog(@"user %@",delegate2.userID);
        NSLog(@"user %@",delegate2.publicToken);
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request];
        self.socket.delegate = self;
        //
        //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //     open socket
        
        //        if(delegate2.instrumentToken.count>0)
        //    {
        //
        //    }
        
        if(delegate2.homeNewCompanyArray.count>0)
        {
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
            [self.socket open];

        }else
        {
            self.activityIndicator.hidden=YES;
            self.ifEmptyView.hidden=NO;
            self.marketTableView.hidden=YES;
        }

    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {@try {
        instrumentTokensDict = [[NSMutableDictionary alloc]init];
     NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"https://ws-api.upstox.com/?apiKey=QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1&token=%@",delegate2.accessToken]];
        
        //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
           //                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
             //                                              timeoutInterval:40.0];
       
     NSURLRequest *request = [NSURLRequest requestWithURL:url];
        //     create the socket and assign delegate
        self.socket = [PSWebSocket clientSocketWithRequest:request];
        self.socket.delegate = self;
        //
        //    self.socket.delegateQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //     open socket
        
        //        if(delegate2.instrumentToken.count>0)
        //    {
        //
        //    }
        
        if(delegate2.homeNewCompanyArray.count>0)
        {
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
            [self.socket open];

        }else
        {
            self.activityIndicator.hidden=YES;
            self.ifEmptyView.hidden=NO;
            self.marketTableView.hidden=YES;
        }

        
    }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
    
    }

}

- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
    NSLog(@"The websocket handshake completed and is now open!");
    
    NSLog(@"%@",delegate2.homeInstrumentToken);
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
        localInstrumentToken=[[NSMutableArray alloc]init];
        
        localInstrumentToken=delegate2.homeInstrumentToken;
        
        localInstrumentToken=[[[localInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
        
        if(localInstrumentToken.count>0)
        {
            
            NSDictionary * subscribeDict=@{@"a": @"subscribe", @"v":localInstrumentToken};
            
            
            NSData *json;
            
            NSError * error;
            
            
            
            
            // Dictionary convertable to JSON ?
            if ([NSJSONSerialization isValidJSONObject:subscribeDict])
            {
                // Serialize the dictionary
                json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
                
                // If no errors, let's view the JSON
                if (json != nil && error == nil)
                {
                    string1 = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                    
                    //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                    //
                    
                    NSLog(@"JSON: %@",string1);
                    
                }
            }
            
            
            [self.socket send:string1];
            
            
            
            
            
            
            NSArray * mode=@[@"full",localInstrumentToken];
            
            subscribeDict=@{@"a": @"mode", @"v": mode};
            
            
            
            //     Dictionary convertable to JSON ?
            if ([NSJSONSerialization isValidJSONObject:subscribeDict])
            {
                // Serialize the dictionary
                json = [NSJSONSerialization dataWithJSONObject:subscribeDict options:NSJSONWritingPrettyPrinted error:&error];
                
                
                // If no errors, let's view the JSON
                if (json != nil && error == nil)
                {
                    _message = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                    
                    //            delegate2.message=[NSString stringWithFormat:@"%@",str];
                    
                    NSLog(@"JSON: %@",_message);
                    
                }
            }
            [self.socket send:_message];
            
        }
        
        
    }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    
    {
        
//       [self unSubMethod];
        
        
        
    }
}

/*!
 @discussion used to unsubscribe symbols(upstox).
 */

-(void)unSubMethod
{
    
    localInstrumentToken=[[NSMutableArray alloc]init];
    
    localInstrumentToken=delegate2.homeInstrumentToken;
    
//    localInstrumentToken=[[[localInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
    
        
        
    symbolArray=[[NSMutableArray alloc]init];
    
    NSString * symbol;
//
//    NSMutableArray * exchangeArray=[[NSMutableArray alloc]init];
    
    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
    {
        NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i] objectForKey:@"segment"]];
        
       if([exchange containsString:@"NSE"])
       {
           
           
           exchange=@"nse_eq";
           
       }
        
       else if([exchange containsString:@"BSE"])
        {
            
             exchange=@"bse_eq";
            
        }
        
       else if([exchange containsString:@"NFO"])
       {
           
            exchange=@"nse_fo";
           
       }
        
       else if([exchange containsString:@"BFO"])
       {
           
            exchange=@"bse_fo";
           
       }
        
       else if([exchange containsString:@"CDS"])
       {
           
           exchange=@"ncd_fo";
           
       }
        
       else if([exchange containsString:@"MCX"])
       {
           
           exchange=@"mcx_fo";
           
       }
        
        
        if([exchange containsString:@"fo"])
        {
//             [symbolArray addObject:[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"]];
            
            symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"];
        }
        
        else
        {
//        [symbolArray addObject:[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"]];
            symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"];
        }
    
        if([symbol containsString:@"&"])
        {
            symbol=[symbol stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        }
  
    
//    NSArray * exchangeArray=@[@"bse_eq",@"nse_eq ",@"nse_eq ",@"nse_fo ",@"ncd_fo",@"mcx_fo ",@"nse_index",@"bcd_fo",@"bse_index"];
    
//        NSString * result = [[symbolArray valueForKey:@"description"] componentsJoinedByString:@","];
//
  
        
         NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
        NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                                   @"authorization": access,
                                   };
        NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/unsub/full/%@/?symbol=%@",exchange,symbol];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:40.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            
                                                            NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSLog(@"%@",json);
                                                        }
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
//                                                            if(i==delegate2.homeNewCompanyArray.count-1)
//                                                            {
//                                                                if(disapperCheck==true)
//                                                                {
//                                                                    disapperCheck=false;
//
//                                                                    StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
//
//                                                                    [self.navigationController pushViewController:stockDetails animated:YES];
//
//                                                                }
//
//                                                                else
//                                                                {
//
//                                                                    [self subMethod];
//                                                                    [self.socket close];
//                                                                    [self webSocket];
//                                                                }
//
//
//                                                            }
//
                                                            
                                                            
                                                        });
                                                    }];
        [dataTask resume];
        
        
        
    }
    
    
    
}


/*!
 @discussion used to subscribe symbols(upstox).
 */
-(void)subMethod
{
    
    localInstrumentToken=[[NSMutableArray alloc]init];
    
    localInstrumentToken=delegate2.homeInstrumentToken;
    symbolArray=[[NSMutableArray alloc]init];
    
    NSString * symbol;
    //
    //    NSMutableArray * exchangeArray=[[NSMutableArray alloc]init];
    NSLog(@"Home company:%@",delegate2.homeNewCompanyArray);
    
    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
    {
        NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i] objectForKey:@"segment"]];
        
        if([exchange containsString:@"NSE"])
        {
            
            exchange=@"nse_eq";
            
        }
        
        else if([exchange containsString:@"BSE"])
        {
            
            exchange=@"bse_eq";
            
        }
        
        else if([exchange containsString:@"NFO"])
        {
            
            exchange=@"nse_fo";
            
        }
        
        else if([exchange containsString:@"BFO"])
        {
            
            exchange=@"bse_fo";
            
        }
        
        else if([exchange containsString:@"CDS"])
        {
            
            exchange=@"ncd_fo";
            
        }
        
        else if([exchange containsString:@"MCX"])
        {
            
            exchange=@"mcx_fo";
            
        }
        
        if([exchange containsString:@"fo"])
        {
            //             [symbolArray addObject:[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"]];
            symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"];
        }
        
        else
        {
            //        [symbolArray addObject:[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"]];
            symbol=[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"];
        }
        
        
        if([symbol containsString:@"&"])
        {
            symbol=[symbol stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        }
        
        
        
        //    NSArray * exchangeArray=@[@"bse_eq",@"nse_eq ",@"nse_eq ",@"nse_fo ",@"ncd_fo",@"mcx_fo ",@"nse_index",@"bcd_fo",@"bse_index"];
        
        //        NSString * result = [[symbolArray valueForKey:@"description"] componentsJoinedByString:@","];
        //
        
        
        
    
    NSString * access=[NSString stringWithFormat:@"Bearer %@",delegate2.accessToken];
    NSDictionary *headers = @{ @"x-api-key": @"QuDE91Dz4W6vDU6uc015K2FupnZgeceZ6pyH03q1",
                               @"authorization": access,
                               };
    NSString * url = [NSString stringWithFormat:@"https://api.upstox.com/live/feed/sub/full/%@/?symbol=%@",exchange,symbol];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData* data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSMutableArray * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",json);
                                                        
                                                    }
                                                    
                                                
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{

                                                       if(i==delegate2.homeNewCompanyArray.count-1)
                                                       {
                                                           [self webSocket];
                                                        self.marketTableView.delegate=self;
                                                        self.marketTableView.dataSource=self;
                                                        [self.marketTableView reloadData];




                                                       }



                                                    });

                                                    
                                                    
                                                }];
    [dataTask resume];
        
    }


}


- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    
    
    @try {
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
        NSLog(@"The websocket received a message: %@",message);
        
        
        
        NSData * data=[NSKeyedArchiver archivedDataWithRootObject:message];
        
       
        
        NSLog(@"size of Object: %zd",malloc_size((__bridge const void *)(message)));
        
        if(data.length > 247)
        {
            
            
            NSLog(@"The websocket received a message: %@",message);
            
            
            id packets = [message subdataWithRange:NSMakeRange(0,2)];
            packetsNumber = CFSwapInt16BigToHost(*(int16_t*)([packets bytes]));
            NSLog(@" number of packets %i",packetsNumber);
            
            int startingPosition = 2;
            //            if([instrumentTokensDict allKeys])
            //                [instrumentTokensDict removeAllObjects];
            
            for( int i =0; i< packetsNumber ; i++)
            {
                
                
                NSData * packetsLengthData = [message subdataWithRange:NSMakeRange(startingPosition,2)];
                packetsLength = CFSwapInt16BigToHost(*(int16_t*)([packetsLengthData bytes]));
                startingPosition = startingPosition + 2;
                
                id packetQuote = [message subdataWithRange:NSMakeRange(startingPosition,packetsLength)];
                
                
                
                
                id instrumentTokenData = [packetQuote subdataWithRange:NSMakeRange(0, 4)];
                int32_t instrumentTokenValue = CFSwapInt32BigToHost(*(int32_t*)([instrumentTokenData bytes]));
                
                id lastPrice = [packetQuote subdataWithRange:NSMakeRange(4,4)];
                int32_t lastPriceValue = CFSwapInt32BigToHost(*(int32_t*)([lastPrice bytes]));
                
                
                
                
                
                
                id closingPrice = [packetQuote subdataWithRange:NSMakeRange(40,4)];
                int32_t closePriceValue = CFSwapInt32BigToHost(*(int32_t*)([closingPrice bytes]));
                
                
                
                
                
                
                //                id marketDepth = [packetQuote subdataWithRange:NSMakeRange(44,120)];
                //                int32_t marketDepthValue = CFSwapInt32BigToHost(*(int32_t*)([marketDepth bytes]));
                //
                //                NSLog(@"%@",marketDepth);
                
                //                NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.filteredCompanyName objectAtIndex:i]valueForKey:@"segment"]];
                //
                //                if([exchange containsString:@"CDS"])
                //                {
                //                    lastPriceFlaotValue = (float)lastPriceValue/10000000;
                //
                //                    closePriceFlaotValue = (float)closePriceValue/10000000;
                //
                //                    NSLog(@"CLOSE %f",closePriceFlaotValue);
                //
                //                    changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                //
                //                    changeFloatPercentageValue = (changeFloatValue/closePriceFlaotValue)/100;
                //                }
                //                else
                //                {
                
                
                lastPriceFlaotValue = (float)lastPriceValue/100;
                
                closePriceFlaotValue = (float)closePriceValue/100;
                
                NSLog(@"CLOSE %f",closePriceFlaotValue);
                
                changeFloatValue = lastPriceFlaotValue-closePriceFlaotValue;
                
                changeFloatPercentageValue = ((lastPriceFlaotValue-closePriceFlaotValue) *100/closePriceFlaotValue);
                //                }
                
                startingPosition = startingPosition + packetsLength;
                
                
                @autoreleasepool {
                    
                    tmpDict = [[NSMutableDictionary alloc]init];
                    [tmpDict setValue:[NSString stringWithFormat:@"%d",instrumentTokenValue] forKey:@"InstrumentToken"];
                    
                    NSString *tmpInstrument = [NSString stringWithFormat:@"%d",instrumentTokenValue];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",closePriceFlaotValue] forKey:@"ClosePriceValue"];
                    
                    
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",lastPriceFlaotValue] forKey:@"LastPriceValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatValue] forKey:@"ChangeValue"];
                    
                    [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changeFloatPercentageValue] forKey:@"ChangePercentageValue"];
                    NSLog(@"%@",tmpDict);
                    
                    
                    [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                    
                    NSLog(@"%@",instrumentTokensDict);
                    
                    
                    
                    
                    
                    //  [shareListArray addObject:tmpDict];
                    
                }
                
                
            }
            
            NSLog(@"%@",instrumentTokensDict);
            
            if(allKeysList.count>0)
            {
                [allKeysList removeAllObjects];
            }
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            NSLog(@"%@",allKeysList);
            
            allKeysList1=[[NSMutableArray alloc]init];
            
            for(int i=0;i<localInstrumentToken.count;i++)
            {
                
                for(int j=0;j<allKeysList.count;j++)
                {
                    NSNumber * number1=[localInstrumentToken objectAtIndex:i];
                    int int1=[number1 intValue];
                    NSNumber * number2=[allKeysList objectAtIndex:j];
                    
                    int int2=[number2 intValue];
                    
                    if(int1==int2)
                    {
                        [allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                    }
                }
                
                
            }
            
            allKeysList1=[[[allKeysList1 reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.marketTableView.delegate=self;
                self.marketTableView.dataSource=self;
                
                [self.marketTableView reloadData];
                
                //            NSLog(@"%@",shareListArray);
                
                
            });
            
            
            //  [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
            
            [self.activityIndicator stopAnimating];
            self.activityIndicator.hidden=YES;
            
            
            
            
        }
        
        if(allKeysList1.count>0)
        {
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
        }else
        {
            
            [self.activityIndicator stopAnimating];
            self.activityIndicator.hidden=YES;
            
            //        if(scripCheck==true)
            //        {
            //
            //        dispatch_async(dispatch_get_main_queue(), ^{
            //            UIAlertController * alert = [UIAlertController
            //                                         alertControllerWithTitle:@"Order Status"
            //                                         message:@"No trading for this symbol"
            //                                         preferredStyle:UIAlertControllerStyleAlert];
            //
            //            //Add Buttons
            //
            //            UIAlertAction* okButton = [UIAlertAction
            //                                       actionWithTitle:@"Ok"
            //                                       style:UIAlertActionStyleDefault
            //                                       handler:^(UIAlertAction * action) {
            //                                           //Handle your yes please button action here
            //                                           self.ifEmptyView.hidden=NO;
            //                                           self.marketTableView.hidden=YES;
            //                                           scripCheck=false;
            //                                           
            //                                       }];
            //            //Add your buttons to alert controller
            //            
            //            [alert addAction:okButton];
            //            
            //            
            //            [self presentViewController:alert animated:YES completion:nil];
            
            
            
            
            
            
            //        });
            
        }
            
        }
        
        else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        
        {
           
            NSString *detailsString = [[NSString alloc] initWithData:message encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"%@",detailsString);
            
            NSArray * completeArray=[[NSArray alloc]init];
            
           completeArray= [detailsString componentsSeparatedByString:@";"] ;
            NSLog(@"%@",completeArray);
            
            NSMutableDictionary * tmpDict=[[NSMutableDictionary alloc]init];
            
            for (int i=0; i<completeArray.count; i++) {
            
                NSMutableArray * individualCompanyArray=[[NSMutableArray alloc]init];
                NSString * innerStr=[NSString stringWithFormat:@"%@",[completeArray objectAtIndex:i]];
                
                individualCompanyArray=[[innerStr componentsSeparatedByString:@","] mutableCopy];
//            for(int i=0;i<localInstrumentToken.count;i++)
//            {
            
           
                 NSString * instrument;
                
                for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
                {
                    NSString * zambalaSymbol;
                    NSString * zambalaExchange;
                    
                   
                  
                    NSString * upStoxSymbol=[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:2]];
                    
                     NSString * upStoxExchange=[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:1]];
                    
                    if([upStoxExchange containsString:@"NSE_FO"])
                    {
                        upStoxExchange =@"NFO";
                        
                    }
                   else  if([upStoxExchange containsString:@"BSE_FO"])
                    {
                        
                         upStoxExchange =@"BFO";
                    }
                    
                   else  if([upStoxExchange containsString:@"NCD_FO"])
                   {
                       
                       upStoxExchange =@"CDS";
                   }
                    
                   else  if([upStoxExchange containsString:@"MCX_FO"])
                   {
                       
                       upStoxExchange =@"MCX";
                   }
                    
                   else  if([upStoxExchange containsString:@"NSE_EQ"])
                   {
                       
                       upStoxExchange =@"NSE";
                   }
                    
                   else  if([upStoxExchange containsString:@"BSE_EQ"])
                   {
                       
                       upStoxExchange =@"BSE";
                   }
                  
                    @try
                    {
                        
                        NSString * exchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"segment"]];
                        
                        if([exchange containsString:@"FUT"]||[exchange containsString:@"OPT"]||[exchange containsString:@"MCX"])
                        {
                             zambalaSymbol=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"securitydesc"]];
                            
                            zambalaExchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"exchange"]];
                            
                        }
                        
                        else
                        {
                            
                            zambalaSymbol=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"symbol"]];
                            
                            zambalaExchange=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"exchange"]];
                            
                        }
                       
                        
                    }
                  @catch (NSException *exception) {
                    NSLog(@"Something missing...");
                      
                      
                   }
                  @finally {
                    
                   }
                    
                    
                    if([upStoxSymbol isEqualToString:zambalaSymbol]&&[upStoxExchange containsString:zambalaExchange])
                    {
                        
                        instrument=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"instrument_token"]];
                        
                       
                    }
                    
                }
                
                if(instrument.length!=0)

                {
                tmpDict = [[NSMutableDictionary alloc]init];
                [tmpDict setValue:[NSString stringWithFormat:@"%@",instrument] forKey:@"InstrumentToken"];
                
                NSString *tmpInstrument = [NSString stringWithFormat:@"%@",instrument];
                
                float close=[[individualCompanyArray objectAtIndex:4] floatValue];
                float ltp=[[individualCompanyArray objectAtIndex:3] floatValue];
                
                [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:4]] forKey:@"ClosePriceValue"];
                
                
                
                [tmpDict setValue:[NSString stringWithFormat:@"%@",[individualCompanyArray objectAtIndex:3]] forKey:@"LastPriceValue"];
                
                
                float change=ltp-close;
                
                
                [tmpDict setValue:[NSString stringWithFormat:@"%.2f",change] forKey:@"ChangeValue"];
                
                float changePer = ((ltp-close) *100/close);
                
                [tmpDict setValue:[NSString stringWithFormat:@"%.2f",changePer] forKey:@"ChangePercentageValue"];
                NSLog(@"%@",tmpDict);
                
                
                [instrumentTokensDict setValue:tmpDict forKeyPath:tmpInstrument];
                
                NSLog(@"%@",instrumentTokensDict);
                
                
                
                
                
                //  [shareListArray addObject:tmpDict];
                
            
            
            
        
        
        NSLog(@"%@",instrumentTokensDict);
        
        if(allKeysList.count>0)
        {
            [allKeysList removeAllObjects];
        }
        allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
        
        NSLog(@"%@",allKeysList);
        
        allKeysList1=[[NSMutableArray alloc]init];
        
        for(int i=0;i<localInstrumentToken.count;i++)
        {
            
            for(int j=0;j<allKeysList.count;j++)
            {
                NSNumber * number1=[localInstrumentToken objectAtIndex:i];
                int int1=[number1 intValue];
                NSNumber * number2=[allKeysList objectAtIndex:j];
                
                int int2=[number2 intValue];
                
                if(int1==int2)
                {
                    [allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                }
            }
            
            
        }
        
        allKeysList1=[[[allKeysList1 reverseObjectEnumerator] allObjects] mutableCopy];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.marketTableView.delegate=self;
            self.marketTableView.dataSource=self;
            
            [self.marketTableView reloadData];
            
            //            NSLog(@"%@",shareListArray);
            
            
        });
        
        
        //  [self.activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:YES];
        
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden=YES;
        
        
        
        
    
    
    if(allKeysList1.count>0)
    {
        self.ifEmptyView.hidden=YES;
        self.marketTableView.hidden=NO;
    }else
    {
        
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden=YES;
        
        //        if(scripCheck==true)
        //        {
        //
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            UIAlertController * alert = [UIAlertController
        //                                         alertControllerWithTitle:@"Order Status"
        //                                         message:@"No trading for this symbol"
        //                                         preferredStyle:UIAlertControllerStyleAlert];
        //
        //            //Add Buttons
        //
        //            UIAlertAction* okButton = [UIAlertAction
        //                                       actionWithTitle:@"Ok"
        //                                       style:UIAlertActionStyleDefault
        //                                       handler:^(UIAlertAction * action) {
        //                                           //Handle your yes please button action here
        //                                           self.ifEmptyView.hidden=NO;
        //                                           self.marketTableView.hidden=YES;
        //                                           scripCheck=false;
        //
        //                                       }];
        //            //Add your buttons to alert controller
        //
        //            [alert addAction:okButton];
        //
        //
        //            [self presentViewController:alert animated:YES completion:nil];
        
        
        
        
        
        
        //        });
        
    }
        
                
            }
    }
        }
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    

        
     
    }
    



    


- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
}
- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessageWithData:(NSData *)data
{
     NSLog(@"The websocket received a message: %@", data);
}
- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

      // TableView Delegate and  Data Sources//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // return [filteredArray count];
//   if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
//   {
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
    if(allKeysList1.count>0)
    {
    return [allKeysList1 count];
    }
        
    }else if ([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        return delegate2.homeInstrumentToken.count;
    }
    
    else
    {
        
        return  [delegate2.localExchageToken count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
     MarketTableViewCell * cell = [self.marketTableView dequeueReusableCellWithIdentifier:@"Cell"forIndexPath:indexPath];
    
    NSString * str=@"%";
    
    @try {
        
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
//        if(allKeysList1.count>0)
//        {
            
            
            NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList1 objectAtIndex:indexPath.row]]];
            
            for (int i=0 ;i < delegate2.homeNewCompanyArray.count ; i++)
            {
                
                NSString *str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"instrument_token"]];
                NSString *str_KiteToken = [NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
                
                NSLog(@"%@",delegate2.homeNewCompanyArray);
                
                if([str_ServerToken isEqualToString:str_KiteToken])
                {
                    cell.companyName.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"symbol"]];
                    @try {
                        option=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"optiontype"]];
                        
                        NSString *myString = [[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"expiryseries"];
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        dateFormatter.dateFormat = @"yyyy-MM-dd";
                        NSDate *yourDate = [dateFormatter dateFromString:myString];
                        dateFormatter.dateFormat = @"dd MMM yyyy";
                        NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                        
                        finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Something missing...");
                    }
                    @finally {
                        
                    }
                    
                    
                    
                    if([option isEqualToString:@"FUT"])
                    {
                        
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],finalDate];
                        
                    }
                    else if([option isEqualToString:@"CE"])
                    {
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@ CE  %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"strikeprice"],finalDate];
                    }
                    
                    else if([option isEqualToString:@"PE"])
                    {
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@ PE  %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"strikeprice"],finalDate];
                    }
                    
                    else
                    {
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"]];
                    }
                    
                    
                    //check//
                    
                    
                    
                        NSString * priceString=[tmp valueForKey:@"LastPriceValue"];
                    
                    
                    
                    NSString * changeStrPer=[tmp valueForKey:@"ChangePercentageValue"];
                    
                    
                   // cell.priceLbl.text=priceString;
                    
                    if(priceString.length>0)
                    {
                        
                        //    NSNumberFormatter *formatString = [[NSNumberFormatter alloc] init];
                        //    NSString * previousValue = cell.priceLbl.text;
                        //    NSNumber *number2 = [formatString numberFromString:previousValue];
                        
                        
                        
                        CGFloat number2 = (CGFloat)[cell.priceLbl.text floatValue];
                        NSLog(@"number2 %f",number2);
                        
                            if([cell.exchangeLbl.text containsString:@"CDS"])
                            {
                                
                                
                                NSString * str=[tmp valueForKey:@"LastPriceValue"];
                                
                            float priceValue=[str floatValue];
                                
                                float priceValue1;
                                
                                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                                {
                                    
                                     priceValue1= (float) priceValue/100000;
                                }
                                
                                else  if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                                {
                                    
                                    priceValue1=priceValue;
                                }
                            
                           
                            
                            NSString * priceStr=[NSString stringWithFormat:@"%.4f",priceValue1];
                            
                            CGFloat number1=(CGFloat)[priceStr floatValue];
                            
                            NSLog(@"number1 %f",number1);
                            
                            if(number1<number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                cell.priceLbl.text=priceStr;
                            }
                            
                            else if(number1>number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                cell.priceLbl.text=priceStr;
                            }
                                
                            }
                            
                            
                            
                        
                        
                        else
                        {
                            float price1=[[tmp valueForKey:@"LastPriceValue"] floatValue];
                            NSString * price=[NSString stringWithFormat:@"%.2f",price1];
                            CGFloat number1=(CGFloat)[price floatValue];
                            
                            NSLog(@"number1 %f",number1);
                            
                            if(number1<number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                cell.priceLbl.text=price;
                            }
                            
                            else if(number1>number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                cell.priceLbl.text=price;
                            }
                            
                        }
                            
                        
                        
                        
                        
                        
                    }
                    
                    
                    
                    //        if(priceString.length>7)
                    //        {
                    //            NSString * str=[tmp valueForKey:@"LastPriceValue"];
                    //            float priceValue=[str floatValue];
                    //
                    //            float priceValue1= (float) priceValue/100000;
                    //
                    //            NSString * priceStr=[NSString stringWithFormat:@"%.2f",priceValue1];
                    //
                    //            cell.priceLbl.text=priceStr;
                    //        }
                    //
                    //        else
                    //        {
                    //            cell.priceLbl.text =[tmp valueForKey:@"LastPriceValue"];
                    //        }
                    //
                    
                    
                    NSString * changeStr=[tmp valueForKey:@"ChangeValue"];
                    
                    if([cell.exchangeLbl.text containsString:@"CDS"])
                    {
                        
                        
                        if([changeStrPer containsString:@"-"])
                        {
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        else{
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        }
                        
                        
                        
                        
                        NSString * str=[tmp valueForKey:@"ChangeValue"];
                        float priceValue=[str floatValue];
                        
                        float priceValue1;
                        
                        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                        {
                              priceValue1= (float) priceValue/100000;
                            
                        }
                        
                        else
                        {
                            
                            priceValue1=priceValue;
                        }
                        
                       
                        
                        NSString * priceStr=[NSString stringWithFormat:@"%.2f",priceValue1];
                        
                        
                        
                        cell.percentLbl.text = priceStr;
                        
                        
                    }
                    
                    else
                    {
                        
                        if([changeStr containsString:@"-"])
                        {
                            cell.percentLbl.text = [tmp valueForKey:@"ChangeValue"];
                            cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
                        }
                        else{
                            cell.percentLbl.text = [tmp valueForKey:@"ChangeValue"];
                            cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
                        }
                        
                        if([changeStrPer containsString:@"-"])
                        {
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        else{
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",[tmp valueForKey:@"ChangePercentageValue"],str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        }
                        
                    }
                    
                    
                    
                    
                    
                    //

                    
                    
                    
                    
                    break;
                }
                
            }
            
            //        UISwipeGestureRecognizer* swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedLeft:)];
            //        [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
            //        [cell addGestureRecognizer:swipeGestureLeft];
            //        
            //        
            //        UISwipeGestureRecognizer* swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedRight:)];
            //        [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
            //        [cell addGestureRecognizer:swipeGestureRight];
            
            [cell.tradeBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.resultsTodayLabel.hidden=YES;
            
            int result = [[[[responseDict1 objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"result"] intValue];
            if(result==0)
            {
                cell.resultsTodayLabel.hidden=YES;
            }else if(result==1)
            {
                cell.resultsTodayLabel.hidden=NO;
            }
        //}
            
//        }
        
//        else
//        {
//            
//            cell.priceLbl.text=[[localExchangeTokensArray objectAtIndex:indexPath.row] objectForKey:@"dbLTP"];
//             cell.companyName.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:indexPath.row]valueForKey:@"symbol"]];
//            
//            NSString * localExchange=[[localExchangeTokensArray objectAtIndex:indexPath.row] objectForKey:@"stExchange"];
//            
//            if([localExchange isEqualToString:@"NSECM"])
//            {
//                localExchange=@"NSE";
//            }
//            
//           else if([localExchange isEqualToString:@"NSEFO"])
//            {
//                localExchange=@"NFO";
//            }
//           else if([localExchange isEqualToString:@"BSECM"])
//           {
//               localExchange=@"BSE";
//           }
//           else if([localExchange isEqualToString:@"BSEFO"])
//           {
//               localExchange=@"BFO";
//           }
//            
//            cell.exchangeLbl.text=localExchange;
//            
//            float dbPrice=[[[localExchangeTokensArray objectAtIndex:indexPath.row] objectForKey:@"dbLTP"] floatValue];
//            
//            float close=[[[localExchangeTokensArray objectAtIndex:indexPath.row] objectForKey:@"dbClose"] floatValue];
//            
//            float change=dbPrice-close;
//            
//            
//            cell.percentLbl.text=[NSString stringWithFormat:@"%.2f",change];
//            
//            float changePercentage=((dbPrice-close) *100/close);
//            
//            cell.changePerLbl.text=[NSString stringWithFormat:@"%.2f",changePercentage];
//            cell.resultsTodayLabel.hidden=YES;
//            
//            
//            [cell.tradeBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//            int result = [[[[responseDict1 objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"result"] intValue];
//            if(result==0)
//            {
//                cell.resultsTodayLabel.hidden=YES;
//            }else if(result==1)
//            {
//                cell.resultsTodayLabel.hidden=NO;
//            }
//
//            
//            
       }
        
        else
        {
//        if(allKeysList1.count>0)
//        {
            //
            
            NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList1 objectAtIndex:indexPath.row]]];
            
            for (int i=0 ;i < delegate2.homeNewCompanyArray.count ; i++)
            {
                
                NSString *str_ServerToken = [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"exchange_token"]];
                NSString *str_KiteToken = [NSString stringWithFormat:@"%@",[tmp valueForKey:@"stSecurityID"]];
                
                NSLog(@"%@",delegate2.homeNewCompanyArray);
                
                if([str_KiteToken containsString:str_ServerToken ])
                {
                    cell.companyName.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"symbol"]];
                    @try {
                        option=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"optiontype"]];
                        
                        NSString *myString = [[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"expiryseries"];
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        dateFormatter.dateFormat = @"yyyy-MM-dd";
                        NSDate *yourDate = [dateFormatter dateFromString:myString];
                        dateFormatter.dateFormat = @"dd MMM yyyy";
                        NSLog(@"%@",[dateFormatter stringFromDate:yourDate]);
                        
                        finalDate=[[dateFormatter stringFromDate:yourDate] uppercaseString];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Something missing...");
                    }
                    @finally {
                        
                    }
                    
                    
                    
                    if([option isEqualToString:@"FUT"])
                    {
                        
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],finalDate];
                        
                    }
                    else if([option isEqualToString:@"CE"])
                    {
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@ CE  %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"strikeprice"],finalDate];
                    }
                    
                    else if([option isEqualToString:@"PE"])
                    {
                        
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@ %@ PE  %@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"],[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"strikeprice"],finalDate];
                    }
                    
                    else
                    {
                        cell.exchangeLbl.text= [NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"segment"]];
                    }
                    
                    
                    //check//
                    
                    
                    
                    float  priceString=[[tmp valueForKey:@"dbLTP"] floatValue];
                    
                    float close=[[tmp valueForKey:@"dbClose"] floatValue];
                    //
                    float change=priceString-close;
                    //
                    //
                    //            cell.percentLbl.text=[NSString stringWithFormat:@"%.2f",change];
                    //
                    float changePercentage=((priceString-close) *100/close);
                    
                    
                    
                    NSString * changeStrPer=[NSString stringWithFormat:@"%.2f",changePercentage];
                    NSString * changeValue=[NSString stringWithFormat:@"%.2f",change];
                    
                    NSString * priceStr = [NSString stringWithFormat:@"%.2f",priceString];
                    
                   
                    
                   // if(cell.priceLbl.text.length>0)
                    if(priceStr.length>0)
                    {
                        
                         //cell.priceLbl.text=@"0.00";
                        
                        //    NSNumberFormatter *formatString = [[NSNumberFormatter alloc] init];
                        //    NSString * previousValue = cell.priceLbl.text;
                        //    NSNumber *number2 = [formatString numberFromString:previousValue];
                        
                      //  cell.priceLbl.text=[NSString stringWithFormat:@"%.2f",priceString];
                        
                        CGFloat number2 = (CGFloat)[cell.priceLbl.text floatValue];
                        NSLog(@"number2 %f",number2);
                        
                        if([cell.exchangeLbl.text containsString:@"CDS"])
                        {
                            
                            
                            NSString * str=[tmp valueForKey:@"dbLTP"];
                            
                            float priceValue=[str floatValue];
                            
                            float priceValue1= (float) priceValue/100000;
                            
                            NSString * priceStr=[NSString stringWithFormat:@"%.4f",priceValue1];
                            
                            CGFloat number1=(CGFloat)[priceStr floatValue];
                            
                            NSLog(@"number1 %f",number1);
                            
                            if(number1<number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                cell.priceLbl.text=priceStr;
                            }
                            
                            else if(number1>number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                cell.priceLbl.text=priceStr;
                            }
                            
                        }
                        
                        
                        
                        
                        
                        else
                        {
                            NSString * price=[tmp valueForKey:@"dbLTP"];
                            CGFloat number1=(CGFloat)[price floatValue];
                            
                            NSLog(@"number1 %f",number1);
                            
                            if(number1<number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                                cell.priceLbl.text=price;
                            }
                            
                            else if(number1>number2)
                            {
                                //            cell.priceLbl.backgroundColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:0.34];
                                
                                cell.priceLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                                cell.priceLbl.text=price;
                            }
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    
                    //        if(priceString.length>7)
                    //        {
                    //            NSString * str=[tmp valueForKey:@"LastPriceValue"];
                    //            float priceValue=[str floatValue];
                    //
                    //            float priceValue1= (float) priceValue/100000;
                    //
                    //            NSString * priceStr=[NSString stringWithFormat:@"%.2f",priceValue1];
                    //
                    //            cell.priceLbl.text=priceStr;
                    //        }
                    //
                    //        else
                    //        {
                    //            cell.priceLbl.text =[tmp valueForKey:@"LastPriceValue"];
                    //        }
                    //
                    
                    
                    NSString * changeStr=changeValue;
                    
                    if([cell.exchangeLbl.text containsString:@"CDS"])
                    {
                        
                        
                        if([changeStrPer containsString:@"-"])
                        {
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",changeStrPer,str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        else{
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",changeStrPer,str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        }
                        
                        
                        
                        
                        NSString * str=changeValue;
                        float priceValue=[str floatValue];
                        
                        float priceValue1= (float) priceValue/100000;
                        
                        NSString * priceStr=[NSString stringWithFormat:@"%.2f",priceValue1];
                        
                        
                        
                        cell.percentLbl.text = priceStr;
                        
                        
                    }
                    
                    else
                    {
                        
                        if([changeStr containsString:@"-"])
                        {
                            cell.percentLbl.text = changeValue;
                            //        cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
                        }
                        else{
                            cell.percentLbl.text =changeValue;
                            //        cell.percentLbl.textColor=[UIColor colorWithRed:(6/255.0) green:(31/255.0) blue:(43/255.0) alpha:1];
                        }
                        
                        if([changeStrPer containsString:@"-"])
                        {
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",changeStrPer,str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1];
                        }
                        else{
                            cell.changePerLbl.text = [NSString stringWithFormat:@"%@%@",changeStrPer,str];
                            cell.changePerLbl.textColor=[UIColor colorWithRed:(27/255.0) green:(160/255.0) blue:(33/255.0) alpha:1];
                        }
                        
                    }
                    
                    
                    
                    
                    
                    //
                    
                    
                    
                    
                    
                    break;
                }
                
            }
            
            //        UISwipeGestureRecognizer* swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedLeft:)];
            //        [swipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
            //        [cell addGestureRecognizer:swipeGestureLeft];
            //
            //
            //        UISwipeGestureRecognizer* swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwipedRight:)];
            //        [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
            //        [cell addGestureRecognizer:swipeGestureRight];
            
            [cell.tradeBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.resultsTodayLabel.hidden=YES;
            
            int result = [[[[responseDict1 objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"result"] intValue];
            if(result==0)
            {
                cell.resultsTodayLabel.hidden=YES;
            }else if(result==1)
            {
                cell.resultsTodayLabel.hidden=NO;
            }
//        }
            
        }

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
        return cell;
    
        
}

//- (void)cellSwipedLeft:(UIGestureRecognizer *)gestureRecognizer {
//    
//    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
//    {
//        [self.socket close];
//    }
//    
//    
//}
//
//- (void)cellSwipedRight:(UIGestureRecognizer *)gestureRecognizer {
//    
//    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
//    {
//         [self webSocket];
//    }
//    
//   
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    delegate2.navigationCheck=@"Marketwatch";
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Market Watch stock detail page"];

    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
    
    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList1 objectAtIndex:indexPath.row]]];
    delegate2.instrumentDepthStr=[tmp valueForKey:@"InstrumentToken"];
//    delegate2.symbolDepthStr=[[delegate2.filteredCompanyName objectAtIndex:indexPath.row]valueForKey:@"symbol"];
    NSLog(@"%@",tmp);
        
    }
    
    else
    {
        
        delegate2.instrumentDepthStr=[allKeysList1 objectAtIndex:indexPath.row];
    }
   
    
    MarketTableViewCell * cell = [self.marketTableView cellForRowAtIndexPath:indexPath];

    
    
    delegate2.symbolDepthStr=cell.companyName.text;
    
        delegate2.exchaneLblStr=cell.exchangeLbl.text;
    
    
    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
    {
        int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"instrument_token"] intValue];
        
        if(instrument==[delegate2.instrumentDepthStr intValue])
        {
    
    delegate2.expirySeriesValue=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]objectForKey:@"expiryseries"]];
    
        }
        
        
    }
    
    
    delegate2.LTPStr=cell.priceLbl.text;
    
  
     NSLog(@"%@",delegate2.symbolDepthStr);
    

    delegate2.depth=@"MARKETWATCH";
     NSLog(@"%@",delegate2.depth);
    
    @try {


//        if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
//        {
//                    disapperCheck=true;
//                    [self unSubMethod];
//
//        }
        
//        else
//        {
            StockView1 * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock1"];
            
            [self.navigationController pushViewController:stockDetails animated:YES];
            
//        }

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
        
        
    
   
}
/*!
 @discussion used to delete symbol from list.
 */

- (IBAction)editTableViewBtn:(id)sender
  {
      @try {
          if(editCheck==true)
          {
              @try {
                  [self.socket close];
                  
                [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
              [self.marketTableView setEditing: YES animated: YES];
                  
              }
              @catch (NSException *exception) {
                  NSLog(@"%@", exception.reason);
              }
              @finally {
                  
              }
              
              
              
              editCheck=false;
          }
          
          else if(editCheck==false)
          {
              @try {
                  [self webSocket];
                  [self.marketTableView setEditing: NO animated: YES];
                  
              }
              @catch (NSException *exception) {
                  NSLog(@"%@", exception.reason);
              }
              @finally {
                  
              }
              
              
              
              editCheck=true;
              
          }
          

          
      }
      @catch (NSException * e) {
          NSLog(@"Exception: %@", e);
      }
      @finally {
          NSLog(@"finally");
      }
      
  }
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.marketTableView.editing)
    {
        return UITableViewCellEditingStyleDelete;
    }
     return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
   // [self.socket close];
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove the row from data model
    
    
    
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
//    if(prefs!=nil)
//    {
    
        @try {
//            
//           
//            
//            [delegate2.homeInstrumentToken removeObjectAtIndex:indexPath.row];
//             [ delegate2.homeinstrumentName removeObjectAtIndex:indexPath.row];
//             [ delegate2.homeLotDepth removeObjectAtIndex:indexPath.row];
//             [ delegate2.homeFilterCompanyName removeObjectAtIndex:indexPath.row];
//            [ delegate2.homeMtExchange removeObjectAtIndex:indexPath.row];
//            [ delegate2.homeExchangeToken removeObjectAtIndex:indexPath.row];
//            
//            
//            
//            
//            
//            NSLog(@"%ld",(long)indexPath.row);
//            
//             NSLog(@"%@",delegate2.homeInstrumentToken);
//             NSLog(@"%@",delegate2.homeinstrumentName);
//             NSLog(@"%@",delegate2.homeLotDepth);
//             NSLog(@"%@",delegate2.homeFilterCompanyName);
//            
//            
//            
//            
//            
//            [prefs removeObjectForKey:@"instrumentToken"] ;
//            [prefs removeObjectForKey:@"instrumentNameArr"];
//            [prefs removeObjectForKey:@"sampleDepthLo"];
//            [prefs removeObjectForKey:@"exchange"];
//            [prefs removeObjectForKey:@"filteredCompanyNam"];
//             [prefs removeObjectForKey:@"exchange_token"];
//            [prefs removeObjectForKey:@"exchangevalue"];
//            
//            
//            [prefs setObject: delegate2.homeFilterCompanyName forKey:@"filteredCompanyName"];
//            [prefs setObject: delegate2.homeinstrumentName forKey:@"instrumentNameArr"];
//            [prefs setObject:delegate2.homeLotDepth  forKey:@"sampleDepthLot"];
//            [prefs setObject:delegate2.homeInstrumentToken forKey:@"instrumentToken"];
//            [prefs setObject:delegate2.exchange forKey:@"exchange"];
//            [prefs setObject:delegate2.homeExchangeToken forKey:@"exchange_token"];
//            [prefs setObject:delegate2.homeMtExchange forKey:@"exchangevalue"];
        
//            [delegate2.userMarketWatch removeObjectAtIndex:indexPath.row];
//
//            int token;
//
//
//            token=[[[delegate2.homeNewCompanyArray objectAtIndex:indexPath.row]valueForKey:@"instrument_token"]intValue];
//
//
            
            
//            for(int i=0;i<delegate2.userMarketWatch.count;i++)
//            {
//                int token1=[[[delegate2.userMarketWatch objectAtIndex:i] objectForKey:@"instrument_token"] intValue];
//                
//              if(token1==token)
//                  
//              {
//                  
//                 
//              }
//            }
            
            
    /////
            
           
                
                if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList1 objectAtIndex:indexPath.row]]];
                    delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
                }
                else
                {
                    
                    delegate2.instrumentDepthStr=[allKeysList1 objectAtIndex:indexPath.row];
                }
                if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
                {
                    NSMutableArray * newArray=[[NSMutableArray alloc]init];
                    newArray= delegate2.homeNewCompanyArray;
                    
//                    newArray=[[[newArray reverseObjectEnumerator] allObjects] mutableCopy];
                    
                    
                    for(int i=0;i<newArray.count;i++)
                    {
                        int instrument=[[[newArray objectAtIndex:i]valueForKey:@"instrument_token"]intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
//                         isDelete=[NSString stringWithFormat:@"%@",[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@""]];
                             [delegate2.userMarketWatch removeObjectAtIndex:i];
                            [delegate2.homeInstrumentToken removeObjectAtIndex:i];
                            [self tmpDictMethod];
                            [self webSocket];
                            //[self unSubMethod];
                            [self instruments];
                            
                           
                                
                        }
                            
                        }
                    
                    }
                    
            
                
                else  if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
                {
                    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
                    {
                        int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"instrument_token"] intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
                            
                           [delegate2.userMarketWatch removeObjectAtIndex:i];
                             [self instruments];
                            
                        }
                    }
                    
                    
                }
            
                else
                {
                    for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
                    {
                        int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"exchange_token"] intValue];
                        
                        if(instrument==[delegate2.instrumentDepthStr intValue])
                        {
                            
                            [delegate2.userMarketWatch removeObjectAtIndex:i];
                            //[delegate2.homeNewCompanyArray removeObjectAtIndex:i];
                            
                            [delegate2.localExchageToken removeObjectAtIndex:i];
                            [self tmpDictMethod];
                            [self instruments];
                            
                            
                        }
                    }
                    
                    
                }
                
                
            
            
            
            
            
            
            
            
            
//            [self resultCheck];
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
    
   
        
    
    
    
    
   [self.marketTableView setEditing: NO animated: YES];
   
}


/*!
 @discussion used to navigate to order page.
 */
-(void)yourButtonClicked:(UIButton*)sender

{
    
   
    delegate2.navigationCheck=@"Marketwatch";
    if(delegate2.depthLotSize.count)
    {
        [delegate2.depthLotSize removeAllObjects];
    }
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.marketTableView];
    
    
    
    NSIndexPath *indexPath = [self.marketTableView indexPathForRowAtPoint:buttonPosition];
    MarketTableViewCell *cell = [self.marketTableView cellForRowAtIndexPath:indexPath];
    
    @try {
        
        if([delegate2.brokerNameStr isEqualToString:@"Zerodha"]||[delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc]initWithDictionary:[instrumentTokensDict valueForKey:[allKeysList1 objectAtIndex:indexPath.row]]];
        delegate2.instrumentDepthStr=[NSString stringWithFormat:@"%@",[tmp valueForKey:@"InstrumentToken"]];
        }
        else
        {
            
             delegate2.instrumentDepthStr=[allKeysList1 objectAtIndex:indexPath.row];
        }
        if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
        {
            NSMutableArray * newArray=[[NSMutableArray alloc]init];
            newArray= delegate2.homeNewCompanyArray;
            
              newArray=[[[newArray reverseObjectEnumerator] allObjects] mutableCopy];
            
            
            for(int i=0;i<newArray.count;i++)
            {
                 int instrument=[[[newArray objectAtIndex:i]valueForKey:@"instrument_token"]intValue];
                
                if(instrument==[delegate2.instrumentDepthStr intValue])
                {
                    
                    delegate2.tradingSecurityDes=[[newArray objectAtIndex:i]valueForKey:@"securitydesc"];
                    
                    @try {
                        NSString * localStr=[NSString stringWithFormat:@"%@",[[newArray objectAtIndex:i]valueForKey:@"lotsize"]];
                        [delegate2.depthLotSize addObject:localStr];
                        
                        NSLog(@"%@",delegate2.depthLotSize);
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                
                }
            }
            
        }
        
        else  if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
        {
            for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
            {
               int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"instrument_token"] intValue];
                
                if(instrument==[delegate2.instrumentDepthStr intValue])
                {
                    
                    delegate2.tradingSecurityDes=[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"securitydesc"];
                    
                    @try {
                        NSString * localStr=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"lotsize"]];
                        [delegate2.depthLotSize addObject:localStr];
                        
                        NSLog(@"%@",delegate2.depthLotSize);
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                }
            }
            
            
        }
        
        else
        {
            for(int i=0;i<delegate2.homeNewCompanyArray.count;i++)
            {
                int instrument=[[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"exchange_token"] intValue];
                
                if(instrument==[delegate2.instrumentDepthStr intValue])
                {
                    
                    delegate2.tradingSecurityDes=[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"securitydesc"];
                    
                    @try {
                        NSString * localStr=[NSString stringWithFormat:@"%@",[[delegate2.homeNewCompanyArray objectAtIndex:i]valueForKey:@"lotsize"]];
                        [delegate2.depthLotSize addObject:localStr];
                        
                        NSLog(@"%@",delegate2.depthLotSize);
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                    
                }
            }
            
            
        }

       
        
        NSLog(@"%@", delegate2.tradingSecurityDes);
        
        delegate2.symbolDepthStr=cell.companyName.text;
        delegate2.orderSegment=cell.exchangeLbl.text;
        
        NSLog(@"%@",delegate2.symbolDepthStr);
        
        
        
        
        if(delegate2.instrumentDepthStr.length>0 && delegate2.symbolDepthStr.length>0 && delegate2.orderSegment.length>0)
        {
            
            StockOrderView * stockDetails=[self.storyboard instantiateViewControllerWithIdentifier:@"stock"];
            
            delegate2.tradeBtnBool=true;
            
            
            [self.navigationController pushViewController:stockDetails animated:YES];
        }
            
        
        

        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
//   [delegate2.broadCastInputStream close];
    
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
     [self.socket close];
        disapperCheck=true;
        [self unSubMethod];

    }
}
/*!
 @discussion used to upload list of symbols to zambala server.
 */
-(void)instruments
{
    @try {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
//        if(prefs !=nil)
//        {
//            
//            // getting an NSString
//            
//            //        delegate2.homeInstrumentToken=[prefs objectForKey:@"instrumentToken"];
//            //        delegate2.homeinstrumentName=[prefs objectForKey:@"instrumentNameArr"];
//            //        delegate2.homeLotDepth=[prefs objectForKey:@"sampleDepthLot"];
//            //        delegate2.homeExchange=[prefs objectForKey:@"exchange"];
//            //        delegate2.homeFilterCompanyName=[prefs objectForKey:@"filteredCompanyName"];
//            
//            delegate2.homeInstrumentToken = [NSMutableArray arrayWithArray:[prefs objectForKey:@"instrumentToken"]];
//            delegate2.homeinstrumentName = [NSMutableArray arrayWithArray:[prefs objectForKey:@"instrumentNameArr"]];
//            delegate2.homeLotDepth = [NSMutableArray arrayWithArray:[prefs objectForKey:@"sampleDepthLot"]];
//            delegate2.homeExchange = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"exchange"]];
//            delegate2.homeFilterCompanyName = [NSMutableArray arrayWithArray:[prefs objectForKey:@"filteredCompanyName"]];
//            
//             delegate2.homeExchangeToken = [NSMutableArray arrayWithArray:[prefs objectForKey:@"exchange_token"]];
//             delegate2.homeMtExchange = [NSMutableArray arrayWithArray:[prefs objectForKey:@"exchangevalue"]];
//            
//            @try {
//                delegate2.homeLotSizeArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"lotsize"]];
//                delegate2.homeExpiryDate = [NSMutableArray arrayWithArray:[prefs objectForKey:@"expiryseries"]];
//                
//            } @catch (NSException *exception) {
//                
//            } @finally {
//                
//            }
//            
//            @try {
//                delegate2.homeOptionArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"optiontype"]];
//                
//                delegate2.homeStrkeArray = [NSMutableArray arrayWithArray:[prefs objectForKey:@"strikeprice"]];
//            } @catch (NSException *exception) {
//                
//            } @finally {
//                
//            }
//            
//            
//          
//           
//            scripDetailsArray=[[NSMutableArray alloc]init];
//            scripDetails=[[NSMutableDictionary alloc]init];
//            
//            
//            
//            
//            for(int i=0;i<delegate2.userMarketWatch.count;i++)
//            {
//                 NSMutableDictionary * tmp_dict=[[NSMutableDictionary alloc]init];
//                
//                [tmp_dict setObject:[delegate2.userMarketWatch objectAtIndex:i] forKey:@"instrument_token"];
//                
//                 [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"name"] forKey:@"name"];
//                 [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"symbol"] forKey:@"symbol"];
//                 [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange"] forKey:@"exchange"];
//                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"segment"] forKey:@"segment"];
//                 [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange_token"] forKey:@"exchange_token"];
//                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchangevalue"] forKey:@"exchangevalue"];
//                [tmp_dict setObject:@"0" forKey:@"ltp"];
//                
//                
//                @try {
//             [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"fincode"] forKey:@"fincode"];
//                } @catch (NSException *exception) {
//                    [tmp_dict setObject:@"0" forKey:@"fincode"];
//                } @finally {
//                    
//                }
//                
//                @try {
//                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"expiryseries"] forKey:@"expiryseries"];
//                     [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"lotsize"] forKey:@"lotsize"];
//                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"securitydesc"] forKey:@"securitydesc"];
//                } @catch (NSException *exception) {
//                    [tmp_dict setObject:@"0" forKey:@"lotsize"];
//                    [tmp_dict setObject:@"0" forKey:@"expiryseries"];
//                } @finally {
//                    
//                }
//                
//                @try {
//                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"optiontype"] forKey:@"optiontype"];
//                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"strikeprice"] forKey:@"strikeprice"];
//                } @catch (NSException *exception) {
//                    [tmp_dict setObject:@"0" forKey:@"optiontype"];
//                    [tmp_dict setObject:@"0" forKey:@"strikeprice"];
//                } @finally {
//                    
//                }
//
//                
//                
//                
//                [scripDetailsArray insertObject:tmp_dict atIndex:i];
//                
//            }
//            
////            NSCharacterSet * set=[NSCharacterSet newlineCharacterSet];
////            
////            NSString * localString=[NSString stringWithFormat:@"%@",scripDetailsArray];
////            
////            NSString * localSring1=[localString stringByReplacingOccurrencesOfString:@"(" withString:@"["]
////            ;
////            
////            NSString * localSring2=[localSring1 stringByReplacingOccurrencesOfString:@")" withString:@"]"];
////            
////             NSString * localSring3 = [localSring2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
////            NSString * localSring4 = [localSring3 stringByReplacingOccurrencesOfString:@" " withString:@""];
////            
////            NSString * localSring5 = [[localSring4 componentsSeparatedByCharactersInSet:set]componentsJoinedByString:@""];
//        
//            
//            
//            
//            
//            
//            
//            [scripDetails setObject:scripDetailsArray forKey:@"data"];
//
//            
//            
//          
//            
//            
//            NSLog(@"%@",scripDetails);
//            
//            
//            
//            
//            
//            
//        }
//        
//        else
//        {
//            
//            delegate2.homeExchange=delegate2.exchange;
//            delegate2.homeinstrumentName=delegate2.instrumentNameArr;
//            delegate2.homeInstrumentToken=delegate2.instrumentToken;
//            delegate2.homeLotDepth=delegate2.sampleDepthLot;
//            delegate2.homeFilterCompanyName=delegate2.filteredCompanyName;
//            delegate2.homeExchangeToken=delegate2.exchangeToken;
//            delegate2.homeMtExchange=delegate2.mtExchange;
//            
//            
            scripDetailsArray=[[NSMutableArray alloc]init];
            scripDetails=[[NSMutableDictionary alloc]init];
//
//
        if(delegate2.userMarketWatch.count>0)
        {
            

            
            NSMutableArray * uniqueArray=[[NSMutableArray alloc]init];
            
            for(int i=0;i<delegate2.userMarketWatch.count;i++)
            {
                
                
            
            NSMutableDictionary* E1 = [delegate2.userMarketWatch objectAtIndex:i];
                
                NSString * string=[NSString stringWithFormat:@"%@",[E1 objectForKey:@"exchange_token"]];
            BOOL hasDuplicate = [[uniqueArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"exchange_token == %@",string]] count] > 0;
            
            if (!hasDuplicate)
            {
                [uniqueArray addObject:E1];
                NSLog(@"No unique");
                
            }
                
                else
                    
                {
                    
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Symbol already added in your list" preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        
                    }];
                    
                    UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                }
                
                
            }
            
            


            
            for(int i=0;i<uniqueArray.count;i++)
            {
                
                NSMutableDictionary * tmp_dict=[[NSMutableDictionary alloc]init];
                //                [tmp_dict setObject:[delegate2.userMarketWatch objectAtIndex:i] forKey:@"instrument_token"];
                
//                [tmp_dict setObject:[uniqueArray objectAtIndex:i] forKey:@"name"];
                
                tmp_dict=[uniqueArray objectAtIndex:i];
                
                //                  [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"symbol"] forKey:@"symbol"];
                //
                //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange"] forKey:@"exchange"];
                //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"segment"] forKey:@"segment"];
                //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchange_token"] forKey:@"exchange_token"];
                //                [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"exchangevalue"] forKey:@"exchangevalue"];
                //                [tmp_dict setObject:@"0" forKey:@"ltp"];
                
                //                @try {
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"fincode"] forKey:@"fincode"];
                //                } @catch (NSException *exception) {
                //                    [tmp_dict setObject:@"0"forKey:@"fincode"];
                //                } @finally {
                //
                //                }
                //
                //                @try {
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"expiryseries"] forKey:@"expiryseries"];
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"lotsize"] forKey:@"lotsize"];
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"securitydesc"] forKey:@"securitydesc"];
                //                } @catch (NSException *exception) {
                //                    [tmp_dict setObject:@"0" forKey:@"lotsize"];
                //                    [tmp_dict setObject:@"0" forKey:@"expiryseries"];
                //                } @finally {
                //
                //                }
                //
                //                @try {
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"optiontype"] forKey:@"optiontype"];
                //                    [tmp_dict setObject:[[delegate2.userMarketWatch objectAtIndex:i]objectForKey:@"strikeprice"] forKey:@"strikeprice"];
                //                } @catch (NSException *exception) {
                //                    [tmp_dict setObject:@"0" forKey:@"optiontype"];
                //                    [tmp_dict setObject:@"0" forKey:@"strikeprice"];
                //                } @finally {
                //
                //                }
                
                
                
                
                [scripDetailsArray insertObject:tmp_dict atIndex:i];
                
                //                NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:scripDetailsArray];
                //                NSArray *arrayWithoutDuplicates = [scripDetailsArray array];
                
                //            }
                
                //             NSCharacterSet * set=[NSCharacterSet newlineCharacterSet];
                //
                //            NSString * string=@"";
                //
                //            NSString * localString=[NSString stringWithFormat:@"%@",scripDetailsArray];
                //
                //            NSString * localSring1=[localString stringByReplacingOccurrencesOfString:@"(" withString:@"["]
                //            ;
                //
                //            NSString * localSring2=[localSring1 stringByReplacingOccurrencesOfString:@")" withString:@"]"];
                //
                //            NSString * localSring3 = [localSring2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                //            NSString * localSring4 = [localSring3 stringByReplacingOccurrencesOfString:@" " withString:@""];
                //
                //
                //
                //            NSString * localSring5 = [[localSring4 componentsSeparatedByCharactersInSet:set]componentsJoinedByString:@""];
                //
                //
                
                
                
                
                [scripDetails setObject:scripDetailsArray forKey:@"data"];
                
                
                NSLog(@"%@",scripDetails);
                
                
            }
            
            NSLog(@"%@",delegate2.homeInstrumentToken);
            
        }
            
        }
        @catch (NSException * e) {
            NSLog(@"Exception: %@", e);
        }
        @finally {
            NSLog(@"finally");
        }
        
        [self resultCheck];
        
        
        
    
    
}

//RECHABILITY//

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus  netStatus=[self.reach currentReachabilityStatus];
    if(netStatus==NotReachable)
    {
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    
    
}

-(BOOL)networkStatus
{
    
    
    Reachability * reach=[Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus  netStatus=[reach currentReachabilityStatus];
    
    if(netStatus==NotReachable)
    {
        
        
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Warning" message:@"No Internet Connection" preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        
        
    }
    else
    {
        
    }
    return YES;
    
    
}
/*!
 @discussion used to upload list of symbols to zambala server.
 */
-(void)resultCheck
{
    @try {
        
   
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               };
    
    
    
    
    NSString * localUrl=[NSString stringWithFormat:@"%@clienttrade/watch",delegate2.baseUrl];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    NSDictionary * parameters = @{
                   @"clientid":delegate2.userID,
                   @"watchinfo":scripDetails,
                   
                   
                   };
    
    NSData * postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                            
                                                        NSMutableDictionary * responseDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"%@",responseDict);

                                                        
                                                        
                                                    
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                        [self getScripMethod];
                                                        
                                                    });
                                                }];
    [dataTask resume];
    
    
}
@catch (NSException * e) {
    NSLog(@"Exception: %@", e);
}
@finally {
    NSLog(@"finally");
}



}

/*!
 @discussion used to get list of symbols to zambala server.
 */
-(void)getScripMethod
{
    @try
    {
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               };
    
    NSString * localUrl=[NSString stringWithFormat:@"%@clienttrade/watch/%@",delegate2.baseUrl,delegate2.userID];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:localUrl]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
   
    
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        
                                                        responseDict1=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                       
                                                        
                                                        
                                                        NSLog(@"Response dict1:%@",responseDict1);
                                                        
                                                        delegate2.localExchageToken=[[NSMutableArray alloc]init];
                                                        delegate2.homeNewCompanyArray=[[NSMutableArray alloc]init];
                                                        delegate2.userMarketWatch=[[NSMutableArray alloc]init];
                                                        delegate2.homeInstrumentToken=[[NSMutableArray alloc]init];
                                                        
                                                        for(int i=0;i<[[responseDict1 objectForKey:@"data"] count];i++)
                                                        {
//                                                            [delegate2.localExchageToken addObject:[[[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"] objectForKey:@"exchange_token"]];
//                                                            [delegate2.homeInstrumentToken addObject:[[[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"] objectForKey:@"instrument_token"]];
//
//                                                           [delegate2.homeNewCompanyArray addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"name"]];
//
//                                                            [delegate2.userMarketWatch addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"name"]];
//
                                                            
                                                            [delegate2.localExchageToken addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"exchange_token"]];
                                                            [delegate2.homeInstrumentToken addObject:[[[responseDict1 objectForKey:@"data"]objectAtIndex:i] objectForKey:@"instrument_token"]];
                                                            
                                                            [delegate2.homeNewCompanyArray addObject:[[responseDict1 objectForKey:@"data"]objectAtIndex:i]];
                                                            
                                                            [delegate2.userMarketWatch addObject:[[responseDict1 objectForKey:@"data"]objectAtIndex:i]];
                                                        }
                                                        
                                                        
                                                        
                                                        Mixpanel *mixpanel1 = [Mixpanel sharedInstance];
                                                        
                                                        [mixpanel1 identify:delegate2.userID];
                                                        
                                                        
                                                        
                                                        NSMutableArray * scripArray=[[NSMutableArray alloc]init];
                                                        
                                                        for(int i=0;i<[[responseDict1 objectForKey:@"data"]count];i++)
                                                        {
                                                            NSString * string2=[[NSString alloc]init];
                                                            string2=[NSString stringWithFormat:@"%@", [[[responseDict1 objectForKey:@"data"]objectAtIndex:i]objectForKey:@"symbol"]];
                                                            [scripArray addObject:string2];
                                                                                               }
                                                        
                                                        
                                                        NSArray * scripArrayNew=[scripArray mutableCopy];
                                                        
                                                        [mixpanel1.people set:@{@"Scrips":scripArrayNew}];
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                    
                                                   
                                                        
                                                        delegate2.marketWatchBool=false;
                                                        delegate2.searchToWatch=false;
                                                        [self tmpDictMethod];
                                                    
                                                    
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        
                                                        
                                                        [self socketHitMethod];
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
    
    
}
@catch (NSException * e) {
    NSLog(@"Exception: %@", e);
}
@finally {
    NSLog(@"finally");
}



    
}




/*!
 @discussion used to send broadcast request to multitrade.
 */


-(void)broadCastRequest
{
    
   
    
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    
//    localInstrumentToken=[[NSMutableArray alloc]init];
    
    NSLog(@"%@",delegate2.homeInstrumentToken);
    //BSE
  // localInstrumentToken=[[NSMutableArray alloc]initWithObjects:@"532921",@"500325", nil];
//    localInstrumentToken=[[NSMutableArray alloc]initWithObjects:@"500325",nil];
  // localInstrumentToken=[[NSMutableArray alloc]initWithObjects:@"2885",@"11957", nil];
    
//    localInstrumentToken=delegate2.homeExchangeToken;
//    
//    localInstrumentToken=[[[localInstrumentToken reverseObjectEnumerator] allObjects] mutableCopy];
    
//    instrumentTokensDict = [[NSMutableDictionary alloc]init];
    
    for(int i=0;i<delegate2.localExchageToken.count;i++)
    {
    TagEncode * tag = [[TagEncode alloc]init];
    delegate2.mtCheck=true;
    
    
    //  [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Watch];
    
    
        [tag TagData:sharedManager.shMsgCode shValueMethod:sharedManager.Broadcast];
//        [tag TagData:sharedManager.stSecurityID stringMethod:[localInstrumentToken objectAtIndex:i]]; //2885
        [tag TagData:sharedManager.stSecurityID stringMethod:[delegate2.localExchageToken objectAtIndex:i]];
        [tag TagData:sharedManager.stExchange stringMethod:[[delegate2.homeNewCompanyArray objectAtIndex:i] objectForKey:@"exchangevalue"]];
        [tag GetBuffer];
       [self newMessage];
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
        
   }

}
/*!
 @discussion used to send message to socket.
 */
-(void)newMessage
{
    //    delegate1.outputStream = (__bridge NSOutputStream *)writeStream;
    //
    //    [delegate1.outputStream setDelegate:self];
    //
    //    [delegate1.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    //
    //
    //    [delegate1.outputStream open];
    //
    
    Byte UUID[delegate2.btOutBuffer.count];
    
    for (int i = 0; i < delegate2.btOutBuffer.count; i++) {
        NSString * string=[NSString stringWithFormat:@"%@",[delegate2.btOutBuffer objectAtIndex:i]];
        
        NSString *hex1 = [NSString stringWithFormat:@"%lX",
                          (unsigned long)[string integerValue]];
        NSLog(@"%@", hex1);
        
        
        
        
        //        int newInt=[hex1 intValue];
        
        
        NSString *finalHex = [NSString stringWithFormat:@"0x%@",
                              hex1];
        NSLog(@"%@", finalHex);
        //
        //
        NSScanner *scanner=[NSScanner scannerWithString:finalHex];
        unsigned int temp;
        [scanner scanHexInt:&temp];
        NSLog(@"%d",temp);
        
        UUID[i]=temp;
        NSLog(@"%hhu",UUID[i]);
        
    }
    
    
    unsigned c = (unsigned int)delegate2.btOutBuffer.count;
    uint8_t *bytes = malloc(sizeof(*bytes) * c);
    
    unsigned i;
    for (i = 0; i < c; i++)
    {
        NSString *str = [delegate2.btOutBuffer objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    
    
    //    uint8_t *readBytes = (uint8_t *)[newData bytes];
    
  [delegate2.broadCastOutputstream write:bytes maxLength:delegate2.btOutBuffer.count];
    
}

/*!
 @discussion recieves message from socket(multitrade).
 */
- (void)showMainMenu:(NSNotification *)note
{
    
//     localExchangeTokensArray=[[NSMutableArray alloc]init];
//    
//    for(int i=0;i<delegate2.localExchageToken.count;i++)
//    {
//        NSString * first=[delegate2.localExchageToken objectAtIndex:i];
//
//        
//        
//        for(int j=0;j<delegate2.marketWatch1.count;j++)
//        {
//            NSString * second=[NSString stringWithFormat:@"%@",[[delegate2.marketWatch1 objectAtIndex:j] objectForKey:@"stSecurityID"]];
//            
//            if([second containsString:first])
//            {
//                [localExchangeTokensArray addObject:[delegate2.marketWatch1 objectAtIndex:j]];
//                
//            }
//        }
//        
//        
//        
//    }
//    
    ///
    
    @try {
        
   
    
//    instrumentTokensDict=[[NSMutableDictionary alloc]init];
    NSString * newExchangeToken;
    
    
    for(int i=0;i<delegate2.marketWatch1.count;i++)
    {
        NSString * tmpInstrument=[NSString stringWithFormat:@"%@",[[delegate2.marketWatch1 objectAtIndex:i] objectForKey:@"stSecurityID"]];
        
        for(int j=0;j<delegate2.localExchageToken.count;j++)
        {
            NSString * exchangeToken=[NSString stringWithFormat:@"%@",[delegate2.localExchageToken objectAtIndex:j]];
            
            if([tmpInstrument containsString:exchangeToken])
            {
                newExchangeToken=exchangeToken;
                 [instrumentTokensDict setValue:[delegate2.marketWatch1 objectAtIndex:i] forKeyPath:newExchangeToken];
            }
            
            
        }
        
        
       
        
    }
    
    
    NSLog(@"%@",instrumentTokensDict);
    

    
    if(allKeysList.count>0)
    {
        [allKeysList removeAllObjects];
    }
    allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
    
    NSLog(@"%@",allKeysList);
    
    allKeysList1=[[NSMutableArray alloc]init];
    
    for(int i=0;i<delegate2.localExchageToken.count;i++)
    {
        
        for(int j=0;j<allKeysList.count;j++)
        {
            NSNumber * number1=[delegate2.localExchageToken objectAtIndex:i];
            int int1=[number1 intValue];
            NSNumber * number2=[allKeysList objectAtIndex:j];
            
            int int2=[number2 intValue];
            
            if(int1==int2)
            {
                [allKeysList1 addObject:[allKeysList objectAtIndex:j]];
            }
        }
        
        
    }
//
    
    
    
    //  [shareListArray addObject:tmpDict];
    
    
    
    
    
    
   
    
    
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
         self.ifEmptyView.hidden=YES;
        self.marketTableView.hidden=NO;
        self.activityIndicator.hidden=YES;
        [self.activityIndicator stopAnimating];
        
        self.marketTableView.delegate=self;
        self.marketTableView.dataSource=self;
        
        [self.marketTableView reloadData];

        
    });
    
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    

    
    
    //         NSLog(@"Received Notification - Someone seems to have logged in");
}

/*!
 @discussion used to send broadcast request(multitrade).
 */
- (void)showMainMenu1:(NSNotification *)note
{
    [self broadCastRequest];
    
    //         NSLog(@"Received Notification - Someone seems to have logged in");
}

/*!
 @discussion used to send message to socket(multitrade).
 */
- (void)orderrequest:(NSNotification *)note
{
//    if(delegate2.broadcastOpen==true)
//    {
//        delegate2.broadcastOpen=false;
//        
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
//    }

//    delegate2.marketWatch1=[[NSMutableArray alloc]init];
    [self newMessage];
    
    //         NSLog(@"Received Notification - Someone seems to have logged in");
}

/*!
 @discussion used to open socket when app resign active.
 */

- (void)backMethod:(NSNotification *)note
{
    
//    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
//    {
//      sad  [self instruments];
//
//    }
//
//    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
//    {
////        [self webSocket];
//        [self instruments];
//    }
//    else
//    {
//
//
//        //    if(delegate2.searchToWatch==true)
//        //    {
//        //        delegate2.searchToWatch=false;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
//        [self instruments];
//
//        //    }
//        //
//        //    else
//        //    {
//        //         [[NSNotificationCenter defaultCenter] postNotificationName:@"orderwatch" object:nil];
//        //
//        //    }
//
//    }
    
    [self webSocket];
    [self tmpDictMethod];
    [self socketHitMethod];
}


-(void)viewDidDisappear:(BOOL)animated
{
    
//    [delegate2.broadCastInputStream close];
}

-(void)socketHitMethod
{
    
    
    if([delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        
        [self webSocket];
        
    }
    
    else if([delegate2.brokerNameStr isEqualToString:@"Upstox"])
    {
        [self subMethod];
        //[self.socket close];
        //[self webSocket];
    }
    
    
    
    else
    {
        
        if(delegate2.homeNewCompanyArray.count>0)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"broad" object:nil];
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
            [self broadCastRequest];
            
        }else
        {
            self.activityIndicator.hidden=YES;
            self.ifEmptyView.hidden=NO;
            self.marketTableView.hidden=YES;
        }
        
    }
    
}



-(void)tmpDictMethod
{
    
    if([delegate2.brokerNameStr isEqualToString:@"Upstox"]||[delegate2.brokerNameStr isEqualToString:@"Zerodha"])
    {
        instrumentTokensDict=[[NSMutableDictionary alloc]init];
        NSLog(@"Home Instrument token:%@",delegate2.homeInstrumentToken);
        
        if(delegate2.homeInstrumentToken.count>0)
        {
            for(int i=0;i<delegate2.homeInstrumentToken.count;i++)
            {
                tmpDict = [[NSMutableDictionary alloc]init];
                [tmpDict setValue:@"0.00" forKey:@"ClosePriceValue"];
                [tmpDict setValue:@"0.00" forKey:@"LastPriceValue"];
                [tmpDict setValue:@"0.00" forKey:@"ChangeValue"];
                [tmpDict setValue:@"0.00" forKey:@"ChangePercentageValue"];
                [tmpDict setValue:[NSString stringWithFormat:@"%@",[delegate2.homeInstrumentToken objectAtIndex:i]] forKey:@"InstrumentToken"];
                [instrumentTokensDict setValue:tmpDict forKeyPath:[NSString stringWithFormat:@"%@",[delegate2.homeInstrumentToken objectAtIndex:i]]];
            }
            
            allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
            
            NSLog(@"%@",allKeysList);
            
            allKeysList1=[[NSMutableArray alloc]init];
            
            for(int i=0;i<delegate2.homeInstrumentToken.count;i++)
            {
                
                for(int j=0;j<allKeysList.count;j++)
                {
                    NSNumber * number1=[delegate2.homeInstrumentToken objectAtIndex:i];
                    int int1=[number1 intValue];
                    NSNumber * number2=[allKeysList objectAtIndex:j];
                    
                    int int2=[number2 intValue];
                    
                    if(int1==int2)
                    {
                        [allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                    }
                }
                
                
            }
            
            self.activityIndicator.hidden=YES;
            self.ifEmptyView.hidden=YES;
            self.marketTableView.hidden=NO;
           
            
            dispatch_async(dispatch_get_main_queue(), ^{

                self.marketTableView.delegate=self;
                self.marketTableView.dataSource=self;
                [self.marketTableView reloadData];
            });
            
           
        }
        
    }
    
    

    else
        
    {
    if(delegate2.localExchageToken.count>0)
    {
    
    instrumentTokensDict=[[NSMutableDictionary alloc]init];
    
   
   
    for(int i=0;i<delegate2.localExchageToken.count;i++)
    {
    tmpDict = [[NSMutableDictionary alloc]init];
    [tmpDict setValue:@"0.00" forKey:@"dbLTP"];
    [tmpDict setValue:@"0.00" forKey:@"dbClose"];
    [tmpDict setValue:[NSString stringWithFormat:@"%@",[delegate2.localExchageToken objectAtIndex:i]] forKey:@"stSecurityID"];
        
          [instrumentTokensDict setValue:tmpDict forKeyPath:[delegate2.localExchageToken objectAtIndex:i]];
       
        
    }
        
        allKeysList = [[instrumentTokensDict allKeys]mutableCopy];
        
        NSLog(@"%@",allKeysList);
        
        allKeysList1=[[NSMutableArray alloc]init];
        
        for(int i=0;i<delegate2.localExchageToken.count;i++)
        {
            
            for(int j=0;j<allKeysList.count;j++)
            {
                NSNumber * number1=[delegate2.localExchageToken objectAtIndex:i];
                int int1=[number1 intValue];
                NSNumber * number2=[allKeysList objectAtIndex:j];
                
                int int2=[number2 intValue];
                
                if(int1==int2)
                {
                    [allKeysList1 addObject:[allKeysList objectAtIndex:j]];
                }
            }
            
            
        }
        
        
        self.activityIndicator.hidden=YES;
        self.ifEmptyView.hidden=YES;
        self.marketTableView.hidden=NO;
        
   
        dispatch_async(dispatch_get_main_queue(), ^{

            self.marketTableView.delegate=self;
            self.marketTableView.dataSource=self;
            [self.marketTableView reloadData];
        });
        

    NSLog(@"%@",tmpDict);
    
    NSLog(@"%@",instrumentTokensDict);
        
        
    }
    
    }
    
}



@end
